package de.oelkers.stardust.graphics.assets.loading;

import common.TemporaryFileExtension;
import de.oelkers.stardust.utils.container.Container;
import de.oelkers.stardust.utils.container.FixedSizeContainer;
import org.awaitility.Durations;
import org.awaitility.core.ThrowingRunnable;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import static common.TemporaryFileExtension.EXTENSION;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class FileAssetLoaderUnitTest {

    @Test
    @ExtendWith(TemporaryFileExtension.class)
    public void testDataDataIsUpdatedOnFileChange(File file) throws IOException {
        AtomicInteger counter = new AtomicInteger();
        TestAssetContainer assetContainer = new TestAssetContainer();
        try (FileAssetLoader assetLoader = new FileAssetLoader()) {
            assetLoader.registerLoader(assetContainer.getClass(), filePaths -> new String[] { "test-" + counter.getAndIncrement() }, EXTENSION);
            assetLoader.loadAndObserve(assetContainer, file.getPath());
            updateAndCheck(assetLoader, () -> assertEquals("test-0", assetContainer.injectedData));
            try (FileWriter fileWriter = new FileWriter(file)) {
                fileWriter.append("Test");
            }
            updateAndCheck(assetLoader, () -> assertEquals("test-1", assetContainer.injectedData));
        }
    }

    private static void updateAndCheck(AssetLoader assetLoader, ThrowingRunnable assertion) {
        await().atMost(Durations.FIVE_SECONDS).untilAsserted(() -> {
            assetLoader.update();
            assertion.run();
        });
    }

    private static class TestAssetContainer implements AssetContainer<String> {

        private String injectedData;

        @Override
        public void inject(Container<String> container) {
            injectedData = container.getOnly();
        }

        @Override
        public Container<String> getContainer() {
            return new FixedSizeContainer<>(1);
        }
    }
}