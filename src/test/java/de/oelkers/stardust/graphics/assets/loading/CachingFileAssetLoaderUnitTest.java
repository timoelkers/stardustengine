package de.oelkers.stardust.graphics.assets.loading;

import common.TemporaryFileExtension;
import de.oelkers.stardust.utils.container.Container;
import de.oelkers.stardust.utils.container.FixedSizeContainer;
import de.oelkers.stardust.utils.threading.ParallelRunner;
import de.oelkers.stardust.utils.threading.Runner;
import org.awaitility.Durations;
import org.awaitility.core.ThrowingRunnable;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import static common.TemporaryFileExtension.EXTENSION;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CachingFileAssetLoaderUnitTest {

    @Test
    @ExtendWith(TemporaryFileExtension.class)
    public void testThatAssetDataIsCachedBasedOnPathAndAssetType(File file) {
        try (CachingFileAssetLoader assetLoader = new CachingFileAssetLoader()) {
            TestAssetContainer assetContainer = new TestAssetContainer();
            AtomicInteger loaderCalled = new AtomicInteger(0);
            assetLoader.registerLoader(TestAssetContainer.class, filePaths -> {
                loaderCalled.incrementAndGet();
                return new String[]{"Expected"};
            }, EXTENSION);
            assetLoader.load(assetContainer, file.getPath());
            updateAndCheck(assetLoader, () -> assertEquals("Expected", assetContainer.injectedData));
            TestAssetContainer newContainer = new TestAssetContainer();
            assetLoader.load(newContainer, file.getPath());
            updateAndCheck(assetLoader, () -> assertEquals("Expected", newContainer.injectedData));
            assertEquals(1, loaderCalled.get());
        }
    }

    @Test
    @ExtendWith(TemporaryFileExtension.class)
    public void testThatAssetDataIsNotCachedIfAssetTypeDiffers(File file) {
        try (CachingFileAssetLoader assetLoader = new CachingFileAssetLoader()) {
            TestAssetContainer assetContainer = new TestAssetContainer();
            AtomicInteger loaderCalled = new AtomicInteger(0);
            AsynchronousAssetLoader<String> loader = filePaths -> {
                loaderCalled.incrementAndGet();
                return new String[]{"Expected"};
            };
            assetLoader.registerLoader(TestAssetContainer.class, loader, EXTENSION);
            assetLoader.registerLoader(DifferentTestAssetContainer.class, loader, EXTENSION);
            assetLoader.load(assetContainer, file.getPath());
            updateAndCheck(assetLoader, () -> assertEquals("Expected", assetContainer.injectedData));
            DifferentTestAssetContainer differentAssetContainer = new DifferentTestAssetContainer();
            assetLoader.load(differentAssetContainer, file.getPath());
            updateAndCheck(assetLoader, () -> assertEquals("Expected", differentAssetContainer.injectedData));
            assertEquals(2, loaderCalled.get());
        }
    }

    @Test
    @ExtendWith(TemporaryFileExtension.class)
    public void testThatConcurrentCallsDoNotLoadDataTwice(File file) {
        try (CachingFileAssetLoader assetLoader = new CachingFileAssetLoader()) {
            TestAssetContainer assetContainer = new TestAssetContainer();
            AtomicInteger loaderCalled = new AtomicInteger(0);
            assetLoader.registerLoader(TestAssetContainer.class, filePaths -> {
                loaderCalled.incrementAndGet();
                try {
                    Thread.sleep(25);
                } catch (InterruptedException ignored) {}
                return new String[]{"Expected"};
            }, EXTENSION);
            Runner runner = new ParallelRunner();
            runner.addRunnable(() -> assetLoader.load(assetContainer, file.getPath()), 10);
            runner.run().waitForCompletion();
            updateAndCheck(assetLoader, () -> assertEquals("Expected", assetContainer.injectedData));
            assertEquals(1, loaderCalled.get());
        }
    }

    @Test
    @ExtendWith(TemporaryFileExtension.class)
    public void testThatAllAssetContainerAreUpdatedWhenSourceChanges(File file) throws IOException {
        try (CachingFileAssetLoader assetLoader = new CachingFileAssetLoader()) {
            TestAssetContainer assetContainer = new TestAssetContainer();
            AtomicInteger counter = new AtomicInteger(0);
            assetLoader.registerLoader(TestAssetContainer.class, filePaths -> new String[]{"test-" + counter.getAndIncrement()}, EXTENSION);
            assetLoader.loadAndObserve(assetContainer, file.getPath());
            updateAndCheck(assetLoader, () -> assertEquals("test-0", assetContainer.injectedData));
            TestAssetContainer newContainer = new TestAssetContainer();
            assetLoader.loadAndObserve(newContainer, file.getPath());
            updateAndCheck(assetLoader, () -> assertEquals("test-0", newContainer.injectedData));
            try (FileWriter fileWriter = new FileWriter(file)) {
                fileWriter.append("Test");
            }
            updateAndCheck(assetLoader, () -> {
                assertEquals("test-1", assetContainer.injectedData);
                assertEquals("test-1", newContainer.injectedData);
            });
        }
    }

    private static void updateAndCheck(AssetLoader assetLoader, ThrowingRunnable assertion) {
        await().atMost(Durations.FIVE_SECONDS).untilAsserted(() -> {
            assetLoader.update();
            assertion.run();
        });
    }

    private static class TestAssetContainer implements AssetContainer<String> {

        protected String injectedData;

        @Override
        public void inject(Container<String> container) {
            injectedData = container.getOnly();
        }

        @Override
        public Container<String> getContainer() {
            return new FixedSizeContainer<>(1);
        }
    }

    private static class DifferentTestAssetContainer extends TestAssetContainer {}
}