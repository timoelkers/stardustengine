package de.oelkers.stardust.graphics.assets.loading;

import de.oelkers.stardust.utils.container.Container;
import de.oelkers.stardust.utils.container.FixedSizeContainer;
import org.awaitility.Durations;
import org.awaitility.core.ThrowingRunnable;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.concurrent.atomic.AtomicReference;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;

public class AssetLoaderUnitTest {

    @Test
    public void testThatResultFromLoaderIsInjected() {
        TestAssetContainer assetContainer = new TestAssetContainer();
        try (AssetLoader assetLoader = new AssetLoader()) {
            assetLoader.load(assetContainer, () -> "expected");
            updateAndCheck(assetLoader, () -> assertEquals("expected", assetContainer.injectedData));
        }
    }

    @Test
    public void testThatDataIsLoadedAsynchronously() {
        AtomicReference<Thread> loadingThread = new AtomicReference<>();
        AssetContainer<String> assetContainer = new TestAssetContainer();
        try (AssetLoader assetLoader = new AssetLoader()) {
            assetLoader.load(assetContainer, () -> {
                loadingThread.set(Thread.currentThread());
                return null;
            });
            updateAndCheck(assetLoader, () -> {
                assertNotNull(loadingThread.get());
                assertNotEquals(Thread.currentThread(), loadingThread.get());
            });
        }
    }

    @Test
    public void testThatDataIsInjectedOnCallingThread() {
        TestAssetContainer assetContainer = new TestAssetContainer();
        try (AssetLoader assetLoader = new AssetLoader()) {
            assetLoader.load(assetContainer, () -> null);
            updateAndCheck(assetLoader, () -> assertEquals(Thread.currentThread(), assetContainer.injectingThread));
        }
    }

    @Test
    public void testThatExceptionsWhileLoadingArePropagated() {
        RuntimeException expectedException = new RuntimeException();
        AtomicReference<Throwable> foundThrowable = new AtomicReference<>();
        Thread.currentThread().setUncaughtExceptionHandler((thread, throwable) -> foundThrowable.set(throwable));
        AssetContainer<String> assetContainer = new TestAssetContainer();
        try (AssetLoader assetLoader = new AssetLoader()) {
            assetLoader.load(assetContainer, () -> { throw expectedException; });
            updateAndCheck(assetLoader, () -> assertEquals(expectedException, foundThrowable.get()));
        }
    }

    @Test
    public void testThatTaskIsOnlyRemovedFromQueueIfConditionIsMet() {
        try (AssetLoader assetLoader = new AssetLoader()) {
            AssetContainer<String> assetContainer = new TestAssetContainer();
            assetLoader.load(assetContainer, () -> null);
            waitForTaskCountOf(assetLoader, 1);
            assetLoader.update(0);
            assertEquals(1, assetLoader.getFinishedTasksCount());
            assetLoader.update(Duration.ofMillis(0));
            assertEquals(1, assetLoader.getFinishedTasksCount());
            assetLoader.update();
            assertEquals(0, assetLoader.getFinishedTasksCount());
        }
    }

    private static void waitForTaskCountOf(AssetLoader assetLoader, int expectedTaskCount) {
        await().atMost(Durations.FIVE_SECONDS).until(() -> assetLoader.getFinishedTasksCount() == expectedTaskCount);
    }

    private static void updateAndCheck(AssetLoader assetLoader, ThrowingRunnable assertion) {
        await().atMost(Durations.FIVE_SECONDS).untilAsserted(() -> {
            assetLoader.update();
            assertion.run();
        });
    }

    private static class TestAssetContainer implements AssetContainer<String> {

        private String injectedData;
        private Thread injectingThread;

        @Override
        public void inject(Container<String> container) {
            injectedData = container.getOnly();
            injectingThread = Thread.currentThread();
        }

        @Override
        public Container<String> getContainer() {
            return new FixedSizeContainer<>(1);
        }
    }
}