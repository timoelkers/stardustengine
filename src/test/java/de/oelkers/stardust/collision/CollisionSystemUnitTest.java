package de.oelkers.stardust.collision;

import de.oelkers.stardust.entities.Entity;
import de.oelkers.stardust.math.shapes.BoundingBox;
import de.oelkers.stardust.math.shapes.Ray;
import de.oelkers.stardust.math.transformations.Transform3D;
import de.oelkers.stardust.math.transformations.TransformSpace;
import de.oelkers.stardust.math.transformations.Transformable3D;
import de.oelkers.stardust.math.vectors.ReadOnlyVector3f;
import de.oelkers.stardust.math.vectors.Vector3f;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertSame;

class CollisionSystemUnitTest {

    @Test
    public void testThatEntitiesAreSortedOnRayAxis() {
        CollisionSystem collisionSystem = new CollisionSystem();
        Entity entity1 = withPosition(new Vector3f(0, 10, 0));
        collisionSystem.addEntity(entity1);
        Entity entity2 = withPosition(new Vector3f(0, 15, 0));
        collisionSystem.addEntity(entity2);
        Entity entity3 = withPosition(new Vector3f(0, 5, 0));
        collisionSystem.addEntity(entity3);
        Ray ray = new Ray(Vector3f.ZERO, Vector3f.UP);
        Optional<Entity> hit = collisionSystem.getFirstRayHit(ray);
        assertSame(entity3, hit.get());
        collisionSystem.removeEntity(entity3);
        hit = collisionSystem.getFirstRayHit(ray);
        assertSame(entity1, hit.get());
    }

    private static Entity withPosition(ReadOnlyVector3f position) {
        Entity entity = new Entity(position.toString());
        Transformable3D transform = new Transform3D();
        BoundingBox box = new BoundingBox(Vector3f.ZERO, Vector3f.ONE);
        transform.addChild(box.getShapeTransform());
        transform.translate(position, TransformSpace.WORLD);
        entity.addComponent(box);
        return entity;
    }
}