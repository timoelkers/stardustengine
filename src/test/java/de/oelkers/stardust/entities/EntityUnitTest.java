package de.oelkers.stardust.entities;

import de.oelkers.stardust.utils.threading.ParallelRunner;
import de.oelkers.stardust.utils.threading.Runner;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class EntityUnitTest {

    @Test
    public void testThatGetNameReturnsSpecifiedName() {
        Entity entity = new Entity("TestEntity");
        assertEquals("TestEntity", entity.getName());
    }

    @Test
    public void testThatComponentsCanBeQueried() {
        Entity entity = new Entity("TestEntity");
        Component testComponent = new TestComponent1();
        entity.addComponent(testComponent);
        assertEquals(testComponent, entity.getComponentOrNull(TestComponent1.class));
    }

    @Test
    public void testThatComponentsCanByQueriedBySuperclasses() {
        Entity entity = new Entity("TestEntity");
        Component testComponent = new TestComponent1();
        entity.addComponent(testComponent);
        assertEquals(testComponent, entity.getComponentOrNull(TestSuperComponent.class));
    }

    @Test
    public void testThatComponentsCanBeQueriedByInterface() {
        Entity entity = new Entity("TestEntity");
        Component testComponent = new TestComponent2();
        entity.addComponent(testComponent);
        assertEquals(testComponent, entity.getComponentOrNull(TestChildInterface.class));
        assertEquals(testComponent, entity.getComponentOrNull(TestSuperInterface.class));
    }

    @Test
    public void testThatComponentsCanNotBeQueriedByComponentType() {
        Entity entity = new Entity("TestEntity");
        Component testComponent = new TestComponent2();
        entity.addComponent(testComponent);
        assertFalse(entity.hasComponent(Component.class));
    }

    @Test
    public void testThatQueryingNonExistingComponentWorks() {
        Entity entity = new Entity("TestEntity");
        assertFalse(entity.getComponent(TestComponent1.class).isPresent());
        assertNull(entity.getComponentOrNull(TestComponent1.class));
    }

    @Test
    public void testThatComponentIsAddedOnlyOnce() {
        Entity entity = new Entity("TestEntity");
        Component testComponent = new TestComponent1();
        entity.addComponent(testComponent);
        entity.addComponent(testComponent);
        assertEquals(1, entity.getComponentCount());
    }

    @Test
    public void testThatComponentsCanBeRemoved() {
        Entity entity = new Entity("TestEntity");
        Component testComponent = new TestComponent1();
        entity.addComponent(testComponent);
        entity.removeComponent(testComponent);
        assertFalse(entity.hasComponent(TestComponent1.class));
    }

    @Test
    public void testThatRemovingComponentAlsoRemovesSuperclasses() {
        Entity entity = new Entity("TestEntity");
        Component testComponent = new TestComponent2();
        entity.addComponent(testComponent);
        entity.removeComponent(testComponent);
        assertFalse(entity.hasComponent(TestSuperComponent.class));
        assertFalse(entity.hasComponent(TestSuperInterface.class));
    }

    @Test
    public void testThatCheckingForMultipleComponentsAtOnceWorks() {
        Entity entity = new Entity("TestEntity");
        entity.addComponent(new TestComponent1());
        entity.addComponent(new TestComponent2());
        assertTrue(entity.hasComponents(TestComponent1.class, TestComponent2.class, TestSuperComponent.class));
    }

    @Test
    public void testThatIdCreationIsThreadSafe() {
        Collection<Entity> entities = new ArrayList<>(10);
        Runner runner = new ParallelRunner();
        for (int i = 0; i < 10; i++) {
            runner.addRunnables(() -> entities.add(new Entity("TestEntity")));
        }
        runner.run();
        runner.waitForCompletion();
        assertEquals(entities.size(), entities.stream().map(Entity::getId).distinct().count());
    }

    @Test
    public void testThatLockingAComponentPreventsRemoval() {
        Entity entity = new Entity("TestEntity");
        Component testComponent = new TestComponent1();
        entity.addComponent(testComponent);
        entity.lockComponent(TestComponent1.class);
        assertTrue(entity.isComponentLocked(TestComponent1.class));
        assertEquals(getClass().getName(), entity.getLockOwner(TestComponent1.class).get());
        assertThrows(IllegalStateException.class, () -> entity.removeComponent(testComponent));
    }

    @Test
    public void testThatLockingASuperClassPreventsChildClassesFromBeingRemoved() {
        Entity entity = new Entity("TestEntity");
        Component testComponent = new TestComponent1();
        entity.addComponent(testComponent);
        entity.lockComponent(TestSuperComponent.class);
        assertThrows(IllegalStateException.class, () -> entity.removeComponent(testComponent));
    }

    @Test
    public void testThatComponentsCanBeUnlocked() {
        Entity entity = new Entity("TestEntity");
        Component testComponent = new TestComponent1();
        entity.addComponent(testComponent);
        entity.lockComponent(TestComponent1.class);
        entity.unlockComponent(TestComponent1.class);
        assertFalse(entity.isComponentLocked(TestComponent1.class));
        assertFalse(entity.getLockOwner(TestComponent1.class).isPresent());
        assertDoesNotThrow(() -> entity.removeComponent(testComponent));
    }

    @Test
    public void testThatComponentsCanOnlyBeUnlockedByClassThatLocked() {
        Entity entity = new Entity("TestEntity");
        Component testComponent = new TestComponent1();
        entity.addComponent(testComponent);
        entity.lockComponent(TestComponent1.class);
        assertThrows(IllegalStateException.class, () -> DoFromDifferentClass.unlock(entity));
    }

    @Test
    public void testThatComponentsCanNotBeLockedTwiceByDifferentClasses() {
        Entity entity = new Entity("TestEntity");
        Component testComponent = new TestComponent1();
        entity.addComponent(testComponent);
        entity.lockComponent(TestComponent1.class);
        assertThrows(IllegalStateException.class, () -> DoFromDifferentClass.lock(entity));
    }

    @Test
    public void testThatComponentsCanBeUnlockedIfNotLocked() {
        Entity entity = new Entity("TestEntity");
        Component testComponent = new TestComponent1();
        entity.addComponent(testComponent);
        assertDoesNotThrow(() -> entity.unlockComponent(TestComponent1.class));
    }

    private static class TestSuperComponent implements Component {}

    private interface TestSuperInterface extends Component {}

    private interface TestChildInterface extends TestSuperInterface {}

    private static final class TestComponent1 extends TestSuperComponent {}

    private static final class TestComponent2 extends TestSuperComponent implements TestChildInterface {}

    private static final class DoFromDifferentClass {

        private static void lock(Entity entity) {
            entity.lockComponent(TestComponent1.class);
        }

        private static void unlock(Entity entity) {
            entity.unlockComponent(TestComponent1.class);
        }
    }
}