package de.oelkers.stardust.entities;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BaseEntitySystemUnitTest {

    @Test
    public void testThatAddingWithoutEntityGroupAddsToGlobalGroup() {
        EntitySystem entitySystem = new TestEntitySystem(true);
        entitySystem.addEntity(new Entity("TestEntity"));
        assertEquals(1, entitySystem.getSize(BaseEntitySystem.GLOBAL_GROUP));
    }

    @Test
    public void testThatGetSizeWithoutEntityGroupGetsSizeOfGlobalGroup() {
        EntitySystem entitySystem = new TestEntitySystem(true);
        entitySystem.addEntity(new Entity("TestEntity"), BaseEntitySystem.GLOBAL_GROUP);
        assertEquals(1, entitySystem.getSize());
    }

    @Test
    public void testThatGetTotalSizeCountsEntitiesInDifferentGroups() {
        EntitySystem entitySystem = new TestEntitySystem(true);
        entitySystem.addEntity(new Entity("TestEntity"));
        entitySystem.addEntity(new Entity("TestEntity"), "OtherGroup");
        assertEquals(2, entitySystem.getTotalSize());
    }

    @Test
    public void testThatForEachIteratesOverGlobalGroupIfNoGroupIsSpecified() {
        EntitySystem entitySystem = new TestEntitySystem(true);
        entitySystem.addEntity(new Entity("TestEntity"));
        AtomicInteger counter = new AtomicInteger(0);
        entitySystem.forEach(e -> counter.incrementAndGet());
        assertEquals(1, counter.get());
        entitySystem.forEach(e -> counter.incrementAndGet(), e -> true);
        assertEquals(2, counter.get());
    }

    @Test
    public void testThatForEachDoesNotIterateOverSubsystem() {
        EntitySystem entitySystem = new TestEntitySystem(true);
        EntitySystem subSystem = new TestEntitySystem(true);
        entitySystem.registerSubsystem(subSystem);
        entitySystem.addEntity(new Entity("TestEntity"));
        AtomicInteger counter = new AtomicInteger();
        entitySystem.forEach(e -> counter.getAndIncrement());
        assertEquals(1, counter.get());
    }

    @Test
    public void testThatEntitiesWithInsufficientComponentsAreRejected() {
        EntitySystem entitySystem = new TestEntitySystem(false);
        entitySystem.addEntity(new Entity("TestEntity"));
        assertEquals(0, entitySystem.getSize());
    }

    @Test
    public void testThatEntitiesWhichAreAlreadyInTheSystemAreRejected() {
        EntitySystem entitySystem = new TestEntitySystem(true);
        Entity entity = new Entity("TestEntity");
        entitySystem.addEntity(entity);
        entitySystem.addEntity(entity);
        assertEquals(1, entitySystem.getSize());
    }

    @Test
    public void testThatEntitiesWhichAreInADifferentGroupAreNotRejected() {
        EntitySystem entitySystem = new TestEntitySystem(true);
        Entity entity = new Entity("TestEntity");
        entitySystem.addEntity(entity);
        entitySystem.addEntity(entity, "OtherGroup");
        assertEquals(1, entitySystem.getSize());
        assertEquals(1, entitySystem.getSize("OtherGroup"));
    }

    @Test
    public void testThatEntitiesAreBeRemovedFromMultipleGroups() {
        EntitySystem entitySystem = new TestEntitySystem(true);
        Entity entity = new Entity("TestEntity");
        entitySystem.addEntity(entity);
        entitySystem.addEntity(entity, "OtherGroup");
        entitySystem.removeEntity(entity);
        assertEquals(0, entitySystem.getSize());
        assertEquals(0, entitySystem.getSize("OtherGroup"));
    }

    @Test
    public void testThatEntitiesAreAddedToSubsystems() {
        EntitySystem entitySystem = new TestEntitySystem(true);
        EntitySystem subSystem = new TestEntitySystem(true);
        entitySystem.registerSubsystem(subSystem);
        entitySystem.addEntity(new Entity("TestEntity"));
        assertEquals(1, entitySystem.getSize());
        assertEquals(1, subSystem.getSize());
    }

    @Test
    public void testThatEntitiesAreRemovedFromSubsystems() {
        EntitySystem entitySystem = new TestEntitySystem(true);
        EntitySystem subSystem = new TestEntitySystem(true);
        entitySystem.registerSubsystem(subSystem);
        Entity entity = new Entity("TestEntity");
        entitySystem.addEntity(entity);
        entitySystem.removeEntity(entity);
        assertEquals(0, entitySystem.getSize());
        assertEquals(0, subSystem.getSize());
    }

    private static final class TestEntitySystem extends BaseEntitySystem<Set<Entity>> {

        private final boolean allowEntities;

        private TestEntitySystem(boolean allowEntities) {
            super(HashSet::new);
            this.allowEntities = allowEntities;
        }

        @Override
        public boolean acceptsEntity(Entity entity) {
            return allowEntities;
        }
    }
}