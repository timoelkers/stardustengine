package de.oelkers.stardust.math.shapes;

import de.oelkers.stardust.math.internal.MathDelegate;
import de.oelkers.stardust.math.transformations.TransformSpace;
import de.oelkers.stardust.math.vectors.Vector3f;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BoundingBoxUnitTest {

    @Test
    public void testThatPointIsContained() {
        BoundingBox boundingBox = new BoundingBox(new Vector3f(-0.5f, -0.5f, -0.5f), new Vector3f(0.5f, 0.5f, 0.5f));
        boundingBox.getShapeTransform().rotate(Vector3f.RIGHT, (float) MathDelegate.toRadians(90), TransformSpace.WORLD);
        assertTrue(boundingBox.contains(0, 0, 0));
        assertTrue(boundingBox.contains(-0.49f, -0.49f, -0.49f));
        assertTrue(boundingBox.contains(0.49f, -0.49f, -0.49f));
        assertTrue(boundingBox.contains(-0.49f, 0.49f, -0.49f));
        assertTrue(boundingBox.contains(-0.49f, -0.49f, 0.49f));
        assertTrue(boundingBox.contains(0.49f, 0.49f, -0.49f));
        assertTrue(boundingBox.contains(-0.49f, 0.49f, 0.49f));
        assertTrue(boundingBox.contains(0.49f, -0.49f, 0.49f));
        assertTrue(boundingBox.contains(0.49f, 0.49f, 0.49f));
    }

    @Test
    public void testThatPointIsNotContained() {
        BoundingBox boundingBox = new BoundingBox(new Vector3f(-0.5f, -0.5f, -0.5f), new Vector3f(0.5f, 0.5f, 0.5f));
        boundingBox.getShapeTransform().rotate(Vector3f.UP, MathDelegate.PI_FLOAT / 4f, TransformSpace.WORLD);
        assertFalse(boundingBox.contains(-0.49f, -0.49f, -0.49f));
        assertFalse(boundingBox.contains(0.49f, -0.49f, -0.49f));
        assertFalse(boundingBox.contains(-0.49f, 0.49f, -0.49f));
        assertFalse(boundingBox.contains(-0.49f, -0.49f, 0.49f));
        assertFalse(boundingBox.contains(0.49f, 0.49f, -0.49f));
        assertFalse(boundingBox.contains(-0.49f, 0.49f, 0.49f));
        assertFalse(boundingBox.contains(0.49f, -0.49f, 0.49f));
        assertFalse(boundingBox.contains(0.49f, 0.49f, 0.49f));
    }

    @Test
    public void testThatRayIsIntersected() {
        BoundingBox boundingBox = new BoundingBox(new Vector3f(-0.5f, -0.5f, -0.5f), new Vector3f(0.5f, 0.5f, 0.5f));
        boundingBox.getShapeTransform().rotate(Vector3f.RIGHT, (float) MathDelegate.toRadians(90), TransformSpace.WORLD);
        assertTrue(boundingBox.intersects(new Ray(Vector3f.LEFT, Vector3f.RIGHT)));
        assertTrue(boundingBox.intersects(new Ray(Vector3f.RIGHT, Vector3f.LEFT)));
        assertTrue(boundingBox.intersects(new Ray(Vector3f.BACK, Vector3f.FORWARD)));
        assertTrue(boundingBox.intersects(new Ray(Vector3f.FORWARD, Vector3f.BACK)));
        assertTrue(boundingBox.intersects(new Ray(Vector3f.DOWN, Vector3f.UP)));
        assertTrue(boundingBox.intersects(new Ray(Vector3f.UP, Vector3f.DOWN)));
    }

    @Test
    public void testThatRayIsNotIntersected() {
        BoundingBox boundingBox = new BoundingBox(new Vector3f(-0.5f, -0.5f, -0.5f), new Vector3f(0.5f, 0.5f, 0.5f));
        boundingBox.getShapeTransform().rotate(Vector3f.RIGHT, (float) MathDelegate.toRadians(90), TransformSpace.WORLD);
        assertFalse(boundingBox.intersects(new Ray(new Vector3f(-1, -1, -1), new Vector3f(1, -1, -1))));
        assertFalse(boundingBox.intersects(new Ray(new Vector3f(-1, -1, -1), new Vector3f(-1, 1, -1))));
        assertFalse(boundingBox.intersects(new Ray(new Vector3f(-1, -1, -1), new Vector3f(-1, -1, 1))));
    }
}