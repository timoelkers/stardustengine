package de.oelkers.stardust.utils.timer;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DefaultStopWatchParameterizedUnitTest {

    public static Stream<DefaultTimeProvider> getTimeProvider() {
        // the GLFW timer requires initialisation of GLFW, which is overkill for this unit test
        return Arrays.stream(DefaultTimeProvider.values()).filter(timeProvider -> timeProvider != DefaultTimeProvider.GLFW_TIMER);
    }

    @ParameterizedTest
    @MethodSource("getTimeProvider")
    public void testThatElapsedTimeIsZeroBeforeStart(TimeProvider timeProvider) {
        StopWatch stopWatch = new DefaultStopWatch(timeProvider);
        assertEquals(0, stopWatch.getElapsedTime());
    }

    @ParameterizedTest
    @MethodSource("getTimeProvider")
    public void testThatElapsedTimeWithoutTimeUnitUsesTimeProvidersTimeUnit(TimeProvider timeProvider) throws InterruptedException {
        StopWatch stopWatch = new DefaultStopWatch(timeProvider).start();
        Thread.sleep(10);
        long elapsed = TimeUnit.MILLISECONDS.convert(stopWatch.stop().getElapsedTime(), timeProvider.getTimeUnit());
        assertEquals(10, elapsed, 1);
    }

    @ParameterizedTest
    @MethodSource("getTimeProvider")
    public void testThatElapsedTimeSeemsToBeCorrectBeforeStop(TimeProvider timeProvider) throws InterruptedException {
        StopWatch stopWatch = new DefaultStopWatch(timeProvider).start();
        Thread.sleep(10);
        assertEquals(10, stopWatch.getElapsedTime(TimeUnit.MILLISECONDS), 1);
    }

    @ParameterizedTest
    @MethodSource("getTimeProvider")
    public void testThatElapsedTimeSeemsToBeCorrectAfterStop(TimeProvider timeProvider) throws InterruptedException {
        StopWatch stopWatch = new DefaultStopWatch(timeProvider).start();
        Thread.sleep(10);
        assertEquals(10, stopWatch.stop().getElapsedTime(TimeUnit.MILLISECONDS), 1);
    }
}