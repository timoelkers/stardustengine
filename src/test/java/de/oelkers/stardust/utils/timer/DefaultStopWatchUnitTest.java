package de.oelkers.stardust.utils.timer;

import de.oelkers.stardust.config.ConfigStore;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DefaultStopWatchUnitTest {

    @Test
    public void testThatStartSetsStateFlagsCorrectly() {
        DefaultStopWatch stopWatch = new DefaultStopWatch().start();
        assertTrue(stopWatch.isStarted());
        assertTrue(stopWatch.isRunning());
        assertFalse(stopWatch.isStopped());
    }

    @Test
    public void testThatStopSetsStateFlagsCorrectly() {
        DefaultStopWatch stopWatch = new DefaultStopWatch().start().stop();
        assertTrue(stopWatch.isStarted());
        assertFalse(stopWatch.isRunning());
        assertTrue(stopWatch.isStopped());
    }

    @Test
    public void testThatDefaultConstructorUsesConfiguredTimeProvider() {
        ConfigStore.get().setTimeProvider(DefaultTimeProvider.SYSTEM_MILLI_TIMER);
        DefaultStopWatch stopWatch = new DefaultStopWatch();
        assertEquals(DefaultTimeProvider.SYSTEM_MILLI_TIMER, stopWatch.getTimeProvider());
        ConfigStore.get().setTimeProvider(DefaultTimeProvider.SYSTEM_NANO_TIMER);
        stopWatch = new DefaultStopWatch();
        assertEquals(DefaultTimeProvider.SYSTEM_NANO_TIMER, stopWatch.getTimeProvider());
    }
}