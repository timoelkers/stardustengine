package de.oelkers.stardust.utils;

import de.oelkers.stardust.utils.threading.ParallelRunner;
import de.oelkers.stardust.utils.threading.Runner;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.*;

public class LazyUnitTest {

    @Test
    public void testThatValueIsMemoized() {
        AtomicInteger methodCalled = new AtomicInteger();
        Supplier<Integer> lazy = new Lazy<>(methodCalled::getAndIncrement);
        lazy.get();
        lazy.get();
        assertSame(1, methodCalled.get());
    }

    @Test
    public void testThatDelegateIsExecutedLazily() {
        AtomicBoolean methodCalled = new AtomicBoolean();
        Supplier<Boolean> lazy = new Lazy<>(() -> methodCalled.getAndSet(true));
        assertFalse(methodCalled.get());
        lazy.get();
        assertTrue(methodCalled.get());
    }

    @Test
    public void testThatDelegateIsOnlyCalledOnce() {
        AtomicInteger methodCalled = new AtomicInteger();
        Lazy<Integer> lazy = new Lazy<>(methodCalled::getAndIncrement);
        Runner runner = new ParallelRunner();
        runner.addRunnable(lazy::get, 10);
        runner.run().waitForCompletion();
        assertSame(1, methodCalled.get());
    }
}