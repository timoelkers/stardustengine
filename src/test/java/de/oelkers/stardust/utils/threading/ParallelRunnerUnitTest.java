package de.oelkers.stardust.utils.threading;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ParallelRunnerUnitTest {

    @Test
    public void testThatAllRunnablesAreRun() {
        Runner runner = new ParallelRunner();
        AtomicInteger methodsRun = new AtomicInteger();
        for (int i = 0; i < 10; i++) {
            runner.addRunnables(methodsRun::getAndIncrement);
        }
        runner.run().waitForCompletion();
        assertEquals(10, methodsRun.get());
    }

    @Test
    public void testThatRunnerCanBeClearedAndRestarted() {
        Runner runner = new ParallelRunner();
        AtomicInteger methodsRun = new AtomicInteger();
        runner.addRunnables(methodsRun::getAndIncrement);
        runner.run().waitForCompletion();
        assertEquals(1, methodsRun.get());
        runner.clear().addRunnables(methodsRun::getAndIncrement);
        runner.run().waitForCompletion();
        assertEquals(2, methodsRun.get());
    }

    @Test
    public void testThatWaitForCompletionCanBeInterrupted() {
        Runner runner = new ParallelRunner();
        runner.addRunnables(getEndlessRunningRunnable()).run();
        getInterruptingThread(50).start();
        runner.waitForCompletion();
        assertTrue(Thread.interrupted());
    }

    @Test
    public void testThatWaitForCompletionReturnsOnTimeout() {
        Runner runner = new ParallelRunner();
        runner.addRunnables(getEndlessRunningRunnable()).run();
        runner.waitForCompletion(Duration.ofMillis(50));
    }

    private static Runnable getEndlessRunningRunnable() {
        return () -> {
            while(!Thread.currentThread().isInterrupted());
        };
    }

    private static Thread getInterruptingThread(int waitTimeMillis) {
        Thread threadToInterrupt = Thread.currentThread();
        return new Thread(() -> {
            try {
                Thread.sleep(waitTimeMillis);
                threadToInterrupt.interrupt();
            } catch (InterruptedException ignored) {}
        });
    }

}