package de.oelkers.stardust.utils.objectpools;

import de.oelkers.stardust.utils.threading.ParallelRunner;
import de.oelkers.stardust.utils.threading.Runner;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class GrowingObjectPoolUnitTest {

    @Test
    public void testThatANewObjectIsCreatedIfPoolIsEmpty() {
        ObjectPool<Object> objectPool = new GrowingObjectPool<>(Object::new);
        objectPool.get();
        assertEquals(1, objectPool.getPoolSize());
    }

    @Test
    public void testThatFreedObjectCanBeQueriedAgain() {
        ObjectPool<Object> objectPool = new GrowingObjectPool<>(Object::new);
        Object object = objectPool.get();
        objectPool.free(object);
        assertEquals(1, objectPool.getFreeSize());
        assertEquals(object, objectPool.get());
    }

    @Test
    public void testThatObjectCanNotBeFreedSuccessively() {
        ObjectPool<Object> objectPool = new GrowingObjectPool<>(Object::new);
        Object object = objectPool.get();
        objectPool.free(object);
        objectPool.free(object);
        assertEquals(1, objectPool.getFreeSize());
    }

    @Test
    public void testThatQueryingIsThreadSafe() {
        Runner runner = new ParallelRunner();
        ObjectPool<Object> objectPool = new GrowingObjectPool<>(Object::new);
        for (int i = 0; i < 10; i++) {
            runner.addRunnables(objectPool::get);
        }
        runner.run().waitForCompletion();
        assertEquals(10, objectPool.getPoolSize());
    }

    @Test
    public void testThatFreeingIsThreadSafe() {
        Runner runner = new ParallelRunner();
        ObjectPool<Object> objectPool = new GrowingObjectPool<>(Object::new);
        Object object = objectPool.get();
        for (int i = 0; i < 10; i++) {
            runner.addRunnables(() -> objectPool.free(object));
        }
        runner.run().waitForCompletion();
        assertEquals(1, objectPool.getFreeSize());
    }

    @Test
    public void testThatConsumeFreesTheObject() {
        ObjectPool<Object> objectPool = new GrowingObjectPool<>(Object::new);
        objectPool.consume(Assertions::assertNotNull);
        assertEquals(1, objectPool.getFreeSize());
    }

    @Test
    public void testThatApplyFreesTheObject() {
        ObjectPool<Object> objectPool = new GrowingObjectPool<>(Object::new);
        assertEquals("Expected", objectPool.apply(o -> {
            assertNotNull(o);
            return "Expected";
        }));
        assertEquals(1, objectPool.getFreeSize());
    }
}