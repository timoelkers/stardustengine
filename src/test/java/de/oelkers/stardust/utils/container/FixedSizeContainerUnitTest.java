package de.oelkers.stardust.utils.container;

import de.oelkers.stardust.utils.container.Container.ExpectationNotMetException;
import de.oelkers.stardust.utils.container.Container.RestrictionNotMetException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FixedSizeContainerUnitTest {

    @Test
    public void testThatSizeIsEnforcedOnFilling() {
        Container<String> containerMultiple = new FixedSizeContainer<>(3);
        assertThrows(RestrictionNotMetException.class, () -> containerMultiple.set("test"));
        assertThrows(RestrictionNotMetException.class, () -> containerMultiple.set(new String[] { "test", "test" }));
        assertDoesNotThrow(() -> containerMultiple.set(new String[] { "test", "test", "test" }));

        Container<String> containerSingle = new FixedSizeContainer<>(1);
        assertThrows(RestrictionNotMetException.class, () -> containerSingle.set(new String[] { "test", "test" }));
        assertDoesNotThrow(() -> containerSingle.set("test"));
    }

    @Test
    public void testThatNullIsReturnedIfContainerIsEmpty() {
        Container<String> container = new FixedSizeContainer<>(3);
        assertNull(container.getMultiple());
        container = new FixedSizeContainer<>(1);
        assertNull(container.getOnly());
    }

    @Test
    public void testThatIncorrectSizesAreReportedWhenReading() {
        Container<String> containerMultiple = new FixedSizeContainer<>(3);
        assertThrows(ExpectationNotMetException.class, containerMultiple::getOnly);
        assertDoesNotThrow(containerMultiple::getMultiple);
        Container<String> containerSingle = new FixedSizeContainer<>(1);
        assertThrows(ExpectationNotMetException.class, containerSingle::getMultiple);
        assertDoesNotThrow(containerSingle::getOnly);
    }
}