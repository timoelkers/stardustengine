package de.oelkers.stardust.utils.files;

import common.TemporaryFileExtension;
import de.oelkers.stardust.utils.ArrayUtils;
import org.awaitility.Durations;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

import static de.oelkers.stardust.utils.files.FileWatcher.THREAD_NAME;
import static org.awaitility.Awaitility.waitAtMost;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FileWatcherUnitTest {

    @Test
    @ExtendWith(TemporaryFileExtension.class)
    public void testThatCallbackIsExecutedOnFileChanges(File file) throws IOException {
        AtomicBoolean callbackExecuted = new AtomicBoolean();
        try (FileWatcher fileWatcher = new FileWatcher()) {
            fileWatcher.watch(file.getPath(), () -> callbackExecuted.set(true));
            try (FileWriter fileWriter = new FileWriter(file)) {
                fileWriter.append("Test");
            }
            waitAtMost(Durations.FIVE_SECONDS).untilAsserted(() -> assertTrue(callbackExecuted.get()));
        }
    }

    @Test
    public void testThatPollingThreadIsNotStartedOnObjectConstruction() throws InterruptedException {
        try (FileWatcher ignored = new FileWatcher()) {
            Thread.sleep(100);
            Thread[] threads = getAllThreads();
            assertTrue(Arrays.stream(threads).noneMatch(t -> THREAD_NAME.equals(t.getName())));
        }
    }

    private static Thread[] getAllThreads() {
        ThreadGroup rootGroup = Thread.currentThread().getThreadGroup();
        ThreadGroup parentGroup;
        while ((parentGroup = rootGroup.getParent()) != null) {
            rootGroup = parentGroup;
        }
        Thread[] threads = new Thread[rootGroup.activeCount()];
        while (rootGroup.enumerate(threads) == threads.length) {
            threads = new Thread[threads.length + threads.length / 4];
        }
        return ArrayUtils.trim(threads);
    }
}