package common;

import org.junit.jupiter.api.extension.*;

import java.io.File;
import java.io.IOException;

public class TemporaryFileExtension implements ParameterResolver, BeforeEachCallback, AfterEachCallback {

    public static final String EXTENSION = ".test";

    private File file;

    @Override
    public void beforeEach(ExtensionContext extensionContext) throws IOException {
        file = File.createTempFile("StardustEngine", EXTENSION);
    }

    @Override
    public void afterEach(ExtensionContext extensionContext) {
        if (!file.delete()) {
            throw new IllegalStateException("Unable to delete file " + file + '.');
        }
    }

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return parameterContext.getParameter().getType() == File.class;
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return file;
    }
}
