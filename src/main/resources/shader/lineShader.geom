#version 420 core

layout (lines) in;
layout (triangle_strip, max_vertices = 4) out;

in VertexData {
    vec2 uvs;
} gs_in[2];

out VertexData {
    vec2 uvs;
} gs_out;

uniform float lineWidth;
uniform bool useFixedViewport;

void main() {
    gl_ViewportIndex = useFixedViewport ? 1 : 0;
    vec3 start = gl_in[0].gl_Position.xyz;
    vec3 end = gl_in[1].gl_Position.xyz;
    vec3 perpendicular = cross(normalize(end - start), vec3(0.0, 0.0, -1.0)) * lineWidth;
    gl_Position = vec4(start + perpendicular, 1.0);
    gs_out.uvs = gs_in[0].uvs;
    EmitVertex();
    gl_Position = vec4(start - perpendicular, 1.0);
    EmitVertex();
    gl_Position = vec4(end + perpendicular, 1.0);
    gs_out.uvs = gs_in[1].uvs;
    EmitVertex();
    gl_Position = vec4(end - perpendicular, 1.0);
    EmitVertex();
    EndPrimitive();
}
