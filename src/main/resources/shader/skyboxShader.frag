#version 420 core

in VertexData {
    vec3 textureDir;
} fs_in;

out vec4 out_color;

layout(binding = 0) uniform samplerCube skyboxTexture;

void main(void) {
	out_color = texture(skyboxTexture, fs_in.textureDir);
    out_color = applyNextTechnique(out_color);
}
