#version 420 core

layout(location = 0) in vec2 in_position;
layout(location = 1) in vec2 in_uvs;

out VertexData {
	vec2 uvs;
} vs_out;

uniform mat4 modelMatrix;

void main(void) {
	vs_out.uvs = in_uvs;
	gl_Position = modelMatrix * vec4(in_position, 0.0, 1.0);
}
