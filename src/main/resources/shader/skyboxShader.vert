#version 420 core

layout(location = 0) in vec3 in_position;

out VertexData {
	vec3 textureDir;
} vs_out;

uniform mat4 modelViewProjectionMatrix;

void main(void) {
	vs_out.textureDir = vec3(-in_position.x, in_position.y, in_position.z);
 	gl_Position = modelViewProjectionMatrix * vec4(in_position, 1.0);
}
