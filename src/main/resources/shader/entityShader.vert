#version 420 core

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec2 in_uvs;
layout(location = 2) in vec3 in_normal;

out VertexData {
	vec2 uvs;
	vec3 surfaceNormal;
	vec3 worldPosition;
} vs_out;

uniform mat4 normalMatrix;
uniform mat4 modelMatrix;
uniform mat4 modelViewProjectionMatrix;

void main(void) {
	vs_out.uvs = in_uvs;
	vs_out.surfaceNormal = mat3(normalMatrix) * in_normal;
	vec4 localPosition = vec4(in_position, 1.0f);
	vs_out.worldPosition = (modelMatrix * localPosition).xyz;
	gl_Position = modelViewProjectionMatrix * localPosition;
}
