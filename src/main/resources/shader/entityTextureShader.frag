#version 420 core

in VertexData {
    vec2 uvs;
    vec3 surfaceNormal;
    vec3 worldPosition;
} fs_in;

out vec4 out_color;

layout(binding = 0) uniform sampler2D texture2D;

void main(void) {
    out_color = texture(texture2D, fs_in.uvs);
    out_color = applyNextTechnique(out_color);
}
