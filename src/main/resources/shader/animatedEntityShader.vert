#version 420 core

#define MAX_JOINTS 50
#define MAX_WEIGHTS 4

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec2 in_uvs;
layout(location = 2) in vec3 in_normal;
layout(location = 3) in ivec4 in_jointIndices;
layout(location = 4) in vec4 in_weights;

out VertexData {
    vec2 uvs;
    vec3 surfaceNormal;
    vec3 worldPosition;
} vs_out;

uniform mat4 normalMatrix;
uniform mat4 modelMatrix;
uniform mat4 modelViewProjectionMatrix;
uniform mat4 jointTransforms[MAX_JOINTS];

void main(void) {
    vec4 localPosition = vec4(in_position, 1.0f);
    vec4 localNormal = vec4(in_normal, 0.0f);
    vec4 totalLocalPos = vec4(0.0f);
    vec4 totalNormal = vec4(0.0f);
    for (int i = 0; i < MAX_WEIGHTS; i++) {
        mat4 jointTransform = jointTransforms[in_jointIndices[i]];
        vec4 posePosition = jointTransform * localPosition;
        totalLocalPos += posePosition * in_weights[i];
        vec4 worldNormal = jointTransform * localNormal;
        totalNormal += worldNormal * in_weights[i];
    }
    vs_out.uvs = in_uvs;
    vs_out.surfaceNormal = mat3(normalMatrix) * totalNormal.xyz;
    vs_out.worldPosition = (modelMatrix * localPosition).xyz;
    gl_Position = modelViewProjectionMatrix * totalLocalPos;
}
