#version 420 core

uniform bool useFixedViewport;

void main() {
    gl_ViewportIndex = useFixedViewport ? 1 : 0;
}
