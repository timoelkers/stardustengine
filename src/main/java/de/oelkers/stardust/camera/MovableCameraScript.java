package de.oelkers.stardust.camera;

import de.oelkers.stardust.math.transformations.TransformSpace;
import de.oelkers.stardust.math.vectors.ReadOnlyVector3f;
import de.oelkers.stardust.math.vectors.Vector3f;
import de.oelkers.stardust.window.ButtonState;
import de.oelkers.stardust.window.Key;
import de.oelkers.stardust.window.StardustWindow;

public abstract class MovableCameraScript extends CameraScript {

    private float forwardSpeed = 100;
    private float upwardSpeed = 100;
    private float strafeSpeed = 100;
    private boolean ignoreYaw, ignorePitch;

    private final Vector3f movement = new Vector3f();

    protected MovableCameraScript(Camera camera, StardustWindow window) {
        super(camera);
        window.addKeyCallback(this, this::calculateAcceleration);
    }

    /**
     * Returns the number of units the camera moves forward or backward (along the z-axis)
     * in one second (assuming a keyboard sensitivity of one).
     */
    public float getForwardSpeed() {
        return forwardSpeed;
    }

    public void setForwardSpeed(float forwardSpeed) {
        this.forwardSpeed = forwardSpeed;
    }

    /**
     * Returns the number of units the camera moves upward or downward (along the y-axis)
     * in one second (assuming a keyboard sensitivity of one).
     */
    public float getUpwardSpeed() {
        return upwardSpeed;
    }

    public void setUpwardSpeed(float upwardSpeed) {
        this.upwardSpeed = upwardSpeed;
    }

    /**
     * Returns the number of units the camera moves sideways (along the x-axis)
     * in one second (assuming a keyboard sensitivity of one).
     */
    public float getStrafeSpeed() {
        return strafeSpeed;
    }

    public void setStrafeSpeed(float strafeSpeed) {
        this.strafeSpeed = strafeSpeed;
    }

    public boolean isIgnoreYaw() {
        return ignoreYaw;
    }

    public final void setIgnoreYaw(boolean ignoreYaw) {
        this.ignoreYaw = ignoreYaw;
    }

    public boolean isIgnorePitch() {
        return ignorePitch;
    }

    public final void setIgnorePitch(boolean ignorePitch) {
        this.ignorePitch = ignorePitch;
    }

    public ReadOnlyVector3f getDirection() {
        return movement;
    }

    @Override
    public void update(double frameTimeSeconds) {
        if (enabled && !movement.isZero()) {
            Vector3f.POOL.consume(vector -> {
                movement.scale((float) frameTimeSeconds * keyboardSensitivity, vector);
                if (ignorePitch && ignoreYaw) {
                    camera.getTransform().translate(vector, TransformSpace.WORLD);
                } else if (ignorePitch) {
                    float pitch = (float) camera.getPitchRad();
                    camera.getTransform().rotate(Vector3f.RIGHT, -pitch, TransformSpace.LOCAL);
                    camera.getTransform().translate(vector, TransformSpace.LOCAL);
                    camera.getTransform().rotate(Vector3f.RIGHT, pitch, TransformSpace.LOCAL);
                } else if (ignoreYaw) {
                    float yaw = (float) camera.getYawRad();
                    camera.getTransform().rotate(Vector3f.UP, -yaw, TransformSpace.LOCAL);
                    camera.getTransform().translate(vector, TransformSpace.LOCAL);
                    camera.getTransform().rotate(Vector3f.UP, yaw, TransformSpace.LOCAL);
                } else {
                    camera.getTransform().translate(vector, TransformSpace.LOCAL);
                }
                camera.updateView();
            });
        }
    }

    private void calculateAcceleration(Key key, ButtonState state) {
        if (state != ButtonState.REPEATED) {
            if (key == Key.KEY_W) {
                movement.add(0, 0, state == ButtonState.PRESSED ? -forwardSpeed : forwardSpeed);
            } else if (key == Key.KEY_A) {
                movement.add(state == ButtonState.PRESSED ? -strafeSpeed : strafeSpeed, 0, 0);
            } else if (key == Key.KEY_S) {
                movement.add(0, 0, state == ButtonState.PRESSED ? forwardSpeed : -forwardSpeed);
            } else if (key == Key.KEY_D) {
                movement.add(state == ButtonState.PRESSED ? strafeSpeed : -strafeSpeed, 0, 0);
            } else if (key == Key.KEY_Q) {
                movement.add(0, state == ButtonState.PRESSED ? -upwardSpeed : upwardSpeed, 0);
            } else if (key == Key.KEY_E) {
                movement.add(0, state == ButtonState.PRESSED ? upwardSpeed : -upwardSpeed, 0);
            }
        }
    }
}
