package de.oelkers.stardust.camera;

import de.oelkers.stardust.math.matrices.ReadOnlyMatrix4f;
import de.oelkers.stardust.math.shapes.Plane;
import de.oelkers.stardust.math.shapes.Plane.PlaneSide;
import de.oelkers.stardust.math.vectors.ReadOnlyVector3f;
import de.oelkers.stardust.math.vectors.ReadOnlyVector4f;
import de.oelkers.stardust.math.vectors.Vector3f;
import de.oelkers.stardust.math.vectors.Vector4f;
import de.oelkers.stardust.utils.ArrayUtils;

import java.util.Arrays;

public class ViewFrustum {

    public enum FrustumPlane {

        LEFT(0),
        RIGHT(1),
        BOTTOM(2),
        TOP(3),
        NEAR(4),
        FAR(5);

        private final int index;

        private int getIndex() {
            return index;
        }

        FrustumPlane(int index) {
            this.index = index;
        }
    }

    private static final ReadOnlyVector4f[] CUBE_VERTICES = {
            new Vector4f(-1, -1, -1, 1), new Vector4f(1, -1, -1, 1),
            new Vector4f(1, 1, -1, 1), new Vector4f(-1, 1, -1, 1),
            new Vector4f(-1, -1, 1, 1), new Vector4f(1, -1, 1, 1),
            new Vector4f(1, 1, 1, 1), new Vector4f(-1, 1, 1, 1)
    };

    private final Vector3f[] planePoints = ArrayUtils.setAll(Vector3f::new, new Vector3f[8]);
    private final Plane[] frustumPlanes = ArrayUtils.setAll(Plane::new, new Plane[6]);

    /**
     * Updates the view frustum (visible area on the screen) for the given camera. This transforms the
     * coordinates of a cube with the inverse projection-view matrix and calculates the planes by
     * its corner coordinates. This is normally called from the camera itself, so unless you have your very
     * own camera implementation, don`t call this method.
     *
     * @param camera the camera you want to update the frustum of
     */
    public void update(Camera camera) {
        ReadOnlyMatrix4f inverseProjectionView = camera.getInverseProjectionViewMatrix();
        Vector4f.POOL.consume(vector -> {
            for (int i = 0; i < CUBE_VERTICES.length; i++) {
                CUBE_VERTICES[i].transform(inverseProjectionView, vector);
                vector.project(planePoints[i]);
            }
        });
        frustumPlanes[FrustumPlane.NEAR.getIndex()].set(planePoints[1], planePoints[0], planePoints[2]);
        frustumPlanes[FrustumPlane.FAR.getIndex()].set(planePoints[4], planePoints[5], planePoints[7]);
        frustumPlanes[FrustumPlane.LEFT.getIndex()].set(planePoints[0], planePoints[4], planePoints[3]);
        frustumPlanes[FrustumPlane.RIGHT.getIndex()].set(planePoints[5], planePoints[1], planePoints[6]);
        frustumPlanes[FrustumPlane.TOP.getIndex()].set(planePoints[2], planePoints[3], planePoints[6]);
        frustumPlanes[FrustumPlane.BOTTOM.getIndex()].set(planePoints[4], planePoints[0], planePoints[1]);
    }

    public Plane getFrustumPlane(FrustumPlane planeOrientation) {
        return frustumPlanes[planeOrientation.getIndex()];
    }

    /** Checks if the given point is visible on the screen (in the cameras view frustum).
     * This checks the point against each plane, if it is outside of any plane, it will
     * return false.
     *
     * @param point the point you want to check in world coordinates
     * @return whether or not the point is visible
     */
    public boolean isPointInFrustum(ReadOnlyVector3f point) {
        return isPointInFrustum(point.getX(), point.getY(), point.getZ());
    }

    /** Checks if the given point is visible on the screen (in the cameras view frustum).
     * This checks the point against each plane, if it is outside of any plane, it will
     * return false immediately.
     *
     * @param x the x-coordinate of the point
     * @param y the y-coordinate of the point
     * @param z the z-coordinate of the point
     * @return whether or not the point is visible
     */
    public boolean isPointInFrustum(float x, float y, float z) {
        return Arrays.stream(frustumPlanes).noneMatch(frustumPlane -> frustumPlane.pointOnPlane(x, y, z) == PlaneSide.BACK);
    }

    /** Checks if all of the given points are visible on the screen (in the cameras view frustum).
     * This checks every point against the frustum, if any of the points is not visible, it
     * will return false immediately.
     *
     * @param points the points you want to check in world coordinates
     * @return whether or not all of the points are visible
     */
    public boolean arePointsInFrustum(ReadOnlyVector3f[] points) {
        return Arrays.stream(points).allMatch(this::isPointInFrustum);
    }

    /** Checks if the given collision shape is visible on the screen (in the cameras view frustum).
     * This checks the transformed world coordinates of the specified shape against each plane and detects
     * if all vertices are outside the same plane. Unlike the faster {@link #isShapeInFrustumFast(ReadOnlyVector3f[])}
     * version, this will also return true if the shape intersects the view but all vertices are outside of it.
     *
     * @param vertices the vertices which define the shape in world coordinates
     * @return whether or not the shape is visible
     */
    public boolean isShapeInFrustum(ReadOnlyVector3f[] vertices) {
        return Arrays.stream(frustumPlanes)
                .mapToInt(frustumPlane -> (int) Arrays.stream(vertices).filter(vertex -> frustumPlane.pointOnPlane(vertex) == PlaneSide.BACK).count())
                .noneMatch(outCount -> outCount == vertices.length);
    }

    /** Checks if the given collision shape is visible on the screen (in the cameras view frustum).
     * This performs a fast check and returns as soon as any point of the shape is inside
     * the frustum. Although this can result in a false negative if all points are
     * outside the frustum, but the shape is still intersecting the view (for very large
     * objects or high zoom values), it is much faster.
     *
     * @param vertices the vertices which define the shape in world coordinates
     * @return whether or not the shape is visible
     */
    public boolean isShapeInFrustumFast(ReadOnlyVector3f[] vertices) {
        return Arrays.stream(vertices).anyMatch(this::isPointInFrustum);
    }

    /** Checks if the given sphere is visible on the screen (in the cameras view frustum).
     * This checks if the distance from the point to the plane is smaller than the
     * spheres radius.
     *
     * @param center the center of the sphere
     * @param radius the radius of the sphere
     * @return whether or not the sphere is visible
     */
    public boolean isSphereInFrustum(ReadOnlyVector3f center, float radius) {
        return isSphereInFrustum(center.getX(), center.getY(), center.getZ(), radius);
    }

    /** Checks if the given sphere is visible on the screen (in the cameras view frustum).
     * This checks if the distance from the point to the plane is smaller than the
     * spheres radius.
     *
     * @param x the x-coordinate of the spheres center
     * @param y the y-coordinate of the spheres center
     * @param z the z-coordinate of the spheres center
     * @param radius the radius of the sphere
     * @return whether or not the sphere is visible
     */
    public boolean isSphereInFrustum(float x, float y, float z, float radius) {
        return Arrays.stream(frustumPlanes).noneMatch(frustumPlane -> frustumPlane.distance(x, y, z) < -radius);
    }
}
