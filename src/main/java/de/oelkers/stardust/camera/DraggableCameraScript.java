package de.oelkers.stardust.camera;

import de.oelkers.stardust.window.ButtonState;
import de.oelkers.stardust.window.MouseButton;
import de.oelkers.stardust.window.StardustWindow;

/**
 * This camera script behaves similar to a {@link FreeFlyCameraScript}, but it only calculates the rotation when the
 * mouse button specified by {@link #setDragButton(MouseButton) setDragButton()} is pressed and it initially ignores
 * the pitch of the camera when moving in the view direction.
 *
 * @see FreeFlyCameraScript
 */
public class DraggableCameraScript extends FreeFlyCameraScript {

    private MouseButton dragButton = MouseButton.BUTTON_RIGHT;
    private boolean dragging;

    public DraggableCameraScript(Camera camera, StardustWindow window) {
        super(camera, window);
        window.addMouseButtonCallback(this, (button, state) -> {
            if (button == dragButton) {
                if (state != ButtonState.REPEATED) {
                    dragging = state == ButtonState.PRESSED;
                }
            }
        });
        setIgnorePitch(true);
    }

    public MouseButton getDragButton() {
        return dragButton;
    }

    public void setDragButton(MouseButton dragButton) {
        this.dragButton = dragButton;
    }

    @Override
    protected void calculateRotation(double mouseX, double mouseY) {
        if (dragging) {
            super.calculateRotation(mouseX, mouseY);
        }
    }
}
