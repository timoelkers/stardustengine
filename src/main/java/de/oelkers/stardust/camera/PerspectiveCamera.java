package de.oelkers.stardust.camera;

import de.oelkers.stardust.math.internal.MathDelegate;
import de.oelkers.stardust.math.matrices.Matrix4f;
import de.oelkers.stardust.window.StardustWindow;

public class PerspectiveCamera extends Camera {

    private final float fieldOfView;

    /**
     * Creates a camera with a perspective projection matrix based on the given frustum. This central projection is
     * used to picture your three dimensional objects onto a two dimensional screen while keeping an illusion
     * of a third dimension. It is used to transform objects from view space to clip space.
     *
     * @param fieldOfView the field of view in degrees in which objects should be rendered
     * @param nearPlane the distance to the front clipping plane
     * @param farPlane the distance to the back clipping plane
     */
    public PerspectiveCamera(StardustWindow window, float fieldOfView, float nearPlane, float farPlane) {
        super(window, nearPlane, farPlane);
        this.fieldOfView = fieldOfView;
    }

    public float getFieldOfView() {
        return fieldOfView;
    }

    @Override
    protected void updateProjectionMatrix(Matrix4f projectionMatrix) {
        float yScale = (float) (1.0f / MathDelegate.tan(MathDelegate.toRadians(fieldOfView / 2.0f)));
        float xScale = yScale / getWindow().getAspectRatio();
        float frustumLength = getFarPlane() - getNearPlane();

        projectionMatrix.setIdentity();
        projectionMatrix.setValue(0, 0, xScale);
        projectionMatrix.setValue(1, 1, yScale);
        projectionMatrix.setValue(2, 2, -((getFarPlane() + getNearPlane()) / frustumLength));
        projectionMatrix.setValue(2, 3, -1.0f);
        projectionMatrix.setValue(3, 2, -(2.0f * getFarPlane() * getNearPlane() / frustumLength));
        projectionMatrix.setValue(3, 3, 0.0f);
    }
}
