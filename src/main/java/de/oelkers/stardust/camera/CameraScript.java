package de.oelkers.stardust.camera;

public abstract class CameraScript {

    protected final Camera camera;
    protected boolean enabled = true;
    protected float keyboardSensitivity = 1f;
    protected volatile float mouseSensitivity = 1f;

    protected CameraScript(Camera camera) {
        this.camera = camera;
    }

    public Camera getCamera() {
        return camera;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public float getMouseSensitivity() {
        return mouseSensitivity;
    }

    public void setMouseSensitivity(float mouseSensitivity) {
        this.mouseSensitivity = mouseSensitivity;
    }

    public float getKeyboardSensitivity() {
        return keyboardSensitivity;
    }

    public void setKeyboardSensitivity(float keyboardSensitivity) {
        this.keyboardSensitivity = keyboardSensitivity;
    }

    public abstract void update(double frameTimeSeconds);
}
