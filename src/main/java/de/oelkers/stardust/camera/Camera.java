package de.oelkers.stardust.camera;

import de.oelkers.stardust.math.matrices.Matrix4f;
import de.oelkers.stardust.math.matrices.ReadOnlyMatrix4f;
import de.oelkers.stardust.math.shapes.Ray;
import de.oelkers.stardust.math.transformations.Transform3D;
import de.oelkers.stardust.math.transformations.Transformable3D;
import de.oelkers.stardust.math.vectors.Vector3f;
import de.oelkers.stardust.math.vectors.Vector4f;
import de.oelkers.stardust.window.StardustWindow;

public abstract class Camera {

    private final StardustWindow window;
    private final float nearPlane, farPlane;
    private final Transform3D cameraTransform = new Transform3D();
    private final Matrix4f viewMatrix = new Matrix4f();
    private final Matrix4f projectionMatrix = new Matrix4f();
    private final Matrix4f projectionViewMatrix = new Matrix4f();
    private final Matrix4f inverseProjectionViewMatrix = new Matrix4f();
    private final ViewFrustum viewFrustum = new ViewFrustum();

    protected Camera(StardustWindow window, float nearPlane, float farPlane) {
        this.window = window;
        this.nearPlane = nearPlane;
        this.farPlane = farPlane;
        window.addResizeCallback(this, (width, height) -> updateProjection());
    }

    public StardustWindow getWindow() {
        return window;
    }

    public float getNearPlane() {
        return nearPlane;
    }

    public float getFarPlane() {
        return farPlane;
    }

    public Transformable3D getTransform() {
        return cameraTransform;
    }

    public ReadOnlyMatrix4f getProjectionMatrix() {
        return projectionMatrix;
    }

    public ReadOnlyMatrix4f getViewMatrix() {
        return viewMatrix;
    }

    public ReadOnlyMatrix4f getProjectionViewMatrix() {
        return projectionViewMatrix;
    }

    public ReadOnlyMatrix4f getInverseProjectionViewMatrix() {
        return inverseProjectionViewMatrix;
    }

    public ViewFrustum getViewFrustum() {
        return viewFrustum;
    }

    public double getPitchRad() {
        return cameraTransform.getOrientation().getPitchRad();
    }

    public double getPitchDeg() {
        return cameraTransform.getOrientation().getPitchDeg();
    }

    public double getYawRad() {
        return cameraTransform.getOrientation().getYawRad();
    }

    public double getYawDeg() {
        return cameraTransform.getOrientation().getYawDeg();
    }

    public double getRollRad() {
        return cameraTransform.getOrientation().getRollRad();
    }

    public double getRollDeg() {
        return cameraTransform.getOrientation().getRollDeg();
    }

    public Camera updateView() {
        cameraTransform.getModelMatrix().invert(viewMatrix);
        updateFrustum();
        return this;
    }

    public Camera updateProjection() {
        updateProjectionMatrix(projectionMatrix);
        updateFrustum();
        return this;
    }

    public Ray getMouseRay(float x, float y) {
        return Matrix4f.POOL.apply(matrix -> {
            return Vector4f.POOL.apply(vector -> {
                vector.set(window.toClipSpaceX(x), window.toClipSpaceY(y), -1.0f, 1.0f);
                vector.transform(projectionMatrix.invert(matrix));
                vector.set(vector.getX(), vector.getY(), -1.0f, 0.0f);
                vector.transform(viewMatrix.invert(matrix));
                Vector3f direction = new Vector3f(vector.getX(), vector.getY(), vector.getZ()).normalize();
                return new Ray(cameraTransform.getPosition(), direction);
            });
        });
    }

    private void updateFrustum() {
        projectionMatrix.mul(viewMatrix, projectionViewMatrix);
        projectionViewMatrix.invert(inverseProjectionViewMatrix);
        viewFrustum.update(this);
    }

    protected abstract void updateProjectionMatrix(Matrix4f projectionMatrix);
}
