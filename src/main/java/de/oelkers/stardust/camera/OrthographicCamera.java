package de.oelkers.stardust.camera;

import de.oelkers.stardust.math.matrices.Matrix4f;
import de.oelkers.stardust.window.StardustWindow;

public class OrthographicCamera extends Camera {

    public OrthographicCamera(StardustWindow window, float nearPlane, float farPlane) {
        super(window, nearPlane, farPlane);
    }

    @Override
    protected void updateProjectionMatrix(Matrix4f projectionMatrix) {
        projectionMatrix.setIdentity();
        projectionMatrix.setValue(0, 0, 2.0f / getWindow().getWidth());
        projectionMatrix.setValue(1, 1, 2.0f / getWindow().getHeight());
        projectionMatrix.setValue(2, 2, -2.0f / getFarPlane() - getNearPlane());
    }
}
