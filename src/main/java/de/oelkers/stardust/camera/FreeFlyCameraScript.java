package de.oelkers.stardust.camera;

import de.oelkers.stardust.math.transformations.TransformSpace;
import de.oelkers.stardust.math.vectors.Vector3f;
import de.oelkers.stardust.window.StardustWindow;

/**
 * This camera script translates the camera with the W (forward), A (left), S (backward), D (right), Q (downward),
 * E (upward) keys alongside the direction of view, which is changed with your mouse.
 * Moving the mouse on the x-axis changes the yaw and moving on the y-axis changes the pitch of the camera.
 * A {@link de.oelkers.stardust.window.CursorMode#CAPTURED captured input mode} is recommended for this script.
 *
 * @see DraggableCameraScript
 */
public class FreeFlyCameraScript extends MovableCameraScript {

    private float pitchSpeed = 0.002f;
    private float yawSpeed = 0.002f;

    private final MouseDelta mouseDelta = new MouseDelta();

    public FreeFlyCameraScript(Camera camera, StardustWindow window) {
        super(camera, window);
        window.addMousePositionCallback(this, this::calculateRotation);
    }

    /**
     * Returns the amount of radians the camera rotates around the x-axis for each
<     * screen coordinate the mouse cursor moved on the y-axis (assuming a mouse sensitivity of one).
     */
    public float getPitchSpeed() {
        return pitchSpeed;
    }

    public void setPitchSpeed(float pitchSpeed) {
        this.pitchSpeed = pitchSpeed;
    }

    /**
     * Returns the amount of radians the camera rotates around the y-axis for each
     * screen coordinate the mouse cursor moved on the x-axis (assuming a mouse sensitivity of one).
     */
    public float getYawSpeed() {
        return yawSpeed;
    }

    public void setYawSpeed(float yawSpeed) {
        this.yawSpeed = yawSpeed;
    }

    protected void calculateRotation(double mouseX, double mouseY) {
        float yaw = mouseDelta.getXDelta(mouseX) * yawSpeed * mouseSensitivity;
        float pitch = mouseDelta.getYDelta(mouseY) * pitchSpeed * mouseSensitivity;
        camera.getTransform().rotate(Vector3f.UP, yaw, TransformSpace.WORLD);
        camera.getTransform().rotate(Vector3f.RIGHT, pitch, TransformSpace.LOCAL);
        camera.updateView();
    }

    private static class MouseDelta {

        private boolean firstXValue = true, firstYValue = true;
        private double lastMouseX, lastMouseY;

        private float getXDelta(double currentX) {
            if (firstXValue) {
                lastMouseX = currentX;
                firstXValue = false;
            }
            float delta = (float) (lastMouseX - currentX);
            lastMouseX = currentX;
            return delta;
        }

        private float getYDelta(double currentY) {
            if (firstYValue) {
                lastMouseY = currentY;
                firstYValue = false;
            }
            float delta = (float) (lastMouseY - currentY);
            lastMouseY = currentY;
            return delta;
        }
    }
}
