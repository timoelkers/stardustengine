package de.oelkers.stardust.graphs;

import de.oelkers.stardust.entities.Entity;
import de.oelkers.stardust.graphics.assets.models.Model2D;
import de.oelkers.stardust.graphics.assets.models.ModelProvider;
import de.oelkers.stardust.graphics.assets.textures.Texture2D;
import de.oelkers.stardust.graphics.rendering.RenderSystem;
import de.oelkers.stardust.graphics.rendering.ToggleRendering;
import de.oelkers.stardust.math.internal.MathDelegate;
import de.oelkers.stardust.math.transformations.Transform2D;
import de.oelkers.stardust.math.transformations.Transformable2D;
import de.oelkers.stardust.math.vectors.ReadOnlyVector2f;
import de.oelkers.stardust.math.vectors.Vector2f;
import net.jcip.annotations.ThreadSafe;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static de.oelkers.stardust.graphics.rendering.components.RenderTechniques.withTexture;

@ThreadSafe
public final class LineGraph extends ToggleRendering {

    private final RenderSystem renderSystem;
    private final int xSections, ySections;
    private final float xSectionScale, ySectionScale;
    private final float xOrigin, yOrigin;
    private final Texture2D lineTexture;
    private final Model2D lineModel;

    private final List<Entity> lines = new ArrayList<>();
    private final Vector2f previousPoint = new Vector2f();
    private final Vector2f currentPoint = new Vector2f();
    private float currentXScale;

    private LineGraph(Builder builder) {
        super(builder.renderSystem);
        renderSystem = builder.renderSystem;
        xSections = builder.xSections;
        ySections = builder.ySections;
        xOrigin = builder.originX;
        yOrigin = builder.originY;
        lineTexture = builder.lineTexture;
        lineModel = builder.lineModel;
        xSectionScale = builder.scaleX * 2 / (builder.xSections - 1);
        ySectionScale = builder.scaleY * 2 / (builder.ySections - 1);
    }

    public synchronized void addEntry(int ySection) {
        if (ySection < 0 || ySection > ySections - 1) {
            throw new IllegalArgumentException("Line graph entry must be between 0 and " + (ySections - 1) + ", you provided "  + ySection);
        }
        currentPoint.set(xOrigin + currentXScale, yOrigin + ySection * ySectionScale);
        if (currentXScale != 0) {
            // we have to synchronize here to prevent race conditions where lines are rendered while
            // transformations are being applied to some of them, leading to incorrect results
            synchronized (renderSystem) {
                if (lines.size() == xSections - 1) {
                    shiftTransformationsBack();
                    transformCurrentLine(lines.get(lines.size() - 1), currentPoint, previousPoint);
                } else {
                    transformCurrentLine(getNewLine(), currentPoint, previousPoint);
                }
            }
        }
        if (lines.size() == xSections - 1) {
            previousPoint.set(previousPoint.getX(), currentPoint.getY());
        } else {
            currentXScale += xSectionScale;
            previousPoint.set(currentPoint);
        }
    }

    @Override
    protected Collection<Entity> getEntities() {
        return lines;
    }

    private Entity getNewLine() {
        Entity line = new Entity("lineGraphEntry");
        withTexture(lineModel, lineTexture).build(line);
        line.addComponent(new Transform2D());
        return addEntity(line);
    }

    private void shiftTransformationsBack() {
        for (int i = 0; i < lines.size() - 1; i++) {
            Transform2D currentTransform = lines.get(i).getComponentOrNull(Transform2D.class);
            Transform2D nextTransform = lines.get(i + 1).getComponentOrNull(Transform2D.class);
            float currentX = currentTransform.getPosition().getX();
            float nextY = nextTransform.getPosition().getY();
            currentTransform.translateTo(currentX, nextY);
            currentTransform.rotateTo(nextTransform.getRotation());
            currentTransform.scaleTo(nextTransform.getScale());
        }
    }

    private static void transformCurrentLine(Entity currentLine, ReadOnlyVector2f point1, ReadOnlyVector2f point2) {
        Transformable2D transform = currentLine.getComponentOrNull(Transformable2D.class);
        Vector2f.POOL.consume(vector -> transform.translateTo(point1.add(point2, vector).scale(0.5f)));
        transform.rotateTo((float) MathDelegate.atan((point2.getY() - point1.getY()) / (point2.getX() - point1.getX())));
        transform.scaleTo((float) (point1.distance(point2) * 0.5), 1);
    }

    public static class Builder {

        private final RenderSystem renderSystem;
        private int xSections = 1, ySections = 1;
        private float originX, originY;
        private float scaleX, scaleY;
        private Texture2D lineTexture;
        private final Model2D lineModel;

        public Builder(RenderSystem renderSystem, ModelProvider modelProvider) {
            this.renderSystem = renderSystem;
            lineModel = modelProvider.getLine();
        }

        public Builder withSections(int xSections, int ySections) {
            if (xSections < 2) {
                throw new IllegalArgumentException("You need at least 2 sections on the x-axis, you provided " + xSections);
            }
            if (ySections < 2) {
                throw new IllegalArgumentException("You need at least 2 sections on the y-axis, you provided " + ySections);
            }
            this.xSections = xSections;
            this.ySections = ySections;
            return this;
        }

        public Builder withOrigin(float originX, float originY) {
            this.originX = originX;
            this.originY = originY;
            return this;
        }

        public Builder withOrigin(ReadOnlyVector2f origin) {
            originX = origin.getX();
            originY = origin.getY();
            return this;
        }

        public Builder withScale(float scaleX, float scaleY) {
            this.scaleX = scaleX;
            this.scaleY = scaleY;
            return this;
        }

        public Builder withScale(ReadOnlyVector2f scale) {
            scaleX = scale.getX();
            scaleY = scale.getY();
            return this;
        }

        public Builder withColor(Color color) {
            lineTexture = new Texture2D(color);
            return this;
        }

        public LineGraph build() {
            if (lineTexture == null) {
                lineTexture = new Texture2D(Color.BLACK);
            }
            return new LineGraph(this);
        }
    }
}
