package de.oelkers.stardust.collision;

import de.oelkers.stardust.entities.BaseEntitySystem;
import de.oelkers.stardust.entities.Entity;
import de.oelkers.stardust.entities.RootEntitySystem;
import de.oelkers.stardust.math.shapes.CollisionComponent3D;
import de.oelkers.stardust.math.shapes.Ray;
import de.oelkers.stardust.math.vectors.ReadOnlyVector3f;
import de.oelkers.stardust.math.vectors.Vector3f;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class CollisionSystem extends BaseEntitySystem<List<Entity>> {

    public CollisionSystem() {
        super(ArrayList::new);
        RootEntitySystem.get().registerSubsystem(this);
    }

    public Optional<Entity> getFirstRayHit(Ray ray) {
        return getFirstRayHit(ray, GLOBAL_GROUP);
    }

    public Optional<Entity> getFirstRayHit(Ray ray, String entityGroup) {
        List<Entity> entities = getEntities(entityGroup);
        entities.sort(new RayComparator(ray));
        for (Entity entity : entities) {
            if (entity.getComponentOrNull(CollisionComponent3D.class).intersects(ray)) {
                return Optional.of(entity);
            }
        }
        return Optional.empty();
    }

    @Override
    public boolean acceptsEntity(Entity entity) {
        return entity.hasComponent(CollisionComponent3D.class);
    }

    private static final class RayComparator implements Comparator<Entity>, Serializable {

        @Serial
        private static final long serialVersionUID = 1L;

        private final Ray ray;

        private RayComparator(Ray ray) {
            this.ray = ray;
        }

        @Override
        public int compare(Entity o1, Entity o2) {
            ReadOnlyVector3f first = projectToAxis(o1.getComponentOrNull(CollisionComponent3D.class).getShapeTransform().getPosition());
            ReadOnlyVector3f second = projectToAxis(o2.getComponentOrNull(CollisionComponent3D.class).getShapeTransform().getPosition());
            return Double.compare(ray.getOrigin().distance(first), ray.getOrigin().distance(second));
        }

        private ReadOnlyVector3f projectToAxis(ReadOnlyVector3f point) {
            Vector3f originToPoint = point.sub(ray.getOrigin(), new Vector3f());
            Vector3f projection = ray.getDirection().scale(originToPoint.dot(ray.getDirection()), originToPoint);
            return projection.add(ray.getOrigin());
        }
    }
}
