package de.oelkers.stardust.config;

import org.lwjgl.opengl.GL43;

import java.util.Arrays;

@FunctionalInterface
public interface StardustErrorCallback {

    enum ErrorType {

        ERROR(GL43.GL_DEBUG_TYPE_ERROR),
        DEPRECATED_BEHAVIOR(GL43.GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR),
        UNDEFINED_BEHAVIOR(GL43.GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR),
        PORTABILITY(GL43.GL_DEBUG_TYPE_PORTABILITY),
        PERFORMANCE(GL43.GL_DEBUG_TYPE_PERFORMANCE),
        MARKER(GL43.GL_DEBUG_TYPE_MARKER),
        UNDEFINED(GL43.GL_DEBUG_TYPE_OTHER);

        private final int glCode;

        ErrorType(int glCode) {
            this.glCode = glCode;
        }

        public static ErrorType getByTypeCode(int typeCode) {
            return Arrays.stream(values()).filter(errorType -> errorType.glCode == typeCode).findAny().orElse(null);
        }
    }

    enum Severity {

        HIGH(GL43.GL_DEBUG_SEVERITY_HIGH),
        MEDIUM(GL43.GL_DEBUG_SEVERITY_MEDIUM),
        LOW(GL43.GL_DEBUG_SEVERITY_LOW),
        NOTIFICATION(GL43.GL_DEBUG_SEVERITY_NOTIFICATION);

        private final int glCode;

        Severity(int glCode) {
            this.glCode = glCode;
        }

        public static Severity getBySeverityCode(int severityCode) {
            return Arrays.stream(values()).filter(severity -> severity.glCode == severityCode).findAny().orElse(null);
        }
    }

    void onError(int errorCode, String description, ErrorType errorType, Severity severity);
}
