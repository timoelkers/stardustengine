package de.oelkers.stardust.config;

import de.oelkers.stardust.utils.timer.DefaultTimeProvider;
import de.oelkers.stardust.utils.timer.TimeProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class ConfigStore {

    private static final Logger LOGGER = LogManager.getLogger(ConfigStore.class);
    private static final ConfigStore INSTANCE = new ConfigStore();

    public static ConfigStore get() {
        return INSTANCE;
    }

    private StardustErrorCallback errorCallback = getDefaultErrorCallback();
    private TimeProvider timeProvider = DefaultTimeProvider.SYSTEM_MILLI_TIMER;
    private LoadingStrategy loadingStrategy = LoadingStrategy.LAZY;
    private boolean useStrictMath = false;
    private boolean useIndividualViewports = false;

    public StardustErrorCallback getErrorCallback() {
        return errorCallback;
    }

    public void setErrorCallback(StardustErrorCallback errorCallback) {
        this.errorCallback = errorCallback;
    }

    public TimeProvider getTimeProvider() {
        return timeProvider;
    }

    /**
     * Assigns the given {@link TimeProvider} as default for timing based tasks across the whole engine.
     * Implementations are allowed to use other providers with a higher frequency if it is necessary to
     * fulfill their tasks. They will never choose on with a lower frequency. The default time provider
     * is {@link DefaultTimeProvider#SYSTEM_MILLI_TIMER}.
     */
    public void setTimeProvider(TimeProvider timeProvider) {
        this.timeProvider = timeProvider;
    }

    public boolean isUsingStrictMath() {
        return useStrictMath;
    }

    public void useStrictMath(boolean useStrictMath) {
        this.useStrictMath = useStrictMath;
    }

    public LoadingStrategy getLoadingStrategy() {
        return loadingStrategy;
    }

    public void setLoadingStrategy(LoadingStrategy loadingStrategy) {
        this.loadingStrategy = loadingStrategy;
    }

    public boolean isUsingIndividualViewports() {
        return useIndividualViewports;
    }

    public void useIndividualViewports(boolean useIndividualViewports) {
        this.useIndividualViewports = useIndividualViewports;
    }

    public StardustErrorCallback getDefaultErrorCallback() {
        return (error, description, errorType, severity) -> {
            StringBuilder errorMessage = new StringBuilder();
            errorMessage.append(description).append(System.lineSeparator());
            errorMessage.append("\tType: ").append(errorType).append(System.lineSeparator());
            errorMessage.append("\tErrorCode: ").append(error).append(System.lineSeparator());
            errorMessage.append("\tSeverity: ").append(severity).append(System.lineSeparator());
            errorMessage.append("\tStackTrace: ").append(System.lineSeparator());
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            for (int i = 4; i < stackTrace.length; i++) {
                errorMessage.append("\t\t").append(stackTrace[i]).append(System.lineSeparator());
            }
            LOGGER.error(errorMessage);
        };
    }
}
