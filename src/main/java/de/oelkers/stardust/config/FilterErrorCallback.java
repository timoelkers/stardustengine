package de.oelkers.stardust.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.regex.Pattern;

public class FilterErrorCallback implements StardustErrorCallback {

    private final StardustErrorCallback delegate;
    private final Collection<Pattern> messageFilters = new ArrayList<>();
    private final Collection<ErrorType> errorTypeFilters = new ArrayList<>();
    private final Collection<Severity> severityFilters = new ArrayList<>();

    public FilterErrorCallback() {
        this(ConfigStore.get().getDefaultErrorCallback());
    }

    public FilterErrorCallback(StardustErrorCallback delegate) {
        this.delegate = delegate;
    }

    public FilterErrorCallback addFilter(Pattern... patterns) {
        Collections.addAll(messageFilters, patterns);
        return this;
    }

    public FilterErrorCallback addFilter(String... patterns) {
        for (String filter : patterns) {
            addFilter(Pattern.compile(filter));
        }
        return this;
    }

    public FilterErrorCallback addFilter(ErrorType... errorTypes) {
        Collections.addAll(errorTypeFilters, errorTypes);
        return this;
    }

    public FilterErrorCallback addFilter(Severity... severities) {
        Collections.addAll(severityFilters, severities);
        return this;
    }

    @Override
    public void onError(int errorCode, String description, ErrorType errorType, Severity severity) {
        if (!isFiltered(description, errorType, severity)) {
            delegate.onError(errorCode, description, errorType, severity);
        }
    }

    private boolean isFiltered(CharSequence message, ErrorType errorType, Severity severity) {
        return messageFilters.stream().anyMatch(messageFilter -> messageFilter.matcher(message).matches()) ||
                errorTypeFilters.stream().anyMatch(errorTypeFilter -> errorTypeFilter == errorType) ||
                severityFilters.stream().anyMatch(severityFilter -> severityFilter == severity);
    }
}
