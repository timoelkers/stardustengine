package de.oelkers.stardust.config;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

public class SpamPreventingErrorCallback implements StardustErrorCallback {

    private static final Pattern ANY_NUMBER = Pattern.compile("0|[1-9]\\d*(?:[.]\\d+)?");
    private final StardustErrorCallback delegate;
    private final Set<String> errorMessages = new HashSet<>();
    private boolean ignoreNumbers;

    public SpamPreventingErrorCallback() {
        this(ConfigStore.get().getDefaultErrorCallback());
    }

    public SpamPreventingErrorCallback(StardustErrorCallback delegate) {
        this.delegate = delegate;
    }

    public SpamPreventingErrorCallback setIgnoreNumbers(boolean ignoreNumbers) {
        this.ignoreNumbers = ignoreNumbers;
        return this;
    }

    @Override
    public void onError(int errorCode, String description, ErrorType errorType, Severity severity) {
        String checkForMatch = description;
        if (ignoreNumbers) {
            checkForMatch = ANY_NUMBER.matcher(description).replaceAll("");
        }
        if (errorMessages.add(checkForMatch)) {
            delegate.onError(errorCode, description, errorType, severity);
        }
    }
}
