package de.oelkers.stardust.config;

public enum LoadingStrategy {

    EAGER,
    LAZY
}
