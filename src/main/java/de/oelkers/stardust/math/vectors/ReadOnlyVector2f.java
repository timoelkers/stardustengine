package de.oelkers.stardust.math.vectors;

import de.oelkers.stardust.utils.ByteSize;

import java.io.Serializable;
import java.nio.ByteBuffer;

/**
 * Represents a read only view over a {@link Vector2f}. All method are guaranteed
 * not to change the state of the vector and prevent you from getting access to any
 * concrete implementation. All implementations are serializable.
 */
public interface ReadOnlyVector2f extends Serializable {

    float getX();
    float getY();
    boolean isZero();
    float getLengthSquared();
    double getLength();
    float dot(float x, float y);
    float dot(ReadOnlyVector2f vector);
    double distance(float x, float y);
    double distance(ReadOnlyVector2f vector);
    Vector2f normalize(Vector2f dest);
    Vector2f add(float x, float y, Vector2f dest);
    Vector2f add(ReadOnlyVector2f vector, Vector2f dest);
    Vector2f sub(float x, float y, Vector2f dest);
    Vector2f sub(ReadOnlyVector2f vector, Vector2f dest);
    Vector2f scale(float scalar, Vector2f dest);
    Vector2f scale(ReadOnlyVector2f vector, Vector2f dest);
    Vector2f lerp(ReadOnlyVector2f end, float amount, Vector2f dest);
    ByteSize getByteSize();
    void store(ByteBuffer buffer, int index);
    void store(ByteBuffer buffer);
}
