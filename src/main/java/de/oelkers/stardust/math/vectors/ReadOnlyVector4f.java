package de.oelkers.stardust.math.vectors;

import de.oelkers.stardust.math.matrices.ReadOnlyMatrix4f;
import de.oelkers.stardust.utils.ByteSize;

import java.io.Serializable;
import java.nio.ByteBuffer;

/**
 * Represents a read only view over a {@link Vector4f}. All method are guaranteed
 * not to change the state of the vector and prevent you from getting access to any
 * concrete implementation. All implementations are serializable.
 */
public interface ReadOnlyVector4f extends Serializable {

    float getX();
    float getY();
    float getZ();
    float getW();
    boolean isZero();
    float getLengthSquared();
    double getLength();
    float dot(float x, float y, float z, float w);
    float dot(ReadOnlyVector4f vector);
    double distance(float x, float y, float z, float w);
    double distance(ReadOnlyVector4f vector);
    Vector4f normalize(Vector4f dest);
    Vector4f add(float x, float y, float z, float w, Vector4f dest);
    Vector4f add(ReadOnlyVector4f vector, Vector4f dest);
    Vector4f sub(float x, float y, float z, float w, Vector4f dest);
    Vector4f sub(ReadOnlyVector4f vector, Vector4f dest);
    Vector4f scale(float scalar, Vector4f dest);
    Vector4f scale(ReadOnlyVector4f vector, Vector4f dest);
    Vector4f lerp(ReadOnlyVector4f end, float amount, Vector4f dest);
    Vector4f transform(ReadOnlyMatrix4f matrix, Vector4f dest);
    Vector3f project(Vector3f dest);
    ByteSize getByteSize();
    void store(ByteBuffer buffer, int index);
    void store(ByteBuffer buffer);
}
