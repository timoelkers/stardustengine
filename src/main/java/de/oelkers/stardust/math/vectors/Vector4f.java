package de.oelkers.stardust.math.vectors;

import de.oelkers.stardust.math.internal.MathDelegate;
import de.oelkers.stardust.math.matrices.ReadOnlyMatrix4f;
import de.oelkers.stardust.utils.ByteSize;
import de.oelkers.stardust.utils.buffers.BufferElement;
import de.oelkers.stardust.utils.objectpools.GrowingObjectPool;
import de.oelkers.stardust.utils.objectpools.ObjectPool;

import java.io.Serial;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.Objects;

/**
 * Represents a 4-dimensional vector of float values. Objects are mutable by design (for performance reasons)
 * and all methods returning {@code Vector4f} will return the destination vector to allow for method chaining.
 * If no destination is given, then the own object will be the target of the operation.
 */
public final class Vector4f implements ReadOnlyVector4f, BufferElement<Vector4f> {

    @Serial
    private static final long serialVersionUID = 1L;

    public static final ObjectPool<Vector4f> POOL = new GrowingObjectPool<>(Vector4f::new);
    public static final ReadOnlyVector4f ONE = new Vector4f(1, 1, 1, 1);
    public static final ReadOnlyVector4f ZERO = new Vector4f(0, 0, 0, 0);
    public static final ByteSize SIZE = ByteSize.ofBytes(4 * Float.BYTES);

    private float x, y, z, w;

    public Vector4f() {}

    public Vector4f(ReadOnlyVector3f xyz, float w) {
        set(xyz.getX(), xyz.getY(), xyz.getZ(), w);
    }

    public Vector4f(float x, float y, float z, float w) {
        set(x, y, z, w);
    }

    public Vector4f(ReadOnlyVector4f src) {
        set(src);
    }

    @Override
    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    @Override
    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    @Override
    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    @Override
    public float getW() {
        return w;
    }

    public void setW(float w) {
        this.w = w;
    }

    public Vector4f set(float x, float y, float z, float w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        return this;
    }

    public Vector4f set(ReadOnlyVector4f src) {
        return set(src.getX(), src.getY(), src.getZ(), src.getW());
    }

    @Override
    public boolean isZero() {
        return x == 0 && y == 0 && z == 0 && w == 0;
    }

    public Vector4f setZero() {
        return set(0, 0, 0, 0);
    }

    public Vector4f negate() {
        return set(-x, -y, -z, -w);
    }

    public Vector4f normalize() {
        return normalize(this);
    }

    @Override
    public Vector4f normalize(Vector4f dest) {
        float length = getLengthSquared();
        if (length == 0f || length == 1f) {
            return dest.set(this);
        }
        return scale(1f / (float) MathDelegate.sqrt(length), dest);
    }

    @Override
    public float getLengthSquared() {
        return dot(this);
    }

    @Override
    public double getLength() {
        return MathDelegate.sqrt(getLengthSquared());
    }

    public Vector4f setLength(float length) {
        float oldLen = (float) getLength();
        return oldLen == 0 || oldLen == length ? this : scale(length / oldLen);
    }

    public Vector4f add(float x, float y, float z, float w) {
        return add(x, y, z, w, this);
    }

    public Vector4f add(ReadOnlyVector4f vector) {
        return add(vector, this);
    }

    @Override
    public Vector4f add(float x, float y, float z, float w, Vector4f dest) {
        return dest.set(this.x + x, this.y + y, this.z + z, this.w + w);
    }

    @Override
    public Vector4f add(ReadOnlyVector4f vector, Vector4f dest) {
        return add(vector.getX(), vector.getY(), vector.getZ(), vector.getW(), dest);
    }

    public Vector4f sub(float x, float y, float z, float w) {
        return sub(x, y, z, w, this);
    }

    public Vector4f sub(ReadOnlyVector4f vector) {
        return sub(vector, this);
    }

    @Override
    public Vector4f sub(float x, float y, float z, float w, Vector4f dest) {
        return dest.set(this.x - x, this.y - y, this.z - z, this.w - w);
    }

    @Override
    public Vector4f sub(ReadOnlyVector4f vector, Vector4f dest) {
        return sub(vector.getX(), vector.getY(), vector.getZ(), vector.getW(), dest);
    }

    public Vector4f scale(float scalar) {
        return scale(scalar, this);
    }

    @Override
    public Vector4f scale(float scalar, Vector4f dest) {
        return dest.set(x * scalar, y * scalar, z * scalar, w * scalar);
    }

    public Vector4f scale(ReadOnlyVector4f vector) {
        return scale(vector, this);
    }

    @Override
    public Vector4f scale(ReadOnlyVector4f vector, Vector4f dest) {
        return dest.set(x * vector.getX(), y * vector.getY(), z * vector.getZ(), w * vector.getW());
    }

    @Override
    public float dot(float x, float y, float z, float w) {
        return this.x * x + this.y * y + this.z * z + this.w * w;
    }

    @Override
    public float dot(ReadOnlyVector4f vector) {
        return dot(vector.getX(), vector.getY(), vector.getZ(), vector.getW());
    }

    @Override
    public double distance(float x, float y, float z, float w) {
        float a = x - this.x;
        float b = y - this.y;
        float c = z - this.z;
        float d = w - this.w;
        return MathDelegate.sqrt(a * a + b * b + c * c + d * d);
    }

    @Override
    public double distance(ReadOnlyVector4f vector) {
        return distance(vector.getX(), vector.getY(), vector.getZ(), vector.getW());
    }

    @Override
    public Vector4f lerp(ReadOnlyVector4f end, float amount, Vector4f dest) {
        dest.x = x + (end.getX() - x) * amount;
        dest.y = y + (end.getY() - y) * amount;
        dest.z = z + (end.getZ() - z) * amount;
        dest.w = w + (end.getW() - w) * amount;
        return dest;
    }

    public Vector4f transform(ReadOnlyMatrix4f matrix) {
        return transform(matrix, this);
    }

    @Override
    public Vector4f transform(ReadOnlyMatrix4f matrix, Vector4f dest) {
        float a = matrix.getValue(0, 0) * x + matrix.getValue(1, 0) * y + matrix.getValue(2, 0) * z + matrix.getValue(3, 0) * w;
        float b = matrix.getValue(0, 1) * x + matrix.getValue(1, 1) * y + matrix.getValue(2, 1) * z + matrix.getValue(3, 1) * w;
        float c = matrix.getValue(0, 2) * x + matrix.getValue(1, 2) * y + matrix.getValue(2, 2) * z + matrix.getValue(3, 2) * w;
        float d = matrix.getValue(0, 3) * x + matrix.getValue(1, 3) * y + matrix.getValue(2, 3) * z + matrix.getValue(3, 3) * w;
        return dest.set(a, b, c, d);
    }

    @Override
    public Vector3f project(Vector3f dest) {
        return dest.set(x, y, z).scale(1.0f / w);
    }

    @Override
    public Vector4f load(ByteBuffer buffer) {
        x = buffer.getFloat();
        y = buffer.getFloat();
        z = buffer.getFloat();
        w = buffer.getFloat();
        return this;
    }

    public Vector4f load(FloatBuffer buffer) {
        x = buffer.get();
        y = buffer.get();
        z = buffer.get();
        w = buffer.get();
        return this;
    }

    @Override
    public ByteSize getByteSize() {
        return SIZE;
    }

    @Override
    public void store(ByteBuffer buffer, int index) {
        buffer.putFloat(index, x);
        buffer.putFloat(index + Float.BYTES, y);
        buffer.putFloat(index + Float.BYTES * 2, z);
        buffer.putFloat(index + Float.BYTES * 3, w);
    }

    @Override
    public void store(ByteBuffer buffer) {
        BufferElement.super.store(buffer);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ReadOnlyVector4f)) {
            return false;
        }
        ReadOnlyVector4f other = (ReadOnlyVector4f) obj;
        return getX() == other.getX() && getY() == other.getY() && getZ() == other.getZ() && getW() == other.getW();
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z, w);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '[' + x + ", " + y + ", " + z + ", " + w + ']';
    }
}
