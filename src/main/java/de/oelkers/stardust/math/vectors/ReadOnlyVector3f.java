package de.oelkers.stardust.math.vectors;

import de.oelkers.stardust.math.matrices.ReadOnlyMatrix4f;
import de.oelkers.stardust.utils.ByteSize;

import java.io.Serializable;
import java.nio.ByteBuffer;

/**
 * Represents a read only view over a {@link Vector3f}. All method are guaranteed
 * not to change the state of the vector and prevent you from getting access to any
 * concrete implementation. All implementations are serializable.
 */
public interface ReadOnlyVector3f extends Serializable {

    float getX();
    float getY();
    float getZ();
    boolean isZero();
    float getLengthSquared();
    double getLength();
    float dot(float x, float y, float z);
    float dot(ReadOnlyVector3f vector);
    double distance(float x, float y, float z);
    double distance(ReadOnlyVector3f vector);
    Vector3f normalize(Vector3f dest);
    Vector3f add(float x, float y, float z, Vector3f dest);
    Vector3f add(ReadOnlyVector3f vector, Vector3f dest);
    Vector3f sub(float x, float y, float z, Vector3f dest);
    Vector3f sub(ReadOnlyVector3f vector, Vector3f dest);
    Vector3f cross(float x, float y, float z, Vector3f dest);
    Vector3f cross(ReadOnlyVector3f vector, Vector3f dest);
    Vector3f scale(float scalar, Vector3f dest);
    Vector3f scale(ReadOnlyVector3f vector, Vector3f dest);
    Vector3f lerp(ReadOnlyVector3f end, float amount, Vector3f dest);
    Vector3f transform(ReadOnlyMatrix4f matrix, Vector3f dest);
    ByteSize getByteSize();
    void store(ByteBuffer buffer, int index);
    void store(ByteBuffer buffer);
}
