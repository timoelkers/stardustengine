package de.oelkers.stardust.math.vectors;

import de.oelkers.stardust.math.internal.MathDelegate;
import de.oelkers.stardust.math.matrices.ReadOnlyMatrix4f;
import de.oelkers.stardust.utils.ByteSize;
import de.oelkers.stardust.utils.buffers.BufferElement;
import de.oelkers.stardust.utils.objectpools.GrowingObjectPool;
import de.oelkers.stardust.utils.objectpools.ObjectPool;

import java.io.Serial;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.Objects;

/**
 * Represents a 3-dimensional vector of float values. Objects are mutable by design (for performance reasons)
 * and all methods returning {@code Vector3f} will return the destination vector to allow for method chaining.
 * If no destination is given, then the own object will be the target of the operation.
 */
public final class Vector3f implements ReadOnlyVector3f, BufferElement<Vector3f> {

    @Serial
    private static final long serialVersionUID = 1L;

    public static final ObjectPool<Vector3f> POOL = new GrowingObjectPool<>(Vector3f::new);
    public static final ReadOnlyVector3f UP = new Vector3f(0, 1, 0);
    public static final ReadOnlyVector3f DOWN = new Vector3f(0, -1, 0);
    public static final ReadOnlyVector3f RIGHT = new Vector3f(1, 0, 0);
    public static final ReadOnlyVector3f LEFT = new Vector3f(-1, 0, 0);
    public static final ReadOnlyVector3f FORWARD = new Vector3f(0, 0, 1);
    public static final ReadOnlyVector3f BACK = new Vector3f(0, 0, -1);
    public static final ReadOnlyVector3f ONE = new Vector3f(1, 1, 1);
    public static final ReadOnlyVector3f ZERO = new Vector3f(0, 0, 0);
    public static final ByteSize SIZE = ByteSize.ofBytes(3 * Float.BYTES);

    private float x, y, z;

    public Vector3f() {}

    public Vector3f(ReadOnlyVector2f xy, float z) {
        set(xy.getX(), xy.getY(), z);
    }

    public Vector3f(float x, float y, float z) {
        set(x, y, z);
    }

    public Vector3f(ReadOnlyVector3f src) {
        set(src);
    }

    @Override
    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    @Override
    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    @Override
    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public Vector3f set(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
        return this;
    }

    public Vector3f set(ReadOnlyVector3f src) {
        return set(src.getX(), src.getY(), src.getZ());
    }

    @Override
    public boolean isZero() {
        return x == 0 && y == 0 && z == 0;
    }

    public Vector3f setZero() {
        return set(0, 0, 0);
    }

    public Vector3f negate() {
        return set(-x, -y, -z);
    }

    public Vector3f normalize() {
        return normalize(this);
    }

    @Override
    public Vector3f normalize(Vector3f dest) {
        float length = getLengthSquared();
        if (length == 0f || length == 1f) {
            return dest.set(this);
        }
        return scale(1f / (float) MathDelegate.sqrt(length), dest);
    }

    @Override
    public float getLengthSquared() {
        return dot(this);
    }

    @Override
    public double getLength() {
        return MathDelegate.sqrt(getLengthSquared());
    }

    public Vector3f setLength(float length) {
        float oldLen = (float) getLength();
        return oldLen == 0 || oldLen == length ? this : scale(length / oldLen);
    }

    public Vector3f add(float x, float y, float z) {
        return add(x, y, z, this);
    }

    public Vector3f add(ReadOnlyVector3f vector) {
        return add(vector, this);
    }

    @Override
    public Vector3f add(float x, float y, float z, Vector3f dest) {
        return dest.set(this.x + x, this.y + y, this.z + z);
    }

    @Override
    public Vector3f add(ReadOnlyVector3f vector, Vector3f dest) {
        return add(vector.getX(), vector.getY(), vector.getZ(), dest);
    }

    public Vector3f sub(float x, float y, float z) {
        return sub(x, y, z, this);
    }

    public Vector3f sub(ReadOnlyVector3f vector) {
        return sub(vector, this);
    }

    @Override
    public Vector3f sub(float x, float y, float z, Vector3f dest) {
        return dest.set(this.x - x, this.y - y, this.z - z);
    }

    @Override
    public Vector3f sub(ReadOnlyVector3f vector, Vector3f dest) {
        return sub(vector.getX(), vector.getY(), vector.getZ(), dest);
    }

    public Vector3f cross(float x, float y, float z) {
        return cross(x, y, z, this);
    }

    public Vector3f cross(ReadOnlyVector3f vector) {
        return cross(vector, this);
    }

    @Override
    public Vector3f cross(float x, float y, float z, Vector3f dest) {
        float a = this.y * z - this.z * y;
        float b = this.z * x - this.x * z;
        float c = this.x * y - this.y * x;
        return dest.set(a, b, c);
    }

    @Override
    public Vector3f cross(ReadOnlyVector3f vector, Vector3f dest) {
        return cross(vector.getX(), vector.getY(), vector.getZ(), dest);
    }

    public Vector3f scale(float scalar) {
        return scale(scalar, this);
    }

    @Override
    public Vector3f scale(float scalar, Vector3f dest) {
        return dest.set(x * scalar, y * scalar, z * scalar);
    }

    public Vector3f scale(ReadOnlyVector3f vector) {
        return scale(vector, this);
    }

    @Override
    public Vector3f scale(ReadOnlyVector3f vector, Vector3f dest) {
        return dest.set(x * vector.getX(), y * vector.getY(), z * vector.getZ());
    }

    @Override
    public float dot(float x, float y, float z) {
        return this.x * x + this.y * y + this.z * z;
    }

    @Override
    public float dot(ReadOnlyVector3f vector) {
        return dot(vector.getX(), vector.getY(), vector.getZ());
    }

    @Override
    public double distance(float x, float y, float z) {
        float a = x - this.x;
        float b = y - this.y;
        float c = z - this.z;
        return MathDelegate.sqrt(a * a + b * b + c * c);
    }

    @Override
    public double distance(ReadOnlyVector3f vector) {
        return distance(vector.getX(), vector.getY(), vector.getZ());
    }

    @Override
    public Vector3f lerp(ReadOnlyVector3f end, float amount, Vector3f dest) {
        dest.x = x + (end.getX() - x) * amount;
        dest.y = y + (end.getY() - y) * amount;
        dest.z = z + (end.getZ() - z) * amount;
        return dest;
    }

    public Vector3f transform(ReadOnlyMatrix4f matrix) {
        return transform(matrix, this);
    }

    @Override
    public Vector3f transform(ReadOnlyMatrix4f matrix, Vector3f dest) {
        float a = matrix.getValue(0, 0) * x + matrix.getValue(1, 0) * y + matrix.getValue(2, 0) * z + matrix.getValue(3, 0);
        float b = matrix.getValue(0, 1) * x + matrix.getValue(1, 1) * y + matrix.getValue(2, 1) * z + matrix.getValue(3, 1);
        float c = matrix.getValue(0, 2) * x + matrix.getValue(1, 2) * y + matrix.getValue(2, 2) * z + matrix.getValue(3, 2);
        return dest.set(a, b, c);
    }

    @Override
    public Vector3f load(ByteBuffer buffer) {
        x = buffer.getFloat();
        y = buffer.getFloat();
        z = buffer.getFloat();
        return this;
    }

    public Vector3f load(FloatBuffer buffer) {
        x = buffer.get();
        y = buffer.get();
        z = buffer.get();
        return this;
    }

    @Override
    public ByteSize getByteSize() {
        return SIZE;
    }

    @Override
    public void store(ByteBuffer buffer, int index) {
        buffer.putFloat(index, x);
        buffer.putFloat(index + Float.BYTES, y);
        buffer.putFloat(index + Float.BYTES * 2, z);
    }

    @Override
    public void store(ByteBuffer buffer) {
        BufferElement.super.store(buffer);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ReadOnlyVector3f)) {
            return false;
        }
        ReadOnlyVector3f other = (ReadOnlyVector3f) obj;
        return getX() == other.getX() && getY() == other.getY() && getZ() == other.getZ();
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '[' + x + ", " + y + ", " + z + ']';
    }
}
