package de.oelkers.stardust.math.vectors;

import de.oelkers.stardust.math.internal.MathDelegate;
import de.oelkers.stardust.utils.ByteSize;
import de.oelkers.stardust.utils.buffers.BufferElement;
import de.oelkers.stardust.utils.objectpools.GrowingObjectPool;
import de.oelkers.stardust.utils.objectpools.ObjectPool;

import java.io.Serial;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.Objects;

/**
 * Represents a 2-dimensional vector of float values. Objects are mutable by design (for performance reasons)
 * and all methods returning {@code Vector2f} will return the destination vector to allow for method chaining.
 * If no destination is given, then the own object will be the target of the operation.
 */
public final class Vector2f implements ReadOnlyVector2f, BufferElement<Vector2f> {

    @Serial
    private static final long serialVersionUID = 1L;

    public static final ObjectPool<Vector2f> POOL = new GrowingObjectPool<>(Vector2f::new);
    public static final ReadOnlyVector2f UP = new Vector2f(0, 1);
    public static final ReadOnlyVector2f DOWN = new Vector2f(0, -1);
    public static final ReadOnlyVector2f RIGHT = new Vector2f(1, 0);
    public static final ReadOnlyVector2f LEFT = new Vector2f(-1, 0);
    public static final ReadOnlyVector2f ONE = new Vector2f(1, 1);
    public static final ReadOnlyVector2f ZERO = new Vector2f(0, 0);
    public static final ByteSize SIZE = ByteSize.ofBytes(2 * Float.BYTES);

    private float x, y;

    public Vector2f() {}

    public Vector2f(float x, float y) {
        set(x, y);
    }

    public Vector2f(ReadOnlyVector2f src) {
        set(src);
    }

    public void setX(float x) {
        this.x = x;
    }

    @Override
    public float getX() {
        return x;
    }

    public void setY(float y) {
        this.y = y;
    }

    @Override
    public float getY() {
        return y;
    }

    public Vector2f set(float x, float y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public Vector2f set(ReadOnlyVector2f src) {
        return set(src.getX(), src.getY());
    }

    @Override
    public boolean isZero() {
        return x == 0 && y == 0;
    }

    public Vector2f setZero() {
        return set(0, 0);
    }

    public Vector2f negate() {
        return set(-x, -y);
    }

    public Vector2f normalize() {
        return normalize(this);
    }

    @Override
    public Vector2f normalize(Vector2f dest) {
        float length = getLengthSquared();
        if (length == 0f || length == 1f) {
            return dest.set(this);
        }
        return scale(1f / (float) MathDelegate.sqrt(length), dest);
    }
    
    @Override
    public float getLengthSquared() {
        return dot(this);
    }

    @Override
    public double getLength() {
        return MathDelegate.sqrt(getLengthSquared());
    }

    public Vector2f setLength(float length) {
        float oldLen = (float) getLength();
        return oldLen == 0 || oldLen == length ? this : scale(length / oldLen);
    }

    public Vector2f add(float x, float y) {
        return add(x, y, this);
    }

    public Vector2f add(ReadOnlyVector2f vector) {
        return add(vector, this);
    }

    @Override
    public Vector2f add(float x, float y, Vector2f dest) {
        return dest.set(this.x + x, this.y + y);
    }

    @Override
    public Vector2f add(ReadOnlyVector2f vector, Vector2f dest) {
        return add(vector.getX(), vector.getY(), dest);
    }

    public Vector2f sub(float x, float y) {
        return sub(x, y, this);
    }

    public Vector2f sub(ReadOnlyVector2f vector) {
        return sub(vector, this);
    }

    @Override
    public Vector2f sub(float x, float y, Vector2f dest) {
        return dest.set(this.x - x, this.y - y);
    }

    @Override
    public Vector2f sub(ReadOnlyVector2f vector, Vector2f dest) {
        return sub(vector.getX(), vector.getY(), dest);
    }

    public Vector2f scale(float scalar) {
        return scale(scalar, this);
    }

    @Override
    public Vector2f scale(float scalar, Vector2f dest) {
        return dest.set(x * scalar, y * scalar);
    }

    public Vector2f scale(ReadOnlyVector2f vector) {
        return scale(vector, this);
    }

    @Override
    public Vector2f scale(ReadOnlyVector2f vector, Vector2f dest) {
        return dest.set(x * vector.getX(), y * vector.getY());
    }

    @Override
    public float dot(float x, float y) {
        return this.x * x + this.y * y;
    }

    @Override
    public float dot(ReadOnlyVector2f vector) {
        return dot(vector.getX(), vector.getY());
    }

    @Override
    public double distance(float x, float y) {
        float a = x - this.x;
        float b = y - this.y;
        return MathDelegate.sqrt(a * a + b * b);
    }

    @Override
    public double distance(ReadOnlyVector2f vector) {
        return distance(vector.getX(), vector.getY());
    }

    @Override
    public Vector2f lerp(ReadOnlyVector2f end, float amount, Vector2f dest) {
        dest.x = x + (end.getX() - x) * amount;
        dest.y = y + (end.getY() - y) * amount;
        return dest;
    }

    @Override
    public Vector2f load(ByteBuffer buffer) {
        x = buffer.getFloat();
        y = buffer.getFloat();
        return this;
    }

    public Vector2f load(FloatBuffer buffer) {
        x = buffer.get();
        y = buffer.get();
        return this;
    }

    @Override
    public ByteSize getByteSize() {
        return SIZE;
    }

    @Override
    public void store(ByteBuffer buffer, int index) {
        buffer.putFloat(index, x);
        buffer.putFloat(index + Float.BYTES, y);
    }

    @Override
    public void store(ByteBuffer buffer) {
        BufferElement.super.store(buffer);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ReadOnlyVector2f)) {
            return false;
        }
        ReadOnlyVector2f other = (ReadOnlyVector2f) obj;
        return getX() == other.getX() && getY() == other.getY();
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '[' + x + ", " + y + ']';
    }
}
