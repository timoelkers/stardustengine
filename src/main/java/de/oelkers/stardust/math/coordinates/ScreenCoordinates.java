package de.oelkers.stardust.math.coordinates;

import de.oelkers.stardust.math.vectors.ReadOnlyVector2f;
import de.oelkers.stardust.math.vectors.Vector2f;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

public final class ScreenCoordinates implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;
    public static final ScreenCoordinates ZERO_ABSOLUTE = absolute(0, 0);
    public static final ScreenCoordinates ZERO_RELATIVE = relative(0, 0);

    private final ScreenCoordinateX xPos;
    private final ScreenCoordinateY yPos;

    private ScreenCoordinates(ScreenCoordinateX xPos, ScreenCoordinateY yPos) {
        this.xPos = xPos;
        this.yPos = yPos;
    }

    public static ScreenCoordinates from(ScreenCoordinateX xPos, ScreenCoordinateY yPos) {
        return new ScreenCoordinates(xPos, yPos);
    }

    /**
     * Describes screen coordinates using absolute pixel positions from the top left corner (also known
     * as screen space). There are no restrictions on the values, but only those that fit the screen are rendered.
     *
     * @param xPos the pixel position from the left
     * @param yPos the pixel position from the top
     */
    public static ScreenCoordinates absolute(float xPos, float yPos) {
        return from(ScreenCoordinateX.absolute(xPos), ScreenCoordinateY.absolute(yPos));
    }

    /**
     * Describes screen coordinates using absolute pixel positions from the top left corner (also known
     * as screen space). There are no restrictions on the values, but only those that fit the screen are visible.
     *
     * @param position the pixel position from the top left corner
     */
    public static ScreenCoordinates absolute(ReadOnlyVector2f position) {
        return absolute(position.getX(), position.getY());
    }

    /**
     * Describes screen coordinates relative to the center of the screen with positive x values going to the right
     * and positive y values going to the top (also known as clip space). There are no restrictions on the values,
     * but only those between -1 and 1 are visible.
     *
     * @param xPos the relative position from the center to the right of the screen
     * @param yPos the relative position from the center to the top of the screen
     */
    public static ScreenCoordinates relative(float xPos, float yPos) {
        return from(ScreenCoordinateX.relative(xPos), ScreenCoordinateY.relative(yPos));
    }

    /**
     * Describes screen coordinates relative to the center of the screen with positive x values going to the right
     * and positive y values going to the top (also known as clip space). There are no restrictions on the values,
     * but only those between -1 and 1 are visible.
     *
     * @param position the relative position from the center
     */
    public static ScreenCoordinates relative(ReadOnlyVector2f position) {
        return relative(position.getX(), position.getY());
    }

    public ScreenCoordinateX getX() {
        return xPos;
    }

    public ScreenCoordinateY getY() {
        return yPos;
    }

    public Vector2f asScreenSpaceCoordinates(ScreenClipSpaceConverter converter) {
        return new Vector2f(xPos.asScreenSpaceCoordinate(converter), yPos.asScreenSpaceCoordinate(converter));
    }

    public Vector2f asClipSpaceCoordinates(ScreenClipSpaceConverter converter) {
        return new Vector2f(xPos.asClipSpaceCoordinate(converter), yPos.asClipSpaceCoordinate(converter));
    }

    public Vector2f asClipSpaceScale(ScreenClipSpaceConverter converter) {
        return new Vector2f(xPos.asClipSpaceScale(converter), yPos.asClipSpaceScale(converter));
    }

    public Vector2f asClipSpaceTranslation(ScreenClipSpaceConverter converter, ReadOnlyVector2f clipSpaceScale) {
        return new Vector2f(xPos.asClipSpaceTranslation(converter, clipSpaceScale.getX()), yPos.asClipSpaceTranslation(converter, clipSpaceScale.getY()));
    }

    public ScreenCoordinates add(ScreenCoordinates other, ScreenClipSpaceConverter converter) {
        return from(xPos.add(other.xPos, converter), yPos.add(other.yPos, converter));
    }

    public ScreenCoordinates sub(ScreenCoordinates other, ScreenClipSpaceConverter converter) {
        return from(xPos.sub(other.xPos, converter), yPos.sub(other.yPos, converter));
    }

    public ScreenCoordinates scale(ScreenCoordinates other, ScreenClipSpaceConverter converter) {
        return from(xPos.scale(other.xPos, converter), yPos.scale(other.yPos, converter));
    }

    public ScreenCoordinates scale(float scalar, ScreenClipSpaceConverter converter) {
        return from(xPos.scale(scalar, converter), yPos.scale(scalar, converter));
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof ScreenCoordinates)) return false;
        ScreenCoordinates that = (ScreenCoordinates) obj;
        return Objects.equals(xPos, that.xPos) && Objects.equals(yPos, that.yPos);
    }

    @Override
    public int hashCode() {
        return Objects.hash(xPos, yPos);
    }

    @Override
    public String toString() {
        return xPos + ", " + yPos;
    }
}
