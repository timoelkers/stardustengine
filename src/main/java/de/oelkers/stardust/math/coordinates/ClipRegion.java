package de.oelkers.stardust.math.coordinates;

import org.lwjgl.opengl.GL11;

import java.io.Serial;
import java.io.Serializable;

public final class ClipRegion implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private final ScreenCoordinateX xPos;
    private final ScreenCoordinateY yPos;
    private final ScreenCoordinateX width;
    private final ScreenCoordinateY height;
    private final boolean scaling;
    private transient int calculatedXPos;
    private transient int calculatedYPos;
    private transient int calculatedWidth;
    private transient int calculatedHeight;
    private transient boolean initialized;

    /**
     * Sets the clip region to a fixed rectangle with the specified coordinates. Note that contrary to the rest of the engine,
     * the yPos specifies the lower left corner of the rectangle.
     */
    public static ClipRegion fixed(ScreenCoordinateX xPos, ScreenCoordinateY yPos, ScreenCoordinateX width, ScreenCoordinateY height) {
        return new ClipRegion(xPos, yPos, width, height, false);
    }

    /**
     * Sets the clip region to a rectangle with the specified coordinates scaling with the attached window.
     * Note that contrary to the rest of the engine, the yPos specifies the lower left corner of the rectangle.
     */
    public static ClipRegion scaling(ScreenCoordinateX xPos, ScreenCoordinateY yPos, ScreenCoordinateX width, ScreenCoordinateY height) {
        return new ClipRegion(xPos, yPos, width, height, true);
    }

    private ClipRegion(ScreenCoordinateX xPos, ScreenCoordinateY yPos, ScreenCoordinateX width, ScreenCoordinateY height, boolean scaling) {
        this.xPos = xPos;
        this.yPos = yPos;
        this.width = width;
        this.height = height;
        this.scaling = scaling;
    }

    public void apply(ScreenClipSpaceConverter converter) {
        if (scaling || !initialized) {
            calculatedXPos = (int) xPos.asScreenSpaceCoordinate(converter);
            calculatedYPos = (int) yPos.asScreenSpaceCoordinate(converter);
            calculatedWidth = (int) width.asScreenSpaceCoordinate(converter);
            calculatedHeight = (int) height.asScreenSpaceCoordinate(converter);
            initialized = true;
        }
        GL11.glScissor(calculatedXPos, calculatedYPos, calculatedWidth, calculatedHeight);
    }
}
