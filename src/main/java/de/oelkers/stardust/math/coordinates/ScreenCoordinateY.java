package de.oelkers.stardust.math.coordinates;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

public final class ScreenCoordinateY implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;
    public static final ScreenCoordinateY ZERO_ABSOLUTE = absolute(0);
    public static final ScreenCoordinateY ZERO_RELATIVE = relative(0);

    private final float yPos;
    private final boolean screenSpace;

    private ScreenCoordinateY(float yPos, boolean screenSpace) {
        this.yPos = yPos;
        this.screenSpace = screenSpace;
    }

    /**
     * Describes a screen coordinate using absolute pixel positions from the top (also known as screen space).
     * There are no restrictions on the value, but only one that fits the screen is rendered.
     *
     * @param yPos the pixel position from the top
     */
    public static ScreenCoordinateY absolute(float yPos) {
        return new ScreenCoordinateY(yPos, true);
    }

    /**
     * Describes a screen coordinate relative to the center of the screen with positive values going to the top
     * (also known as clip space). There are no restrictions on the value, but only one between -1 and 1 is visible.
     *
     * @param yPos the relative position from the center to the top of the screen
     */
    public static ScreenCoordinateY relative(float yPos) {
        return new ScreenCoordinateY(yPos, false);
    }

    public float asScreenSpaceCoordinate(ScreenClipSpaceConverter converter) {
        return screenSpace ? yPos : converter.toScreenSpaceY(yPos);
    }

    public float asClipSpaceCoordinate(ScreenClipSpaceConverter converter) {
        return screenSpace ? converter.toClipSpaceY(yPos) : yPos;
    }

    public float asClipSpaceScale(ScreenClipSpaceConverter converter) {
        return (1 - asClipSpaceCoordinate(converter)) / 2;
    }

    public float asClipSpaceTranslation(ScreenClipSpaceConverter converter, float clipSpaceScale) {
        return asClipSpaceCoordinate(converter) - clipSpaceScale;
    }

    public ScreenCoordinateY add(ScreenCoordinateY other, ScreenClipSpaceConverter converter) {
        return absolute(asScreenSpaceCoordinate(converter) + other.asScreenSpaceCoordinate(converter));
    }

    public ScreenCoordinateY sub(ScreenCoordinateY other, ScreenClipSpaceConverter converter) {
        return absolute(asScreenSpaceCoordinate(converter) - other.asScreenSpaceCoordinate(converter));
    }

    public ScreenCoordinateY scale(ScreenCoordinateY other, ScreenClipSpaceConverter converter) {
        return absolute(asScreenSpaceCoordinate(converter) * other.asScreenSpaceCoordinate(converter));
    }

    public ScreenCoordinateY scale(float scalar, ScreenClipSpaceConverter converter) {
        return absolute(asScreenSpaceCoordinate(converter) * scalar);
    }

    public boolean isGreater(ScreenCoordinateY other, ScreenClipSpaceConverter converter) {
        return screenSpace ? yPos > other.asScreenSpaceCoordinate(converter) : yPos < other.asClipSpaceCoordinate(converter);
    }

    public boolean isGreaterOrEqual(ScreenCoordinateY other, ScreenClipSpaceConverter converter) {
        return screenSpace ? yPos >= other.asScreenSpaceCoordinate(converter) : yPos <= other.asClipSpaceCoordinate(converter);
    }

    public boolean isLess(ScreenCoordinateY other, ScreenClipSpaceConverter converter) {
        return screenSpace ? yPos < other.asScreenSpaceCoordinate(converter) : yPos > other.asClipSpaceCoordinate(converter);
    }

    public boolean isLessOrEqual(ScreenCoordinateY other, ScreenClipSpaceConverter converter) {
        return screenSpace ? yPos <= other.asScreenSpaceCoordinate(converter) : yPos >= other.asClipSpaceCoordinate(converter);
    }

    public boolean isEqual(ScreenCoordinateY other, ScreenClipSpaceConverter converter) {
        return yPos == inOurSpace(other, converter);
    }

    public boolean isZero() {
        return yPos == 0;
    }

    public boolean isPositive() {
        return screenSpace ? yPos > 0 : yPos < 0;
    }

    public boolean isNegative() {
        return screenSpace ? yPos < 0 : yPos > 0;
    }

    private float inOurSpace(ScreenCoordinateY other, ScreenClipSpaceConverter converter) {
        return screenSpace ? other.asScreenSpaceCoordinate(converter) : other.asClipSpaceCoordinate(converter);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof ScreenCoordinateY)) return false;
        ScreenCoordinateY that = (ScreenCoordinateY) obj;
        return Float.compare(that.yPos, yPos) == 0 && screenSpace == that.screenSpace;
    }

    @Override
    public int hashCode() {
        return Objects.hash(yPos, screenSpace);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '[' + screenSpace + ", " + yPos + ']';
    }
}
