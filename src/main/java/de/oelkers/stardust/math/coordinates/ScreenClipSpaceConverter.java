package de.oelkers.stardust.math.coordinates;

import de.oelkers.stardust.math.vectors.ReadOnlyVector2f;
import de.oelkers.stardust.math.vectors.Vector2f;

public interface ScreenClipSpaceConverter {

    /**
     * Performs a viewport transformation and converts the given clip space
     * x coordinate into a screen space coordinate. See {@link ScreenCoordinateX}
     * on details and the orientation of these coordinate systems.
     *
     * @param xPos the x coordinate in clip space
     */
    float toScreenSpaceX(float xPos);

    /**
     * Performs a viewport transformation and converts the given clip space
     * y coordinate into a screen space coordinate. See {@link ScreenCoordinateY}
     * on details and the orientation of these coordinate systems.
     *
     * @param yPos the y coordinate in clip space
     */
    float toScreenSpaceY(float yPos);

    /**
     * Performs a viewport transformation and converts the given clip space
     * coordinates into screen space coordinates. See {@link ScreenCoordinates}
     * on details and the orientation of these coordinate systems.
     *
     * @param xPos the x coordinate in clip space
     * @param yPos the y coordinate in clip space
     * @return a new vector containing the screen space coordinates
     */
    default Vector2f toScreenSpace(float xPos, float yPos) {
        return new Vector2f(toScreenSpaceX(xPos), toScreenSpaceY(yPos));
    }

    /**
     * Performs a viewport transformation and converts the given clip space
     * coordinates into screen space coordinates. See {@link ScreenCoordinates}
     * on details and the orientation of these coordinate systems.
     *
     * @param clipCoordinates the coordinates in clip space
     * @return a new vector containing the screen space coordinates
     */
    default Vector2f toScreenSpace(ReadOnlyVector2f clipCoordinates) {
        return toScreenSpace(clipCoordinates.getX(), clipCoordinates.getY());
    }

    /**
     * Performs a viewport transformation and converts the given screen space
     * x coordinate into a clip space coordinate. See {@link ScreenCoordinateX}
     * on details and the orientation of these coordinate systems.
     *
     * @param xPos the x coordinate in screen space
     */
    float toClipSpaceX(float xPos);

    /**
     * Performs a viewport transformation and converts the given screen space
     * y coordinate into a clip space coordinate. See {@link ScreenCoordinateY}
     * on details and the orientation of these coordinate systems.
     *
     * @param yPos the Y coordinate in screen space
     */
    float toClipSpaceY(float yPos);

    /**
     * Performs a viewport transformation and converts the given screen space
     * coordinates into clip space coordinates. See {@link ScreenCoordinates}
     * on details and the orientation of these coordinate systems.
     *
     * @param xPos the x coordinate in screen space
     * @param yPos the y coordinate in screen space
     * @return a new vector containing the clip space coordinates
     */
    default Vector2f toClipSpace(float xPos, float yPos) {
        return new Vector2f(toClipSpaceX(xPos), toClipSpaceY(yPos));
    }

    /**
     * Performs a viewport transformation and converts the given screen space
     * coordinates into clip space coordinates. See {@link ScreenCoordinates}
     * on details and the orientation of these coordinate systems.
     *
     * @param screenCoordinates the coordinates in screen space
     * @return a new vector containing the clip space coordinates
     */
    default Vector2f toClipSpace(ReadOnlyVector2f screenCoordinates) {
        return toClipSpace(screenCoordinates.getX(), screenCoordinates.getY());
    }
}
