package de.oelkers.stardust.math.coordinates;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

public final class ScreenCoordinateX implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;
    public static final ScreenCoordinateX ZERO_ABSOLUTE = absolute(0);
    public static final ScreenCoordinateX ZERO_RELATIVE = relative(0);

    private final float xPos;
    private final boolean screenSpace;

    private ScreenCoordinateX(float xPos, boolean screenSpace) {
        this.xPos = xPos;
        this.screenSpace = screenSpace;
    }

    /**
     * Describes a screen coordinate using absolute pixel positions from the left (also known as screen space).
     * There are no restrictions on the value, but only one that fits the screen is rendered.
     *
     * @param xPos the pixel position from the left
     */
    public static ScreenCoordinateX absolute(float xPos) {
        return new ScreenCoordinateX(xPos, true);
    }

    /**
     * Describes a screen coordinate relative to the center of the screen with positive values going to the right
     * (also known as clip space). There are no restrictions on the value, but only one between -1 and 1 is visible.
     *
     * @param xPos the relative position from the center to the right of the screen
     */
    public static ScreenCoordinateX relative(float xPos) {
        return new ScreenCoordinateX(xPos, false);
    }

    public float asScreenSpaceCoordinate(ScreenClipSpaceConverter converter) {
        return screenSpace ? xPos : converter.toScreenSpaceX(xPos);
    }

    public float asClipSpaceCoordinate(ScreenClipSpaceConverter converter) {
        return screenSpace ? converter.toClipSpaceX(xPos) : xPos;
    }

    public float asClipSpaceScale(ScreenClipSpaceConverter converter) {
        return (1 + asClipSpaceCoordinate(converter)) / 2;
    }

    public float asClipSpaceTranslation(ScreenClipSpaceConverter converter, float clipSpaceScale) {
        return asClipSpaceCoordinate(converter) + clipSpaceScale;
    }

    public ScreenCoordinateX add(ScreenCoordinateX other, ScreenClipSpaceConverter converter) {
        return absolute(asScreenSpaceCoordinate(converter) + other.asScreenSpaceCoordinate(converter));
    }

    public ScreenCoordinateX sub(ScreenCoordinateX other, ScreenClipSpaceConverter converter) {
        return absolute(asScreenSpaceCoordinate(converter) - other.asScreenSpaceCoordinate(converter));
    }

    public ScreenCoordinateX scale(ScreenCoordinateX other, ScreenClipSpaceConverter converter) {
        return absolute(asScreenSpaceCoordinate(converter) * other.asScreenSpaceCoordinate(converter));
    }

    public ScreenCoordinateX scale(float scalar, ScreenClipSpaceConverter converter) {
        return absolute(asScreenSpaceCoordinate(converter) * scalar);
    }

    public boolean isGreater(ScreenCoordinateX other, ScreenClipSpaceConverter converter) {
        return xPos > inOurSpace(other, converter);
    }

    public boolean isGreaterOrEqual(ScreenCoordinateX other, ScreenClipSpaceConverter converter) {
        return xPos >= inOurSpace(other, converter);
    }

    public boolean isLess(ScreenCoordinateX other, ScreenClipSpaceConverter converter) {
        return xPos < inOurSpace(other, converter);
    }

    public boolean isLessOrEqual(ScreenCoordinateX other, ScreenClipSpaceConverter converter) {
        return xPos <= inOurSpace(other, converter);
    }

    public boolean isEqual(ScreenCoordinateX other, ScreenClipSpaceConverter converter) {
        return xPos == inOurSpace(other, converter);
    }

    public boolean isZero() {
        return xPos == 0;
    }

    public boolean isPositive() {
        return xPos > 0;
    }

    public boolean isNegative() {
        return xPos < 0;
    }

    private float inOurSpace(ScreenCoordinateX other, ScreenClipSpaceConverter converter) {
        return screenSpace ? other.asScreenSpaceCoordinate(converter) : other.asClipSpaceCoordinate(converter);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof ScreenCoordinateX)) return false;
        ScreenCoordinateX that = (ScreenCoordinateX) obj;
        return Float.compare(that.xPos, xPos) == 0 && screenSpace == that.screenSpace;
    }

    @Override
    public int hashCode() {
        return Objects.hash(xPos, screenSpace);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '[' + screenSpace + ", " + xPos + ']';
    }
}
