package de.oelkers.stardust.math.quaternions;

import de.oelkers.stardust.math.matrices.Matrix4f;
import de.oelkers.stardust.math.vectors.Vector3f;
import de.oelkers.stardust.utils.ByteSize;

import java.io.Serializable;
import java.nio.ByteBuffer;

public interface ReadOnlyQuaternion extends Serializable {

    float getX();
    float getY();
    float getZ();
    float getW();
    double getPitchRad();
    double getPitchDeg();
    double getYawRad();
    double getYawDeg();
    double getRollRad();
    double getRollDeg();
    double getAxisAngleRad(Vector3f dest);
    double getAxisAngleDeg(Vector3f dest);
    double getAngleRad();
    double getAngleDeg();
    float getLengthSquared();
    double getLength();
    float dot(float x, float y, float z, float w);
    float dot(ReadOnlyQuaternion quaternion);
    void getRotationMatrix(Matrix4f dest);
    Quaternion add(float x, float y, float z, float w, Quaternion dest);
    Quaternion add(ReadOnlyQuaternion quaternion, Quaternion dest);
    Quaternion sub(float x, float y, float z, float w, Quaternion dest);
    Quaternion sub(ReadOnlyQuaternion quaternion, Quaternion dest);
    Quaternion scale(float scalar, Quaternion dest);
    Quaternion scale(ReadOnlyQuaternion quaternion, Quaternion dest);
    Quaternion lerp(ReadOnlyQuaternion end, float amount, Quaternion dest);
    Quaternion slerp(ReadOnlyQuaternion quaternion, float amount, Quaternion dest);
    ByteSize getByteSize();
    void store(ByteBuffer buffer, int index);
    void store(ByteBuffer buffer);
}
