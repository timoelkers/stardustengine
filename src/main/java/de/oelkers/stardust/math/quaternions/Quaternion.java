package de.oelkers.stardust.math.quaternions;

import de.oelkers.stardust.math.internal.MathDelegate;
import de.oelkers.stardust.math.matrices.Matrix4f;
import de.oelkers.stardust.math.matrices.ReadOnlyMatrix4f;
import de.oelkers.stardust.math.vectors.ReadOnlyVector3f;
import de.oelkers.stardust.math.vectors.Vector3f;
import de.oelkers.stardust.utils.ByteSize;
import de.oelkers.stardust.utils.buffers.BufferElement;
import de.oelkers.stardust.utils.objectpools.GrowingObjectPool;
import de.oelkers.stardust.utils.objectpools.ObjectPool;

import java.io.Serial;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.Objects;

public final class Quaternion implements ReadOnlyQuaternion, BufferElement<Quaternion> {

    @Serial
    private static final long serialVersionUID = 1L;

    public static final ObjectPool<Quaternion> POOL = new GrowingObjectPool<>(Quaternion::new);
    public static final Quaternion IDENTITY = new Quaternion();
    public static final ByteSize SIZE = ByteSize.ofBytes(4 * Float.BYTES);

    private float x, y, z, w;

    public Quaternion() {
        setIdentity();
    }

    public Quaternion(ReadOnlyQuaternion src) {
        set(src);
    }

    public Quaternion(float x, float y, float z, float w) {
        set(x, y, z, w);
    }

    public Quaternion(ReadOnlyVector3f axis, float angle) {
        setFromAxisAngle(axis, angle);
    }

    public Quaternion(float pitch, float yaw, float roll) {
        setFromEulerRad(pitch, yaw, roll);
    }

    public Quaternion(ReadOnlyMatrix4f matrix) {
        setFromMatrix(matrix);
    }

    @Override
    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    @Override
    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    @Override
    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    @Override
    public float getW() {
        return w;
    }

    public void setW(float w) {
        this.w = w;
    }

    public Quaternion set(float x, float y, float z, float w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        return this;
    }

    public Quaternion set(ReadOnlyQuaternion quaternion) {
        return set(quaternion.getX(), quaternion.getY(), quaternion.getZ(), quaternion.getW());
    }

    public Quaternion setIdentity() {
        return set(0, 0, 0, 1);
    }

    public Quaternion setFromAxisAngle(ReadOnlyVector3f axis, float angle) {
        float halfAngle = angle * 0.5f;
        float sinAngle = (float) MathDelegate.sin(halfAngle);
        x = axis.getX() * sinAngle;
        y = axis.getY() * sinAngle;
        z = axis.getZ() * sinAngle;
        w = (float) MathDelegate.cos(halfAngle);
        return this;
    }

    /**
     * Sets this quaternion based on the given pitch, yaw and roll in radians. It will basically create
     * a quaternion for each rotation and multiply them together to form the final orientation. This
     * is actually faster than the described method.
     *
     * @param pitch the pitch (euler rotation around the x-axis) of the rotation in radians
     * @param yaw   the yaw (euler rotation around the y-axis) of the rotation in radians
     * @param roll  the roll (euler rotation around the z-axis) of the rotation in radians
     * @return this quaternion for method chaining
     */
    public Quaternion setFromEulerRad(float pitch, float yaw, float roll) {
        float halfPitch = pitch * 0.5f;
        float sinPitch = (float) MathDelegate.sin(halfPitch);
        float cosPitch = (float) MathDelegate.cos(halfPitch);
        float halfYaw = yaw * 0.5f;
        float sinYaw = (float) MathDelegate.sin(halfYaw);
        float cosYaw = (float) MathDelegate.cos(halfYaw);
        float halfRoll = roll * 0.5f;
        float sinRoll = (float) MathDelegate.sin(halfRoll);
        float cosRoll = (float) MathDelegate.cos(halfRoll);
        x = cosYaw * sinPitch * cosRoll + sinYaw * cosPitch * sinRoll;
        y = sinYaw * cosPitch * cosRoll - cosYaw * sinPitch * sinRoll;
        z = cosYaw * cosPitch * sinRoll - sinYaw * sinPitch * cosRoll;
        w = cosYaw * cosPitch * cosRoll + sinYaw * sinPitch * sinRoll;
        return this;
    }

    /**
     * Sets this quaternion based on the given pitch, yaw and roll in degrees. It will basically create
     * a quaternion for each rotation and multiply them together to form the final orientation. This
     * is actually faster than the described method.
     *
     * @param pitch the pitch (euler rotation around the x-axis) of the rotation in degrees
     * @param yaw   the yaw (euler rotation around the y-axis) of the rotation in degrees
     * @param roll  the roll (euler rotation around the z-axis) of the rotation in degrees
     * @return this quaternion for method chaining
     */
    public Quaternion setFromEulerDeg(float pitch, float yaw, float roll) {
        return setFromEulerRad((float) MathDelegate.toRadians(pitch), (float) MathDelegate.toRadians(yaw), (float) MathDelegate.toRadians(roll));
    }

    public Quaternion setFromMatrix(ReadOnlyMatrix4f matrix) {
        float diagonal = matrix.getValue(0, 0) + matrix.getValue(1, 1) + matrix.getValue(2, 2);
        if (diagonal > 0) {
            float w4 = (float) (MathDelegate.sqrt(diagonal + 1f) * 2f);
            w = w4 / 4f;
            x = (matrix.getValue(2, 1) - matrix.getValue(1, 2)) / w4;
            y = (matrix.getValue(0, 2) - matrix.getValue(2, 0)) / w4;
            z = (matrix.getValue(1, 0) - matrix.getValue(0, 1)) / w4;
        } else if (matrix.getValue(0, 0) > matrix.getValue(1, 1) && matrix.getValue(0, 0) > matrix.getValue(2, 2)) {
            float x4 = (float) (MathDelegate.sqrt(1f + matrix.getValue(0, 0) - matrix.getValue(1, 1) - matrix.getValue(2, 2)) * 2f);
            w = (matrix.getValue(2, 1) - matrix.getValue(1, 2)) / x4;
            x = x4 / 4f;
            y = (matrix.getValue(0, 1) + matrix.getValue(1, 0)) / x4;
            z = (matrix.getValue(0, 2) + matrix.getValue(2, 0)) / x4;
        } else if (matrix.getValue(1, 1) > matrix.getValue(2, 2)) {
            float y4 = (float) (MathDelegate.sqrt(1f + matrix.getValue(1, 1) - matrix.getValue(0, 0) - matrix.getValue(2, 2)) * 2f);
            w = (matrix.getValue(0, 2) - matrix.getValue(2, 0)) / y4;
            x = (matrix.getValue(0, 1) + matrix.getValue(1, 0)) / y4;
            y = y4 / 4f;
            z = (matrix.getValue(1, 2) + matrix.getValue(2, 1)) / y4;
        } else {
            float z4 = (float) (MathDelegate.sqrt(1f + matrix.getValue(2, 2) - matrix.getValue(0, 0) - matrix.getValue(1, 1)) * 2f);
            w = (matrix.getValue(1, 0) - matrix.getValue(0, 1)) / z4;
            x = (matrix.getValue(0, 2) + matrix.getValue(2, 0)) / z4;
            y = (matrix.getValue(1, 2) + matrix.getValue(2, 1)) / z4;
            z = z4 / 4f;
        }
        return this;
    }

    @Override
    public double getPitchRad() {
        float length = getLengthSquared();
        float gimbalPole = x * y + z * w;
        if (gimbalPole > 0.499 * length || gimbalPole < -0.499 * length) {
            return 0;
        }
        return MathDelegate.atan2(2 * x * w - 2 * y * z, -x * x + y * y - z * z + w * w);
    }

    @Override
    public double getPitchDeg() {
        return MathDelegate.toDegrees(getPitchRad());
    }

    @Override
    public double getYawRad() {
        float length = getLengthSquared();
        float gimbalPole = x * y + z * w;
        if (gimbalPole > 0.499 * length) {
            return 2 * MathDelegate.atan2(x, w);
        }
        if (gimbalPole < -0.499 * length) {
            return -2 * MathDelegate.atan2(x, w);
        }
        return MathDelegate.atan2(2 * y * w - 2 * x * z, x * x - y * y - z * z + w * w);
    }

    @Override
    public double getYawDeg() {
        return MathDelegate.toDegrees(getYawRad());
    }

    @Override
    public double getRollRad() {
        float length = getLengthSquared();
        float gimbalPole = x * y + z * w;
        if (gimbalPole > 0.499 * length) {
            return MathDelegate.PI_DOUBLE / 2.0;
        }
        if (gimbalPole < -0.499 * length) {
            return -MathDelegate.PI_DOUBLE / 2.0;
        }
        return MathDelegate.asin(2 * gimbalPole / (double) length);
    }

    @Override
    public double getRollDeg() {
        return MathDelegate.toDegrees(getRollRad());
    }

    /**
     * Returns the angle in radians of the rotation this quaternion is representing. It also sets the
     * given dest vector to the computed rotation axis. This will actually normalize this quaternion if
     * needed and obviously override the given vector.
     *
     * @param dest the vector you want the axis to be set to
     * @return the angle in radians of this rotation
     */
    @Override
    public double getAxisAngleRad(Vector3f dest) {
        if (w > 1) {
            normalize();
        }
        double angle = 2.0 * MathDelegate.acos(w);
        double s = MathDelegate.sqrt(1 - w * w);
        if (s < 0.000001f) {
            dest.set(x, y, z);
        } else {
            dest.set((float) (x / s), (float) (y / s), (float) (z / s));
        }
        return angle;
    }

    /**
     * Returns the angle in degrees of the rotation this quaternion is representing. It also sets the
     * given dest vector to the computed rotation axis. This will actually normalize this quaternion if
     * needed and obviously override the given vector.
     *
     * @param dest the vector you want the axis to be set to
     * @return the angle in degrees of this rotation
     */
    @Override
    public double getAxisAngleDeg(Vector3f dest) {
        return MathDelegate.toDegrees(getAxisAngleRad(dest));
    }

    /**
     * Returns the angle in radians of the the rotation this quaternion is representing. It does not alter
     * or normalize any of this quaternions values. If you want to get the rotation axis as well, you should
     * use {@link #getAxisAngleRad(Vector3f)} instead.
     *
     * @return the angle in radians of this rotation
     */
    @Override
    public double getAngleRad() {
        double w = this.w > 1 ? this.w / getLength() : this.w;
        return 2.0 * MathDelegate.acos(w);
    }

    /**
     * Returns the angle in degrees of the the rotation this quaternion is representing. It does not alter
     * or normalize any of this quaternions values. If you want to get the rotation axis as well, you should
     * use {@link #getAxisAngleDeg(Vector3f)} instead.
     *
     * @return the angle in degrees of this rotation
     */
    @Override
    public double getAngleDeg() {
        return MathDelegate.toDegrees(getAngleRad());
    }

    @Override
    public void getRotationMatrix(Matrix4f dest) {
        dest.setValue(0, 0, 1.0f - 2.0f * (y * y + z * z));
        dest.setValue(0, 1, 2.0f * (x * y - z * w));
        dest.setValue(0, 2, 2.0f * (x * z + y * w));
        dest.setValue(0, 3, 0.0f);
        dest.setValue(1, 0, 2.0f * (x * y + z * w));
        dest.setValue(1, 1, 1.0f - 2.0f * (x * x + z * z));
        dest.setValue(1, 2, 2.0f * (y * z - x * w));
        dest.setValue(1, 3, 0.0f);
        dest.setValue(2, 0, 2.0f * (x * z - y * w));
        dest.setValue(2, 1, 2.0f * (y * z + x * w));
        dest.setValue(2, 2, 1.0f - 2.0f * (x * x + y * y));
        dest.setValue(2, 3, 0.0f);
    }

    public Quaternion negate() {
        return set(-x, -y, -z, -w);
    }

    public Quaternion normalize() {
        return normalize(this);
    }

    public Quaternion normalize(Quaternion dest) {
        float length = getLengthSquared();
        if (length == 0f || length == 1f) {
            return dest.set(this);
        }
        return scale(1f / (float) MathDelegate.sqrt(length), dest);
    }

    @Override
    public float getLengthSquared() {
        return dot(this);
    }

    @Override
    public double getLength() {
        return MathDelegate.sqrt(getLengthSquared());
    }

    public Quaternion setLength(float length) {
        float oldLen = (float) getLength();
        return oldLen == 0 || oldLen == length ? this : scale(length / oldLen);
    }

    public Quaternion add(float x, float y, float z, float w) {
        return add(x, y, z, w, this);
    }

    public Quaternion add(ReadOnlyQuaternion quaternion) {
        return add(quaternion.getX(), quaternion.getY(), quaternion.getZ(), quaternion.getW());
    }

    @Override
    public Quaternion add(float x, float y, float z, float w, Quaternion dest) {
        return dest.set(this.x + x, this.y + y, this.z + z, this.w + w);
    }

    @Override
    public Quaternion add(ReadOnlyQuaternion quaternion, Quaternion dest) {
        return add(quaternion.getX(), quaternion.getY(), quaternion.getZ(), quaternion.getW(), dest);
    }

    public Quaternion sub(float x, float y, float z, float w) {
        return sub(x, y, z, w, this);
    }

    public Quaternion sub(ReadOnlyQuaternion quaternion) {
        return sub(quaternion.getX(), quaternion.getY(), quaternion.getZ(), quaternion.getW());
    }

    @Override
    public Quaternion sub(float x, float y, float z, float w, Quaternion dest) {
        return dest.set(this.x - x, this.y - y, this.z - z, this.w - w);
    }

    @Override
    public Quaternion sub(ReadOnlyQuaternion quaternion, Quaternion dest) {
        return sub(quaternion.getX(), quaternion.getY(), quaternion.getZ(), quaternion.getW(), dest);
    }

    public Quaternion scale(float scalar) {
        return scale(scalar, this);
    }

    @Override
    public Quaternion scale(float scalar, Quaternion dest) {
        return dest.set(x * scalar, y * scalar, z * scalar, w * scalar);
    }

    public Quaternion scale(ReadOnlyQuaternion quaternion) {
        return scale(quaternion, this);
    }

    @Override
    public Quaternion scale(ReadOnlyQuaternion quaternion, Quaternion dest) {
        return dest.set(x * quaternion.getX(), y * quaternion.getY(), z * quaternion.getZ(), w * quaternion.getW());
    }

    @Override
    public float dot(float x, float y, float z, float w) {
        return this.x * x + this.y * y + this.z * z + this.w * w;
    }

    @Override
    public float dot(ReadOnlyQuaternion quaternion) {
        return dot(quaternion.getX(), quaternion.getY(), quaternion.getZ(), quaternion.getW());
    }

    @Override
    public Quaternion lerp(ReadOnlyQuaternion end, float amount, Quaternion dest) {
        float dot = dot(end);
        float amountI = 1.0f - amount;
        if (dot < 0.0f) {
            dest.x = amountI * getX() - amount * end.getX();
            dest.y = amountI * getY() - amount * end.getY();
            dest.z = amountI * getZ() - amount * end.getZ();
            dest.w = amountI * getW() - amount * end.getW();
        } else {
            dest.x = amountI * getX() + amount * end.getX();
            dest.y = amountI * getY() + amount * end.getY();
            dest.z = amountI * getZ() + amount * end.getZ();
            dest.w = amountI * getW() + amount * end.getW();
        }
        return dest;
    }

    @Override
    public Quaternion slerp(ReadOnlyQuaternion quaternion, float amount, Quaternion dest) {
        float dot = dot(quaternion);
        float absDot = dot < 0.0f ? -dot : dot;
        float scale1 = 1.0f - amount;
        float scale2 = amount;
        if (1.0f - absDot > 0.1f) {
            float angle = (float) MathDelegate.acos(absDot);
            float invSinTheta = (float) (1.0 / MathDelegate.sin(angle));
            scale1 = (float) (MathDelegate.sin(scale1 * angle) * invSinTheta);
            scale2 = (float) (MathDelegate.sin(scale2 * angle) * invSinTheta);
        }
        if (dot < 0.0f) {
            scale2 = -scale2;
        }
        float x = scale1 * this.x + scale2 * quaternion.getX();
        float y = scale1 * this.y + scale2 * quaternion.getY();
        float z = scale1 * this.z + scale2 * quaternion.getZ();
        float w = scale1 * this.w + scale2 * quaternion.getW();
        return dest.set(x, y, z, w);
    }

    @Override
    public Quaternion load(ByteBuffer buffer) {
        x = buffer.getFloat();
        y = buffer.getFloat();
        z = buffer.getFloat();
        w = buffer.getFloat();
        return this;
    }

    public Quaternion load(FloatBuffer buffer) {
        x = buffer.get();
        y = buffer.get();
        z = buffer.get();
        w = buffer.get();
        return this;
    }

    @Override
    public ByteSize getByteSize() {
        return SIZE;
    }

    @Override
    public void store(ByteBuffer buffer, int index) {
        buffer.putFloat(index, x);
        buffer.putFloat(index + Float.BYTES, y);
        buffer.putFloat(index + Float.BYTES * 2, z);
        buffer.putFloat(index + Float.BYTES * 3, w);
    }

    @Override
    public void store(ByteBuffer buffer) {
        BufferElement.super.store(buffer);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ReadOnlyQuaternion)) {
            return false;
        }
        ReadOnlyQuaternion other = (ReadOnlyQuaternion) obj;
        return getX() == other.getX() && getY() == other.getY() && getZ() == other.getZ() && getW() == other.getW();
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z, w);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '[' + x + ", " + y + ", " + z + ", " + w + ']';
    }
}
