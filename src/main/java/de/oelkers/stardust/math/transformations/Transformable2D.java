package de.oelkers.stardust.math.transformations;

import de.oelkers.stardust.math.vectors.ReadOnlyVector2f;
import de.oelkers.stardust.math.vectors.Vector2f;

public interface Transformable2D extends Transformable {

    ReadOnlyVector2f getPosition();
    ReadOnlyVector2f getScale();
    float getRotation();
    Transformable2D addChild(Transformable2D child, TransformationType... transformationTypes);
    Transformable2D removeChild(Transformable2D child);
    Transformable2D translate(ReadOnlyVector2f translation, TransformSpace space);
    Transformable2D rotate(float rotation);
    Transformable2D scale(ReadOnlyVector2f scale);

    default Transformable2D addChild(Transformable2D child) {
        return addChild(child, TransformationType.values());
    }

    default Transformable2D translate(float xTranslation, float yTranslation, TransformSpace space) {
        return Vector2f.POOL.apply(vector -> translate(vector.set(xTranslation, yTranslation), space));
    }

    default Transformable2D scale(float scalar) {
        return scale(scalar, scalar);
    }

    default Transformable2D scale(float xScale, float yScale) {
        return Vector2f.POOL.apply(vector -> scale(vector.set(xScale, yScale)));
    }

    default Transformable2D translateTo(float xTranslation, float yTranslation) {
        return translate(xTranslation - getPosition().getX(), yTranslation - getPosition().getY(), TransformSpace.WORLD);
    }

    default Transformable2D translateTo(ReadOnlyVector2f translation) {
        return translateTo(translation.getX(), translation.getY());
    }

    default Transformable2D rotateTo(float rotation) {
        return rotate(rotation - getRotation());
    }

    default Transformable2D scaleTo(float xScale, float yScale) {
        return scale(xScale / getScale().getX(), yScale / getScale().getY());
    }

    default Transformable2D scaleTo(ReadOnlyVector2f scale) {
        return scaleTo(scale.getX(), scale.getY());
    }
}
