package de.oelkers.stardust.math.transformations;

import de.oelkers.stardust.math.internal.MathDelegate;
import de.oelkers.stardust.math.matrices.Matrix4f;
import de.oelkers.stardust.math.matrices.ReadOnlyMatrix4f;
import de.oelkers.stardust.math.vectors.ReadOnlyVector2f;
import de.oelkers.stardust.math.vectors.Vector2f;
import de.oelkers.stardust.math.vectors.Vector3f;

import java.io.Serial;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static de.oelkers.stardust.math.transformations.TransformationType.*;
import static de.oelkers.stardust.utils.collections.CollectionUtils.newEnumSetOf;

/**
 * This class encapsulates object transformations (translations, rotations and scalings)
 * and keeps a reference to the so called model matrix (or transformation matrix).
 * It also keeps track of the current position, rotation and scale and provides you a read-only view over them.
 * Additionally, you are able to create a tree-like structure of transformations, where each child
 * is also affected by transformations of the parent.
 */
public class Transform2D implements Transformable2D {

    @Serial
    private static final long serialVersionUID = 1L;

    private final Matrix4f modelMatrix = new Matrix4f();
    private final Map<Transformable2D, Set<TransformationType>> children = new HashMap<>();
    private final Vector2f position = new Vector2f();
    private final Vector2f scale = new Vector2f(Vector2f.ONE);
    private float rotation;

    @Override
    public ReadOnlyVector2f getPosition() {
        return position;
    }

    @Override
    public ReadOnlyVector2f getScale() {
        return scale;
    }

    @Override
    public float getRotation() {
        return rotation;
    }

    @Override
    public Transformable2D addChild(Transformable2D child, TransformationType... transformationTypes) {
        children.put(child, newEnumSetOf(TransformationType.class, transformationTypes));
        return this;
    }

    @Override
    public Transformable2D removeChild(Transformable2D child) {
        children.remove(child);
        return this;
    }

    @Override
    public Transformable2D translate(ReadOnlyVector2f translation, TransformSpace space) {
        modelMatrix.translate(translation, space);
        modelMatrix.getTranslation(position);
        Set<TransformationType> transformationTypes = getAdditionalTransformationTypes(TRANSLATION);
        for (Map.Entry<Transformable2D, Set<TransformationType>> entry : children.entrySet()) {
            if (entry.getValue().containsAll(transformationTypes)) {
                entry.getKey().translate(translation, space);
            }
        }
        return this;
    }

    @Override
    public Transformable2D rotate(float rotation) {
        this.rotation += rotation;
        this.rotation %= MathDelegate.TWO_PI_FLOAT;
        modelMatrix.rotate(rotation, Vector3f.FORWARD);
        Set<TransformationType> transformationTypes = getAdditionalTransformationTypes(ROTATION);
        for (Map.Entry<Transformable2D, Set<TransformationType>> entry : children.entrySet()) {
            if (entry.getValue().containsAll(transformationTypes)) {
                entry.getKey().rotate(rotation);
            }
        }
        return this;
    }

    @Override
    public Transformable2D scale(ReadOnlyVector2f scale) {
        this.scale.scale(scale);
        modelMatrix.scale(scale);
        Set<TransformationType> transformationTypes = getAdditionalTransformationTypes(SCALING);
        for (Map.Entry<Transformable2D, Set<TransformationType>> entry : children.entrySet()) {
            if (entry.getValue().containsAll(transformationTypes)) {
                entry.getKey().scale(scale);
            }
        }
        return this;
    }

    @Override
    public ReadOnlyMatrix4f getModelMatrix() {
        return modelMatrix;
    }

    protected Set<TransformationType> getAdditionalTransformationTypes(TransformationType transformationType) {
        return Set.of(transformationType);
    }
}
