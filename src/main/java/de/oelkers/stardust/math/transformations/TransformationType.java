package de.oelkers.stardust.math.transformations;

public enum TransformationType {

    TRANSLATION, ROTATION, SCALING
}
