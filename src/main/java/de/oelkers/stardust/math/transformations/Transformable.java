package de.oelkers.stardust.math.transformations;

import de.oelkers.stardust.entities.Component;
import de.oelkers.stardust.math.matrices.ReadOnlyMatrix4f;

public interface Transformable extends Component {

    ReadOnlyMatrix4f getModelMatrix();
}
