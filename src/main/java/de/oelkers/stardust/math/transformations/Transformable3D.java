package de.oelkers.stardust.math.transformations;

import de.oelkers.stardust.math.matrices.Matrix4f;
import de.oelkers.stardust.math.matrices.ReadOnlyMatrix4f;
import de.oelkers.stardust.math.quaternions.Quaternion;
import de.oelkers.stardust.math.quaternions.ReadOnlyQuaternion;
import de.oelkers.stardust.math.vectors.ReadOnlyVector3f;
import de.oelkers.stardust.math.vectors.Vector3f;

public interface Transformable3D extends Transformable {

    ReadOnlyVector3f getPosition();
    ReadOnlyVector3f getScale();
    ReadOnlyQuaternion getOrientation();
    ReadOnlyMatrix4f getRotationMatrix();
    Transformable3D addChild(Transformable3D child, TransformationType... transformationTypes);
    Transformable3D removeChild(Transformable3D child);
    Transformable3D translate(ReadOnlyVector3f translation, TransformSpace space);
    Transformable3D rotate(ReadOnlyMatrix4f matrix, TransformSpace space);
    Transformable3D scale(ReadOnlyVector3f scale);

    default Transformable3D addChild(Transformable3D child) {
        return addChild(child, TransformationType.values());
    }

    default Transformable3D translate(float xTranslation, float yTranslation, float zTranslation, TransformSpace space) {
        return Vector3f.POOL.apply(vector -> translate(vector.set(xTranslation, yTranslation, zTranslation), space));
    }

    default Transformable3D rotate(ReadOnlyVector3f axis, float angle, TransformSpace space) {
        return Quaternion.POOL.apply(quaternion -> rotate(quaternion.setFromAxisAngle(axis, angle), space));
    }

    default Transformable3D rotate(float pitch, float yaw, float roll, TransformSpace space) {
        return Quaternion.POOL.apply(quaternion -> rotate(quaternion.setFromEulerRad(pitch, yaw, roll), space));
    }

    default Transformable3D rotate(ReadOnlyQuaternion orientation, TransformSpace space) {
        return Matrix4f.POOL.apply(matrix -> {
            matrix.setIdentity();
            orientation.getRotationMatrix(matrix);
            return rotate(matrix, space);
        });
    }

    default Transformable3D scale(float scalar) {
        return scale(scalar, scalar, scalar);
    }

    default Transformable3D scale(float xScale, float yScale, float zScale) {
        return Vector3f.POOL.apply(vector -> scale(vector.set(xScale, yScale, zScale)));
    }

    default Transformable3D translateTo(float xTranslation, float yTranslation, float zTranslation) {
        return translate(xTranslation - getPosition().getX(), yTranslation - getPosition().getY(), zTranslation - getPosition().getZ(), TransformSpace.WORLD);
    }

    default Transformable3D translateTo(ReadOnlyVector3f translation) {
        return translateTo(translation.getX(), translation.getY(), translation.getZ());
    }

    default Transformable3D rotateTo(ReadOnlyVector3f axis, float angle) {
        return Quaternion.POOL.apply(quaternion -> rotateTo(quaternion.setFromAxisAngle(axis, angle)));
    }

    default Transformable3D rotateTo(float pitch, float yaw, float roll) {
        return Quaternion.POOL.apply(quaternion -> rotateTo(quaternion.setFromEulerRad(pitch, yaw, roll)));
    }

    default Transformable3D rotateTo(ReadOnlyQuaternion orientation) {
        return Matrix4f.POOL.apply(matrix -> {
            matrix.setIdentity();
            orientation.getRotationMatrix(matrix);
            return rotateTo(matrix);
        });
    }

    default Transformable3D rotateTo(ReadOnlyMatrix4f matrix) {
        return Matrix4f.POOL.apply(inverted -> {
            getRotationMatrix().invert(inverted);
            return rotate(matrix.mul(inverted, inverted), TransformSpace.WORLD);
        });
    }

    default Transformable3D scaleTo(float xScale, float yScale, float zScale) {
        return scale(xScale / getScale().getX(), yScale / getScale().getY(), zScale / getScale().getZ());
    }

    default Transformable3D scaleTo(ReadOnlyVector3f scale) {
        return scaleTo(scale.getX(), scale.getY(), scale.getZ());
    }
}
