package de.oelkers.stardust.math.transformations;

import de.oelkers.stardust.math.matrices.Matrix4f;
import de.oelkers.stardust.math.matrices.ReadOnlyMatrix4f;
import de.oelkers.stardust.math.quaternions.Quaternion;
import de.oelkers.stardust.math.quaternions.ReadOnlyQuaternion;
import de.oelkers.stardust.math.vectors.ReadOnlyVector3f;
import de.oelkers.stardust.math.vectors.Vector3f;

import java.io.Serial;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static de.oelkers.stardust.math.transformations.TransformationType.*;
import static de.oelkers.stardust.utils.collections.CollectionUtils.newEnumSetOf;

/**
 * This class encapsulates object transformations (translations, rotations and scalings)
 * and keeps a reference to the so called model matrix (or transformation matrix).
 * It also keeps track of the current position, orientation and scale and provides you a read-only view over them.
 * Additionally, you are able to create a tree-like structure of transformations, where each child
 * is also affected by transformations of the parent.
 */
public class Transform3D implements Transformable3D {

    @Serial
    private static final long serialVersionUID = 1L;

    private final Matrix4f modelMatrix = new Matrix4f();
    private final Map<Transformable3D, Set<TransformationType>> children = new HashMap<>();
    private final Matrix4f rotationMatrix = new Matrix4f();
    private final Vector3f position = new Vector3f();
    private final Vector3f scale = new Vector3f(Vector3f.ONE);
    private final Quaternion orientation = new Quaternion();

    @Override
    public ReadOnlyVector3f getPosition() {
        return position;
    }

    @Override
    public ReadOnlyVector3f getScale() {
        return scale;
    }

    @Override
    public ReadOnlyQuaternion getOrientation() {
        return orientation;
    }

    @Override
    public ReadOnlyMatrix4f getRotationMatrix() {
        return rotationMatrix;
    }

    @Override
    public Transformable3D addChild(Transformable3D child, TransformationType... transformationTypes) {
        children.put(child, newEnumSetOf(TransformationType.class, transformationTypes));
        return this;
    }

    @Override
    public Transformable3D removeChild(Transformable3D child) {
        children.remove(child);
        return this;
    }

    @Override
    public Transformable3D translate(ReadOnlyVector3f translation, TransformSpace space) {
        modelMatrix.translate(translation, space);
        modelMatrix.getTranslation(position);
        Set<TransformationType> transformationTypes = getAdditionalTransformationTypes(TRANSLATION);
        for (Map.Entry<Transformable3D, Set<TransformationType>> entry : children.entrySet()) {
            if (entry.getValue().containsAll(transformationTypes)) {
                entry.getKey().translate(translation, space);
            }
        }
        return this;
    }

    @Override
    public Transformable3D rotate(ReadOnlyMatrix4f matrix, TransformSpace space) {
        if (space == TransformSpace.LOCAL) {
            rotationMatrix.mul(matrix, rotationMatrix);
        } else {
            matrix.mul(rotationMatrix, rotationMatrix);
        }
        orientation.setFromMatrix(rotationMatrix);
        modelMatrix.setRotation(rotationMatrix);
        modelMatrix.scale(scale);
        Set<TransformationType> transformationTypes = getAdditionalTransformationTypes(ROTATION);
        for (Map.Entry<Transformable3D, Set<TransformationType>> entry : children.entrySet()) {
            if (entry.getValue().containsAll(transformationTypes)) {
                entry.getKey().rotate(matrix, space);
            }
        }
        return this;
    }

    @Override
    public Transformable3D scale(ReadOnlyVector3f scale) {
        this.scale.scale(scale);
        modelMatrix.scale(scale);
        Set<TransformationType> transformationTypes = getAdditionalTransformationTypes(SCALING);
        for (Map.Entry<Transformable3D, Set<TransformationType>> entry : children.entrySet()) {
            if (entry.getValue().containsAll(transformationTypes)) {
                entry.getKey().scale(scale);
            }
        }
        return this;
    }

    @Override
    public ReadOnlyMatrix4f getModelMatrix() {
        return modelMatrix;
    }

    protected Set<TransformationType> getAdditionalTransformationTypes(TransformationType transformationType) {
        return Set.of(transformationType);
    }
}
