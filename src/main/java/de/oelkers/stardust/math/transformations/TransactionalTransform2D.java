package de.oelkers.stardust.math.transformations;

import de.oelkers.stardust.math.vectors.ReadOnlyVector2f;
import de.oelkers.stardust.math.vectors.Vector2f;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serial;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Set;

import static de.oelkers.stardust.math.transformations.TransformationType.*;

public class TransactionalTransform2D extends Transform2D {

    @Serial
    private static final long serialVersionUID = 1L;

    private transient Set<TransformationType> transformationTypes = EnumSet.noneOf(TransformationType.class);
    private transient Collection<Transformation> transformations = new ArrayList<>();

    @Override
    public TransactionalTransform2D translate(ReadOnlyVector2f translation, TransformSpace space) {
        if (!Vector2f.ZERO.equals(translation)) {
            transformationTypes.add(TRANSLATION);
            ReadOnlyVector2f copy = new Vector2f(translation);
            transformations.add(Transformation.fromTranslation(() -> super.translate(copy, space)));
        }
        return this;
    }

    @Override
    public TransactionalTransform2D rotate(float rotation) {
        if (rotation != 0) {
            transformationTypes.add(ROTATION);
            transformations.add(Transformation.fromRotation(() -> super.rotate(rotation)));
        }
        return this;
    }

    @Override
    public TransactionalTransform2D scale(ReadOnlyVector2f scale) {
        if (!Vector2f.ONE.equals(scale)) {
            transformationTypes.add(SCALING);
            ReadOnlyVector2f copy = new Vector2f(scale);
            transformations.add(Transformation.fromScaling(() -> super.scale(copy)));
        }
        return this;
    }

    public TransactionalTransform2D rollback() {
        transformationTypes.clear();
        transformations.clear();
        return this;
    }

    public TransactionalTransform2D commit() {
        for (Transformation transformation : transformations) {
            transformation.apply();
        }
        rollback();
        return this;
    }

    @Override
    protected Set<TransformationType> getAdditionalTransformationTypes(TransformationType transformationType) {
        return transformationTypes;
    }

    @Serial
    private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
        stream.defaultReadObject();
        transformations = new ArrayList<>();
        transformationTypes = EnumSet.noneOf(TransformationType.class);
    }

    private final static class Transformation {

        private Runnable translation;
        private Runnable rotation;
        private Runnable scaling;

        private static Transformation fromTranslation(Runnable translation) {
            Transformation transformation = new Transformation();
            transformation.translation = translation;
            return transformation;
        }

        private static Transformation fromRotation(Runnable rotation) {
            Transformation transformation = new Transformation();
            transformation.rotation = rotation;
            return transformation;
        }

        private static Transformation fromScaling(Runnable scaling) {
            Transformation transformation = new Transformation();
            transformation.scaling = scaling;
            return transformation;
        }

        private void apply() {
            if (translation != null) {
                translation.run();
            } else if (rotation != null) {
                rotation.run();
            } else if (scaling != null) {
                scaling.run();
            }
        }
    }
}
