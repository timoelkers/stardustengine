package de.oelkers.stardust.math.transformations;

import de.oelkers.stardust.math.matrices.Matrix4f;
import de.oelkers.stardust.math.matrices.ReadOnlyMatrix4f;
import de.oelkers.stardust.math.vectors.ReadOnlyVector3f;
import de.oelkers.stardust.math.vectors.Vector3f;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serial;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Set;

import static de.oelkers.stardust.math.transformations.TransformationType.*;

public class TransactionalTransform3D extends Transform3D {

    @Serial
    private static final long serialVersionUID = 1L;

    private transient Set<TransformationType> transformationTypes = EnumSet.noneOf(TransformationType.class);
    private transient Collection<Transformation> transformations = new ArrayList<>();

    @Override
    public TransactionalTransform3D translate(ReadOnlyVector3f translation, TransformSpace space) {
        if (!Vector3f.ZERO.equals(translation)) {
            transformationTypes.add(TRANSLATION);
            ReadOnlyVector3f copy = new Vector3f(translation);
            transformations.add(Transformation.fromTranslation(() -> super.translate(copy, space)));
        }
        return this;
    }

    @Override
    public TransactionalTransform3D rotate(ReadOnlyMatrix4f matrix, TransformSpace space) {
        if (!Matrix4f.IDENTITY.equals(matrix)) {
            transformationTypes.add(ROTATION);
            ReadOnlyMatrix4f copy = new Matrix4f(matrix);
            transformations.add(Transformation.fromRotation(() -> super.rotate(copy, space)));
        }
        return this;
    }

    @Override
    public TransactionalTransform3D scale(ReadOnlyVector3f scale) {
        if (!Vector3f.ONE.equals(scale)) {
            transformationTypes.add(SCALING);
            ReadOnlyVector3f copy = new Vector3f(scale);
            transformations.add(Transformation.fromScaling(() -> super.scale(copy)));
        }
        return this;
    }

    public TransactionalTransform3D rollback() {
        transformationTypes.clear();
        transformations.clear();
        return this;
    }

    public TransactionalTransform3D commit() {
        for (Transformation transformation : transformations) {
            transformation.apply();
        }
        rollback();
        return this;
    }

    @Override
    protected Set<TransformationType> getAdditionalTransformationTypes(TransformationType transformationType) {
        return transformationTypes;
    }

    @Serial
    private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
        stream.defaultReadObject();
        transformations = new ArrayList<>();
        transformationTypes = EnumSet.noneOf(TransformationType.class);
    }

    private final static class Transformation {

        private Runnable translation;
        private Runnable rotation;
        private Runnable scaling;

        private static Transformation fromTranslation(Runnable translation) {
            Transformation transformation = new Transformation();
            transformation.translation = translation;
            return transformation;
        }

        private static Transformation fromRotation(Runnable rotation) {
            Transformation transformation = new Transformation();
            transformation.rotation = rotation;
            return transformation;
        }

        private static Transformation fromScaling(Runnable scaling) {
            Transformation transformation = new Transformation();
            transformation.scaling = scaling;
            return transformation;
        }

        private void apply() {
            if (translation != null) {
                translation.run();
            } else if (rotation != null) {
                rotation.run();
            } else if (scaling != null) {
                scaling.run();
            }
        }
    }
}
