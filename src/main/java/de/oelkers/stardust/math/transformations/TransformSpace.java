package de.oelkers.stardust.math.transformations;

public enum TransformSpace {

    /**
     * Transformations are applied relative to the local coordinate system, which means
     * that each transformation is influenced by previous ones. For example a
     * translation after a rotation changes the direction of translation.
     */
    LOCAL,

    /**
     * Transformations are applied relative to the world coordinate system, which
     * is completely static. This means transformations can be made independently
     * of previous ones.
     */
    WORLD
}
