package de.oelkers.stardust.math.internal;

import de.oelkers.stardust.config.ConfigStore;

// to get up to date with the java.lang.Math API, you have to do the following:
// to allow IDEA to create delegation methods, you have to create an instance of java.lang.Math
// using reflection and add it as a member of this class
// after creating the delegating methods, do a regexp replacement using
// "Math\.(.*\(.*\))" as lookup and "ConfigStore.get().isUsingStrictMath() ? StrictMath.$1 : $0" as replacement
@SuppressWarnings("NonReproducibleMathCall")
public final class MathDelegate {

    private MathDelegate() {}

    public static final float PI_FLOAT = (float) Math.PI;
    public static final double PI_DOUBLE = Math.PI;
    public static final float TWO_PI_FLOAT = 2 * PI_FLOAT;
    public static final double TWO_PI_DOUBLE = 2 * PI_DOUBLE;
    public static final float HALF_PI_FLOAT = PI_FLOAT / 2;
    public static final double HALF_PI_DOUBLE = PI_DOUBLE / 2;

    public static double log(double x, int base) {
        return Math.log(x) / Math.log(base);
    }

    public static double sin(double a) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.sin(a) : Math.sin(a);
    }

    public static double cos(double a) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.cos(a) : Math.cos(a);
    }

    public static double tan(double a) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.tan(a) : Math.tan(a);
    }

    public static double asin(double a) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.asin(a) : Math.asin(a);
    }

    public static double acos(double a) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.acos(a) : Math.acos(a);
    }

    public static double atan(double a) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.atan(a) : Math.atan(a);
    }

    public static double toRadians(double angdeg) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.toRadians(angdeg) : Math.toRadians(angdeg);
    }

    public static double toDegrees(double angrad) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.toDegrees(angrad) : Math.toDegrees(angrad);
    }

    public static double exp(double a) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.exp(a) : Math.exp(a);
    }

    public static double log(double a) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.log(a) : Math.log(a);
    }

    public static double log10(double a) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.log10(a) : Math.log10(a);
    }

    public static double sqrt(double a) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.sqrt(a) : Math.sqrt(a);
    }

    public static double cbrt(double a) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.cbrt(a) : Math.cbrt(a);
    }

    public static double IEEEremainder(double f1, double f2) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.IEEEremainder(f1, f2) : Math.IEEEremainder(f1, f2);
    }

    public static double ceil(double a) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.ceil(a) : Math.ceil(a);
    }

    public static double floor(double a) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.floor(a) : Math.floor(a);
    }

    public static double rint(double a) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.rint(a) : Math.rint(a);
    }

    public static double atan2(double y, double x) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.atan2(y, x) : Math.atan2(y, x);
    }

    public static double pow(double a, double b) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.pow(a, b) : Math.pow(a, b);
    }

    public static int round(float a) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.round(a) : Math.round(a);
    }

    public static long round(double a) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.round(a) : Math.round(a);
    }

    public static double random() {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.random() : Math.random();
    }

    public static int addExact(int x, int y) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.addExact(x, y) : Math.addExact(x, y);
    }

    public static long addExact(long x, long y) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.addExact(x, y) : Math.addExact(x, y);
    }

    public static int subtractExact(int x, int y) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.subtractExact(x, y) : Math.subtractExact(x, y);
    }

    public static long subtractExact(long x, long y) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.subtractExact(x, y) : Math.subtractExact(x, y);
    }

    public static int multiplyExact(int x, int y) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.multiplyExact(x, y) : Math.multiplyExact(x, y);
    }

    public static long multiplyExact(long x, int y) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.multiplyExact(x, y) : Math.multiplyExact(x, y);
    }

    public static long multiplyExact(long x, long y) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.multiplyExact(x, y) : Math.multiplyExact(x, y);
    }

    public static int incrementExact(int a) {
        return Math.incrementExact(a);
    }

    public static long incrementExact(long a) {
        return Math.incrementExact(a);
    }

    public static int decrementExact(int a) {
        return Math.decrementExact(a);
    }

    public static long decrementExact(long a) {
        return Math.decrementExact(a);
    }

    public static int negateExact(int a) {
        return Math.negateExact(a);
    }

    public static long negateExact(long a) {
        return Math.negateExact(a);
    }

    public static int toIntExact(long value) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.toIntExact(value) : Math.toIntExact(value);
    }

    public static long multiplyFull(int x, int y) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.multiplyFull(x, y) : Math.multiplyFull(x, y);
    }

    public static long multiplyHigh(long x, long y) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.multiplyHigh(x, y) : Math.multiplyHigh(x, y);
    }

    public static int floorDiv(int x, int y) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.floorDiv(x, y) : Math.floorDiv(x, y);
    }

    public static long floorDiv(long x, int y) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.floorDiv(x, y) : Math.floorDiv(x, y);
    }

    public static long floorDiv(long x, long y) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.floorDiv(x, y) : Math.floorDiv(x, y);
    }

    public static int floorMod(int x, int y) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.floorMod(x, y) : Math.floorMod(x, y);
    }

    public static int floorMod(long x, int y) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.floorMod(x, y) : Math.floorMod(x, y);
    }

    public static long floorMod(long x, long y) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.floorMod(x, y) : Math.floorMod(x, y);
    }

    public static int abs(int a) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.abs(a) : Math.abs(a);
    }

    public static long abs(long a) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.abs(a) : Math.abs(a);
    }

    public static float abs(float a) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.abs(a) : Math.abs(a);
    }

    public static double abs(double a) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.abs(a) : Math.abs(a);
    }

    public static int max(int a, int b) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.max(a, b) : Math.max(a, b);
    }

    public static long max(long a, long b) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.max(a, b) : Math.max(a, b);
    }

    public static float max(float a, float b) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.max(a, b) : Math.max(a, b);
    }

    public static double max(double a, double b) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.max(a, b) : Math.max(a, b);
    }

    public static int min(int a, int b) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.min(a, b) : Math.min(a, b);
    }

    public static long min(long a, long b) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.min(a, b) : Math.min(a, b);
    }

    public static float min(float a, float b) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.min(a, b) : Math.min(a, b);
    }

    public static double min(double a, double b) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.min(a, b) : Math.min(a, b);
    }

    public static double fma(double a, double b, double c) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.fma(a, b, c) : Math.fma(a, b, c);
    }

    public static float fma(float a, float b, float c) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.fma(a, b, c) : Math.fma(a, b, c);
    }

    public static double ulp(double d) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.ulp(d) : Math.ulp(d);
    }

    public static float ulp(float f) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.ulp(f) : Math.ulp(f);
    }

    public static double signum(double d) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.signum(d) : Math.signum(d);
    }

    public static float signum(float f) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.signum(f) : Math.signum(f);
    }

    public static double sinh(double x) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.sinh(x) : Math.sinh(x);
    }

    public static double cosh(double x) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.cosh(x) : Math.cosh(x);
    }

    public static double tanh(double x) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.tanh(x) : Math.tanh(x);
    }

    public static double hypot(double x, double y) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.hypot(x, y) : Math.hypot(x, y);
    }

    public static double expm1(double x) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.expm1(x) : Math.expm1(x);
    }

    public static double log1p(double x) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.log1p(x) : Math.log1p(x);
    }

    public static double copySign(double magnitude, double sign) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.copySign(magnitude, sign) : Math.copySign(magnitude, sign);
    }

    public static float copySign(float magnitude, float sign) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.copySign(magnitude, sign) : Math.copySign(magnitude, sign);
    }

    public static int getExponent(float f) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.getExponent(f) : Math.getExponent(f);
    }

    public static int getExponent(double d) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.getExponent(d) : Math.getExponent(d);
    }

    public static double nextAfter(double start, double direction) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.nextAfter(start, direction) : Math.nextAfter(start, direction);
    }

    public static float nextAfter(float start, double direction) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.nextAfter(start, direction) : Math.nextAfter(start, direction);
    }

    public static double nextUp(double d) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.nextUp(d) : Math.nextUp(d);
    }

    public static float nextUp(float f) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.nextUp(f) : Math.nextUp(f);
    }

    public static double nextDown(double d) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.nextDown(d) : Math.nextDown(d);
    }

    public static float nextDown(float f) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.nextDown(f) : Math.nextDown(f);
    }

    public static double scalb(double d, int scaleFactor) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.scalb(d, scaleFactor) : Math.scalb(d, scaleFactor);
    }

    public static float scalb(float f, int scaleFactor) {
        return ConfigStore.get().isUsingStrictMath() ? StrictMath.scalb(f, scaleFactor) : Math.scalb(f, scaleFactor);
    }
}
