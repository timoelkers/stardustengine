package de.oelkers.stardust.math.matrices;

import de.oelkers.stardust.math.internal.MathDelegate;
import de.oelkers.stardust.math.transformations.TransformSpace;
import de.oelkers.stardust.math.vectors.ReadOnlyVector2f;
import de.oelkers.stardust.math.vectors.ReadOnlyVector3f;
import de.oelkers.stardust.math.vectors.Vector2f;
import de.oelkers.stardust.math.vectors.Vector3f;
import de.oelkers.stardust.utils.ByteSize;
import de.oelkers.stardust.utils.buffers.BufferElement;
import de.oelkers.stardust.utils.objectpools.GrowingObjectPool;
import de.oelkers.stardust.utils.objectpools.ObjectPool;

import java.io.Serial;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.Objects;

/**
 * Represents a 4x4 matrix of float values. The matrix is initially in column-major order.
 * Objects are mutable by design (for performance reasons) and all methods returning {@code Matrix4f}
 * will return the destination matrix to allow for method chaining.
 * If no destination is given, then the own object will be the target of the operation.
 */
public final class Matrix4f implements ReadOnlyMatrix4f, BufferElement<Matrix4f> {

    @Serial
    private static final long serialVersionUID = 1L;

    public static final ObjectPool<Matrix4f> POOL = new GrowingObjectPool<>(Matrix4f::new);
    public static final ReadOnlyMatrix4f IDENTITY = new Matrix4f();
    public static final ByteSize SIZE = ByteSize.ofBytes(16 * Float.BYTES);

    private final float[][] m = new float[4][4];

    public Matrix4f() {
        setIdentity();
    }

    public Matrix4f(ReadOnlyMatrix4f src) {
        set(src);
    }

    public Matrix4f set(ReadOnlyMatrix4f src) {
        m[0][0] = src.getValue(0, 0);
        m[0][1] = src.getValue(0, 1);
        m[0][2] = src.getValue(0, 2);
        m[0][3] = src.getValue(0, 3);
        m[1][0] = src.getValue(1, 0);
        m[1][1] = src.getValue(1, 1);
        m[1][2] = src.getValue(1, 2);
        m[1][3] = src.getValue(1, 3);
        m[2][0] = src.getValue(2, 0);
        m[2][1] = src.getValue(2, 1);
        m[2][2] = src.getValue(2, 2);
        m[2][3] = src.getValue(2, 3);
        m[3][0] = src.getValue(3, 0);
        m[3][1] = src.getValue(3, 1);
        m[3][2] = src.getValue(3, 2);
        m[3][3] = src.getValue(3, 3);
        return this;
    }

    public Matrix4f setIdentity() {
        m[0][0] = m[1][1] = m[2][2] = m[3][3] = 1.0f;
        m[0][1] = m[0][2] = m[0][3] = m[1][0] = m[1][2] = m[1][3] = m[2][0] = m[2][1] = m[2][3] = m[3][0] = m[3][1] = m[3][2] = 0.0f;
        return this;
    }

    public Matrix4f setValue(int rowIndex, int columnIndex, float value) {
        m[rowIndex][columnIndex] = value;
        return this;
    }

    @Override
    public float getValue(int rowIndex, int columnIndex) {
        return m[rowIndex][columnIndex];
    }

    public Matrix4f negate() {
        return negate(this);
    }

    @Override
    public Matrix4f negate(Matrix4f dest) {
        dest.m[0][0] -= m[0][0];
        dest.m[0][1] -= m[0][1];
        dest.m[0][2] -= m[0][2];
        dest.m[0][3] -= m[0][3];
        dest.m[1][0] -= m[1][0];
        dest.m[1][1] -= m[1][1];
        dest.m[1][2] -= m[1][2];
        dest.m[1][3] -= m[1][3];
        dest.m[2][0] -= m[2][0];
        dest.m[2][1] -= m[2][1];
        dest.m[2][2] -= m[2][2];
        dest.m[2][3] -= m[2][3];
        dest.m[3][0] -= m[3][0];
        dest.m[3][1] -= m[3][1];
        dest.m[3][2] -= m[3][2];
        dest.m[3][3] -= m[3][3];
        return dest;
    }

    public Matrix4f invert() {
        return invert(this);
    }

    @Override
    public Matrix4f invert(Matrix4f dest) {
        float determinant = getDeterminant();
        if (determinant == 0) {
            throw new IllegalStateException("Unable to invert a matrix with a determinant of 0!");
        }
        float t00 = determinant3x3(m[1][1], m[1][2], m[1][3], m[2][1], m[2][2], m[2][3], m[3][1], m[3][2], m[3][3]);
        float t01 = -determinant3x3(m[1][0], m[1][2], m[1][3], m[2][0], m[2][2], m[2][3], m[3][0], m[3][2], m[3][3]);
        float t02 = determinant3x3(m[1][0], m[1][1], m[1][3], m[2][0], m[2][1], m[2][3], m[3][0], m[3][1], m[3][3]);
        float t03 = -determinant3x3(m[1][0], m[1][1], m[1][2], m[2][0], m[2][1], m[2][2], m[3][0], m[3][1], m[3][2]);

        float t10 = -determinant3x3(m[0][1], m[0][2], m[0][3], m[2][1], m[2][2], m[2][3], m[3][1], m[3][2], m[3][3]);
        float t11 = determinant3x3(m[0][0], m[0][2], m[0][3], m[2][0], m[2][2], m[2][3], m[3][0], m[3][2], m[3][3]);
        float t12 = -determinant3x3(m[0][0], m[0][1], m[0][3], m[2][0], m[2][1], m[2][3], m[3][0], m[3][1], m[3][3]);
        float t13 = determinant3x3(m[0][0], m[0][1], m[0][2], m[2][0], m[2][1], m[2][2], m[3][0], m[3][1], m[3][2]);

        float t20 = determinant3x3(m[0][1], m[0][2], m[0][3], m[1][1], m[1][2], m[1][3], m[3][1], m[3][2], m[3][3]);
        float t21 = -determinant3x3(m[0][0], m[0][2], m[0][3], m[1][0], m[1][2], m[1][3], m[3][0], m[3][2], m[3][3]);
        float t22 = determinant3x3(m[0][0], m[0][1], m[0][3], m[1][0], m[1][1], m[1][3], m[3][0], m[3][1], m[3][3]);
        float t23 = -determinant3x3(m[0][0], m[0][1], m[0][2], m[1][0], m[1][1], m[1][2], m[3][0], m[3][1], m[3][2]);

        float t30 = -determinant3x3(m[0][1], m[0][2], m[0][3], m[1][1], m[1][2], m[1][3], m[2][1], m[2][2], m[2][3]);
        float t31 = determinant3x3(m[0][0], m[0][2], m[0][3], m[1][0], m[1][2], m[1][3], m[2][0], m[2][2], m[2][3]);
        float t32 = -determinant3x3(m[0][0], m[0][1], m[0][3], m[1][0], m[1][1], m[1][3], m[2][0], m[2][1], m[2][3]);
        float t33 = determinant3x3(m[0][0], m[0][1], m[0][2], m[1][0], m[1][1], m[1][2], m[2][0], m[2][1], m[2][2]);

        float determinant_inv = 1f / determinant;
        dest.m[0][0] = t00 * determinant_inv;
        dest.m[1][1] = t11 * determinant_inv;
        dest.m[2][2] = t22 * determinant_inv;
        dest.m[3][3] = t33 * determinant_inv;
        dest.m[0][1] = t10 * determinant_inv;
        dest.m[1][0] = t01 * determinant_inv;
        dest.m[2][0] = t02 * determinant_inv;
        dest.m[0][2] = t20 * determinant_inv;
        dest.m[1][2] = t21 * determinant_inv;
        dest.m[2][1] = t12 * determinant_inv;
        dest.m[0][3] = t30 * determinant_inv;
        dest.m[3][0] = t03 * determinant_inv;
        dest.m[1][3] = t31 * determinant_inv;
        dest.m[3][1] = t13 * determinant_inv;
        dest.m[3][2] = t23 * determinant_inv;
        dest.m[2][3] = t32 * determinant_inv;
        return dest;
    }

    public Matrix4f transpose() {
        return transpose(this);
    }

    @Override
    public Matrix4f transpose(Matrix4f dest) {
        float t00 = m[0][0];
        float t01 = m[1][0];
        float t02 = m[2][0];
        float t03 = m[3][0];
        float t10 = m[0][1];
        float t11 = m[1][1];
        float t12 = m[2][1];
        float t13 = m[3][1];
        float t20 = m[0][2];
        float t21 = m[1][2];
        float t22 = m[2][2];
        float t23 = m[3][2];
        float t30 = m[0][3];
        float t31 = m[1][3];
        float t32 = m[2][3];
        float t33 = m[3][3];

        dest.m[0][0] = t00;
        dest.m[0][1] = t01;
        dest.m[0][2] = t02;
        dest.m[0][3] = t03;
        dest.m[1][0] = t10;
        dest.m[1][1] = t11;
        dest.m[1][2] = t12;
        dest.m[1][3] = t13;
        dest.m[2][0] = t20;
        dest.m[2][1] = t21;
        dest.m[2][2] = t22;
        dest.m[2][3] = t23;
        dest.m[3][0] = t30;
        dest.m[3][1] = t31;
        dest.m[3][2] = t32;
        dest.m[3][3] = t33;
        return dest;
    }

    @Override
    public float getDeterminant() {
        float f = m[0][0] * (m[1][1] * m[2][2] * m[3][3] + m[1][2] * m[2][3] * m[3][1] + m[1][3] * m[2][1] * m[3][2]
                - m[1][3] * m[2][2] * m[3][1] - m[1][1] * m[2][3] * m[3][2] - m[1][2] * m[2][1] * m[3][3]);
        f -= m[0][1] * (m[1][0] * m[2][2] * m[3][3] + m[1][2] * m[2][3] * m[3][0] + m[1][3] * m[2][0] * m[3][2]
                - m[1][3] * m[2][2] * m[3][0] - m[1][0] * m[2][3] * m[3][2] - m[1][2] * m[2][0] * m[3][3]);
        f += m[0][2] * (m[1][0] * m[2][1] * m[3][3] + m[1][1] * m[2][3] * m[3][0] + m[1][3] * m[2][0] * m[3][1]
                - m[1][3] * m[2][1] * m[3][0] - m[1][0] * m[2][3] * m[3][1] - m[1][1] * m[2][0] * m[3][3]);
        f -= m[0][3] * (m[1][0] * m[2][1] * m[3][2] + m[1][1] * m[2][2] * m[3][0] + m[1][2] * m[2][0] * m[3][1]
                - m[1][2] * m[2][1] * m[3][0] - m[1][0] * m[2][2] * m[3][1] - m[1][1] * m[2][0] * m[3][2]);
        return f;
    }

    public Matrix4f add(ReadOnlyMatrix4f matrix) {
        return add(matrix, this);
    }

    @Override
    public Matrix4f add(ReadOnlyMatrix4f matrix, Matrix4f dest) {
        dest.m[0][0] = m[0][0] + matrix.getValue(0, 0);
        dest.m[0][1] = m[0][1] + matrix.getValue(0, 1);
        dest.m[0][2] = m[0][2] + matrix.getValue(0, 2);
        dest.m[0][3] = m[0][3] + matrix.getValue(0, 3);
        dest.m[1][0] = m[1][0] + matrix.getValue(1, 0);
        dest.m[1][1] = m[1][1] + matrix.getValue(1, 1);
        dest.m[1][2] = m[1][2] + matrix.getValue(1, 2);
        dest.m[1][3] = m[1][3] + matrix.getValue(1, 3);
        dest.m[2][0] = m[2][0] + matrix.getValue(2, 0);
        dest.m[2][1] = m[2][1] + matrix.getValue(2, 1);
        dest.m[2][2] = m[2][2] + matrix.getValue(2, 2);
        dest.m[2][3] = m[2][3] + matrix.getValue(2, 3);
        dest.m[3][0] = m[3][0] + matrix.getValue(3, 0);
        dest.m[3][1] = m[3][1] + matrix.getValue(3, 1);
        dest.m[3][2] = m[3][2] + matrix.getValue(3, 2);
        dest.m[3][3] = m[3][3] + matrix.getValue(3, 3);
        return dest;
    }

    public Matrix4f sub(ReadOnlyMatrix4f matrix) {
        return sub(matrix, this);
    }

    @Override
    public Matrix4f sub(ReadOnlyMatrix4f matrix, Matrix4f dest) {
        dest.m[0][0] = m[0][0] - matrix.getValue(0, 0);
        dest.m[0][1] = m[0][1] - matrix.getValue(0, 1);
        dest.m[0][2] = m[0][2] - matrix.getValue(0, 2);
        dest.m[0][3] = m[0][3] - matrix.getValue(0, 3);
        dest.m[1][0] = m[1][0] - matrix.getValue(1, 0);
        dest.m[1][1] = m[1][1] - matrix.getValue(1, 1);
        dest.m[1][2] = m[1][2] - matrix.getValue(1, 2);
        dest.m[1][3] = m[1][3] - matrix.getValue(1, 3);
        dest.m[2][0] = m[2][0] - matrix.getValue(2, 0);
        dest.m[2][1] = m[2][1] - matrix.getValue(2, 1);
        dest.m[2][2] = m[2][2] - matrix.getValue(2, 2);
        dest.m[2][3] = m[2][3] - matrix.getValue(2, 3);
        dest.m[3][0] = m[3][0] - matrix.getValue(3, 0);
        dest.m[3][1] = m[3][1] - matrix.getValue(3, 1);
        dest.m[3][2] = m[3][2] - matrix.getValue(3, 2);
        dest.m[3][3] = m[3][3] - matrix.getValue(3, 3);
        return dest;
    }

    public Matrix4f mul(ReadOnlyMatrix4f matrix) {
        return mul(matrix, this);
    }

    @Override
    public Matrix4f mul(ReadOnlyMatrix4f matrix, Matrix4f dest) {
        float t00 = m[0][0] * matrix.getValue(0, 0) + m[1][0] * matrix.getValue(0, 1) + m[2][0] * matrix.getValue(0, 2) + m[3][0] * matrix.getValue(0, 3);
        float t01 = m[0][1] * matrix.getValue(0, 0) + m[1][1] * matrix.getValue(0, 1) + m[2][1] * matrix.getValue(0, 2) + m[3][1] * matrix.getValue(0, 3);
        float t02 = m[0][2] * matrix.getValue(0, 0) + m[1][2] * matrix.getValue(0, 1) + m[2][2] * matrix.getValue(0, 2) + m[3][2] * matrix.getValue(0, 3);
        float t03 = m[0][3] * matrix.getValue(0, 0) + m[1][3] * matrix.getValue(0, 1) + m[2][3] * matrix.getValue(0, 2) + m[3][3] * matrix.getValue(0, 3);
        float t10 = m[0][0] * matrix.getValue(1, 0) + m[1][0] * matrix.getValue(1, 1) + m[2][0] * matrix.getValue(1, 2) + m[3][0] * matrix.getValue(1, 3);
        float t11 = m[0][1] * matrix.getValue(1, 0) + m[1][1] * matrix.getValue(1, 1) + m[2][1] * matrix.getValue(1, 2) + m[3][1] * matrix.getValue(1, 3);
        float t12 = m[0][2] * matrix.getValue(1, 0) + m[1][2] * matrix.getValue(1, 1) + m[2][2] * matrix.getValue(1, 2) + m[3][2] * matrix.getValue(1, 3);
        float t13 = m[0][3] * matrix.getValue(1, 0) + m[1][3] * matrix.getValue(1, 1) + m[2][3] * matrix.getValue(1, 2) + m[3][3] * matrix.getValue(1, 3);
        float t20 = m[0][0] * matrix.getValue(2, 0) + m[1][0] * matrix.getValue(2, 1) + m[2][0] * matrix.getValue(2, 2) + m[3][0] * matrix.getValue(2, 3);
        float t21 = m[0][1] * matrix.getValue(2, 0) + m[1][1] * matrix.getValue(2, 1) + m[2][1] * matrix.getValue(2, 2) + m[3][1] * matrix.getValue(2, 3);
        float t22 = m[0][2] * matrix.getValue(2, 0) + m[1][2] * matrix.getValue(2, 1) + m[2][2] * matrix.getValue(2, 2) + m[3][2] * matrix.getValue(2, 3);
        float t23 = m[0][3] * matrix.getValue(2, 0) + m[1][3] * matrix.getValue(2, 1) + m[2][3] * matrix.getValue(2, 2) + m[3][3] * matrix.getValue(2, 3);
        float t30 = m[0][0] * matrix.getValue(3, 0) + m[1][0] * matrix.getValue(3, 1) + m[2][0] * matrix.getValue(3, 2) + m[3][0] * matrix.getValue(3, 3);
        float t31 = m[0][1] * matrix.getValue(3, 0) + m[1][1] * matrix.getValue(3, 1) + m[2][1] * matrix.getValue(3, 2) + m[3][1] * matrix.getValue(3, 3);
        float t32 = m[0][2] * matrix.getValue(3, 0) + m[1][2] * matrix.getValue(3, 1) + m[2][2] * matrix.getValue(3, 2) + m[3][2] * matrix.getValue(3, 3);
        float t33 = m[0][3] * matrix.getValue(3, 0) + m[1][3] * matrix.getValue(3, 1) + m[2][3] * matrix.getValue(3, 2) + m[3][3] * matrix.getValue(3, 3);

        dest.m[0][0] = t00;
        dest.m[0][1] = t01;
        dest.m[0][2] = t02;
        dest.m[0][3] = t03;
        dest.m[1][0] = t10;
        dest.m[1][1] = t11;
        dest.m[1][2] = t12;
        dest.m[1][3] = t13;
        dest.m[2][0] = t20;
        dest.m[2][1] = t21;
        dest.m[2][2] = t22;
        dest.m[2][3] = t23;
        dest.m[3][0] = t30;
        dest.m[3][1] = t31;
        dest.m[3][2] = t32;
        dest.m[3][3] = t33;
        return dest;
    }

    public Matrix4f translate(ReadOnlyVector2f vector, TransformSpace space) {
        return translate(vector, space, this);
    }

    @Override
    public Matrix4f translate(ReadOnlyVector2f vector, TransformSpace space, Matrix4f dest) {
        if (space == TransformSpace.LOCAL) {
            dest.m[3][0] += m[0][0] * vector.getX() + m[1][0] * vector.getY();
            dest.m[3][1] += m[0][1] * vector.getX() + m[1][1] * vector.getY();
            dest.m[3][2] += m[0][2] * vector.getX() + m[1][2] * vector.getY();
            dest.m[3][3] += m[0][3] * vector.getX() + m[1][3] * vector.getY();
        } else {
            dest.m[3][0] += vector.getX();
            dest.m[3][1] += vector.getY();
        }
        return dest;
    }

    public Matrix4f translate(ReadOnlyVector3f vector, TransformSpace space) {
        return translate(vector, space, this);
    }

    @Override
    public Matrix4f translate(ReadOnlyVector3f vector, TransformSpace space, Matrix4f dest) {
        if (space == TransformSpace.LOCAL) {
            dest.m[3][0] += m[0][0] * vector.getX() + m[1][0] * vector.getY() + m[2][0] * vector.getZ();
            dest.m[3][1] += m[0][1] * vector.getX() + m[1][1] * vector.getY() + m[2][1] * vector.getZ();
            dest.m[3][2] += m[0][2] * vector.getX() + m[1][2] * vector.getY() + m[2][2] * vector.getZ();
            dest.m[3][3] += m[0][3] * vector.getX() + m[1][3] * vector.getY() + m[2][3] * vector.getZ();
        } else {
            dest.m[3][0] += vector.getX();
            dest.m[3][1] += vector.getY();
            dest.m[3][2] += vector.getZ();
        }
        return dest;
    }

    @Override
    public void getTranslation(Vector2f dest) {
        dest.set(m[3][0], m[3][1]);
    }

    @Override
    public void getTranslation(Vector3f dest) {
        dest.set(m[3][0], m[3][1], m[3][2]);
    }

    public Matrix4f setTranslation(ReadOnlyVector2f translation) {
        m[3][0] = translation.getX();
        m[3][1] = translation.getY();
        return this;
    }

    public Matrix4f setTranslation(ReadOnlyVector3f translation) {
        m[3][0] = translation.getX();
        m[3][1] = translation.getY();
        m[3][2] = translation.getZ();
        return this;
    }

    public Matrix4f rotate(float angle, ReadOnlyVector3f axis) {
        return rotate(angle, axis, this);
    }

    @Override
    public Matrix4f rotate(float angle, ReadOnlyVector3f axis, Matrix4f dest) {
        float c = (float) MathDelegate.cos(angle);
        float s = (float) MathDelegate.sin(angle);
        float oneMinusC = 1.0f - c;
        float xy = axis.getX() * axis.getY();
        float yz = axis.getY() * axis.getZ();
        float xz = axis.getX() * axis.getZ();
        float xs = axis.getX() * s;
        float ys = axis.getY() * s;
        float zs = axis.getZ() * s;

        float f00 = axis.getX() * axis.getX() * oneMinusC + c;
        float f01 = xy * oneMinusC + zs;
        float f02 = xz * oneMinusC - ys;
        float f10 = xy * oneMinusC - zs;
        float f11 = axis.getY() * axis.getY() * oneMinusC + c;
        float f12 = yz * oneMinusC + xs;
        float f20 = xz * oneMinusC + ys;
        float f21 = yz * oneMinusC - xs;
        float f22 = axis.getZ() * axis.getZ() * oneMinusC + c;

        float t00 = m[0][0] * f00 + m[1][0] * f01 + m[2][0] * f02;
        float t01 = m[0][1] * f00 + m[1][1] * f01 + m[2][1] * f02;
        float t02 = m[0][2] * f00 + m[1][2] * f01 + m[2][2] * f02;
        float t03 = m[0][3] * f00 + m[1][3] * f01 + m[2][3] * f02;
        float t10 = m[0][0] * f10 + m[1][0] * f11 + m[2][0] * f12;
        float t11 = m[0][1] * f10 + m[1][1] * f11 + m[2][1] * f12;
        float t12 = m[0][2] * f10 + m[1][2] * f11 + m[2][2] * f12;
        float t13 = m[0][3] * f10 + m[1][3] * f11 + m[2][3] * f12;

        dest.m[2][0] = m[0][0] * f20 + m[1][0] * f21 + m[2][0] * f22;
        dest.m[2][1] = m[0][1] * f20 + m[1][1] * f21 + m[2][1] * f22;
        dest.m[2][2] = m[0][2] * f20 + m[1][2] * f21 + m[2][2] * f22;
        dest.m[2][3] = m[0][3] * f20 + m[1][3] * f21 + m[2][3] * f22;
        dest.m[0][0] = t00;
        dest.m[0][1] = t01;
        dest.m[0][2] = t02;
        dest.m[0][3] = t03;
        dest.m[1][0] = t10;
        dest.m[1][1] = t11;
        dest.m[1][2] = t12;
        dest.m[1][3] = t13;
        return dest;
    }

    public Matrix4f setRotation(ReadOnlyMatrix4f rotation) {
        m[0][0] = rotation.getValue(0, 0);
        m[0][1] = rotation.getValue(0, 1);
        m[0][2] = rotation.getValue(0, 2);
        m[1][0] = rotation.getValue(1, 0);
        m[1][1] = rotation.getValue(1, 1);
        m[1][2] = rotation.getValue(1, 2);
        m[2][0] = rotation.getValue(2, 0);
        m[2][1] = rotation.getValue(2, 1);
        m[2][2] = rotation.getValue(2, 2);
        return this;
    }

    public Matrix4f scale(ReadOnlyVector2f vector) {
        return scale(vector, this);
    }

    @Override
    public Matrix4f scale(ReadOnlyVector2f vector, Matrix4f dest) {
        dest.m[0][0] = m[0][0] * vector.getX();
        dest.m[0][1] = m[0][1] * vector.getX();
        dest.m[0][2] = m[0][2] * vector.getX();
        dest.m[0][3] = m[0][3] * vector.getX();
        dest.m[1][0] = m[1][0] * vector.getY();
        dest.m[1][1] = m[1][1] * vector.getY();
        dest.m[1][2] = m[1][2] * vector.getY();
        dest.m[1][3] = m[1][3] * vector.getY();
        return this;
    }

    public Matrix4f scale(ReadOnlyVector3f vector) {
        return scale(vector, this);
    }

    @Override
    public Matrix4f scale(ReadOnlyVector3f vector, Matrix4f dest) {
        dest.m[0][0] = m[0][0] * vector.getX();
        dest.m[0][1] = m[0][1] * vector.getX();
        dest.m[0][2] = m[0][2] * vector.getX();
        dest.m[0][3] = m[0][3] * vector.getX();
        dest.m[1][0] = m[1][0] * vector.getY();
        dest.m[1][1] = m[1][1] * vector.getY();
        dest.m[1][2] = m[1][2] * vector.getY();
        dest.m[1][3] = m[1][3] * vector.getY();
        dest.m[2][0] = m[2][0] * vector.getY();
        dest.m[2][1] = m[2][1] * vector.getZ();
        dest.m[2][2] = m[2][2] * vector.getZ();
        dest.m[2][3] = m[2][3] * vector.getZ();
        return dest;
    }

    @Override
    public Matrix4f load(ByteBuffer buffer) {
        m[0][0] = buffer.getFloat();
        m[0][1] = buffer.getFloat();
        m[0][2] = buffer.getFloat();
        m[0][3] = buffer.getFloat();
        m[1][0] = buffer.getFloat();
        m[1][1] = buffer.getFloat();
        m[1][2] = buffer.getFloat();
        m[1][3] = buffer.getFloat();
        m[2][0] = buffer.getFloat();
        m[2][1] = buffer.getFloat();
        m[2][2] = buffer.getFloat();
        m[2][3] = buffer.getFloat();
        m[3][0] = buffer.getFloat();
        m[3][1] = buffer.getFloat();
        m[3][2] = buffer.getFloat();
        m[3][3] = buffer.getFloat();
        return this;
    }

    public Matrix4f load(FloatBuffer buffer) {
        m[0][0] = buffer.get();
        m[0][1] = buffer.get();
        m[0][2] = buffer.get();
        m[0][3] = buffer.get();
        m[1][0] = buffer.get();
        m[1][1] = buffer.get();
        m[1][2] = buffer.get();
        m[1][3] = buffer.get();
        m[2][0] = buffer.get();
        m[2][1] = buffer.get();
        m[2][2] = buffer.get();
        m[2][3] = buffer.get();
        m[3][0] = buffer.get();
        m[3][1] = buffer.get();
        m[3][2] = buffer.get();
        m[3][3] = buffer.get();
        return this;
    }

    @Override
    public ByteSize getByteSize() {
        return SIZE;
    }

    @Override
    public void store(ByteBuffer buffer, int index) {
        buffer.putFloat(index, m[0][0]);
        buffer.putFloat(index + Float.BYTES, m[0][1]);
        buffer.putFloat(index + Float.BYTES * 2, m[0][2]);
        buffer.putFloat(index + Float.BYTES * 3, m[0][3]);
        buffer.putFloat(index + Float.BYTES * 4, m[1][0]);
        buffer.putFloat(index + Float.BYTES * 5, m[1][1]);
        buffer.putFloat(index + Float.BYTES * 6, m[1][2]);
        buffer.putFloat(index + Float.BYTES * 7, m[1][3]);
        buffer.putFloat(index + Float.BYTES * 8, m[2][0]);
        buffer.putFloat(index + Float.BYTES * 9, m[2][1]);
        buffer.putFloat(index + Float.BYTES * 10, m[2][2]);
        buffer.putFloat(index + Float.BYTES * 11, m[2][3]);
        buffer.putFloat(index + Float.BYTES * 12, m[3][0]);
        buffer.putFloat(index + Float.BYTES * 13, m[3][1]);
        buffer.putFloat(index + Float.BYTES * 14, m[3][2]);
        buffer.putFloat(index + Float.BYTES * 15, m[3][3]);
    }

    @Override
    public void store(ByteBuffer buffer) {
        BufferElement.super.store(buffer);
    }

    private static float determinant3x3(float t00, float t01, float t02, float t10, float t11, float t12, float t20, float t21, float t22) {
        return t00 * (t11 * t22 - t12 * t21) + t01 * (t12 * t20 - t10 * t22) + t02 * (t10 * t21 - t11 * t20);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ReadOnlyMatrix4f)) {
            return false;
        }
        ReadOnlyMatrix4f other = (ReadOnlyMatrix4f) obj;
        return m[0][0] == other.getValue(0, 0) && m[0][1] == other.getValue(0, 1) && m[0][2] == other.getValue(0, 2) && m[0][3] == other.getValue(0, 3) &&
                m[1][0] == other.getValue(1, 0) && m[1][1] == other.getValue(1, 1) && m[1][2] == other.getValue(1, 2) && m[1][3] == other.getValue(1, 3) &&
                m[2][0] == other.getValue(2, 0) && m[2][1] == other.getValue(2, 1) && m[2][2] == other.getValue(2, 2) && m[2][3] == other.getValue(2, 3) &&
                m[3][0] == other.getValue(3, 0) && m[3][1] == other.getValue(3, 1) && m[3][2] == other.getValue(3, 2) && m[3][3] == other.getValue(3, 3);
    }

    @Override
    public int hashCode() {
        return Objects.hash(m[0][0], m[0][1], m[0][2], m[0][3], m[1][0], m[1][1], m[1][2], m[1][3], m[2][0], m[2][1], m[2][2], m[2][3], m[3][0], m[3][1], m[3][2], m[3][3]);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '[' + m[0][0] + ' ' + m[0][1] + ' ' + m[0][2] + ' ' + m[0][3] + System.lineSeparator() +
                m[1][0] + ' ' + m[1][1] + ' ' + m[1][2] + ' ' + m[1][3] + System.lineSeparator() +
                m[2][0] + ' ' + m[2][1] + ' ' + m[2][2] + ' ' + m[2][3] + System.lineSeparator() +
                m[3][0] + ' ' + m[3][1] + ' ' + m[3][2] + ' ' + m[3][3] + ']';
    }
}
