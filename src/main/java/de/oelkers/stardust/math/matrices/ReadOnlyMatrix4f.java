package de.oelkers.stardust.math.matrices;

import de.oelkers.stardust.math.transformations.TransformSpace;
import de.oelkers.stardust.math.vectors.ReadOnlyVector2f;
import de.oelkers.stardust.math.vectors.ReadOnlyVector3f;
import de.oelkers.stardust.math.vectors.Vector2f;
import de.oelkers.stardust.math.vectors.Vector3f;
import de.oelkers.stardust.utils.ByteSize;

import java.io.Serializable;
import java.nio.ByteBuffer;

/**
 * Represents a read only view over a {@link Matrix4f}. All method are guaranteed
 * not to change the state of the matrix and prevent you from getting access to any
 * concrete implementation. All implementations are serializable.
 */
public interface ReadOnlyMatrix4f extends Serializable {

    float getValue(int rowIndex, int columnIndex);
    float getDeterminant();
    Matrix4f negate(Matrix4f dest);
    Matrix4f invert(Matrix4f dest);
    Matrix4f transpose(Matrix4f dest);
    Matrix4f add(ReadOnlyMatrix4f matrix, Matrix4f dest);
    Matrix4f sub(ReadOnlyMatrix4f matrix, Matrix4f dest);
    Matrix4f mul(ReadOnlyMatrix4f matrix, Matrix4f dest);
    Matrix4f translate(ReadOnlyVector2f vector, TransformSpace space, Matrix4f dest);
    Matrix4f translate(ReadOnlyVector3f vector, TransformSpace space, Matrix4f dest);
    void getTranslation(Vector2f dest);
    void getTranslation(Vector3f dest);
    Matrix4f rotate(float angle, ReadOnlyVector3f axis, Matrix4f dest);
    Matrix4f scale(ReadOnlyVector2f vector, Matrix4f dest);
    Matrix4f scale(ReadOnlyVector3f vector, Matrix4f dest);
    ByteSize getByteSize();
    void store(ByteBuffer buffer, int index);
    void store(ByteBuffer buffer);
}
