package de.oelkers.stardust.math.shapes;

import de.oelkers.stardust.math.internal.MathDelegate;
import de.oelkers.stardust.math.matrices.Matrix4f;
import de.oelkers.stardust.math.matrices.ReadOnlyMatrix4f;
import de.oelkers.stardust.math.vectors.ReadOnlyVector3f;
import de.oelkers.stardust.math.vectors.Vector3f;

import java.io.Serial;
import java.util.Arrays;

public class BoundingBox extends CollisionComponent3D {

    @Serial
    private static final long serialVersionUID = 1L;

    private final Vector3f min;
    private final Vector3f max;

    public BoundingBox(ReadOnlyVector3f[] points) {
        super(8);
        min = new Vector3f();
        max = new Vector3f();
        setMinMax(points);
        setFromMinMax();
    }

    public BoundingBox(ReadOnlyVector3f min, ReadOnlyVector3f max) {
        super(8);
        this.min = new Vector3f(min);
        this.max = new Vector3f(max);
        setFromMinMax();
    }

    @Override
    public boolean intersects(BoundingBox boundingBox) {
        throw new RuntimeException("Not implemented yet!");
    }

    @Override
    public boolean intersects(Ray ray) {
        ReadOnlyMatrix4f modelMatrix = getShapeTransform().getModelMatrix();
        RayIntersectionModel model = new RayIntersectionModel();
        model.deltaPosition = getShapeTransform().getPosition().sub(ray.getOrigin(), new Vector3f());
        model.rayDirection = ray.getDirection();
        ReadOnlyVector3f xAxis = new Vector3f(modelMatrix.getValue(0, 0), modelMatrix.getValue(0, 1), modelMatrix.getValue(0, 2));
        if (!testIntersectionOnAxis(xAxis, min.getX(), max.getX(), model)) {
            return false;
        }
        ReadOnlyVector3f yAxis = new Vector3f(modelMatrix.getValue(1, 0), modelMatrix.getValue(1, 1), modelMatrix.getValue(1, 2));
        if (!testIntersectionOnAxis(yAxis, min.getY(), max.getY(), model)) {
            return false;
        }
        ReadOnlyVector3f zAxis = new Vector3f(modelMatrix.getValue(2, 0), modelMatrix.getValue(2, 1), modelMatrix.getValue(2, 2));
        return testIntersectionOnAxis(zAxis, min.getZ(), max.getZ(), model);
    }

    @Override
    public boolean contains(BoundingBox boundingBox) {
        return Arrays.stream(boundingBox.getWorldVertices()).allMatch(this::contains);
    }

    @Override
    public boolean contains(float x, float y, float z) {
        return Matrix4f.POOL.apply(inverted -> {
            getShapeTransform().getModelMatrix().invert(inverted);
            return Vector3f.POOL.apply(local -> {
                Vector3f transformed = local.set(x, y, z).transform(inverted);
                return isBetween(transformed.getX(), min.getX(), max.getX()) &&
                        isBetween(transformed.getY(), min.getY(), max.getY()) &&
                        isBetween(transformed.getZ(), min.getZ(), max.getZ());
            });
        });
    }

    private static boolean testIntersectionOnAxis(ReadOnlyVector3f axis, float min, float max, RayIntersectionModel model) {
        float e = axis.dot(model.deltaPosition);
        float f = model.rayDirection.dot(axis);
        if (MathDelegate.abs(f) > 0.001f) {
            float intersectionMinDistance = (e + min) / f;
            float intersectionMaxDistance = (e + max) / f;
            if (intersectionMinDistance > intersectionMaxDistance) {
                float tmp = intersectionMinDistance;
                intersectionMinDistance = intersectionMaxDistance;
                intersectionMaxDistance = tmp;
            }
            model.nearIntersection = MathDelegate.max(intersectionMinDistance, model.nearIntersection);
            model.farIntersection = MathDelegate.min(intersectionMaxDistance, model.farIntersection);
            return !(model.farIntersection < model.nearIntersection);
        }
        return !(-e + min > 0.0f) && !(-e + max < 0.0f);
    }

    private void setFromMinMax() {
        vertices[0].set(min.getX(), min.getY(), min.getZ());
        vertices[1].set(min.getX(), min.getY(), max.getZ());
        vertices[2].set(min.getX(), max.getY(), min.getZ());
        vertices[3].set(min.getX(), max.getY(), max.getZ());
        vertices[4].set(max.getX(), min.getY(), min.getZ());
        vertices[5].set(max.getX(), min.getY(), max.getZ());
        vertices[6].set(max.getX(), max.getY(), min.getZ());
        vertices[7].set(max.getX(), max.getY(), max.getZ());
    }

    private void setMinMax(ReadOnlyVector3f[] points) {
        for (int i = 1; i < points.length; i++) {
            ReadOnlyVector3f point = points[i];
            if (point.getX() < min.getX()) {
                min.setX(point.getX());
            }
            if (point.getY() < min.getY()) {
                min.setY(point.getY());
            }
            if (point.getZ() < min.getZ()) {
                min.setZ(point.getZ());
            }
            if (point.getX() > max.getX()) {
                max.setX(point.getX());
            }
            if (point.getY() > max.getY()) {
                max.setY(point.getY());
            }
            if (point.getZ() > max.getZ()) {
                max.setZ(point.getZ());
            }
        }
    }

    private static boolean isBetween(float check, float lowerBound, float higherBound) {
        return check >= lowerBound && check <= higherBound;
    }

    private static final class RayIntersectionModel {

        private ReadOnlyVector3f deltaPosition;
        private ReadOnlyVector3f rayDirection;
        private float nearIntersection = Float.MIN_NORMAL;
        private float farIntersection = Float.MAX_VALUE;
    }
}
