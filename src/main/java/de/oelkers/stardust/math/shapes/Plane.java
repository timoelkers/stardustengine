package de.oelkers.stardust.math.shapes;

import de.oelkers.stardust.math.vectors.ReadOnlyVector3f;
import de.oelkers.stardust.math.vectors.Vector3f;

import java.util.Objects;

public final class Plane {

    public enum PlaneSide {

        ON_PLANE,
        BACK,
        FRONT
    }

    private final Vector3f normal = new Vector3f();
    private float distance;

    public Plane() {}

    public Plane(ReadOnlyVector3f normal, float distance) {
        set(normal, distance);
    }

    public Plane(ReadOnlyVector3f normal, ReadOnlyVector3f point) {
        set(normal, point);
    }

    public Plane(ReadOnlyVector3f point1, ReadOnlyVector3f point2, ReadOnlyVector3f point3) {
        set(point1, point2, point3);
    }

    public void set(ReadOnlyVector3f normal, float distance) {
        this.normal.set(normal).normalize();
        this.distance = distance;
    }

    public void set(ReadOnlyVector3f normal, ReadOnlyVector3f point) {
        this.normal.set(normal).normalize();
        distance = -this.normal.dot(point);
    }

    public void set(ReadOnlyVector3f point1, ReadOnlyVector3f point2, ReadOnlyVector3f point3) {
        Vector3f.POOL.consume(vector -> normal.set(point1).sub(point2).cross(point2.sub(point3, vector)).normalize());
        distance = -normal.dot(point1);
    }

    public float distance(ReadOnlyVector3f point) {
        return distance(point.getX(), point.getY(), point.getZ());
    }

    public float distance(float x, float y, float z) {
        return normal.dot(x, y, z) + distance;
    }

    public PlaneSide pointOnPlane(ReadOnlyVector3f point) {
        return pointOnPlane(point.getX(), point.getY(), point.getZ());
    }

    public PlaneSide pointOnPlane(float x, float y, float z) {
        float distance = distance(x, y, z);
        if (distance == 0) {
            return PlaneSide.ON_PLANE;
        }
        return distance < 0 ? PlaneSide.BACK : PlaneSide.FRONT;
    }

    public ReadOnlyVector3f getNormal() {
        return normal;
    }

    public float getDistance() {
        return distance;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Plane)) {
            return false;
        }
        Plane other = (Plane) obj;
        return normal.equals(other.normal) && distance == other.distance;
    }

    @Override
    public int hashCode() {
        return Objects.hash(normal, distance);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '[' + normal + ", " + distance + ']';
    }
}
