package de.oelkers.stardust.math.shapes;

import de.oelkers.stardust.camera.ViewFrustum;
import de.oelkers.stardust.math.transformations.Transform3D;
import de.oelkers.stardust.math.transformations.Transformable3D;
import de.oelkers.stardust.math.vectors.ReadOnlyVector3f;
import de.oelkers.stardust.math.vectors.Vector3f;
import de.oelkers.stardust.utils.ArrayUtils;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serial;

public abstract class CollisionComponent3D implements CollisionComponent {

    @Serial
    private static final long serialVersionUID = 1L;

    private final Transformable3D shapeTransform = new Transform3D();
    protected final Vector3f[] vertices;
    private transient Vector3f[] worldVertices;

    protected CollisionComponent3D(int vertexSize) {
        vertices = ArrayUtils.setAll(Vector3f::new, new Vector3f[vertexSize]);
        worldVertices = ArrayUtils.setAll(Vector3f::new, new Vector3f[vertexSize]);
    }

    public ReadOnlyVector3f[] getVertices() {
        return vertices;
    }

    public ReadOnlyVector3f[] getWorldVertices() {
        for (int i = 0; i < vertices.length; i++) {
            vertices[i].transform(shapeTransform.getModelMatrix(), worldVertices[i]);
        }
        return worldVertices;
    }

    public Transformable3D getShapeTransform() {
        return shapeTransform;
    }

    @Override
    public boolean isVisible(ViewFrustum frustum) {
        return frustum.isShapeInFrustum(getWorldVertices());
    }

    public abstract boolean intersects(BoundingBox boundingBox);

    public abstract boolean intersects(Ray ray);

    public abstract boolean contains(BoundingBox boundingBox);

    public abstract boolean contains(float x, float y, float z);

    public boolean contains(ReadOnlyVector3f point) {
        return contains(point.getX(), point.getY(), point.getZ());
    }

    @Serial
    private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
        stream.defaultReadObject();
        worldVertices = ArrayUtils.setAll(Vector3f::new, new Vector3f[vertices.length]);
    }
}
