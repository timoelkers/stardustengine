package de.oelkers.stardust.math.shapes;

import de.oelkers.stardust.math.vectors.ReadOnlyVector3f;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

public class Ray implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private final ReadOnlyVector3f origin;
    private final ReadOnlyVector3f direction;

    public Ray(ReadOnlyVector3f origin, ReadOnlyVector3f direction) {
        this.origin = origin;
        this.direction = direction;
    }

    public ReadOnlyVector3f getOrigin() {
        return origin;
    }

    public ReadOnlyVector3f getDirection() {
        return direction;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Ray ray = (Ray) obj;
        return origin.equals(ray.origin) && direction.equals(ray.direction);
    }

    @Override
    public int hashCode() {
        return Objects.hash(origin, direction);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "[origin = " + origin + ", direction = " + direction + ']';
    }
}
