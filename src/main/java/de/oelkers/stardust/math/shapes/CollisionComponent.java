package de.oelkers.stardust.math.shapes;

import de.oelkers.stardust.camera.ViewFrustum;
import de.oelkers.stardust.entities.Component;

public interface CollisionComponent extends Component {

    boolean isVisible(ViewFrustum frustum);
}
