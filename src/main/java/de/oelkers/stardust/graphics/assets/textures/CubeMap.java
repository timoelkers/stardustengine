package de.oelkers.stardust.graphics.assets.textures;

import de.oelkers.stardust.StardustEngine;
import de.oelkers.stardust.math.internal.MathDelegate;
import de.oelkers.stardust.utils.ByteSize;
import de.oelkers.stardust.utils.container.Container;
import de.oelkers.stardust.utils.container.FixedSizeContainer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL13;

import java.io.Serial;
import java.io.Serializable;

public final class CubeMap extends BaseTexture implements Serializable {

    private static final Logger LOGGER = LogManager.getLogger(CubeMap.class);

    private Runnable onDataInjected;

    public CubeMap(TextureData[] textureData, TextureFilter filter, TextureWrap wrap, float anisotropy, boolean compress) {
        super(checkIfValidCubeAndGetType(textureData));
        compressed = compress;
        internalSet(textureData);
        setFilter(filter);
        setWrap(wrap);
        setAnisotropicValue(anisotropy);
    }

    /**
     * Creates a cube map without any data associated with it. The data gets injected at runtime, including the width and
     * the height of the texture so don't rely on that. You can specify different parameter for the texture, including whether the
     * data should be compressed before uploading it to the GPU. Setting this for already compressed textures has no effect.
     * This may cause longer loading times but increases performance. Please note that changing the parameters specified here
     * at runtime without any data being injected yet has no effect.
     *
     * @param filter the texture filter of the texture
     * @param wrap the texture wrap of the texture
     * @param anisotropy the anisotropy value of the texture
     * @param compress whether the image data should be compressed
     */
    public CubeMap(TextureFilter filter, TextureWrap wrap, float anisotropy, boolean compress) {
        super(TextureType.CUBE_MAP);
        compressed = compress;
        onDataInjected = () -> {
            setFilter(filter);
            setWrap(wrap);
            setAnisotropicValue(anisotropy);
        };
    }

    @Override
    public void inject(Container<TextureData> container) {
        TextureData[] textureData = container.getMultiple();
        checkDimensionAndMipMaps(textureData);
        internalSet(textureData);
    }

    @Override
    public Container<TextureData> getContainer() {
        return new FixedSizeContainer<>(6);
    }

    public void set(TextureData[] data) {
        checkIfValidCubeAndGetType(data);
        internalSet(data);
    }

    public void internalSet(TextureData[] data) {
        mipMapsExist = data[0].sizes.length > 1;
        height = data[0].height;
        width = data[0].width;
        bind(0);
        uploadTextureData(data);
        GL11.glTexParameteri(textureType.getGLCode(), GL12.GL_TEXTURE_MAX_LEVEL, data[0].sizes.length - 1);
        textureDataUploaded = true;
        if (onDataInjected != null) {
            onDataInjected.run();
            onDataInjected = null;
        }
    }

    private void uploadTextureData(TextureData[] data) {
        for (int i = 0; i < data.length; i++) {
            int bufferPosition = 0;
            for (int mipMap = 0; mipMap < data[i].sizes.length; mipMap++) {
                data[i].imageData.position(bufferPosition);
                data[i].imageData.limit(bufferPosition + data[i].sizes[mipMap]);
                int mipWidth = (int) (width / MathDelegate.pow(2, mipMap));
                int mipHeight = (int) (height / MathDelegate.pow(2, mipMap));
                bufferPosition += data[i].sizes[mipMap];
                if (data[i].format.isCompressed()) {
                    GL13.glCompressedTexImage2D(GL13.GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, mipMap, data[i].format.getGLCode(), mipWidth, mipHeight, 0, data[i].imageData);
                    byteSize = ByteSize.ofBytes(GL11.glGetTexLevelParameteri(GL13.GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, mipMap, GL13.GL_TEXTURE_COMPRESSED_IMAGE_SIZE));
                    LOGGER.log(StardustEngine.ALLOCATION, byteSize + " bytes of compressed texture data were uploaded (level = " + mipMap + ")!");
                    compressed = true;
                } else if (compressed) {
                    GL11.glTexImage2D(GL13.GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, mipMap, GL13.GL_COMPRESSED_RGBA, mipWidth, mipHeight, 0, data[i].format.getGLCode(), GL11.GL_UNSIGNED_BYTE, data[i].imageData);
                    int uncompressedSize = mipWidth * mipHeight * data[i].format.getBitsPerPixel() / 8;
                    byteSize = ByteSize.ofBytes(GL11.glGetTexLevelParameteri(GL13.GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, mipMap, GL13.GL_TEXTURE_COMPRESSED_IMAGE_SIZE));
                    LOGGER.log(StardustEngine.ALLOCATION, uncompressedSize + " bytes were compressed to " + byteSize + " and uploaded (level = " + mipMap + ")!");
                } else {
                    GL11.glTexImage2D(GL13.GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, mipMap, GL11.GL_RGBA8, mipWidth, mipHeight, 0, data[i].format.getGLCode(), GL11.GL_UNSIGNED_BYTE, data[i].imageData);
                    byteSize = ByteSize.ofBytes(mipWidth * mipHeight * data[i].format.getBitsPerPixel() / 8);
                    LOGGER.log(StardustEngine.ALLOCATION, byteSize + " bytes of uncompressed texture data were uploaded (level = " + mipMap + ")!");
                }
            }
        }
    }

    @Serial
    private Object writeReplace() {
        return writeReplace(SerializationProxy::new);
    }

    private static TextureType checkIfValidCubeAndGetType(TextureData[] textureData) {
        if (textureData.length != 6) {
            throw new IllegalArgumentException("The length of the array of texture data has to be 6 to form a cube!");
        }
        checkDimensionAndMipMaps(textureData);
        return TextureType.CUBE_MAP;
    }

    private static void checkDimensionAndMipMaps(TextureData[] textureData) {
        for (int i = 1; i < textureData.length; i++) {
            if (textureData[i].width != textureData[0].width || textureData[i].height != textureData[0].height) {
                throw new IllegalArgumentException("Width and height of all faces have to be the same for cube maps!");
            }
            if (textureData[i].sizes.length != textureData[0].sizes.length) {
                throw new IllegalArgumentException("The image data has to contain the same amount of mip maps for every face!");
            }
        }
    }

    private static final class SerializationProxy extends BaseSerializationProxy {

        @Serial
        private static final long serialVersionUID = 1L;

        private SerializationProxy(TextureData[] textureData, TextureFilter filter, TextureWrap wrap, float anisotropy) {
            super(textureData, filter, wrap, anisotropy);
        }

        @Serial
        private Object readResolve() {
            onResolve();
            return new CubeMap(textureData, filter, wrap, anisotropy, false);
        }
    }

}
