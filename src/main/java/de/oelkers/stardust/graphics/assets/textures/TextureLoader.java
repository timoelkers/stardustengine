package de.oelkers.stardust.graphics.assets.textures;

import de.oelkers.stardust.graphics.assets.loading.AsynchronousAssetLoader;
import de.oelkers.stardust.graphics.assets.textures.BaseTexture.TextureData;
import de.oelkers.stardust.graphics.assets.textures.BaseTexture.TextureFormat;
import de.oelkers.stardust.math.internal.MathDelegate;
import de.oelkers.stardust.utils.ArrayUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.*;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class TextureLoader implements AsynchronousAssetLoader<TextureData> {

    @Override
    public TextureData[] loadAsync(String... filePaths) {
        TextureData[] textureData = new TextureData[filePaths.length];
        for (int i = 0; i < filePaths.length; i++) {
            float rotation = i == 2 || i == 3 ? MathDelegate.PI_FLOAT : 0;
            textureData[i] = loadTexture(filePaths[i], TextureFormat.BGRA, rotation, true);
        }
        return textureData;
    }

    /**
     * Loads the image data from your given image and decodes it to provide the necessary data to construct a texture. You can specify the desired format as you
     * sometimes need your data to be in a specific format. If the desired format does not support alpha but the image has alpha values, then
     * the final format will be the equivalent format which supports alpha. <strong>No data is lost regardless of the chosen format.</strong><p>
     *
     * Additionally, you can specify if you want the texture to be conform with the left handed coordinate system for UVs, which is used
     * by this engine. This is contrary to most OpenGL cases, but helps with working with compressed textures, cube maps and external resources.
     * You can also specify the clock wise rotation of the image in radians.<p>
     *
     * This uses a very fast method of obtaining the image data for 24 and 32 bit depths and falls back to a slower but more reliable method
     * for different image formats, so you should stay on these bit depths.
     *
     * @param image the image to take the data from
     * @param desiredFormat the desired texture format
     * @param rotationRad the clock wise rotation of the image in radians
     * @param leftHandedUVs whether you want to use the texture for a left handed coordinate system
     * @return the texture data used to create the texture
     */
    public static TextureData loadTexture(BufferedImage image, TextureFormat desiredFormat, float rotationRad, boolean leftHandedUVs) {
        image = leftHandedUVs ? getRotatedImage(image, rotationRad) : getTransformedImage(image, rotationRad);
        return decodeImage(image, desiredFormat);
    }

    /**
     * Loads the image data from your given file path and decodes it to provide the necessary data to construct a texture. You can specify the desired format as you
     * sometimes need your data to be in a specific format. If the desired format does not support alpha but the image has alpha values, then
     * the final format will be the equivalent format which supports alpha. <strong>No data is lost regardless of the chosen format.</strong><p>
     *
     * Additionally, you can specify if you want the texture to be conform with the left handed coordinate system for UVs, which is used
     * by this engine. This is contrary to most OpenGL cases, but helps with working with compressed textures, cube maps and external resources.
     * You can also specify the clock wise rotation of the image in radians.<p>
     *
     * This uses a very fast method of obtaining the image data for 24 and 32 bit depths and falls back to a slower but more reliable method
     * for different image formats, so you should stay on these bit depths.
     *
     * @param filePath the path to the texture file
     * @param desiredFormat the desired texture format
     * @param rotationRad the clock wise rotation of the image in radians
     * @param leftHandedUVs whether you want to use the texture for a left handed coordinate system
     * @return the texture data used to create the texture
     */
    public static TextureData loadTexture(String filePath, TextureFormat desiredFormat, float rotationRad, boolean leftHandedUVs) {
        return loadTexture(loadImage(filePath), desiredFormat, rotationRad, leftHandedUVs);
    }

    /**
     * Loads the image data from your given image and decodes it to provide the necessary data to construct a texture. The image is decoded
     * in the GPU native format BGRA (the format in which most file formats store there data in anyway). There is no rotation applied to the image
     * and you should use the texture for a left handed coordinate system.<p>
     *
     * This uses a very fast method of obtaining the image data for 24 and 32 bit depths and falls back to a slower but more reliable method
     * for different image formats, so you should stay on these bit depths.
     *
     * @param image the image to take the data from
     * @return the texture data used to create the texture
     */
    public static TextureData loadTexture(BufferedImage image) {
        return loadTexture(image, TextureFormat.BGRA, 0, true);
    }

    /**
     * Loads the image data from your given file path and decodes it to provide the necessary data to construct a texture. The image is decoded
     * in the GPU native format BGRA (the format in which most file formats store there data in anyway). There is no rotation applied to the image
     * and you should use the texture for a left handed coordinate system.<p>
     *
     * This uses a very fast method of obtaining the image data for 24 and 32 bit depths and falls back to a slower but more reliable method
     * for different image formats, so you should stay on these bit depths.
     *
     * @param filePath the path to the texture file
     * @return the texture data used to create the texture
     */
    public static TextureData loadTexture(String filePath) {
        return loadTexture(loadImage(filePath));
    }

    /**
     * Decodes a single color into the necessary data to construct a texture containing only this color.
     * The texture width and height will just be 1. The color is decoded in the GPU native format BGRA.
     *
     * @param color the color value between 0f and 1f in the RGBA format
     */
    public static TextureData loadTexture(Color color) {
        return loadTexture(new Color[] { color }, 1, 1);
    }

    /**
     * Decodes the given colors into the necessary data to construct a texture containing these colors.
     * The texture width is the number of elements in each row (must be constant) and the height
     * is the number of rows. The color is decoded in the GPU native format BGRA.
     *
     * @param colors the color values
     */
    public static TextureData loadTexture(Color[][] colors) {
        return loadTexture(ArrayUtils.flatten(colors, colors[0].length), colors[0].length, colors.length);
    }

    /**
     * Decodes the given colors into the necessary data to construct a texture containing these colors.
     * The color is decoded in the GPU native format BGRA.
     *
     * @param colors the color values
     * @param width the width of the texture
     * @param height the height of the texture
     */
    public static TextureData loadTexture(Color[] colors, int width, int height) {
        TextureData textureData = new TextureData();
        textureData.format = TextureFormat.BGRA;
        textureData.width = width;
        textureData.height = height;
        textureData.totalSize = width * height * textureData.format.getBitsPerPixel();
        textureData.sizes = new int[] { textureData.totalSize };
        textureData.imageData = ByteBuffer.allocateDirect(textureData.totalSize).order(ByteOrder.nativeOrder());
        for (Color color : colors) {
            textureData.imageData.put((byte) color.getBlue());
            textureData.imageData.put((byte) color.getGreen());
            textureData.imageData.put((byte) color.getRed());
            textureData.imageData.put((byte) color.getAlpha());
        }
        return textureData;
    }

    private static BufferedImage getRotatedImage(BufferedImage image, float rotation) {
        if (rotation == 0) {
            return image;
        }
        AffineTransform transform = new AffineTransform();
        transform.rotate(rotation, image.getWidth() / 2f, image.getHeight() / 2f);
        BufferedImageOp op = new AffineTransformOp(transform, AffineTransformOp.TYPE_BILINEAR);
        return op.filter(image, new BufferedImage(image.getWidth(), image.getHeight(), image.getType()));
    }

    private static BufferedImage getTransformedImage(BufferedImage image, float rotation) {
        AffineTransform transform = new AffineTransform();
        if (rotation != 0) {
            transform.rotate(rotation, image.getWidth() / 2f, image.getHeight() / 2f);
        }
        transform.scale(1, -1);
        transform.translate(0, -image.getHeight());
        BufferedImageOp op = new AffineTransformOp(transform, AffineTransformOp.TYPE_BILINEAR);
        return op.filter(image, new BufferedImage(image.getWidth(), image.getHeight(), image.getType()));
    }

    private static TextureData decodeImage(BufferedImage image, TextureFormat desiredFormat) {
        if (image.getType() == BufferedImage.TYPE_4BYTE_ABGR) {
            TextureData textureData = getTextureData(image, getFormatWithAlpha(desiredFormat));
            byte[] data = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
            if (desiredFormat == TextureFormat.RGBA) {
                textureData.imageData = ABGRtoRGBA(data);
            } else if (desiredFormat == TextureFormat.BGRA) {
                textureData.imageData = ABGRtoBGRA(data);
            } else {
                throw new IllegalStateException(desiredFormat + " is not supported yet!");
            }
            return textureData;
        }
        if (image.getType() == BufferedImage.TYPE_3BYTE_BGR) {
            TextureData textureData = getTextureData(image, desiredFormat);
            byte[] data = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
            if (desiredFormat == TextureFormat.RGBA) {
                textureData.imageData = BGRtoRGBA(data);
            } else if (desiredFormat == TextureFormat.RGB) {
                textureData.imageData = BGRtoRGB(data);
            } else if (desiredFormat == TextureFormat.BGRA) {
                textureData.imageData = BGRtoBGRA(data);
            } else if (desiredFormat == TextureFormat.BGR) {
                textureData.imageData = BGRtoBGR(data);
            } else {
                throw new IllegalStateException(desiredFormat + " is not supported yet!");
            }
            return textureData;
        }
        return decodeImageSlow(image);
    }

    private static TextureFormat getFormatWithAlpha(TextureFormat originalFormat) {
        if (originalFormat == TextureFormat.RGB) {
            return TextureFormat.RGBA;
        }
        return originalFormat == TextureFormat.BGR ? TextureFormat.BGRA : originalFormat;
    }

    private static TextureData getTextureData(RenderedImage image, TextureFormat desiredFormat) {
        TextureData textureData = new TextureData();
        textureData.width = image.getWidth();
        textureData.height = image.getHeight();
        textureData.totalSize = textureData.width * textureData.height * desiredFormat.getBitsPerPixel() / 8;
        textureData.sizes = new int[] { textureData.totalSize };
        textureData.format = desiredFormat;
        return textureData;
    }

    private static TextureData decodeImageSlow(BufferedImage image) {
        TextureData textureData = getTextureData(image, TextureFormat.BGRA);
        int[] pixels = image.getRGB(0, 0, image.getWidth(), image.getHeight(), null, 0, image.getWidth());
        textureData.imageData = ByteBuffer.allocateDirect(textureData.totalSize).order(ByteOrder.nativeOrder());
        for (int y = 0; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                int pixel = pixels[y * image.getWidth() + x];
                textureData.imageData.put((byte) (pixel & 0xFF));
                textureData.imageData.put((byte) (pixel >> 8 & 0xFF));
                textureData.imageData.put((byte) (pixel >> 16 & 0xFF));
                textureData.imageData.put((byte) (pixel >> 24 & 0xFF));
            }
        }
        textureData.imageData.flip();
        return textureData;
    }

    private static BufferedImage loadImage(String filePath) {
        try (InputStream fileStream = ClassLoader.getSystemResourceAsStream(filePath)) {
            if (fileStream == null) {
                throw new IllegalArgumentException("Unable to find " + filePath + " in the classpath!");
            }
            return ImageIO.read(fileStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static ByteBuffer BGRtoRGB(byte[] data) {
        ByteBuffer imageBuffer = ByteBuffer.allocateDirect(data.length).order(ByteOrder.nativeOrder());
        for (int i = 0; i < data.length; i += 3) {
            imageBuffer.put(data[i + 2]).put(data[i + 1]).put(data[i]);
        }
        return imageBuffer.flip();
    }

    private static ByteBuffer BGRtoRGBA(byte[] data) {
        int bufferSize = data.length + data.length / 3;
        ByteBuffer imageBuffer = ByteBuffer.allocateDirect(bufferSize).order(ByteOrder.nativeOrder());
        for (int i = 0; i < data.length; i += 3) {
            imageBuffer.put(data[i + 2]).put(data[i + 1]).put(data[i]).put((byte) 0xFF);
        }
        return imageBuffer.flip();
    }

    private static ByteBuffer BGRtoBGR(byte[] data) {
        return ByteBuffer.allocateDirect(data.length).put(data).flip().order(ByteOrder.nativeOrder());
    }

    private static ByteBuffer BGRtoBGRA(byte[] data) {
        int bufferSize = data.length + data.length / 3;
        ByteBuffer imageBuffer = ByteBuffer.allocateDirect(bufferSize).order(ByteOrder.nativeOrder());
        for (int i = 0; i < data.length; i += 3) {
            imageBuffer.put(data[i]).put(data[i + 1]).put(data[i + 2]).put((byte) 0xFF);
        }
        return imageBuffer.flip();
    }

    private static ByteBuffer ABGRtoRGBA(byte[] data) {
        ByteBuffer imageBuffer = ByteBuffer.allocateDirect(data.length).order(ByteOrder.nativeOrder());
        for (int i = 0; i < data.length; i += 4) {
            imageBuffer.put(data[i + 3]).put(data[i + 2]).put(data[i + 1]).put(data[i]);
        }
        return imageBuffer.flip();
    }

    private static ByteBuffer ABGRtoBGRA(byte[] data) {
        ByteBuffer imageBuffer = ByteBuffer.allocateDirect(data.length).order(ByteOrder.nativeOrder());
        for (int i = 0; i < data.length; i += 4) {
            imageBuffer.put(data[i + 1]).put(data[i + 2]).put(data[i + 3]).put(data[i]);
        }
        return imageBuffer.flip();
    }
}
