package de.oelkers.stardust.graphics.assets.models;

import de.oelkers.stardust.graphics.assets.models.BaseModel.PrimitiveMode;
import de.oelkers.stardust.math.internal.MathDelegate;
import de.oelkers.stardust.math.vectors.Vector2f;

public final class Model2DCreator {

    private static final int CIRCLE_VERTEX_COUNT = 50;

    private Model2DCreator() {}

    public static Model2D rectangle(boolean isStatic, boolean storeModelData) {
        Vector2f[] vertices = {
                new Vector2f(-1.0f, 1.0f), new Vector2f(-1.0f, -1.0f),
                new Vector2f(1.0f, 1.0f), new Vector2f(1.0f, -1.0f)
        };
        Vector2f[] uvs = {
                new Vector2f(0.0f, 0.0f), new Vector2f(0.0f, 1.0f),
                new Vector2f(1.0f, 0.0f), new Vector2f(1.0f, 1.0f)
        };
        Model2D model = new Model2D();
        model.addVertices(vertices, PrimitiveMode.TRIANGLE_STRIP, isStatic, storeModelData);
        model.addUVs(uvs, isStatic, storeModelData);
        return model;
    }

    public static Model2D triangle(boolean isStatic, boolean storeModelData) {
        Vector2f[] vertices = { new Vector2f(-1.0f, -1.0f), new Vector2f(1.0f, -1.0f), new Vector2f(0.0f, 1.0f) };
        Vector2f[] uvs = { new Vector2f(0.0f, 1.0f), new Vector2f(1.0f, 1.0f), new Vector2f(0.5f, 0.0f) };
        Model2D model = new Model2D();
        model.addVertices(vertices, PrimitiveMode.TRIANGLES, isStatic, storeModelData);
        model.addUVs(uvs, isStatic, storeModelData);
        return model;
    }

    public static Model2D line(boolean isStatic, boolean storeModelData) {
        Vector2f[] vertices = { new Vector2f(-1.0f, 0f), new Vector2f(1.0f, 0f) };
        Model2D model = new Model2D();
        model.addVertices(vertices, PrimitiveMode.LINES, isStatic, storeModelData);
        return model;
    }

    public static Model2D circle(boolean isStatic, boolean storeModelData) {
        Vector2f[] vertices = new Vector2f[CIRCLE_VERTEX_COUNT + 1];
        vertices[0] = new Vector2f(0, 0);
        for (int i = 1; i < vertices.length; i++) {
            double theta = MathDelegate.TWO_PI_DOUBLE * (i - 1) / (CIRCLE_VERTEX_COUNT - 1);
            float x = (float) MathDelegate.cos(theta);
            float y = (float) MathDelegate.sin(theta);
            vertices[i] = new Vector2f(x, y);
        }
        Vector2f[] uvs = new Vector2f[vertices.length];
        for (int i = 0; i < vertices.length; i++) {
            Vector2f vertex = vertices[i];
            float u = vertex.getX() / 2.0f + 0.5f;
            float v = 1 - (vertex.getY() / 2.0f + 0.5f);
            uvs[i] = new Vector2f(u, v);
        }
        Model2D model = new Model2D();
        model.addVertices(vertices, PrimitiveMode.TRIANGLE_FAN, isStatic, storeModelData);
        model.addUVs(uvs, isStatic, storeModelData);
        return model;
    }
}
