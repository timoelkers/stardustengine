package de.oelkers.stardust.graphics.assets.models;

import de.oelkers.stardust.OpenGLContext;
import de.oelkers.stardust.entities.ShamIdentifiedObject;
import de.oelkers.stardust.utils.NonThrowingAutoCloseable;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL32;

import java.util.HashMap;
import java.util.Map;

public abstract class BaseModel extends ShamIdentifiedObject implements NonThrowingAutoCloseable {

    public enum PrimitiveMode {

        TRIANGLES(GL11.GL_TRIANGLES),
        TRIANGLE_STRIP(GL11.GL_TRIANGLE_STRIP),
        TRIANGLE_FAN(GL11.GL_TRIANGLE_FAN),

        POINTS(GL11.GL_POINTS),
        LINES(GL11.GL_LINES),
        LINE_STRIP(GL11.GL_LINE_STRIP),
        LINE_LOOP(GL11.GL_LINE_LOOP),

        TRIANGLES_ADJACENCY(GL32.GL_TRIANGLES_ADJACENCY),
        TRIANGLE_STRIP_ADJACENCY(GL32.GL_TRIANGLE_STRIP_ADJACENCY),
        LINES_ADJACENCY(GL32.GL_LINES_ADJACENCY),
        LINE_STRIP_ADJACENCY(GL32.GL_LINE_STRIP_ADJACENCY);

        private final int glCode;

        PrimitiveMode(int glCode) {
            this.glCode = glCode;
        }

        public int getGlCode() {
            return glCode;
        }
    }

    protected final Map<String, BufferObject> vertexBuffer = new HashMap<>();
    protected PrimitiveMode primitiveMode;
    protected boolean indexed = false;
    protected int vertexCount, attributeCount;
    protected int totalByteSize;

    protected BaseModel() {
        super(GL30.glGenVertexArrays());
    }

    public PrimitiveMode getPrimitiveMode() {
        return primitiveMode;
    }

    public boolean isIndexed() {
        return indexed;
    }

    public int getVertexCount() {
        return vertexCount;
    }

    public int getAttributeCount() {
        return attributeCount;
    }

    public int getByteSize() {
        return totalByteSize;
    }

    public void bind() {
        OpenGLContext.get().bindVertexArray(getId());
    }

    /**
     * Sends a request to render your bound data to the GPU. This is the actual render call,
     * any uniforms or OpenGL states have to be set before calling this method. This method
     * starts the render process, it will return immediately regardless if the data has been
     * rendered to the frame buffer yet.
     */
    public void render() {
        if (vertexCount > 0) {
            if (indexed) {
                GL11.glDrawElements(primitiveMode.getGlCode(), vertexCount, GL11.GL_UNSIGNED_INT, 0);
            } else {
                GL11.glDrawArrays(primitiveMode.getGlCode(), 0, vertexCount);
            }
        }
    }

    @Override
    public void close() {
        for (BufferObject vbo : vertexBuffer.values()) {
            vbo.close();
        }
        GL30.glDeleteVertexArrays(getId());
        OpenGLContext.get().unbindVertexArray(getId());
    }
}
