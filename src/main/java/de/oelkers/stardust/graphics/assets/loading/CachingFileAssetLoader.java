package de.oelkers.stardust.graphics.assets.loading;

import de.oelkers.stardust.utils.ArrayUtils;
import de.oelkers.stardust.utils.container.Container;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class CachingFileAssetLoader extends FileAssetLoader {

    private static final Logger LOGGER = LogManager.getLogger(CachingFileAssetLoader.class);

    private final Map<FilePathsAndAssetTypeKey, Object[]> cachedAssetData = new HashMap<>();
    private final Map<FilePathsAndAssetTypeKey, List<AssetContainer<Object>>> currentlyLoadingAssets = new HashMap<>();
    private final Map<FilePathsAndAssetTypeKey, List<AssetContainer<Object>>> dependantAssetsForReload = new HashMap<>();

    /**
     * Enqueues the loading task for this asset with the matching registered loader, if the data has not
     * been loaded before. If it has been loaded, than the cached data is injected into the asset
     * container right away. This means, contrary to the {@link FileAssetLoader}, that this method has to be called
     * from the thread that currently has the OpenGL context. If its loaded for the first time, then the asset
     * data gets loaded on different threads asynchronously, you can not rely on the time and the order in which
     * the asset data gets injected. You are allowed to specify multiple files if the loader allows it
     * (e.g. for cube maps), an exception is propagated to the exception handler otherwise.
     * The order of files stays consistent across the loading process.
     *
     * @param asset the asset container the data gets injected in
     * @param filePath the path to the asset file
     * @param filePaths additional paths to dependant asset files
     * @param <T> the type of data that gets injected
     * @param <R> the type of the asset container
     */
    @SuppressWarnings("unchecked")
    @Override
    public synchronized <T, R extends AssetContainer<T>> R load(R asset, String filePath, String... filePaths) {
        String[] joinedPaths = ArrayUtils.prepend(filePaths, filePath);
        FilePathsAndAssetTypeKey cacheKey = new FilePathsAndAssetTypeKey(joinedPaths, asset.getClass());
        if (cachedAssetData.containsKey(cacheKey)) {
            injectCachedData(asset, cacheKey);
        } else if (currentlyLoadingAssets.containsKey(cacheKey)) {
            currentlyLoadingAssets.get(cacheKey).add((AssetContainer<Object>) asset);
        } else {
            currentlyLoadingAssets.put(cacheKey, new ArrayList<>());
            super.load(asset, filePath, filePaths);
        }
        return asset;
    }

    /**
     * Loads the asset data using {@link #load(AssetContainer, String, String...)} and watches all
     * provided paths for file content changes. If such changes happen, the asset data is reloaded and
     * enqueued to be reinjected once more. This only reloads the data once for the first asset then
     * enqueues the other asset contains to be injected with the cached data.
     *
     * @param asset the asset container the data gets injected in
     * @param filePath the path to the asset file
     * @param filePaths additional paths to dependant asset files
     * @param <T> the type of data that gets injected
     * @param <R> the type of the asset container
     */
    @SuppressWarnings("unchecked")
    @Override
    public synchronized <T, R extends AssetContainer<T>> R loadAndObserve(R asset, String filePath, String... filePaths) {
        String[] joinedPaths = ArrayUtils.prepend(filePaths, filePath);
        FilePathsAndAssetTypeKey cacheKey = new FilePathsAndAssetTypeKey(joinedPaths, asset.getClass());
        if (dependantAssetsForReload.containsKey(cacheKey)) {
            dependantAssetsForReload.get(cacheKey).add((AssetContainer<Object>) asset);
            injectCachedData(asset, cacheKey);
        } else {
            dependantAssetsForReload.put(cacheKey, new ArrayList<>());
            super.loadAndObserve(asset, filePath, filePaths);
        }
        return asset;
    }

    @Override
    protected void onAssetDataLoaded(String[] filePaths, Object[] data, Class<?> assetType) {
        FilePathsAndAssetTypeKey cacheKey = new FilePathsAndAssetTypeKey(filePaths, assetType);
        if (dependantAssetsForReload.containsKey(cacheKey)) {
            enqueueDataForCachedAssets(dependantAssetsForReload.get(cacheKey), data);
        }
        if (currentlyLoadingAssets.containsKey(cacheKey)) {
            List<AssetContainer<Object>> assets = currentlyLoadingAssets.get(cacheKey);
            enqueueDataForCachedAssets(assets, data);
            currentlyLoadingAssets.remove(cacheKey);
        }
        cachedAssetData.put(cacheKey, data);
        super.onAssetDataLoaded(filePaths, data, assetType);
    }

    @Override
    protected void onAssetDataReload(String filePath, String[] filePaths, Class<?> assetType) {
        cachedAssetData.remove(new FilePathsAndAssetTypeKey(ArrayUtils.prepend(filePaths, filePath), assetType));
        super.onAssetDataReload(filePath, filePaths, assetType);
    }

    private void enqueueDataForCachedAssets(Iterable<AssetContainer<Object>> assets, Object[] data) {
        for (AssetContainer<Object> asset : assets) {
            Container<Object> container = asset.getContainer();
            container.set(data);
            enqueueData(asset, container);
        }
    }

    @SuppressWarnings({"unchecked", "SuspiciousArrayCast"})
    private <T> void injectCachedData(AssetContainer<T> asset, FilePathsAndAssetTypeKey cacheKey) {
        LOGGER.info("Injecting cached asset data from " + String.join(", ", cacheKey.filePaths));
        Object[] assetData = cachedAssetData.get(cacheKey);
        Container<T> container = asset.getContainer();
        container.set((T[]) assetData);
        asset.inject(container);
    }

    private static final class FilePathsAndAssetTypeKey {

        private final String[] filePaths;
        private final Class<?> assetType;

        private FilePathsAndAssetTypeKey(String[] filePaths, Class<?> assetType) {
            this.filePaths = filePaths;
            this.assetType = assetType;
        }

        @Override
        public int hashCode() {
            return Objects.hash(Arrays.hashCode(filePaths), assetType);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            FilePathsAndAssetTypeKey other = (FilePathsAndAssetTypeKey) obj;
            return Arrays.equals(filePaths, other.filePaths) && assetType == other.assetType;
        }
    }
}
