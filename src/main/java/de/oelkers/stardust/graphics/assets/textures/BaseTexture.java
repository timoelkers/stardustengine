package de.oelkers.stardust.graphics.assets.textures;

import de.oelkers.stardust.OpenGLContext;
import de.oelkers.stardust.entities.ShamIdentifiedObject;
import de.oelkers.stardust.graphics.assets.loading.AssetContainer;
import de.oelkers.stardust.graphics.assets.textures.BaseTexture.TextureData;
import de.oelkers.stardust.math.internal.MathDelegate;
import de.oelkers.stardust.utils.ByteSize;
import de.oelkers.stardust.utils.NonThrowingAutoCloseable;
import de.oelkers.stardust.utils.buffers.BufferUtils;
import de.oelkers.stardust.window.HardwareLimitations;
import org.lwjgl.opengl.*;

import java.io.Serial;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public abstract class BaseTexture extends ShamIdentifiedObject implements AssetContainer<TextureData>, NonThrowingAutoCloseable {

    public enum TextureType {

        TEXTURE_1D(GL11.GL_TEXTURE_1D),
        TEXTURE_2D(GL11.GL_TEXTURE_2D),
        CUBE_MAP(GL13.GL_TEXTURE_CUBE_MAP);

        private final int glCode;

        TextureType(int glCode) {
            this.glCode = glCode;
        }

        public int getGLCode() {
            return glCode;
        }
    }

    public enum TextureFormat {

        RGB(GL11.GL_RGB, GL11.GL_RGB8, false, 24, false),
        RGBA(GL11.GL_RGBA, GL11.GL_RGBA8, true, 32, false),
        BGR(GL12.GL_BGR, false, 24, false),
        BGRA(GL12.GL_BGRA, true, 32, false),

        DXT1(EXTTextureCompressionS3TC.GL_COMPRESSED_RGB_S3TC_DXT1_EXT, false, 4, true),
        DXT1A(EXTTextureCompressionS3TC.GL_COMPRESSED_RGBA_S3TC_DXT1_EXT, true, 4, true),
        DXT3(EXTTextureCompressionS3TC.GL_COMPRESSED_RGBA_S3TC_DXT3_EXT, true, 8, true),
        DXT5(EXTTextureCompressionS3TC.GL_COMPRESSED_RGBA_S3TC_DXT5_EXT, true, 8, true);

        private final boolean hasAlpha, isCompressed;
        private final int glCode, alternative, bpp;

        public static TextureFormat from(int glCode) {
            for (TextureFormat format : values()) {
                if (format.glCode == glCode || format.alternative == glCode) {
                    return format;
                }
            }
            throw new RuntimeException("Texture format for code " + glCode + " does not exist!");
        }

        TextureFormat(int glCode, boolean hasAlpha, int bpp, boolean isCompressed) {
            this(glCode, glCode, hasAlpha, bpp, isCompressed);
        }

        TextureFormat(int glCode, int alternative, boolean hasAlpha, int bpp, boolean isCompressed) {
            this.glCode = glCode;
            this.alternative = alternative;
            this.hasAlpha = hasAlpha;
            this.bpp = bpp;
            this.isCompressed = isCompressed;
        }

        public int getGLCode() {
            return glCode;
        }

        public int getBitsPerPixel() {
            return bpp;
        }

        public boolean hasAlpha() {
            return hasAlpha;
        }

        public boolean isCompressed() {
            return isCompressed;
        }
    }

    public enum TextureFilter {

        NEAREST(GL11.GL_NEAREST, GL11.GL_NEAREST, false),
        LINEAR(GL11.GL_LINEAR, GL11.GL_LINEAR, false),
        MIP_MAP(GL11.GL_LINEAR_MIPMAP_LINEAR, GL11.GL_LINEAR, true);

        private final boolean mipMaps;
        private final int minFilter, magFilter;

        TextureFilter(int minFilter, int magFilter, boolean mipMaps) {
            this.mipMaps = mipMaps;
            this.minFilter = minFilter;
            this.magFilter = magFilter;
        }

        public int getMinFilterCode() {
            return minFilter;
        }

        public int getMagFilterCode() {
            return magFilter;
        }

        public boolean hasMipMaps() {
            return mipMaps;
        }
    }

    public enum TextureWrap {

        REPEAT(GL11.GL_REPEAT),
        CLAMP(GL12.GL_CLAMP_TO_EDGE);

        private final int glCode;

        TextureWrap(int glCode) {
            this.glCode = glCode;
        }

        public int getGLCode() {
            return glCode;
        }
    }

    protected final TextureType textureType;
    protected int width, height;
    protected boolean compressed;
    protected boolean mipMapsExist;
    protected boolean textureDataUploaded;
    protected ByteSize byteSize;

    private TextureFilter filter;
    private TextureWrap wrap;
    private float anisotropy;

    protected BaseTexture(TextureType textureType) {
        super(GL11.glGenTextures());
        this.textureType = textureType;
    }

    public void bind(int textureUnit) {
        OpenGLContext.get().bindTexture(getId(), textureType, textureUnit);
    }

    public boolean setWrap(TextureWrap wrap) {
        if (this.wrap == wrap || !textureDataUploaded) {
            return false;
        }
        GL11.glTexParameteri(textureType.getGLCode(), GL11.GL_TEXTURE_WRAP_S, wrap.getGLCode());
        GL11.glTexParameteri(textureType.getGLCode(), GL11.GL_TEXTURE_WRAP_T, wrap.getGLCode());
        if (textureType == TextureType.CUBE_MAP) {
            GL11.glTexParameteri(textureType.getGLCode(), GL12.GL_TEXTURE_WRAP_R, wrap.getGLCode());
        }
        this.wrap = wrap;
        return true;
    }

    public boolean setFilter(TextureFilter filter) {
        if (this.filter == filter || !textureDataUploaded) {
            return false;
        }
        if (filter.hasMipMaps() && !mipMapsExist) {
            mipMapsExist = true;
            GL30.glGenerateMipmap(textureType.getGLCode());
        }
        GL11.glTexParameteri(textureType.getGLCode(), GL11.GL_TEXTURE_MIN_FILTER, filter.minFilter);
        GL11.glTexParameteri(textureType.getGLCode(), GL11.GL_TEXTURE_MAG_FILTER, filter.magFilter);
        this.filter = filter;
        return true;
    }

    public boolean setAnisotropicValue(float anisotropy) {
        if (!textureDataUploaded) {
            return false;
        }
        boolean extensionSupported = HardwareLimitations.get().getCapabilities().GL_EXT_texture_filter_anisotropic;
        if (!extensionSupported || this.anisotropy == anisotropy) {
            return false;
        }
        anisotropy = MathDelegate.min(HardwareLimitations.get().getMaxAnisotropy(), anisotropy);
        GL11.glTexParameterf(textureType.getGLCode(), EXTTextureFilterAnisotropic.GL_TEXTURE_MAX_ANISOTROPY_EXT, anisotropy);
        this.anisotropy = anisotropy;
        return true;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean isCompressed() {
        return compressed;
    }

    public boolean isTextureDataPresent() {
        return textureDataUploaded;
    }

    public TextureType getTextureType() {
        return textureType;
    }

    public TextureFilter getTextureFilter() {
        return filter;
    }

    public TextureWrap getTextureWrap() {
        return wrap;
    }

    public float getAnisotropy() {
        return anisotropy;
    }

    public ByteSize getByteSize() {
        return byteSize;
    }

    @Override
    public void close() {
        GL11.glDeleteTextures(getId());
        OpenGLContext.get().unbindTexture(getId());
    }

    protected BaseSerializationProxy writeReplace(SerializationTarget target) {
        if (textureType == TextureType.CUBE_MAP) {
            return writeReplace(target, 6, GL13.GL_TEXTURE_CUBE_MAP_POSITIVE_X);
        }
        return writeReplace(target, 1, textureType.glCode);
    }

    private BaseSerializationProxy writeReplace(SerializationTarget target, int amount, int textureTarget) {
        // @FutureWork: we don´t serialize mip maps yet, this is fine if mip maps are generated by glGenerateMipmap
        // but other file formats (e.g. DDS) can also supply their own mip maps
        bind(0);
        TextureData[] textureData = new TextureData[amount];
        for (int i = 0; i < amount; i++) {
            textureData[i] = new TextureData();
            int glCode = GL11.glGetTexLevelParameteri(textureTarget + i, 0, GL11.GL_TEXTURE_INTERNAL_FORMAT);
            textureData[i].format = TextureFormat.from(glCode);
            textureData[i].width = width;
            textureData[i].height = height;
            textureData[i].totalSize = width * height * textureData[i].format.getBitsPerPixel() / 8;
            textureData[i].imageData = ByteBuffer.allocateDirect(textureData[i].totalSize).order(ByteOrder.nativeOrder());
            textureData[i].sizes = new int[]{textureData[i].totalSize};
            if (compressed) {
                GL13.glGetCompressedTexImage(textureTarget + i, 0, textureData[i].imageData);
            } else {
                GL11.glGetTexImage(textureTarget + i, 0, textureData[i].format.getGLCode(), GL11.GL_UNSIGNED_BYTE, textureData[i].imageData);
            }
        }
        return target.serialize(textureData, filter, wrap, anisotropy);
    }

    @FunctionalInterface
    protected interface SerializationTarget {

        BaseSerializationProxy serialize(TextureData[] textureData, TextureFilter filter, TextureWrap wrap, float anisotropy);
    }

    public static class TextureData implements Serializable {

        @Serial
        private static final long serialVersionUID = 1L;

        public transient ByteBuffer imageData;
        public int totalSize;
        public int[] sizes;
        public TextureFormat format;
        public int width, height;
    }

    protected static abstract class BaseSerializationProxy implements Serializable {

        @Serial
        private static final long serialVersionUID = 1L;

        protected final TextureData[] textureData;
        protected final TextureFilter filter;
        protected final TextureWrap wrap;
        protected final float anisotropy;
        protected final byte[][] data;

        protected BaseSerializationProxy(TextureData[] textureData, TextureFilter filter, TextureWrap wrap, float anisotropy) {
            this.textureData = textureData;
            this.filter = filter;
            this.wrap = wrap;
            this.anisotropy = anisotropy;
            data = new byte[textureData.length][];
            for (int i = 0; i < textureData.length; i++) {
                data[i] = BufferUtils.toArray(textureData[i].imageData);
            }
        }

        protected void onResolve() {
            for (int i = 0; i < textureData.length; i++) {
                textureData[i].imageData = ByteBuffer.allocateDirect(textureData[i].totalSize).order(ByteOrder.nativeOrder());
                textureData[i].imageData.put(data[i]);
            }
        }
    }
}
