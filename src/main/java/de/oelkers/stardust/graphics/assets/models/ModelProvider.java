package de.oelkers.stardust.graphics.assets.models;

import de.oelkers.stardust.utils.Lazy;
import de.oelkers.stardust.utils.NonThrowingAutoCloseable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import static de.oelkers.stardust.graphics.assets.models.Model2DCreator.*;
import static de.oelkers.stardust.graphics.assets.models.Model3DCreator.*;

public class ModelProvider implements NonThrowingAutoCloseable {

    private final static int DEFAULT_SPHERE_SUBDIVISIONS = 3;

    private final boolean storeModelData;
    private final NamedModel2D rectangle;
    private final NamedModel2D triangle;
    private final NamedModel2D circle;
    private final NamedModel2D line;
    private final NamedModel3D cube;
    private final NamedModel3D plane;
    private final NamedModel3D sphere;
    private final Map<Integer, Model3D> spheres = new HashMap<>();

    public ModelProvider(boolean storeModelData) {
        this.storeModelData = storeModelData;
        rectangle = new NamedModel2D("Rectangle", () -> rectangle(true, storeModelData));
        triangle = new NamedModel2D("Triangle", () -> triangle(true, storeModelData));
        circle = new NamedModel2D("Circle", () -> circle(true, storeModelData));
        line = new NamedModel2D("Line", () -> line(true, storeModelData));
        cube = new NamedModel3D("Cube", () -> cube(true, storeModelData));
        plane = new NamedModel3D("Plane", () -> plane(true, storeModelData));
        sphere = new NamedModel3D("Sphere", () -> sphere(true, storeModelData, DEFAULT_SPHERE_SUBDIVISIONS));
    }

    public Model2D getRectangle() {
        return rectangle.get();
    }

    public Model2D getUniqueRectangle() {
        return rectangle(false, storeModelData);
    }

    public Model2D getTriangle() {
        return triangle.get();
    }

    public Model2D getUniqueTriangle() {
        return triangle(false, storeModelData);
    }

    public Model2D getCircle() {
        return circle.get();
    }

    public Model2D getUniqueCircle() {
        return circle(false, storeModelData);
    }

    public Model2D getLine() {
        return line.get();
    }

    public Model2D getUniqueLine() {
        return line(false, storeModelData);
    }

    public Model3D getCube() {
        return cube.get();
    }

    public Model3D getUniqueCube() {
        return cube(false, storeModelData);
    }

    public Model3D getPlane() {
        return plane.get();
    }

    public Model3D getUniquePlane() {
        return plane(false, storeModelData);
    }

    public Model3D getSphere() {
        return sphere.get();
    }

    public Model3D getUniqueSphere() {
        return sphere(false, storeModelData, DEFAULT_SPHERE_SUBDIVISIONS);
    }

    public Model3D getSphere(int subdivision) {
        if (subdivision == DEFAULT_SPHERE_SUBDIVISIONS) {
            return getSphere();
        }
        return spheres.computeIfAbsent(subdivision, i -> sphere(true, storeModelData, subdivision));
    }

    public Model3D getUniqueSphere(int subdivisions) {
        return sphere(false, storeModelData, subdivisions);
    }

    public Iterable<NamedModel3D> getModel3D() {
        return List.of(cube, plane, sphere);
    }

    public Iterable<NamedModel2D> getModel2D() {
        return List.of(rectangle, triangle, circle, line);
    }

    @Override
    public void close() {
        rectangle.close();
        triangle.close();
        circle.close();
        line.close();
        cube.close();
        plane.close();
        sphere.close();
        for (Model3D model : spheres.values()) {
            model.close();
        }
    }

    private static abstract class NamedModel<T extends BaseModel> implements Supplier<T> {

        private final String name;
        private final Lazy<T> model;

        private NamedModel(String name, Supplier<T> model) {
            this.name = name;
            this.model = new Lazy<>(model);
        }

        @Override
        public String toString() {
            return name;
        }

        @Override
        public T get() {
            return model.get();
        }

        protected void close() {
            model.ifLoaded(BaseModel::close);
        }
    }

    public static final class NamedModel3D extends NamedModel<Model3D> {

        private NamedModel3D(String name, Supplier<Model3D> model) {
            super(name, model);
        }
    }

    public static final class NamedModel2D extends NamedModel<Model2D> {

        private NamedModel2D(String name, Supplier<Model2D> model) {
            super(name, model);
        }
    }
}
