package de.oelkers.stardust.graphics.assets.fonts;

import de.oelkers.stardust.entities.Component;
import de.oelkers.stardust.entities.Entity;
import de.oelkers.stardust.graphics.assets.fonts.FontData.CharacterData;
import de.oelkers.stardust.graphics.assets.loading.AssetContainer;
import de.oelkers.stardust.graphics.assets.loading.CachingFileAssetLoader;
import de.oelkers.stardust.graphics.assets.models.ModelProvider;
import de.oelkers.stardust.graphics.assets.textures.Texture1D;
import de.oelkers.stardust.graphics.assets.textures.Texture2D;
import de.oelkers.stardust.graphics.rendering.BlendMode;
import de.oelkers.stardust.graphics.rendering.RenderSystem;
import de.oelkers.stardust.graphics.rendering.ToggleRendering;
import de.oelkers.stardust.graphics.rendering.components.RenderComponent2D;
import de.oelkers.stardust.graphics.rendering.components.RenderTechniques.RenderComponentBuilder2D;
import de.oelkers.stardust.math.coordinates.*;
import de.oelkers.stardust.math.transformations.Transform2D;
import de.oelkers.stardust.math.transformations.TransformSpace;
import de.oelkers.stardust.math.transformations.Transformable2D;
import de.oelkers.stardust.math.transformations.TransformationType;
import de.oelkers.stardust.math.vectors.Vector2f;
import de.oelkers.stardust.utils.GraphicsStack;
import de.oelkers.stardust.utils.container.Container;
import de.oelkers.stardust.utils.container.FixedSizeContainer;
import net.jcip.annotations.ThreadSafe;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;

import static de.oelkers.stardust.graphics.rendering.components.RenderTechniques.withTexture;
import static de.oelkers.stardust.math.coordinates.ScreenCoordinateX.relative;
import static de.oelkers.stardust.math.coordinates.ScreenCoordinates.relative;
import static java.util.stream.IntStream.range;

@ThreadSafe
@SuppressWarnings("HardcodedLineSeparator")
public final class GuiText extends ToggleRendering implements AssetContainer<FontData> {

    private static final Texture1D SRC_FONT_COLOR = new Texture1D(Color.WHITE);

    private final Collection<Entity> characters = new ArrayList<>();
    private final ModelProvider modelProvider;
    private final ScreenClipSpaceConverter converter;
    private final Vector2f origin;
    private boolean usesLineWrap;
    private float lineWrapPos;
    private final int visibleLineBreakCodePoint;
    private final int invisibleLineBreakCodePoint;
    private float scale;
    private FontData fontData;
    private Color color;
    private final Texture1D fontColorTexture;
    private CharSequence text;
    private final int[] cachedCodePoints;
    private boolean fontDataInjected;
    private boolean charactersPreLoaded;
    private boolean newLineReached;
    private float width, height;
    private final int renderOrder;
    private final boolean fixedViewport;
    private final ClipRegion clipRegion;
    private final Transformable2D parentTransform;
    private final TransformationType[] transformationTypes;

    private GuiText(Builder builder) {
        super(builder.renderSystem);
        modelProvider = builder.modelProvider;
        text = builder.text;
        converter = builder.converter;
        cachedCodePoints = builder.cachedCodePoints;
        origin = builder.origin.asScreenSpaceCoordinates(converter);
        usesLineWrap = builder.lineWrapPos != null;
        lineWrapPos = usesLineWrap ? builder.lineWrapPos.asScreenSpaceCoordinate(converter) : 0;
        visibleLineBreakCodePoint = builder.visibleLineBreakCodePoint;
        invisibleLineBreakCodePoint = builder.invisibleLineBreakCodePoint;
        scale = builder.scale;
        color = builder.color;
        fontColorTexture = new Texture1D(builder.color);
        fontData = builder.fontData;
        renderOrder = builder.renderOrder;
        fixedViewport = builder.fixedViewport;
        clipRegion = builder.clipRegion;
        parentTransform = builder.parentTransform;
        transformationTypes = builder.transformationTypes;
        if (fontData != null) {
            fontDataInjected = true;
            updateCharacters();
        }
    }

    public CharSequence getText() {
        return text;
    }

    public void setText(CharSequence text) {
        if (!this.text.equals(text)) {
            this.text = text;
            updateCharacters();
        }
    }

    public ScreenCoordinateX getWidth() {
        return ScreenCoordinateX.absolute(width);
    }

    public ScreenCoordinateY getHeight() {
        return ScreenCoordinateY.absolute(height);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        if (!this.color.equals(color)) {
            fontColorTexture.set(color);
            this.color = color;
        }
    }

    public FontData getFontData() {
        return fontData;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        if (this.scale != scale) {
            this.scale = scale;
            updateCharacters();
        }
    }

    public void setLineWrapPos(ScreenCoordinateX lineWrapPos) {
        if (usesLineWrap || lineWrapPos != null) {
            if (lineWrapPos == null) {
                usesLineWrap = false;
                updateCharacters();
            } else {
                float newPos = lineWrapPos.asScreenSpaceCoordinate(converter);
                if (this.lineWrapPos != newPos) {
                    this.lineWrapPos = newPos;
                    updateCharacters();
                }
            }
        }
    }

    @Override
    public void inject(Container<FontData> container) {
        fontData = container.getOnly();
        fontDataInjected = true;
        updateCharacters();
    }

    @Override
    public Container<FontData> getContainer() {
        return new FixedSizeContainer<>(1);
    }

    @Override
    protected Collection<Entity> getEntities() {
        return characters;
    }

    // @Performance: consider adjusting the size of the characters and just set a new texture and change the transformation
    // this would mess up the render component order, so we should implement a system that checks the order
    // once in a while (possibly in self adjusted intervals) and corrects them
    private void updateCharacters() {
        if (fontDataInjected) {
            clearEntities();
            preloadCharacterIfNeeded();
            Vector2f cursor = new Vector2f(origin);
            height = fontData.getLineHeight() * scale;
            width = 0;
            newLineReached = false;
            if (usesLineWrap) {
                updateCharactersWithLineWrap(cursor);
            } else {
                text.codePoints().forEachOrdered(codePoint -> handleCharacter(codePoint, cursor));
            }
        }
    }

    private void updateCharactersWithLineWrap(Vector2f cursor) {
        int[] codePoints = text.codePoints().toArray();
        int breakIndex = getLineBreakIndex(codePoints, cursor.getX(), 0);
        for (int i = 0; i < codePoints.length; i++) {
            if (i == breakIndex + 1) {
                newLineReached = true;
                height += fontData.getLineHeight() * scale;
                cursor.set(origin.getX(), cursor.getY() + fontData.getLineHeight() * scale);
                breakIndex = getLineBreakIndex(codePoints, cursor.getX(), i);
            }
            handleCharacter(codePoints[i], cursor);
        }
    }

    private void handleCharacter(int codePoint, Vector2f cursor) {
        if (codePoint != invisibleLineBreakCodePoint && codePoint != '\n' && codePoint != '\r') {
            CharacterData characterData = fontData.getCharacterData(codePoint);
            if (codePoint != ' ') {
                Transformable2D transform = new Transform2D();
                if (parentTransform != null) {
                    parentTransform.addChild(transform, transformationTypes);
                }
                ScreenCoordinates dimension = ScreenCoordinates.absolute(characterData.getWidth() * scale, characterData.getHeight() * scale);
                Vector2f clipSpaceScale = dimension.asClipSpaceScale(converter);
                transform.scale(clipSpaceScale);
                ScreenCoordinates position = ScreenCoordinates.absolute(cursor.getX() + characterData.getXOffset() * scale, cursor.getY() + characterData.getYOffset() * scale);
                transform.translate(position.asClipSpaceTranslation(converter, clipSpaceScale), TransformSpace.WORLD);
                addNewCharacter(characterData.getTexture(), transform);
            }
            if (!newLineReached) {
                width += characterData.getXAdvance() * scale;
            }
            cursor.add(characterData.getXAdvance() * scale, 0);
        }
    }

    private int getLineBreakIndex(int[] codePoints, float currentXPos, int fromIndex) {
        int currentWrapIndex = 0;
        for (int i = fromIndex; i < codePoints.length; i++) {
            int codePoint = codePoints[i];
            if (codePoint == visibleLineBreakCodePoint || codePoint == invisibleLineBreakCodePoint) {
                currentWrapIndex = i;
            }
            if (codePoint == '\n') {
                return i;
            }
            if (codePoint == '\r') {
                if (i != codePoints.length - 1 && codePoints[i + 1] == '\n') {
                    return i + 1;
                }
                return i;
            }
            if (codePoint != invisibleLineBreakCodePoint) {
                CharacterData characterData = fontData.getCharacterData(codePoint);
                currentXPos += characterData.getXOffset() * scale;
                if (currentXPos > lineWrapPos) {
                    return currentWrapIndex == 0 ? i - 1 : currentWrapIndex;
                }
                currentXPos += characterData.getXAdvance() * scale;
            }
        }
        return codePoints.length;
    }

    private void addNewCharacter(Texture2D texture, Component additional) {
        Entity character = new Entity("character");
        RenderComponentBuilder2D builder = withTexture(modelProvider.getRectangle(), texture);
        builder.addColorReplacement(SRC_FONT_COLOR, fontColorTexture).setTolerance(0.5f);
        RenderComponent2D.Builder componentBuilder = builder.getBuilder();
        componentBuilder.withFixedViewport(fixedViewport);
        componentBuilder.withBlendMode(BlendMode.ALPHA);
        componentBuilder.withRenderOrder(renderOrder);
        componentBuilder.withClipRegion(clipRegion);
        character.addComponent(componentBuilder.build());
        character.addComponent(additional);
        addEntity(character);
    }

    private void preloadCharacterIfNeeded() {
        if (!charactersPreLoaded) {
            charactersPreLoaded = true;
            for (int codePoint : cachedCodePoints) {
                fontData.getCharacterData(codePoint).getTexture();
            }
        }
    }

    public static final class Builder {

        private final RenderSystem renderSystem;
        private final ModelProvider modelProvider;
        private Color color = Color.BLACK;
        private final ScreenClipSpaceConverter converter;
        private ScreenCoordinates origin = relative(0, 0);
        private ScreenCoordinateX lineWrapPos = relative(1);
        private int visibleLineBreakCodePoint = '\u0020';
        private int invisibleLineBreakCodePoint = '\u200B';
        private FontData fontData;
        private CharSequence text = "";
        private int[] cachedCodePoints = new int[0];
        private float scale = 1.0f;
        private int renderOrder = RenderComponent2D.Builder.DEFAULT_RENDER_ORDER;
        private boolean fixedViewport = false;
        private ClipRegion clipRegion;
        private Transformable2D parentTransform;
        private TransformationType[] transformationTypes;

        public Builder(GraphicsStack graphicsStack) {
            this(graphicsStack.getRenderSystem(), graphicsStack.getModelProvider(), graphicsStack.getWindow());
        }

        public Builder(RenderSystem renderSystem, ModelProvider modelProvider, ScreenClipSpaceConverter converter) {
            this.renderSystem = renderSystem;
            this.modelProvider = modelProvider;
            this.converter = converter;
        }

        public Builder withOrigin(ScreenCoordinateX xOrigin, ScreenCoordinateY yOrigin) {
            return withOrigin(ScreenCoordinates.from(xOrigin, yOrigin));
        }

        /**
         * Specifies the screen coordinates for the top left corner of the start of the text.
         * The default will be at the center of the screen.
         */
        public Builder withOrigin(ScreenCoordinates origin) {
            this.origin = origin;
            return this;
        }

        /**
         * Specifies the font used to render the text. If possible, you should instead use
         * an asset loader ({@link CachingFileAssetLoader} is a good tool for this task) to load
         * the font asynchronously. As this is the expected way of injecting the font, there
         * is no default font. When none is specified here, it is expected that it is injected at runtime.
         * You can also use {@link AngelCodeFontLoader#DEFAULT_FONT} if you want the actual default font.
         */
        public Builder withFont(FontData fontData) {
            this.fontData = fontData;
            return this;
        }

        public Builder withoutLineWrap() {
            lineWrapPos = null;
            return this;
        }

        /**
         * Specifies the code point of the unicode character that indicates where to break the text
         * if it does not fit the width defined by {@link #withLineWrapAt(ScreenCoordinateX)}.
         * The character is visible at all time and the default is the space (U+0020) character.
         *
         * @param codePoint the code point of the line break character
         */
        public Builder withVisibleLineBreakChar(int codePoint) {
            visibleLineBreakCodePoint = codePoint;
            if (invisibleLineBreakCodePoint == codePoint) {
                invisibleLineBreakCodePoint = '\uFFFF';
            }
            return this;
        }

        /**
         * Specifies the code point of the unicode character that indicates where to break the text
         * if it does not fit the width defined by {@link #withLineWrapAt(ScreenCoordinateX)}.
         * The character is never visible, even if line wrap is disabled and the default is
         * the zero width space character (U+200B).
         *
         * @param codePoint the code point of the line break character
         */
        public Builder withInvisibleLineBreakChar(int codePoint) {
            invisibleLineBreakCodePoint = codePoint;
            if (visibleLineBreakCodePoint == codePoint) {
                visibleLineBreakCodePoint = '\uFFFF';
            }
            return this;
        }

        /**
         * Specifies the screen position that acts a limit on how long the text can become.
         * If characters exceed the limit, they will be rendered on a new line instead.
         * The default position it at the right side of the screen.
         *
         * @param lineWrapPos the screen position that indicates where to wrap texts
         */
        public Builder withLineWrapAt(ScreenCoordinateX lineWrapPos) {
            this.lineWrapPos = lineWrapPos;
            return this;
        }

        /**
         * Specifies the code point range of characters that get loaded immediately, but are not displayed.
         * This is useful as individual characters are lazy loaded and a OpenGL has to be
         * present at that time. When you plan on setting text asynchronously, you have
         * to preload all characters you want to use.
         *
         * @param fromCodePoint the inclusive starting code point
         * @param toCodePoint   the exclusive ending code point
         */
        public Builder withPreLoadedCharacters(int fromCodePoint, int toCodePoint) {
            cachedCodePoints = range(fromCodePoint, toCodePoint).toArray();
            return this;
        }

        /**
         * Specifies the characters that get loaded immediately, but are not displayed.
         * This is useful as individual characters are lazy loaded and a OpenGL has to be
         * present at that time. When you plan on setting text asynchronously, you have
         * to preload all characters you want to use.
         *
         * @param cachedCharacters the characters you want to load immediately
         */
        public Builder withPreLoadedCharacters(CharSequence cachedCharacters) {
            cachedCodePoints = cachedCharacters.codePoints().distinct().toArray();
            return this;
        }

        /**
         * Specifies the scale of the font. The actual size is dependent on the original font
         * size, which you can query with {@link FontData#getSize()}.
         *
         * @param scale the scale applied to the font size
         */
        public Builder withScale(float scale) {
            this.scale = scale;
            return this;
        }

        public Builder withText(CharSequence text) {
            this.text = text;
            return this;
        }

        /**
         * Specifies the color of the font. The default color is {@link Color#BLACK}.
         *
         * @param color the color of the font
         */
        public Builder withColor(Color color) {
            this.color = color;
            return this;
        }

        /**
         * Specifies at which place this text should be rendered in comparison to everything else. A higher
         * number means it should be rendered later, resulting it being rendered on top of everything else.
         * For performance reasons, you should keep all elements of the same type in the same order (e.g.
         * everything 2D related together), as further optimisations can be applied if the order does not change too much.
         * The default value is the {@link RenderComponent2D.Builder#DEFAULT_RENDER_ORDER default 2D order}.
         *
         * @param renderOrder the order in which this text is rendered
         */
        public Builder withRenderOrder(int renderOrder) {
            this.renderOrder = renderOrder;
            return this;
        }

        public Builder withFixedViewport(boolean fixedViewport) {
            this.fixedViewport = fixedViewport;
            return this;
        }

        public Builder withClipRegion(ClipRegion clipRegion) {
            this.clipRegion = clipRegion;
            return this;
        }

        public Builder withParentTransform(Transformable2D parentTransform) {
            return withParentTransform(parentTransform, TransformationType.values());
        }

        public Builder withParentTransform(Transformable2D parentTransform, TransformationType... transformationTypes) {
            this.parentTransform = parentTransform;
            this.transformationTypes = transformationTypes;
            return this;
        }

        public GuiText build() {
            return new GuiText(this);
        }
    }
}
