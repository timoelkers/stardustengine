package de.oelkers.stardust.graphics.assets.fonts;

import de.oelkers.stardust.graphics.assets.fonts.FontData.CharacterData;
import de.oelkers.stardust.graphics.assets.loading.AsynchronousAssetLoader;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.awt.image.SampleModel;
import java.awt.image.WritableRaster;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class AngelCodeFontLoader implements AsynchronousAssetLoader<FontData> {

    public static final FontData DEFAULT_FONT = loadFont("fonts/LiberationMono.fnt");

    @Override
    public FontData[] loadAsync(String... filePaths) {
        if (filePaths.length != 1) {
            throw new IllegalStateException("You have to specify a single path to load fonts.");
        }
        return new FontData[] { loadFont(filePaths[0]) };
    }

    // @FutureWork: we should add support for kerning (how much the cursor should advance
    // depending on the previous and next character)
    public static FontData loadFont(String filePath) {
        InputStream inputStream = ClassLoader.getSystemResourceAsStream(filePath);
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            FontData fontData = new FontData();
            LocalTextureDataHolder localTextureData = new LocalTextureDataHolder();
            while ((line = reader.readLine()) != null) {
                String[] currentLineElements = line.split(" ");
                String lineIdentifier = currentLineElements[0].trim();
                Consumer<IdentifierValueConsumer> lineConsumer = getLineConsumer(currentLineElements);
                if ("info".equals(lineIdentifier)) {
                    readFontData(lineConsumer, fontData);
                } else if ("common".equals(lineIdentifier)) {
                    readCommonData(lineConsumer, fontData, localTextureData);
                } else if ("page".equals(lineIdentifier)) {
                    readTextureData(lineConsumer, localTextureData, filePath);
                } else if ("char".equals(lineIdentifier)) {
                    readCharacterData(lineConsumer, fontData, localTextureData);
                } else if ("chars".equals(lineIdentifier)) {
                    readCharactersData(lineConsumer, fontData);
                }
            }
            return fontData;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void readFontData(Consumer<IdentifierValueConsumer> lineConsumer, FontData fontData) {
        lineConsumer.accept((identifier, value) -> {
            if ("bold".equals(identifier)) {
                fontData.bold = Integer.parseInt(value) != 0;
            } else if ("italic".equals(identifier)) {
                fontData.italic = Integer.parseInt(value) != 0;
            } else if ("charset".equals(identifier)) {
                fontData.charset = value.replace("\"", "").isEmpty() ? "Unicode" : value;
            } else if ("smooth".equals(identifier)) {
                fontData.smoothing = Integer.parseInt(value) != 0;
            } else if ("size".equals(identifier)) {
                fontData.size = Integer.parseInt(value);
            }
        });
    }

    private static void readCommonData(Consumer<IdentifierValueConsumer> lineConsumer, FontData fontData, LocalTextureDataHolder textureData) {
        lineConsumer.accept((identifier, value) -> {
            if ("lineHeight".equals(identifier)) {
                fontData.lineHeight = Integer.parseInt(value);
            } else if ("base".equals(identifier)) {
                fontData.baseLine = Integer.parseInt(value);
            } else if ("pages".equals(identifier)) {
                textureData.textureData = new HashMap<>(Integer.parseInt(value));
            }
        });
    }

    private static void readTextureData(Consumer<IdentifierValueConsumer> lineConsumer, LocalTextureDataHolder textureData, String originalPath) {
        LocalTextureData localTextureData = new LocalTextureData();
        lineConsumer.accept((identifier, value) -> {
            if ("id".equals(identifier)) {
                localTextureData.pageId = Integer.parseInt(value);
            } else if ("file".equals(identifier)) {
                Path imagePath = Path.of(originalPath).getParent().resolve(value.replace("\"", ""));
                localTextureData.image = ImageIO.read(ClassLoader.getSystemResource(imagePath.toString()));
            }
        });
        textureData.textureData.put(localTextureData.pageId, localTextureData.image);
    }

    private static void readCharactersData(Consumer<IdentifierValueConsumer> lineConsumer, FontData fontData) {
        lineConsumer.accept((identifier, value) -> {
            if ("count".equals(identifier)) {
                fontData.characterData = new HashMap<>(Integer.parseInt(value));
            }
        });
    }

    private static void readCharacterData(Consumer<IdentifierValueConsumer> lineConsumer, FontData fontData, LocalTextureDataHolder textureData) {
        LocalCharacterData localCharacterData = new LocalCharacterData();
        CharacterData characterData = new CharacterData();
        lineConsumer.accept((identifier, value) -> {
            if ("id".equals(identifier)) {
                localCharacterData.codePoint = Integer.parseInt(value);
            } else if ("x".equals(identifier)) {
                localCharacterData.textureX = Integer.parseInt(value);
            } else if ("y".equals(identifier)) {
                localCharacterData.textureY = Integer.parseInt(value);
            } else if ("width".equals(identifier)) {
                characterData.width = Integer.parseInt(value);
            } else if ("height".equals(identifier)) {
                characterData.height = Integer.parseInt(value);
            } else if ("xoffset".equals(identifier)) {
                characterData.xOffset = Integer.parseInt(value);
            } else if ("yoffset".equals(identifier)) {
                characterData.yOffset = Integer.parseInt(value);
            } else if ("xadvance".equals(identifier)) {
                characterData.xAdvance = Integer.parseInt(value);
            } else if ("page".equals(identifier)) {
                localCharacterData.texturePage = Integer.parseInt(value);
            }
        });
        localCharacterData.apply(fontData, characterData, textureData);
    }

    private static Consumer<IdentifierValueConsumer> getLineConsumer(String[] currentLineElements) {
        return identifierValueConsumer -> {
            try {
                for (String token : currentLineElements) {
                    if (token.isEmpty()) {
                        continue;
                    }
                    String[] values = token.split("=");
                    if (values.length != 2) {
                        // @Robustness: this might happen if there are spaces in the value or identifier,
                        // which leads to incorrect splitting of the current line
                        // we do not read such values yet (e.g. with "face" identifier), so we ignore it for now
                        continue;
                    }
                    String identifier = values[0].trim();
                    String value = values[1].trim();
                    identifierValueConsumer.consume(identifier, value);
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        };
    }

    @FunctionalInterface
    private interface IdentifierValueConsumer {

        void consume(String identifier, String value) throws IOException;
    }

    private static final class LocalTextureDataHolder {

        private Map<Integer, BufferedImage> textureData;
    }

    private static final class LocalTextureData {

        private int pageId;
        private BufferedImage image;
    }

    private static final class LocalCharacterData {

        private int codePoint;
        private int texturePage;
        private int textureX, textureY;

        private void apply(FontData fontData, CharacterData characterData, LocalTextureDataHolder textureData) {
            if (characterData.getWidth() != 0 && characterData.getHeight() != 0) {
                BufferedImage image = textureData.textureData.get(texturePage);
                // don´t use BufferedImage#getSubimage because it shares the data with the original image, we need a copy
                SampleModel sampleModel = image.getSampleModel().createCompatibleSampleModel(characterData.getWidth(), characterData.getHeight());
                WritableRaster data = image.copyData(Raster.createWritableRaster(sampleModel, new Point(textureX, textureY)));
                data = data.createWritableTranslatedChild(0, 0);
                characterData.textureImage = new BufferedImage(image.getColorModel(), data, image.isAlphaPremultiplied(), null);
            }
            fontData.characterData.put(codePoint, characterData);
        }
    }
}
