package de.oelkers.stardust.graphics.assets.textures;

import de.oelkers.stardust.graphics.assets.loading.AsynchronousAssetLoader;
import de.oelkers.stardust.graphics.assets.textures.BaseTexture.TextureData;
import de.oelkers.stardust.graphics.assets.textures.BaseTexture.TextureFormat;
import de.oelkers.stardust.math.internal.MathDelegate;
import de.oelkers.stardust.utils.streams.EndianDataInputStream;
import de.oelkers.stardust.window.HardwareLimitations;

import java.io.DataInput;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class DDSLoader implements AsynchronousAssetLoader<TextureData> {

    private static final int DDS_MAGIC_NUMBER = 0x20534444;
    private static final int DDS_HEADER_LENGTH = 124;
    private static final int DDSD_MANDATORY = 0x1007;
    private static final int DDSD_MANDATORY_DX10 = 0x6;
    private static final int DDSD_DEPTH = 0x800000;
    private static final int DDPF_SIZE = 32;
    private static final int DDPF_FOURCC = 0x4;
    private static final int DDPF_DXT1 = 0x31545844;
    private static final int DDPF_ALPHAPIXELS = 0x1;
    private static final int DDPF_DXT3 = 0x33545844;
    private static final int DDPF_DXT5 = 0x35545844;
    private static final int DDSCAPS_TEXTURE = 0x1000;
    private static final int DDSCAPS2_CUBEMAP = 0x200;
    private static final int DDSCAPS2_VOLUME = 0x200000;

    @Override
    public TextureData[] loadAsync(String... filePaths) {
        if (filePaths.length != 1) {
            throw new IllegalStateException("Cube maps from DDS files are not supported yet!");
        }
        return new TextureData[] { loadTexture(filePaths[0]) };
    }

    public static TextureData loadTexture(String filePath) {
        if (!HardwareLimitations.get().getCapabilities().GL_EXT_texture_compression_s3tc) {
            throw new IllegalStateException("DDS files are not supported by your GPU!");
        }
        InputStream fileStream = ClassLoader.getSystemResourceAsStream(filePath);
        try (EndianDataInputStream inputStream = new EndianDataInputStream(fileStream).setByteOrder(ByteOrder.LITTLE_ENDIAN)) {
            TextureData textureData = new TextureData();
            readHeader(inputStream, filePath, textureData);
            readData(inputStream, textureData);
            return textureData;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private static void readHeader(DataInput inputStream, String filePath, TextureData textureData) throws IOException {
        if (inputStream.readInt() != DDS_MAGIC_NUMBER) {
            throw new IOException("Incorrect magic number, " + filePath + " is not a valid dds file!");
        }
        if (inputStream.readInt() != DDS_HEADER_LENGTH) {
            throw new IOException("Incorrect header length, " + filePath + " is probably corrupted!");
        }
        int flags = inputStream.readInt();
        if (!isFlagSet(flags, DDSD_MANDATORY) && !isFlagSet(flags, DDSD_MANDATORY_DX10)) {
            throw new IOException("Mandatory flags are missing, " + filePath + " is probably corrupted!");
        }
        if (isFlagSet(flags, DDSD_DEPTH)) {
            throw new IOException("Volume textures are not supported yet!");
        }
        textureData.height = inputStream.readInt();
        textureData.width = inputStream.readInt();
        inputStream.skipBytes(2 * Integer.BYTES);
        int mipMapCount = inputStream.readInt();
        inputStream.skipBytes(11 * Integer.BYTES);
        textureData.format = readPixelFormat(inputStream, filePath);
        if (!isFlagSet(inputStream.readInt(), DDSCAPS_TEXTURE)) {
            throw new IOException(filePath + " does not contain a valid texture!");
        }
        int caps2 = inputStream.readInt();
        if (isFlagSet(caps2, DDSCAPS2_CUBEMAP)) {
            throw new IOException("Cube map textures are not supported yet!");
        }
        if (isFlagSet(caps2, DDSCAPS2_VOLUME)) {
            throw new IOException("Volume textures are not supported yet!");
        }
        inputStream.skipBytes(3 * Integer.BYTES);
        loadSizes(textureData, mipMapCount);
    }

    private static TextureFormat readPixelFormat(DataInput inputStream, String filePath) throws IOException {
        if (inputStream.readInt() != DDPF_SIZE) {
            throw new IOException("Incorrect pixel format header length, " + filePath + " is probably corrupted!");
        }
        int flags = inputStream.readInt();
        if (!isFlagSet(flags, DDPF_FOURCC)) {
            throw new IOException("Uncompressed DDS files are not supported yet!");
        }
        int fourcc = inputStream.readInt();
        inputStream.skipBytes(20);
        return switch (fourcc) {
            case DDPF_DXT1 -> isFlagSet(flags, DDPF_ALPHAPIXELS) ? TextureFormat.DXT1A : TextureFormat.DXT1;
            case DDPF_DXT3 -> TextureFormat.DXT3;
            case DDPF_DXT5 -> TextureFormat.DXT5;
            default -> throw new IllegalStateException("The DDS texture format identified by " + fourcc + " is not supported yet!");
        };
    }

    private static void loadSizes(TextureData textureData, int mipMapCount) {
        int mipWidth = textureData.width;
        int mipHeight = textureData.height;
        textureData.sizes = new int[mipMapCount];
        for (int i = 0; i < mipMapCount; i++) {
            int size = (mipWidth + 3) / 4 * ((mipHeight + 3) / 4) * textureData.format.getBitsPerPixel() * 2;
            textureData.sizes[i] = (size + 3) / 4 * 4;
            textureData.totalSize += textureData.sizes[i];
            mipWidth = MathDelegate.max(mipWidth / 2, 1);
            mipHeight = MathDelegate.max(mipHeight / 2, 1);
        }
    }

    private static void readData(DataInput inputStream, TextureData textureData) throws IOException {
        byte[] data = new byte[textureData.totalSize];
        inputStream.readFully(data);
        textureData.imageData = ByteBuffer.allocateDirect(textureData.totalSize);
        textureData.imageData.put(data);
        textureData.imageData.rewind();
    }

    private static boolean isFlagSet(int value, int flag) {
        return (value & flag) == flag;
    }
}
