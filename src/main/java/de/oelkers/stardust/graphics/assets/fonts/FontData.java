package de.oelkers.stardust.graphics.assets.fonts;

import de.oelkers.stardust.graphics.assets.textures.BaseTexture.TextureData;
import de.oelkers.stardust.graphics.assets.textures.BaseTexture.TextureFilter;
import de.oelkers.stardust.graphics.assets.textures.BaseTexture.TextureWrap;
import de.oelkers.stardust.graphics.assets.textures.Texture2D;
import de.oelkers.stardust.graphics.assets.textures.TextureLoader;
import de.oelkers.stardust.utils.Lazy;

import java.awt.image.BufferedImage;
import java.util.Map;

public final class FontData {

    String charset;
    boolean bold, italic, smoothing;
    int size, lineHeight, baseLine;
    Map<Integer, CharacterData> characterData;

    public String getCharset() {
        return charset;
    }

    public boolean isBold() {
        return bold;
    }

    public boolean isItalic() {
        return italic;
    }

    public boolean isSmoothed() {
        return smoothing;
    }

    public int getSize() {
        return size;
    }

    public int getBaseLine() {
        return baseLine;
    }

    public int getLineHeight() {
        return lineHeight;
    }

    public CharacterData getCharacterData(int codePoint) {
        return characterData.get(codePoint);
    }

    public static class CharacterData {

        BufferedImage textureImage;
        final Lazy<Texture2D> texture = new Lazy<>(() -> {
            TextureData textureData = TextureLoader.loadTexture(textureImage);
            // @Memory: the textureImage can safely be garbage collected, we just had to keep a reference
            // as we would not have the OpenGL context when loading the font asynchronously
            textureImage = null;
            return new Texture2D(textureData, TextureFilter.LINEAR, TextureWrap.CLAMP, 0, true);
        });
        int width, height;
        int xOffset, yOffset;
        int xAdvance;

        public Texture2D getTexture() {
            if (textureImage == null) {
                return texture.isLoaded() ? texture.get() : null;
            }
            return texture.get();
        }

        public int getWidth() {
            return width;
        }

        public int getHeight() {
            return height;
        }

        public int getXOffset() {
            return xOffset;
        }

        public int getYOffset() {
            return yOffset;
        }

        public int getXAdvance() {
            return xAdvance;
        }
    }
}
