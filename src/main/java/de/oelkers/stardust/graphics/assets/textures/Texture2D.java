package de.oelkers.stardust.graphics.assets.textures;

import de.oelkers.stardust.StardustEngine;
import de.oelkers.stardust.math.internal.MathDelegate;
import de.oelkers.stardust.utils.ByteSize;
import de.oelkers.stardust.utils.container.Container;
import de.oelkers.stardust.utils.container.FixedSizeContainer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL13;

import java.awt.*;
import java.io.Serial;
import java.io.Serializable;

public final class Texture2D extends BaseTexture implements Serializable {

    private static final Logger LOGGER = LogManager.getLogger(Texture2D.class);

    private Runnable onDataInjected;

    public Texture2D(TextureData textureData, TextureFilter filter, TextureWrap wrap, float anisotropy, boolean compress) {
        super(TextureType.TEXTURE_2D);
        compressed = compress;
        set(textureData);
        setFilter(filter);
        setWrap(wrap);
        setAnisotropicValue(anisotropy);
    }

    public Texture2D(Color[][] colors) {
        this(TextureLoader.loadTexture(colors), TextureFilter.NEAREST, TextureWrap.CLAMP, 0f, false);
    }

    public Texture2D(int width, int height, Color... colors) {
        this(TextureLoader.loadTexture(colors, width, height), TextureFilter.NEAREST, TextureWrap.CLAMP, 0f, false);
    }

    public Texture2D(Color color) {
        this(TextureLoader.loadTexture(color), TextureFilter.NEAREST, TextureWrap.CLAMP, 0f, false);
    }

    /**
     * Creates a 2D texture object without any data associated with it. The data gets injected at runtime, including the width
     * and the height of the texture so don't rely on that. You can specify different parameter for the texture, including whether the
     * data should be compressed before uploading it to the GPU. Setting this for already compressed textures has no effect.
     * This may cause longer loading times but increases performance. Please note that changing the parameters specified here
     * at runtime without any data being injected yet has no effect.
     *
     * @param filter the texture filter of the texture
     * @param wrap the texture wrap of the texture
     * @param anisotropy the anisotropy value of the texture
     * @param compress whether the image data should be compressed
     */
    public Texture2D(TextureFilter filter, TextureWrap wrap, float anisotropy, boolean compress) {
        super(TextureType.TEXTURE_2D);
        compressed = compress;
        onDataInjected = () -> {
            setFilter(filter);
            setWrap(wrap);
            setAnisotropicValue(anisotropy);
        };
    }

    @Override
    public void inject(Container<TextureData> container) {
        set(container.getOnly());
    }

    @Override
    public Container<TextureData> getContainer() {
        return new FixedSizeContainer<>(1);
    }

    public void set(Color[][] colors) {
        set(TextureLoader.loadTexture(colors));
    }

    public void set(int width, int height, Color... colors) {
        set(TextureLoader.loadTexture(colors, width, height));
    }

    public void set(Color color) {
        set(TextureLoader.loadTexture(color));
    }

    public void set(TextureData textureData) {
        mipMapsExist = textureData.sizes.length > 1;
        height = textureData.height;
        width = textureData.width;
        bind(0);
        uploadTextureData(textureData);
        GL11.glTexParameteri(textureType.getGLCode(), GL12.GL_TEXTURE_MAX_LEVEL, textureData.sizes.length - 1);
        textureDataUploaded = true;
        if (onDataInjected != null) {
            onDataInjected.run();
            onDataInjected = null;
        }
    }

    private void uploadTextureData(TextureData data) {
        int bufferPosition = 0;
        for (int mipMap = 0; mipMap < data.sizes.length; mipMap++) {
            data.imageData.position(bufferPosition);
            data.imageData.limit(bufferPosition + data.sizes[mipMap]);
            int mipWidth = (int) (data.width / MathDelegate.pow(2, mipMap));
            int mipHeight = (int) (data.height / MathDelegate.pow(2, mipMap));
            bufferPosition += data.sizes[mipMap];
            if (data.format.isCompressed()) {
                GL13.glCompressedTexImage2D(textureType.getGLCode(), mipMap, data.format.getGLCode(), mipWidth, mipHeight, 0, data.imageData);
                byteSize = ByteSize.ofBytes(GL11.glGetTexLevelParameteri(textureType.getGLCode(), mipMap, GL13.GL_TEXTURE_COMPRESSED_IMAGE_SIZE));
                LOGGER.log(StardustEngine.ALLOCATION, byteSize + " bytes of compressed texture data were uploaded (level = " + mipMap + ")!");
                compressed = true;
            } else if (compressed) {
                GL11.glTexImage2D(textureType.getGLCode(), mipMap, GL13.GL_COMPRESSED_RGBA, mipWidth, mipHeight, 0, data.format.getGLCode(), GL11.GL_UNSIGNED_BYTE, data.imageData);
                int uncompressedSize = mipWidth * mipHeight * data.format.getBitsPerPixel() / 8;
                byteSize = ByteSize.ofBytes(GL11.glGetTexLevelParameteri(textureType.getGLCode(), mipMap, GL13.GL_TEXTURE_COMPRESSED_IMAGE_SIZE));
                LOGGER.log(StardustEngine.ALLOCATION, uncompressedSize + " bytes were compressed to " + byteSize + " and uploaded (level = " + mipMap + ")!");
            } else {
                GL11.glTexImage2D(textureType.getGLCode(), mipMap, GL11.GL_RGBA8, mipWidth, mipHeight, 0, data.format.getGLCode(), GL11.GL_UNSIGNED_BYTE, data.imageData);
                byteSize = ByteSize.ofBytes(mipWidth * mipHeight * data.format.getBitsPerPixel() / 8);
                LOGGER.log(StardustEngine.ALLOCATION, byteSize + " bytes of uncompressed texture data were uploaded (level = " + mipMap + ")!");
            }
        }
    }

    @Serial
    private Object writeReplace() {
        return writeReplace(SerializationProxy::new);
    }

    private static final class SerializationProxy extends BaseSerializationProxy {

        @Serial
        private static final long serialVersionUID = 1L;

        private SerializationProxy(TextureData[] textureData, TextureFilter filter, TextureWrap wrap, float anisotropy) {
            super(textureData, filter, wrap, anisotropy);
        }

        @Serial
        private Object readResolve() {
            onResolve();
            return new Texture2D(textureData[0], filter, wrap, anisotropy, false);
        }
    }
}
