package de.oelkers.stardust.graphics.assets.loading;

public interface AsynchronousAssetLoader<T> {

    T[] loadAsync(String... filePaths);
}
