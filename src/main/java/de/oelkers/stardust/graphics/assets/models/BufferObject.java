package de.oelkers.stardust.graphics.assets.models;

import de.oelkers.stardust.OpenGLContext;
import de.oelkers.stardust.StardustEngine;
import de.oelkers.stardust.utils.NonThrowingAutoCloseable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL31;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class BufferObject implements NonThrowingAutoCloseable {

    private static final Logger LOGGER = LogManager.getLogger(BufferObject.class);

    public enum BufferTarget {

        INDEX_BUFFER(GL15.GL_ELEMENT_ARRAY_BUFFER),
        VERTEX_BUFFER(GL15.GL_ARRAY_BUFFER),
        TEXTURE_BUFFER(GL31.GL_TEXTURE_BUFFER),
        UNIFORM_BUFFER(GL31.GL_UNIFORM_BUFFER);

        private final int glCode;

        BufferTarget(int glCode) {
            this.glCode = glCode;
        }

        public int getGlCode() {
            return glCode;
        }
    }

    private final int bufferHandle;
    private final BufferTarget target;
    private final boolean isStatic;
    private final int byteSize;

    public BufferObject(BufferTarget target, ByteBuffer data, String dataUsage, boolean isStatic) {
        this.target = target;
        this.isStatic = isStatic;
        bufferHandle = GL15.glGenBuffers();
        byteSize = data.remaining();
        bind();
        GL15.glBufferData(target.getGlCode(), data, isStatic ? GL15.GL_STATIC_DRAW : GL15.GL_DYNAMIC_DRAW);
        LOGGER.log(StardustEngine.ALLOCATION, byteSize + " bytes of model data (" + dataUsage + ") were uploaded!");
    }

    public boolean isStatic() {
        return isStatic;
    }

    public int getByteSize() {
        return byteSize;
    }

    public void bind() {
        OpenGLContext.get().bindBuffer(bufferHandle, target);
    }

    public ByteBuffer getData() {
        ByteBuffer data = ByteBuffer.allocateDirect(byteSize).order(ByteOrder.nativeOrder());
        bind();
        GL15.glGetBufferSubData(target.getGlCode(), 0, data);
        return data;
    }

    public void update(ByteBuffer data) {
        bind();
        GL15.glBufferSubData(target.getGlCode(), 0, data);
    }

    @Override
    public void close() {
        GL15.glDeleteBuffers(bufferHandle);
        OpenGLContext.get().unbindBuffer(bufferHandle);
    }
}
