package de.oelkers.stardust.graphics.assets.models;

import de.oelkers.stardust.graphics.assets.models.BaseModel.PrimitiveMode;
import de.oelkers.stardust.math.internal.MathDelegate;
import de.oelkers.stardust.math.vectors.Vector2f;
import de.oelkers.stardust.math.vectors.Vector3f;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class Model3DCreator {

    private Model3DCreator() {}

    public static Model3D cube(boolean isStatic, boolean storeModelData) {
        Vector3f[] vertices = {
                new Vector3f(1.0f, -1.0f, -1.0f), new Vector3f(1.0f, -1.0f, 1.0f), new Vector3f(-1.0f, -1.0f, 1.0f), new Vector3f(-1.0f, -1.0f, -1.0f),
                new Vector3f(1.0f, 1.0f, -1.0f), new Vector3f(-1.0f, 1.0f, -1.0f), new Vector3f(-1.0f, 1.0f, 1.0f), new Vector3f(1.0f, 1.0f, 1.0f),
                new Vector3f(1.0f, -1.0f, -1.0f), new Vector3f(1.0f, 1.0f, -1.0f), new Vector3f(1.0f, 1.0f, 1.0f), new Vector3f(1.0f, -1.0f, 1.0f),
                new Vector3f(1.0f, -1.0f, 1.0f), new Vector3f(1.0f, 1.0f, 1.0f), new Vector3f(-1.0f, 1.0f, 1.0f), new Vector3f(-1.0f, -1.0f, 1.0f),
                new Vector3f(-1.0f, -1.0f, 1.0f), new Vector3f(-1.0f, 1.0f, 1.0f), new Vector3f(-1.0f, 1.0f, -1.0f), new Vector3f(-1.0f, -1.0f, -1.0f),
                new Vector3f(1.0f, 1.0f, -1.0f), new Vector3f(1.0f, -1.0f, -1.0f), new Vector3f(-1.0f, -1.0f, -1.0f), new Vector3f(-1.0f, 1.0f, -1.0f)
        };
        Vector3f[] normals = {
                new Vector3f(0, -1, 0), new Vector3f(0, -1, 0), new Vector3f(0, -1, 0), new Vector3f(0, -1, 0),
                new Vector3f(0, 1, 0), new Vector3f(0, 1, 0), new Vector3f(0, 1, 0), new Vector3f(0, 1, 0),
                new Vector3f(1, 0, 0), new Vector3f(1, 0, 0), new Vector3f(1, 0, 0), new Vector3f(1, 0, 0),
                new Vector3f(0, 0, 1), new Vector3f(0, 0, 1), new Vector3f(0, 0, 1), new Vector3f(0, 0, 1),
                new Vector3f(-1, 0, 0), new Vector3f(-1, 0, 0), new Vector3f(-1, 0, 0), new Vector3f(-1, 0, 0),
                new Vector3f(0, 0, -1), new Vector3f(0, 0, -1), new Vector3f(0, 0, -1), new Vector3f(0, 0, -1)
        };
        Vector2f[] uvs = {
                new Vector2f(0.0f, 0.0f), new Vector2f(0.0f, 1.0f), new Vector2f(1.0f, 1.0f), new Vector2f(1.0f, 0.0f),
                new Vector2f(1.0f, 0.0f), new Vector2f(0.0f, 0.0f), new Vector2f(0.0f, 1.0f), new Vector2f(1.0f, 1.0f),
                new Vector2f(1.0f, 1.0f), new Vector2f(1.0f, 0.0f), new Vector2f(0.0f, 0.0f), new Vector2f(0.0f, 1.0f),
                new Vector2f(1.0f, 1.0f), new Vector2f(1.0f, 0.0f), new Vector2f(0.0f, 0.0f), new Vector2f(0.0f, 1.0f),
                new Vector2f(1.0f, 1.0f), new Vector2f(1.0f, 0.0f), new Vector2f(0.0f, 0.0f), new Vector2f(0.0f, 1.0f),
                new Vector2f(0.0f, 0.0f), new Vector2f(0.0f, 1.0f), new Vector2f(1.0f, 1.0f), new Vector2f(1.0f, 0.0f)
        };
        int[] indices = {0, 1, 2, 0, 2, 3, 4, 5, 6, 4, 6, 7, 8, 9, 10, 8, 10, 11, 12, 13, 14, 12, 14, 15, 16, 17, 18, 16, 18, 19, 20, 21, 22, 20, 22, 23};

        Model3D model = new Model3D();
        model.addVertices(vertices, PrimitiveMode.TRIANGLES, isStatic, storeModelData);
        model.addIndices(indices, isStatic, storeModelData);
        model.addUVs(uvs, isStatic, storeModelData);
        model.addNormals(normals, isStatic, storeModelData);
        return model;
    }

    public static Model3D plane(boolean isStatic, boolean storeModelData) {
        Vector3f[] vertices = {
                new Vector3f(-1.0f, -1.0f, 0.0f), new Vector3f(1.0f, -1.0f, 0.0f),
                new Vector3f(1.0f, 1.0f, 0.0f), new Vector3f(-1.0f, 1.0f, 0.0f)
        };
        Vector2f[] uvs = {
                new Vector2f(0.0f, 1.0f), new Vector2f(1.0f, 1.0f),
                new Vector2f(1.0f, 0.0f), new Vector2f(0.0f, 0.0f)
        };
        Vector3f[] normals = {
                new Vector3f(0, 0, -1), new Vector3f(0, 0, -1),
                new Vector3f(0, 0, -1), new Vector3f(0, 0, -1)
        };
        int[] indices = {0, 2, 1, 0, 3, 2};

        Model3D model = new Model3D();
        model.addVertices(vertices, PrimitiveMode.TRIANGLES, isStatic, storeModelData);
        model.addIndices(indices, isStatic, storeModelData);
        model.addUVs(uvs, isStatic, storeModelData);
        model.addNormals(normals, isStatic, storeModelData);
        return model;
    }

    public static Model3D sphere(boolean isStatic, boolean storeModelData, int subdivisions) {
        List<Vector3f> vertices = new ArrayList<>();
        float theta = (float) ((1.0 + MathDelegate.sqrt(5.0)) / 2.0);
        vertices.add(new Vector3f(-1, theta, 0).normalize());
        vertices.add(new Vector3f(1, theta, 0).normalize());
        vertices.add(new Vector3f(-1, -theta, 0).normalize());
        vertices.add(new Vector3f(1, -theta, 0).normalize());
        vertices.add(new Vector3f(0, -1, theta).normalize());
        vertices.add(new Vector3f(0, 1, theta).normalize());
        vertices.add(new Vector3f(0, -1, -theta).normalize());
        vertices.add(new Vector3f(0, 1, -theta).normalize());
        vertices.add(new Vector3f(theta, 0, -1).normalize());
        vertices.add(new Vector3f(theta, 0, 1).normalize());
        vertices.add(new Vector3f(-theta, 0, -1).normalize());
        vertices.add(new Vector3f(-theta, 0, 1).normalize());
        int[] indices = {
                0, 11, 5, 0, 5, 1, 0, 1, 7, 0, 7, 10, 0, 10, 11, 1, 5, 9, 5, 11, 4, 11, 10, 2, 10, 7, 6, 7, 1,
                8, 3, 9, 4, 3, 4, 2, 3, 2, 6, 3, 6, 8, 3, 8, 9, 4, 9, 5, 2, 4, 11, 6, 2, 10, 8, 6, 7, 9, 8, 1
        };
        indices = subdivide(vertices, indices, subdivisions);
        Model3D model = new Model3D();
        model.addVertices(vertices.toArray(new Vector3f[0]), PrimitiveMode.TRIANGLES, isStatic, storeModelData);
        model.addIndices(indices, isStatic, storeModelData);
        return model;
    }

    private static int[] subdivide(List<Vector3f> vertices, int[] indices, int subdivisions) {
        if (subdivisions == 0) {
            return indices;
        }
        Map<Long, Integer> cachedMidpoints = new HashMap<>();
        int[] newIndices = new int[indices.length * 4];
        for (int index = 0; index < indices.length; index += 3) {
            int a = addMidPoint(cachedMidpoints, vertices, indices[index], indices[index + 1]);
            int b = addMidPoint(cachedMidpoints, vertices, indices[index + 1], indices[index + 2]);
            int c = addMidPoint(cachedMidpoints, vertices, indices[index + 2], indices[index]);
            newIndices[index * 4] = indices[index];
            newIndices[index * 4 + 1] = a;
            newIndices[index * 4 + 2] = c;
            newIndices[index * 4 + 3] = indices[index + 1];
            newIndices[index * 4 + 4] = b;
            newIndices[index * 4 + 5] = a;
            newIndices[index * 4 + 6] = indices[index + 2];
            newIndices[index * 4 + 7] = c;
            newIndices[index * 4 + 8] = b;
            newIndices[index * 4 + 9] = a;
            newIndices[index * 4 + 10] = b;
            newIndices[index * 4 + 11] = c;
        }
        return subdivide(vertices, newIndices, subdivisions - 1);
    }

    private static int addMidPoint(Map<Long, Integer> cachedMidpoints, List<Vector3f> vertices, int index1, int index2) {
        boolean firstSmaller = index1 < index2;
        long smallerIndex = firstSmaller ? index1 : index2;
        long greaterIndex = firstSmaller ? index2 : index1;
        long key = (smallerIndex << 32) + greaterIndex;
        if (cachedMidpoints.containsKey(key)) {
            return cachedMidpoints.get(key);
        }
        Vector3f pointA = vertices.get(index1);
        Vector3f pointB = vertices.get(index2);
        vertices.add(pointA.add(pointB, new Vector3f()).scale(0.5f).normalize());
        cachedMidpoints.put(key, vertices.size() - 1);
        return vertices.size() - 1;
    }
}
