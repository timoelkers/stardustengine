package de.oelkers.stardust.graphics.assets.models;

import de.oelkers.stardust.graphics.assets.loading.AssetContainer;
import de.oelkers.stardust.graphics.assets.models.BufferObject.BufferTarget;
import de.oelkers.stardust.math.vectors.Vector2f;
import de.oelkers.stardust.math.vectors.Vector3f;
import de.oelkers.stardust.utils.container.Container;
import de.oelkers.stardust.utils.container.FixedSizeContainer;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import java.io.Serial;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.Arrays;

import static de.oelkers.stardust.graphics.assets.models.Model3D.ModelData3D;
import static de.oelkers.stardust.utils.buffers.BufferUtils.*;

public class Model3D extends BaseModel implements AssetContainer<ModelData3D>, Serializable {

    private static final String VERTEX_IDENTIFIER = "vertices";
    private static final String INDEX_IDENTIFIER = "indices";
    private static final String UV_IDENTIFIER = "uvs";
    private static final String NORMAL_IDENTIFIER = "normals";

    private Vector3f[] vertices;
    private int[] indices;
    private Vector2f[] uvs;
    private Vector3f[] normals;

    @Override
    public void inject(Container<ModelData3D> container) {
        ModelData3D modelData = container.getOnly();
        if (modelData.vertices == null || modelData.primitiveMode == null || modelData.vertices.length == 0) {
            throw new IllegalStateException("You have to at least provide vertices and the primitive mode to create a model!");
        }
        addVertices(modelData.vertices, modelData.primitiveMode, true, false);
        if (modelData.indices != null && modelData.indices.length != 0) {
            addIndices(modelData.indices, true, false);
        }
        if (modelData.normals != null && modelData.normals.length != 0) {
            addNormals(modelData.normals, true, false);
        }
        if (modelData.uvs != null && modelData.uvs.length != 0) {
            addUVs(modelData.uvs, true, false);
        }
    }

    @Override
    public Container<ModelData3D> getContainer() {
        return new FixedSizeContainer<>(1);
    }

    /**
     * Adds vertices to the model and allocates the required memory on the GPU. The vertex data will be accessible on
     * the attribute binding 0 in the shader. You have to specify how the vertices should form triangles and whether the vertex data
     * is static. Static vertex data can not be updated anymore but it can be stored in VRAM. You can also specify whether the
     * vertex data should be stored with the model. It will not store a reference to the given array directly.
     *
     * @param vertices the vertices of the model
     * @param primitiveMode the primitive mode
     * @param isStatic whether the data is static
     * @param storeData whether the data is stored
     */
    public void addVertices(Vector3f[] vertices, PrimitiveMode primitiveMode, boolean isStatic, boolean storeData) {
        ByteBuffer buffer = storeUnsafe(vertices);
        vertexBuffer.put(VERTEX_IDENTIFIER, new BufferObject(BufferTarget.VERTEX_BUFFER, buffer, VERTEX_IDENTIFIER, isStatic));
        bind();
        GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 0, 0);
        GL20.glEnableVertexAttribArray(0);
        this.primitiveMode = primitiveMode;
        totalByteSize += buffer.remaining();
        attributeCount++;
        if (!indexed) {
            vertexCount = vertices.length;
        }
        if (storeData) {
            this.vertices = Arrays.copyOf(vertices, vertices.length);
        }
    }

    /**
     * Adds indices to the model and allocates the required memory on the GPU. Indices specify exactly how the given vertices
     * are connected and helps to reduce vertex data size. You have to specify whether the data is static.
     * Static data can not be updated anymore but it can be stored in VRAM. You can also specify whether the vertex data should
     * be stored with the model. It will not store a reference to the given array directly.
     *
     * @param indices the indices of the model
     * @param isStatic whether the data is static
     * @param storeData whether the data is stored
     */
    public void addIndices(int[] indices, boolean isStatic, boolean storeData) {
        ByteBuffer buffer = store(indices);
        vertexBuffer.put(INDEX_IDENTIFIER, new BufferObject(BufferTarget.INDEX_BUFFER, buffer, INDEX_IDENTIFIER, isStatic));
        totalByteSize += buffer.remaining();
        indexed = true;
        vertexCount = indices.length;
        if (storeData) {
            this.indices = Arrays.copyOf(indices, indices.length);
        }
    }

    /**
     * Adds uvs to the model and allocates the required memory on the GPU. The vertex data will be accessible on
     * the attribute binding 1 in the shader. You have to specify whether the vertex data is static.
     * Static vertex data can not be updated anymore but it can be stored in VRAM. You can also specify whether the
     * vertex data should be stored with the model. It will not store a reference to the given array directly.
     *
     * @param uvs the uvs of the model
     * @param isStatic whether the data is static
     * @param storeData whether the data is stored
     */
    public void addUVs(Vector2f[] uvs, boolean isStatic, boolean storeData) {
        ByteBuffer buffer = storeUnsafe(uvs);
        vertexBuffer.put(UV_IDENTIFIER, new BufferObject(BufferTarget.VERTEX_BUFFER, buffer, UV_IDENTIFIER, isStatic));
        bind();
        GL20.glVertexAttribPointer(1, 2, GL11.GL_FLOAT, false, 0, 0);
        GL20.glEnableVertexAttribArray(1);
        totalByteSize += buffer.remaining();
        attributeCount++;
        if (storeData) {
            this.uvs = Arrays.copyOf(uvs, uvs.length);
        }
    }

    /**
     * Adds normals to the model and allocates the required memory on the GPU. The vertex data will be accessible on
     * the attribute binding 2 in the shader. You have to specify whether the vertex data is static.
     * Static vertex data can not be updated anymore but it can be stored in VRAM. You can also specify whether the
     * vertex data should be stored with the model. It will not store a reference to the given array directly.
     *
     * @param normals the normals of the model
     * @param isStatic whether the data is static
     * @param storeData whether the data is stored
     */
    public void addNormals(Vector3f[] normals, boolean isStatic, boolean storeData) {
        ByteBuffer buffer = storeUnsafe(normals);
        vertexBuffer.put(NORMAL_IDENTIFIER, new BufferObject(BufferTarget.VERTEX_BUFFER, buffer, NORMAL_IDENTIFIER, isStatic));
        bind();
        GL20.glVertexAttribPointer(2, 3, GL11.GL_FLOAT, false, 0, 0);
        GL20.glEnableVertexAttribArray(2);
        totalByteSize += buffer.remaining();
        attributeCount++;
        if (storeData) {
            this.normals = Arrays.copyOf(normals, normals.length);
        }
    }

    /**
     * Updates the vertices on the GPU with the data stored with this model and returns if it was successful. You have to change the
     * internal data and can not pass new data as the memory was already allocated and the size can therefore not change. If no vertex
     * data is stored locally or if the data is static, this will return false.
     */
    public boolean updateVertices() {
        BufferObject vbo = vertexBuffer.get(VERTEX_IDENTIFIER);
        if (!storesVertices() || vbo.isStatic()) {
            return false;
        }
        vbo.update(storeUnsafe(vertices));
        return true;
    }

    /**
     * Updates the indices on the GPU with the data stored with this model and returns if it was successful. You have to change the
     * internal data and can not pass new data as the memory was already allocated and the size can therefore not change. If no vertex
     * data is stored locally or if the data is static, this will return false.
     */
    public boolean updateIndices() {
        BufferObject vbo = vertexBuffer.get(INDEX_IDENTIFIER);
        if (!storesIndices() || vbo.isStatic()) {
            return false;
        }
        vbo.update(store(indices));
        return true;
    }

    /**
     * Updates the uvs on the GPU with the data stored with this model and returns if it was successful. You have to change the
     * internal data and can not pass new data as the memory was already allocated and the size can therefore not change. If no vertex
     * data is stored locally or if the data is static, this will return false.
     */
    public boolean updateUVs() {
        BufferObject vbo = vertexBuffer.get(UV_IDENTIFIER);
        if (!storesUVs() || vbo.isStatic()) {
            return false;
        }
        vbo.update(storeUnsafe(uvs));
        return true;
    }

    /**
     * Updates the normals on the GPU with the data stored with this model and returns if it was successful. You have to change the
     * internal data and can not pass new data as the memory was already allocated and the size can therefore not change. If no vertex
     * data is stored locally or if the data is static, this will return false.
     */
    public boolean updateNormals() {
        BufferObject vbo = vertexBuffer.get(NORMAL_IDENTIFIER);
        if (!storesNormals() || vbo.isStatic()) {
            return false;
        }
        vbo.update(storeUnsafe(normals));
        return true;
    }

    public boolean storesVertices() {
        return vertices != null;
    }

    public boolean storesIndices() {
        return indices != null;
    }

    public boolean storesUVs() {
        return uvs != null;
    }

    public boolean storesNormals() {
        return normals != null;
    }

    public Vector3f[] getVertices() {
        if (storesVertices()) {
            return vertices;
        }
        BufferObject vbo = vertexBuffer.get(VERTEX_IDENTIFIER);
        if (vbo == null) {
            return new Vector3f[0];
        }
        Vector3f[] vertices = new Vector3f[(int) (vbo.getByteSize() / Vector3f.SIZE.getBytes())];
        return load(vbo.getData(), vertices, Vector3f::new);
    }

    public int[] getIndices() {
        if (storesIndices()) {
            return indices;
        }
        BufferObject vbo = vertexBuffer.get(INDEX_IDENTIFIER);
        if (vbo == null) {
            return new int[0];
        }
        int[] indices = new int[(int) (vbo.getByteSize() / Integer.BYTES)];
        vbo.getData().asIntBuffer().get(indices);
        return indices;
    }

    public Vector2f[] getUVs() {
        if (storesUVs()) {
            return uvs;
        }
        BufferObject vbo = vertexBuffer.get(UV_IDENTIFIER);
        if (vbo == null) {
            return new Vector2f[0];
        }
        Vector2f[] uvs = new Vector2f[(int) (vbo.getByteSize() / Vector2f.SIZE.getBytes())];
        return load(vbo.getData(), uvs, Vector2f::new);
    }

    public Vector3f[] getNormals() {
        if (storesNormals()) {
            return normals;
        }
        BufferObject vbo = vertexBuffer.get(NORMAL_IDENTIFIER);
        if (vbo == null) {
            return new Vector3f[0];
        }
        Vector3f[] normals = new Vector3f[(int) (vbo.getByteSize() / Vector3f.SIZE.getBytes())];
        return load(vbo.getData(), normals, Vector3f::new);
    }

    @Serial
    protected Object writeReplace() {
        ModelData3D modelData = new ModelData3D();
        modelData.vertices = getVertices();
        modelData.primitiveMode = getPrimitiveMode();
        modelData.indices = getIndices();
        modelData.normals = getNormals();
        modelData.uvs = getUVs();
        return modelData;
    }

    public static class ModelData3D implements Serializable {

        @Serial
        private static final long serialVersionUID = 1L;

        public Vector3f[] vertices;
        public PrimitiveMode primitiveMode;
        public int[] indices;
        public Vector3f[] normals;
        public Vector2f[] uvs;

        @Serial
        protected Object readResolve() {
            AssetContainer<ModelData3D> model = new Model3D();
            Container<ModelData3D> container = model.getContainer();
            container.set(this);
            model.inject(container);
            return model;
        }
    }
}
