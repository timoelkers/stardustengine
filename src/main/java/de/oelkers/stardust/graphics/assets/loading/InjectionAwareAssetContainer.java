package de.oelkers.stardust.graphics.assets.loading;

import de.oelkers.stardust.utils.container.Container;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.function.Supplier;

public class InjectionAwareAssetContainer<T extends AssetContainer<R>, R> implements AssetContainer<R>, Supplier<T> {

    private final Collection<CallbackWithRemovalFlag> injectedCallbacks = new ArrayList<>();
    private final T delegate;

    public InjectionAwareAssetContainer(T delegate) {
        this.delegate = delegate;
    }

    public void addInjectedCallback(Runnable callback) {
        addInjectedCallback(callback, false);
    }

    public void addInjectedCallback(Runnable callback, boolean removeAfterUse) {
        injectedCallbacks.add(new CallbackWithRemovalFlag(callback, removeAfterUse));
    }

    @Override
    public T get() {
        return delegate;
    }

    @Override
    public void inject(Container<R> container) {
        delegate.inject(container);
        Iterator<CallbackWithRemovalFlag> iterator = injectedCallbacks.iterator();
        while (iterator.hasNext()) {
            CallbackWithRemovalFlag callback = iterator.next();
            callback.callback.run();
            if (callback.removeAfterUse) {
                iterator.remove();
            }
        }
    }

    @Override
    public Container<R> getContainer() {
        return delegate.getContainer();
    }

    private static final class CallbackWithRemovalFlag {

        private final Runnable callback;
        private final boolean removeAfterUse;

        private CallbackWithRemovalFlag(Runnable callback, boolean removeAfterUse) {
            this.callback = callback;
            this.removeAfterUse = removeAfterUse;
        }
    }
}
