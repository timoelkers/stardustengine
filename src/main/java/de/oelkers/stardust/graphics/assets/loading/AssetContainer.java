package de.oelkers.stardust.graphics.assets.loading;

import de.oelkers.stardust.utils.container.Container;

public interface AssetContainer<T> {

    void inject(Container<T> container);
    Container<T> getContainer();
}
