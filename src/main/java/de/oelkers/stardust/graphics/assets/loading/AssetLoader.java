package de.oelkers.stardust.graphics.assets.loading;

import de.oelkers.stardust.utils.container.Container;
import de.oelkers.stardust.utils.timer.DefaultStopWatch;
import de.oelkers.stardust.utils.timer.StopWatch;

import java.lang.Thread.UncaughtExceptionHandler;
import java.time.Duration;
import java.util.Queue;
import java.util.concurrent.*;
import java.util.function.Supplier;

/**
 * The asset loader is responsible for efficient loading of any assets implementing {@link AssetContainer}. It loads as
 * much as possible asynchronously and fills a queue with the relevant data. The final step on loading the asset
 * has to be done on the thread that currently has the OpenGL context, which is done by polling the queue with
 * {@link #update()}, which injects the loaded data into the asset container. Any exceptions in the asynchronous part
 * will be reported to the current threads uncaught exception handler and the asset will not be added to the queue.
 */
public class AssetLoader implements AutoCloseable {

    private final UncaughtExceptionHandler exceptionHandler = Thread.currentThread().getUncaughtExceptionHandler();
    private final ExecutorService executor = new ForkJoinPool(Runtime.getRuntime().availableProcessors(), AssetLoader::newThread, exceptionHandler, true);
    private final Queue<AssetData<?>> finishedTasks = new ConcurrentLinkedQueue<>();

    public <T, R extends AssetContainer<T>> R load(R asset, Supplier<T> assetData) {
        loadAsyncSingle(asset, assetData);
        return asset;
    }

    public int getFinishedTasksCount() {
        return finishedTasks.size();
    }

    /**
     * Polls the queue of finished loading tasks and loads the synchronous part of the asset. This typically results in being
     * able to see the asset on the screen. This method has to be called from the thread that has the OpenGL context.
     * After loading one asset, it checks for more finished tasks in the queue, so you might want to use {@link #update(Duration)} or
     * {@link #update(int)} to limit the amount when loading huge assets that might pile up while one is processed.
     * It does not wait for an asset to be available and returns immediately if the queue is empty.
     *
     * @return the number of loaded assets
     */
    public int update() {
        int assetsLoaded = 0;
        AssetData<?> assetData;
        while ((assetData = finishedTasks.poll()) != null) {
            assetData.inject();
            assetsLoaded++;
        }
        return assetsLoaded;
    }

    /**
     * Polls the queue of finished loading tasks and loads the synchronous part of the asset. This typically results in being
     * able to see the asset on the screen. This method has to be called from the thread that has the OpenGL context.
     * This loads as many assets as are in the queue until the given maximum has been reached. It does not wait for
     * an asset to be available and returns immediately if the queue is empty.
     *
     * @param maxAssets the maximum amount of assets to load
     * @return the number of loaded assets
     */
    public int update(int maxAssets) {
        int assetsLoaded = 0;
        AssetData<?> assetData;
        while (assetsLoaded < maxAssets && (assetData = finishedTasks.poll()) != null) {
            assetData.inject();
            assetsLoaded++;
        }
        return assetsLoaded;
    }

   /**
     * Polls the queue of finished loading tasks and loads the synchronous part of the asset. This typically results in being
     * able to see the asset on the screen. This method has to be called from the thread that has the OpenGL context.
     * This loads as many assets as are in the queue, but stops after the given timeout has been reached. It does not wait for
     * an asset to be available and returns immediately if the queue is empty.
     *
     * @param timeout the timeout on when to stop processing finished tasks
     * @return the number of loaded assets
     */
    public int update(Duration timeout) {
        return update(timeout, new DefaultStopWatch());
    }

    /**
     * Polls the queue of finished loading tasks and loads the synchronous part of the asset. This typically results in being
     * able to see the asset on the screen. This method has to be called from the thread that has the OpenGL context.
     * This loads as many assets as are in the queue, but stops after the given timeout has been reached. It does not wait for
     * an asset to be available and returns immediately if the queue is empty.
     *
     * @param timeout the timeout on when to stop processing finished tasks
     * @param stopWatch the stopwatch used to check the timeout
     * @return the number of loaded assets
     */
    public int update(Duration timeout, StopWatch stopWatch) {
        stopWatch.start();
        int assetsLoaded = 0;
        AssetData<?> assetData;
        while (stopWatch.getElapsedTime(TimeUnit.MILLISECONDS) < timeout.toMillis() && (assetData = finishedTasks.poll()) != null) {
            assetData.inject();
            assetsLoaded++;
        }
        stopWatch.stop();
        return assetsLoaded;
    }

    @Override
    public void close() {
        executor.shutdown();
    }

    protected <T> void loadAsyncArray(AssetContainer<T> asset, Supplier<T[]> assetData) {
        loadAsync(asset, () -> {
            Container<T> container = asset.getContainer();
            container.set(assetData.get());
            return container;
        });
    }

    protected <T> void loadAsyncSingle(AssetContainer<T> asset, Supplier<T> assetData) {
        loadAsync(asset, () -> {
            Container<T> container = asset.getContainer();
            container.set(assetData.get());
            return container;
        });
    }

    protected <T> void enqueueData(AssetContainer<T> assetContainer, Container<T> assetData) {
        finishedTasks.add(new AssetData<>(assetContainer, assetData));
    }

    private <T> void loadAsync(AssetContainer<T> asset, Supplier<Container<T>> assetData) {
        CompletableFuture.supplyAsync(assetData, executor).whenComplete((data, throwable) -> {
            if (throwable != null) {
                exceptionHandler.uncaughtException(Thread.currentThread(), throwable.getCause());
            } else {
                enqueueData(asset, data);
            }
        });
    }

    private static ForkJoinWorkerThread newThread(ForkJoinPool forkJoinPool) {
        ForkJoinWorkerThread worker = ForkJoinPool.defaultForkJoinWorkerThreadFactory.newThread(forkJoinPool);
        worker.setName("AssetLoadingThread " + worker.getPoolIndex());
        worker.setDaemon(true);
        return worker;
    }

    private static final class AssetData<D> {

        private final AssetContainer<D> assetContainer;
        private final Container<D> assetData;

        private AssetData(AssetContainer<D> assetContainer, Container<D> assetData) {
            this.assetContainer = assetContainer;
            this.assetData = assetData;
        }

        private void inject() {
            assetContainer.inject(assetData);
        }
    }
}
