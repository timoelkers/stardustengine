package de.oelkers.stardust.graphics.assets.loading;

import de.oelkers.stardust.graphics.animations.AnimatedModel3D;
import de.oelkers.stardust.graphics.animations.AnimationComponent;
import de.oelkers.stardust.graphics.animations.collada.ColladaAnimatedModelLoader;
import de.oelkers.stardust.graphics.animations.collada.ColladaAnimationLoader;
import de.oelkers.stardust.graphics.animations.collada.ColladaModelLoader;
import de.oelkers.stardust.graphics.assets.fonts.AngelCodeFontLoader;
import de.oelkers.stardust.graphics.assets.fonts.GuiText;
import de.oelkers.stardust.graphics.assets.models.Model3D;
import de.oelkers.stardust.graphics.assets.textures.BaseTexture.TextureData;
import de.oelkers.stardust.graphics.assets.textures.CubeMap;
import de.oelkers.stardust.graphics.assets.textures.DDSLoader;
import de.oelkers.stardust.graphics.assets.textures.Texture2D;
import de.oelkers.stardust.graphics.assets.textures.TextureLoader;
import de.oelkers.stardust.utils.ArrayUtils;
import de.oelkers.stardust.utils.files.FileWatcher;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

public class FileAssetLoader extends AssetLoader {

    private static final Logger LOGGER = LogManager.getLogger(FileAssetLoader.class);

    private final Map<FileExtensionAndAssetTypeKey, AsynchronousAssetLoader<?>> loaders = new HashMap<>();
    private final FileWatcher fileWatcher = new FileWatcher();

    /**
     * Constructs the asset loader and registers default loaders for the following file extensions and asset types
     * (you may override these defaults at any time):
     * <li>.png, .bmp, .jpg, .tiff and .dds for {@link Texture2D}</li>
     * <li>.png, .bmp, .jpg and .tiff for {@link CubeMap}</li>
     * <li>.fnt for {@link GuiText}</li>
     * <li>.dae for {@link Model3D}, {@link AnimatedModel3D} and {@link AnimationComponent}</li>
     */
    public FileAssetLoader() {
        AsynchronousAssetLoader<TextureData> textureLoader = new TextureLoader();
        registerLoader(Texture2D.class, textureLoader, ".png", ".bmp", ".jpg", ".tiff");
        registerLoader(CubeMap.class, textureLoader, ".png", ".bmp", ".jpg", ".tiff");
        registerLoader(Texture2D.class, new DDSLoader(), ".dds");
        registerLoader(GuiText.class, new AngelCodeFontLoader(), ".fnt");
        registerLoader(Model3D.class, new ColladaModelLoader(), ".dae");
        registerLoader(AnimatedModel3D.class, new ColladaAnimatedModelLoader(), ".dae");
        registerLoader(AnimationComponent.class, new ColladaAnimationLoader(), ".dae");
    }

    /**
     * Registers the given asynchronous asset loader for the given asset type and the given file extensions.
     * The can only exist one loader for the combination of asset type and file extension, consecutive calls
     * with different loader override each other.
     *
     * @param assetType the type of the asset container
     * @param loader the loader used to load the asset
     * @param fileExtensions the file extensions (including the .) of the asset file
     */
    public final <T> void registerLoader(Class<? extends AssetContainer<T>> assetType, AsynchronousAssetLoader<? extends T> loader, String... fileExtensions) {
        for (String fileExtension : fileExtensions) {
            loaders.put(new FileExtensionAndAssetTypeKey(fileExtension, assetType), loader);
        }
        LOGGER.log(Level.INFO, "Registered asset loader for " + assetType + " and " + String.join(", ", fileExtensions));
    }

    /**
     * Enqueues the loading task for this asset with the matching registered loader. This returns immediately and the
     * actual asset data gets injected into the asset at runtime. The asset data gets loaded on different
     * threads asynchronously, you can not rely on the time and the order in which the asset data gets injected. You are
     * allowed to specify multiple files if the loader allows it (e.g. for cube maps), an exception is propagated to
     * the exception handler otherwise. The order of files stays consistent across the loading process.
     *
     * @param asset the asset container the data gets injected in
     * @param filePath the path to the asset file
     * @param filePaths additional paths to dependant asset files
     * @param <T> the type of data that gets injected
     * @param <R> the type of the asset container
     */
    public <T, R extends AssetContainer<T>> R load(R asset, String filePath, String... filePaths) {
        AsynchronousAssetLoader<T> loader = getLoader(asset, filePath);
        for (String path : filePaths) {
            if (!getLoader(asset, path).equals(loader)) {
                throw new IllegalArgumentException("The given file paths correspond to different asset loaders!");
            }
        }
        String[] joinedPaths = ArrayUtils.prepend(filePaths, filePath);
        LOGGER.info("Loading " + asset + " from " + String.join(", ", joinedPaths));
        loadAsyncArray(asset, () -> {
            T[] assetData = loader.loadAsync(joinedPaths);
            onAssetDataLoaded(joinedPaths, assetData, asset.getClass());
            return assetData;
        });
        return asset;
    }

    /**
     * Loads the asset data using {@link #load(AssetContainer, String, String...)} and watches all
     * provided paths for file content changes. If such changes happen, the asset data is reloaded and
     * enqueued to be reinjected once more.
     *
     * @param asset the asset container the data gets injected in
     * @param filePath the path to the asset file
     * @param filePaths additional paths to dependant asset files
     * @param <T> the type of data that gets injected
     * @param <R> the type of the asset container
     */
    public <T, R extends AssetContainer<T>> R loadAndObserve(R asset, String filePath, String... filePaths) {
        load(asset, filePath, filePaths);
        Runnable onFileChange = () -> {
            onAssetDataReload(filePath, filePaths, asset.getClass());
            load(asset, filePath, filePaths);
        };
        fileWatcher.watch(filePath, onFileChange);
        for (String additional : filePaths) {
            fileWatcher.watch(additional, onFileChange);
        }
        return asset;
    }

    @Override
    public void close() {
        fileWatcher.close();
        super.close();
    }

    /**
     * This method is intended to be overridden by implementations that need a reference
     * to the loaded data. This is called asynchronously on a thread that does not
     * have the OpenGL context, so you can not inject the data at this point.
     *
     * @param filePaths the path to the files the data was loaded from
     * @param data the actual data as provided by the loaders
     * @param assetType the type of the asset
     */
    protected void onAssetDataLoaded(String[] filePaths, Object[] data, Class<?> assetType) {}

    protected void onAssetDataReload(String filePath, String[] filePaths, Class<?> assetType) {}

    @SuppressWarnings("unchecked")
    private <T> AsynchronousAssetLoader<T> getLoader(AssetContainer<T> asset, String filePath) {
        String fileExtension = filePath.substring(filePath.lastIndexOf('.'));
        Class<?> assetType = getActualType(asset);
        return (AsynchronousAssetLoader<T>) Optional.ofNullable(loaders.get(new FileExtensionAndAssetTypeKey(fileExtension, assetType)))
                .orElseThrow(() -> new IllegalStateException("There is no loader registered to load " + assetType + " with " + fileExtension));
    }

    @SuppressWarnings("rawtypes")
    private static Class<?> getActualType(Object asset) {
        if (asset instanceof Supplier) {
            return getActualType(((Supplier) asset).get());
        }
        return asset.getClass();
    }

    private static final class FileExtensionAndAssetTypeKey {

        private final String fileExtension;
        private final Class<?> assetType;

        private FileExtensionAndAssetTypeKey(String fileExtension, Class<?> assetType) {
            this.fileExtension = fileExtension;
            this.assetType = assetType;
        }

        @Override
        public int hashCode() {
            return Objects.hash(fileExtension, assetType);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            FileExtensionAndAssetTypeKey other = (FileExtensionAndAssetTypeKey) obj;
            return fileExtension.equals(other.fileExtension) && assetType == other.assetType;
        }
    }
}
