package de.oelkers.stardust.graphics.lights;

import de.oelkers.stardust.graphics.rendering.components.RenderTechnique;
import de.oelkers.stardust.graphics.shader.UniformAccessor;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

import static de.oelkers.stardust.graphics.rendering.components.RenderTechnique.FilePathOrContent.fromFilePath;

public class LightingTechnique implements RenderTechnique {

    @Serial
    private static final long serialVersionUID = 1L;
    private static final FilePathOrContent FILE_PATH = fromFilePath("techniques/lighting.tech");

    public static final int MAX_DIRECTIONAL_LIGHTS = 3;
    public static final int MAX_POINT_LIGHTS = 15;

    private final List<DirectionalLight> directionalLights = new ArrayList<>();
    private final List<PointLight> pointLights = new ArrayList<>();

    public void addDirectionalLight(DirectionalLight directionalLight) {
        directionalLights.add(directionalLight);
    }

    public void addPointLight(PointLight pointLight) {
        pointLights.add(pointLight);
    }

    @Override
    public int apply(UniformAccessor uniformAccessor, int techniqueNumber, int textureUnitIndex) {
        setDirectionalLights(uniformAccessor, techniqueNumber);
        setPointLights(uniformAccessor, techniqueNumber);
        return textureUnitIndex;
    }

    @Override
    public FilePathOrContent getFilePathOrContent() {
        return FILE_PATH;
    }

    private void setDirectionalLights(UniformAccessor uniformAccessor, int techniqueNumber) {
        int lightCount = Math.min(MAX_DIRECTIONAL_LIGHTS, directionalLights.size());
        for (int i = 0; i < lightCount; i++) {
            DirectionalLight light = directionalLights.get(i);
            uniformAccessor.setUniform("directionalLights_" + techniqueNumber + '[' + i + "].direction", light.getDirection());
            uniformAccessor.setUniform("directionalLights_" + techniqueNumber + '[' + i + "].color", light.getColor());
        }
        uniformAccessor.setUniform("directionalLightCount_" + techniqueNumber, lightCount);
    }

    private void setPointLights(UniformAccessor uniformAccessor, int techniqueNumber) {
        int lightCount = Math.min(MAX_POINT_LIGHTS, pointLights.size());
        for (int i = 0; i < lightCount; i++) {
            PointLight light = pointLights.get(i);
            uniformAccessor.setUniform("pointLights_" + techniqueNumber + '[' + i + "].position", light.getPosition());
            uniformAccessor.setUniform("pointLights_" + techniqueNumber + '[' + i + "].color", light.getColor());
            uniformAccessor.setUniform("pointLights_" + techniqueNumber + '[' + i + "].radius", light.getRadius());
        }
        uniformAccessor.setUniform("pointLightCount_" + techniqueNumber, lightCount);
    }
}
