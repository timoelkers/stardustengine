package de.oelkers.stardust.graphics.lights;

import de.oelkers.stardust.math.vectors.ReadOnlyVector3f;

import java.io.Serial;
import java.io.Serializable;

public class DirectionalLight implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private final ReadOnlyVector3f direction;
    private final ReadOnlyVector3f color;

    public DirectionalLight(ReadOnlyVector3f direction, ReadOnlyVector3f color) {
        this.direction = direction;
        this.color = color;
    }

    public ReadOnlyVector3f getDirection() {
        return direction;
    }

    public ReadOnlyVector3f getColor() {
        return color;
    }
}
