package de.oelkers.stardust.graphics.lights;

import de.oelkers.stardust.math.vectors.ReadOnlyVector3f;

import java.io.Serial;
import java.io.Serializable;

public class PointLight implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private final ReadOnlyVector3f position;
    private final ReadOnlyVector3f color;
    private final float radius;

    public PointLight(ReadOnlyVector3f position, ReadOnlyVector3f color, float radius) {
        this.position = position;
        this.color = color;
        this.radius = radius;
    }

    public ReadOnlyVector3f getPosition() {
        return position;
    }

    public ReadOnlyVector3f getColor() {
        return color;
    }

    public float getRadius() {
        return radius;
    }
}
