package de.oelkers.stardust.graphics.shader;

import de.oelkers.stardust.math.matrices.ReadOnlyMatrix4f;
import de.oelkers.stardust.math.vectors.ReadOnlyVector2f;
import de.oelkers.stardust.math.vectors.ReadOnlyVector3f;
import de.oelkers.stardust.math.vectors.ReadOnlyVector4f;
import org.lwjgl.opengl.GL20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Map;

public class NonCachingUniformAccessor implements UniformAccessor {

    private final ByteBuffer matrixBuffer = ByteBuffer.allocateDirect(Float.BYTES << 4).order(ByteOrder.nativeOrder());
    private final Map<String, Integer> uniformLocations;

    public NonCachingUniformAccessor(Map<String, Integer> uniformLocations) {
        this.uniformLocations = uniformLocations;
    }

    @Override
    public void setUniform(String uniformName, int value) {
        if (uniformLocations.containsKey(uniformName)) {
            GL20.glUniform1i(uniformLocations.get(uniformName), value);
        }
    }

    @Override
    public void setUniform(String uniformName, int... values) {
        for (int i = 0; i < values.length; i++) {
            setUniform(uniformName + '[' + i + ']', values[i]);
        }
    }

    @Override
    public void setUniform(String uniformName, float value) {
        if (uniformLocations.containsKey(uniformName)) {
            GL20.glUniform1f(uniformLocations.get(uniformName), value);
        }
    }

    @Override
    public void setUniform(String uniformName, float... values) {
        for (int i = 0; i < values.length; i++) {
            setUniform(uniformName + '[' + i + ']', values[i]);
        }
    }

    @Override
    public void setUniform(String uniformName, boolean value) {
        if (uniformLocations.containsKey(uniformName)) {
            GL20.glUniform1i(uniformLocations.get(uniformName), value ? 1 : 0);
        }
    }

    @Override
    public void setUniform(String uniformName, boolean... values) {
        for (int i = 0; i < values.length; i++) {
            setUniform(uniformName + '[' + i + ']', values[i]);
        }
    }

    @Override
    public void setUniform(String uniformName, ReadOnlyVector2f value) {
        if (uniformLocations.containsKey(uniformName)) {
            GL20.glUniform2f(uniformLocations.get(uniformName), value.getX(), value.getY());
        }
    }

    @Override
    public void setUniform(String uniformName, ReadOnlyVector2f... values) {
        for (int i = 0; i < values.length; i++) {
            setUniform(uniformName + '[' + i + ']', values[i]);
        }
    }

    @Override
    public void setUniform(String uniformName, ReadOnlyVector3f value) {
        if (uniformLocations.containsKey(uniformName)) {
            GL20.glUniform3f(uniformLocations.get(uniformName), value.getX(), value.getY(), value.getZ());
        }
    }

    @Override
    public void setUniform(String uniformName, ReadOnlyVector3f... values) {
        for (int i = 0; i < values.length; i++) {
            setUniform(uniformName + '[' + i + ']', values[i]);
        }
    }

    @Override
    public void setUniform(String uniformName, ReadOnlyVector4f value) {
        if (uniformLocations.containsKey(uniformName)) {
            GL20.glUniform4f(uniformLocations.get(uniformName), value.getX(), value.getY(), value.getZ(), value.getW());
        }
    }

    @Override
    public void setUniform(String uniformName, ReadOnlyVector4f... values) {
        for (int i = 0; i < values.length; i++) {
            setUniform(uniformName + '[' + i + ']', values[i]);
        }
    }

    @Override
    public void setUniform(String uniformName, ReadOnlyMatrix4f value) {
        if (uniformLocations.containsKey(uniformName)) {
            matrixBuffer.clear();
            value.store(matrixBuffer);
            matrixBuffer.flip();
            GL20.glUniformMatrix4fv(uniformLocations.get(uniformName), false, matrixBuffer.asFloatBuffer());
        }
    }

    @Override
    public void setUniform(String uniformName, ReadOnlyMatrix4f... values) {
        for (int i = 0; i < values.length; i++) {
            setUniform(uniformName + '[' + i + ']', values[i]);
        }
    }
}
