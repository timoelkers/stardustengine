package de.oelkers.stardust.graphics.shader;

import de.oelkers.stardust.OpenGLContext;
import de.oelkers.stardust.config.ConfigStore;
import de.oelkers.stardust.config.StardustErrorCallback.ErrorType;
import de.oelkers.stardust.config.StardustErrorCallback.Severity;
import de.oelkers.stardust.entities.ShamIdentifiedObject;
import de.oelkers.stardust.graphics.shader.Shader.ShaderType;
import de.oelkers.stardust.utils.Lazy;
import de.oelkers.stardust.utils.NonThrowingAutoCloseable;
import de.oelkers.stardust.utils.buffers.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL41;

import java.io.Serial;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.*;
import java.util.regex.Pattern;

public final class ShaderProgram extends ShamIdentifiedObject implements NonThrowingAutoCloseable, Serializable {

    private static final Pattern NORMALIZED_UNIFORM = Pattern.compile("\\[[0-9]+]");

    private final Map<String, Integer> uniformLocations = new HashMap<>();
    private final Collection<Shader> shaders = new ArrayList<>();
    private final UniformAccessor uniformAccessor = new NonCachingUniformAccessor(uniformLocations);
    private String[] uniformNames;

    /**
     * Creates a shader program with the given shaders. Please note that you must provide at least a vertex and fragment shader and
     * that you can provide only one shader for each different shader type.
     *
     * @param shaders the shaders you want to add to this shader program
     */
    public static ShaderProgram newShaderProgram(Shader... shaders) {
        validateShaders(shaders);
        return new ShaderProgram(false, shaders);
    }

    /**
     * Creates a shader program with the given shaders. The program will be created and linked when the program id is first
     * requested (e.g. when binding or using the uniform accessor). This will also delay the creation of the shaders if they
     * are lazy as well. This is useful when you don´t have a graphics context present at the time of creation.
     * Please note that you must provide at least a vertex and fragment shader and that you can provide only one shader
     * for each different shader type.
     *
     * @param shaders the shaders you want to add to this shader program
     */
    public static ShaderProgram newLazyShaderProgram(Shader... shaders) {
        validateShaders(shaders);
        return new ShaderProgram(true, shaders);
    }

    private ShaderProgram(boolean isLazy, Shader... shaders) {
        super(isLazy ? new Lazy<>(() -> createProgram(shaders)) : new Lazy<>(createProgram(shaders)));
        Collections.addAll(this.shaders, shaders);
        if (!isLazy) {
            queryUniformLocations();
        }
    }

    private ShaderProgram(byte[] data, int format) {
        super(new Lazy<>(createProgram(data, format)));
        queryUniformLocations();
    }

    public UniformAccessor getUniformAccessor() {
        if (uniformLocations.isEmpty()) {
            queryUniformLocations();
        }
        return uniformAccessor;
    }

    /**
     * Returns the shader associated with this shader program with the given type, if present.
     * As a program can only have one shader of each type, this will reliably return the same shader for the same type.
     *
     * @param shaderType the shader type you are looking for
     */
    public Optional<Shader> getShader(ShaderType shaderType) {
        return shaders.stream().filter(shader -> shader.getType() == shaderType).findAny();
    }

    public String[] getUniformNames() {
        return uniformNames;
    }

    public void bind() {
        OpenGLContext.get().bindShaderProgram(getId());
    }

    @Override
    public void close() {
        detachAndDeleteShaders(true);
        GL20.glDeleteProgram(getId());
        OpenGLContext.get().unbindShaderProgram(getId());
    }

    private void detachAndDeleteShaders(boolean forceDispose) {
        for (Shader shader : shaders) {
            detachAndDeleteShader(getId(), shader, forceDispose);
        }
    }

    private void queryUniformLocations() {
        String previousUniformName = "";
        int previousUniformLocation = -1;
        int uniformCount = GL20.glGetProgrami(getId(), GL20.GL_ACTIVE_UNIFORMS);
        int maxUniforms = GL20.glGetProgrami(getId(), GL20.GL_ACTIVE_UNIFORM_MAX_LENGTH);
        for (int i = 0; i < uniformCount; i++) {
            IntBuffer voidBuffer = ByteBuffer.allocateDirect(Integer.BYTES).asIntBuffer();
            String name = GL20.glGetActiveUniform(getId(), i, maxUniforms, voidBuffer, voidBuffer);
            int location = GL20.glGetUniformLocation(getId(), name);
            if (previousUniformLocation + 1 != location) {
                handleUniformArray(previousUniformName, previousUniformLocation, location);
            }
            uniformLocations.put(name, location);
            previousUniformName = NORMALIZED_UNIFORM.matcher(name).replaceAll("");
            previousUniformLocation = location;
        }
        uniformNames = uniformLocations.keySet().toArray(new String[0]);
    }

    private void handleUniformArray(String previousUniformName, int previousUniformLocation, int currentUniformLocation) {
        for (int j = 1; j < currentUniformLocation - previousUniformLocation; j++) {
            uniformLocations.put(previousUniformName + '[' + j + ']', previousUniformLocation + j);
        }
    }

    @Serial
    private Object writeReplace() {
        int count = GL11.glGetInteger(GL41.GL_NUM_PROGRAM_BINARY_FORMATS);
        if (count > 0) {
            int[] format = new int[1];
            int length = GL20.glGetProgrami(getId(), GL41.GL_PROGRAM_BINARY_LENGTH);
            ByteBuffer buffer = ByteBuffer.allocateDirect(length).order(ByteOrder.nativeOrder());
            GL41.glGetProgramBinary(getId(), null, format, buffer);
            return new SerializationProxyBinary(BufferUtils.toArray(buffer), format[0]);
        }
        return new SerializationProxyShader(shaders);
    }

    private static final class SerializationProxyBinary implements Serializable {

        @Serial
        private static final long serialVersionUID = 1L;

        private final byte[] data;
        private final int format;

        private SerializationProxyBinary(byte[] data, int format) {
            this.data = data;
            this.format = format;
        }

        @Serial
        private Object readResolve() {
            return new ShaderProgram(data, format);
        }
    }

    private static final class SerializationProxyShader implements Serializable {

        @Serial
        private static final long serialVersionUID = 1L;

        private final Collection<Shader> shaders;

        private SerializationProxyShader(Collection<Shader> shaders) {
            this.shaders = shaders;
        }

        @Serial
        private Object readResolve() {
            return new ShaderProgram(false, shaders.toArray(new Shader[0]));
        }
    }

    private static int createProgram(Shader... shaders) {
        int programId = GL20.glCreateProgram();
        for (Shader shader : shaders) {
            GL20.glAttachShader(programId, shader.getId());
        }
        GL20.glLinkProgram(programId);
        if (GL20.glGetProgrami(programId, GL20.GL_LINK_STATUS) == GL11.GL_FALSE) {
            String programLog = GL20.glGetProgramInfoLog(programId, GL20.glGetProgrami(programId, GL20.GL_INFO_LOG_LENGTH));
            ConfigStore.get().getErrorCallback().onError(0, programLog, ErrorType.ERROR, Severity.HIGH);
            throw new IllegalStateException("ShaderProgram with id " + programId + " could not be linked!");
        }
        detachAndDeleteShaders(programId, false, shaders);
        return programId;
    }

    private static int createProgram(byte[] data, int format) {
        int programId = GL20.glCreateProgram();
        ByteBuffer buffer = ByteBuffer.allocateDirect(data.length).order(ByteOrder.nativeOrder());
        buffer.put(data).flip();
        GL41.glProgramBinary(programId, format, buffer);
        if (GL20.glGetProgrami(programId, GL20.GL_LINK_STATUS) == GL11.GL_FALSE) {
            String programLog = GL20.glGetProgramInfoLog(programId, GL20.glGetProgrami(programId, GL20.GL_INFO_LOG_LENGTH));
            ConfigStore.get().getErrorCallback().onError(0, programLog, ErrorType.ERROR, Severity.HIGH);
            throw new IllegalStateException("ShaderProgram with id " + programId + " could not be linked!");
        }
        return programId;
    }

    private static void detachAndDeleteShaders(int programId, boolean forceDispose, Shader... shaders) {
        for (Shader shader : shaders) {
            detachAndDeleteShader(programId, shader, forceDispose);
        }
    }

    private static void detachAndDeleteShader(int programId, Shader shader, boolean forceDispose) {
        if (forceDispose ^ !shader.isStatic()) {
            GL20.glDetachShader(programId, shader.getId());
            shader.close();
        }
    }

    private static void validateShaders(Shader... shaders) {
        for (Shader shader : shaders) {
            if (isTypePresentTwice(shader.getType())) {
                throw new IllegalArgumentException("A shader program can only have one shader of each type! You tried to add multiple " + shader.getType() + '.');
            }
        }
        if (isTypeAbsent(ShaderType.VERTEX_SHADER, shaders)) {
            throw new IllegalArgumentException("Your provided shaders need to contain a vertex shader!");
        }
        if (isTypeAbsent(ShaderType.FRAGMENT_SHADER, shaders)) {
            throw new IllegalArgumentException("Your provided shaders need to contain a fragment shader!");
        }
    }

    private static boolean isTypeAbsent(ShaderType shaderType, Shader... shaders) {
        return Arrays.stream(shaders).noneMatch(shader -> shader.getType() == shaderType);
    }

    private static boolean isTypePresentTwice(ShaderType shaderType, Shader... shaders) {
        return Arrays.stream(shaders).filter(shader -> shader.getType() == shaderType).count() > 1;
    }
}
