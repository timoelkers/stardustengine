package de.oelkers.stardust.graphics.shader;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import static de.oelkers.stardust.graphics.shader.ShaderGenerator.getShaderSource;
import static java.util.Collections.emptyMap;

public final class FileBasedShader extends Shader {

    private final String shaderPath;
    private final String shaderName;
    private final Map<String, Object> flags;

    public static FileBasedShader newStaticShader(String shaderPath, ShaderType shaderType) {
        return new FileBasedShader(shaderPath, shaderType, true, false);
    }

    public static FileBasedShader newStaticShader(String shaderPath, ShaderType shaderType, Map<String, Object> flags) {
        return new FileBasedShader(shaderPath, shaderType, true, false, flags);
    }

    public static FileBasedShader newStaticLazyShader(String shaderPath, ShaderType shaderType) {
        return new FileBasedShader(shaderPath, shaderType, true, true);
    }

    public static FileBasedShader newStaticLazyShader(String shaderPath, ShaderType shaderType, Map<String, Object> flags) {
        return new FileBasedShader(shaderPath, shaderType, true, true, flags);
    }

    public static FileBasedShader newDynamicShader(String shaderPath, ShaderType shaderType) {
        return new FileBasedShader(shaderPath, shaderType, false, false);
    }

    public static FileBasedShader newDynamicShader(String shaderPath, ShaderType shaderType, Map<String, Object> flags) {
        return new FileBasedShader(shaderPath, shaderType, false, false, flags);
    }

    public static FileBasedShader newDynamicLazyShader(String shaderPath, ShaderType shaderType) {
        return new FileBasedShader(shaderPath, shaderType, false, true);
    }

    public static FileBasedShader newDynamicLazyShader(String shaderPath, ShaderType shaderType, Map<String, Object> flags) {
        return new FileBasedShader(shaderPath, shaderType, false, true, flags);
    }

    private FileBasedShader(String shaderPath, ShaderType shaderType, boolean isStatic, boolean isLazy) {
        this(shaderPath, shaderType, isStatic, isLazy, emptyMap());
    }

    private FileBasedShader(String shaderPath, ShaderType shaderType, boolean isStatic, boolean isLazy, Map<String, Object> flags) {
        super(getShaderSource(shaderPath, flags), shaderType, isStatic, isLazy);
        this.shaderPath = shaderPath;
        this.flags = flags;
        shaderName = getShaderName(shaderPath);
    }

    public String getPath() {
        return shaderPath;
    }

    public String getName() {
        return shaderName;
    }

    public Set<Entry<String, Object>> getFlags() {
        return flags.entrySet();
    }

    public boolean updateSource() {
        return !isStatic() && updateSource(getShaderSource(shaderPath, flags));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '[' + shaderName + ']';
    }

    private static String getShaderName(String shaderPath) {
        String[] directories = shaderPath.split("/");
        return directories[directories.length - 1];
    }
}
