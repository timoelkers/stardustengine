package de.oelkers.stardust.graphics.shader;

import de.oelkers.stardust.math.matrices.ReadOnlyMatrix4f;
import de.oelkers.stardust.math.vectors.ReadOnlyVector2f;
import de.oelkers.stardust.math.vectors.ReadOnlyVector3f;
import de.oelkers.stardust.math.vectors.ReadOnlyVector4f;

public interface UniformAccessor {

    /**
     * Sets the uniform value in the shader to this integer value.
     * Does nothing if the uniform is not active or does not exist,
     *
     * @param uniformName the name of the uniform used in the shader (be careful not to misspell it)
     * @param value the integer value you want to set
     */
    void setUniform(String uniformName, int value);

    /**
     * Sets the uniform value in the shader to this integer array.
     * Does nothing if the uniform is not active or does not exist,
     *
     * @param uniformName the name of the uniform used in the shader (be careful not to misspell it)
     * @param values the integer array you want to set
     */
    void setUniform(String uniformName, int... values);

    /**
     * Sets the uniform value in the shader to this float value.
     * Does nothing if the uniform is not active or does not exist,
     *
     * @param uniformName the name of the uniform used in the shader (be careful not to misspell it)
     * @param value the float value you want to set
     */
    void setUniform(String uniformName, float value);

    /**
     * Sets the uniform value in the shader to this float array.
     * Does nothing if the uniform is not active or does not exist,
     *
     * @param uniformName the name of the uniform used in the shader (be careful not to misspell it)
     * @param values the float array you want to set
     */
    void setUniform(String uniformName, float... values);

    /**
     * Sets the uniform value in the shader to this boolean value.
     * Does nothing if the uniform is not active or does not exist,
     *
     * @param uniformName the name of the uniform used in the shader (be careful not to misspell it)
     * @param value the boolean value you want to set
     */
    void setUniform(String uniformName, boolean value);

    /**
     * Sets the uniform value in the shader to this boolean array.
     * Does nothing if the uniform is not active or does not exist,
     *
     * @param uniformName the name of the uniform used in the shader (be careful not to misspell it)
     * @param values the boolean array you want to set
     */
    void setUniform(String uniformName, boolean... values);

    /**
     * Sets the uniform value in the shader to this 2D vector.
     * Does nothing if the uniform is not active or does not exist,
     *
     * @param uniformName the name of the uniform used in the shader (be careful not to misspell it)
     * @param value the 2D vector you want to set
     */
    void setUniform(String uniformName, ReadOnlyVector2f value);

    /**
     * Sets the uniform value in the shader to this 2D vector array.
     * Does nothing if the uniform is not active or does not exist,
     *
     * @param uniformName the name of the uniform used in the shader (be careful not to misspell it)
     * @param values the 2D vector array you want to set
     */
    void setUniform(String uniformName, ReadOnlyVector2f... values);

    /**
     * Sets the uniform value in the shader to this 3D vector.
     * Does nothing if the uniform is not active or does not exist,
     *
     * @param uniformName the name of the uniform used in the shader (be careful not to misspell it)
     * @param value the 3D vector you want to set
     */
    void setUniform(String uniformName, ReadOnlyVector3f value);

    /**
     * Sets the uniform value in the shader to this 3D vector array.
     * Does nothing if the uniform is not active or does not exist,
     *
     * @param uniformName the name of the uniform used in the shader (be careful not to misspell it)
     * @param values the 3D vector array you want to set
     */
    void setUniform(String uniformName, ReadOnlyVector3f... values);

    /**
     * Sets the uniform value in the shader to this 4D vector.
     * Does nothing if the uniform is not active or does not exist,
     *
     * @param uniformName the name of the uniform used in the shader (be careful not to misspell it)
     * @param value the 4D vector you want to set
     */
    void setUniform(String uniformName, ReadOnlyVector4f value);

    /**
     * Sets the uniform value in the shader to this 4D vector array.
     * Does nothing if the uniform is not active or does not exist,
     *
     * @param uniformName the name of the uniform used in the shader (be careful not to misspell it)
     * @param values the 4D vector array you want to set
     */
    void setUniform(String uniformName, ReadOnlyVector4f... values);

    /**
     * Sets the uniform value in the shader to this matrix.
     * Does nothing if the uniform is not active or does not exist,
     *
     * @param uniformName the name of the uniform used in the shader (be careful not to misspell it)
     * @param value the matrix you want to set
     */
    void setUniform(String uniformName, ReadOnlyMatrix4f value);

    /**
     * Sets the uniform value in the shader to this matrix array.
     * Does nothing if the uniform is not active or does not exist,
     *
     * @param uniformName the name of the uniform used in the shader (be careful not to misspell it)
     * @param values the matrix array you want to set
     */
    void setUniform(String uniformName, ReadOnlyMatrix4f... values);
}
