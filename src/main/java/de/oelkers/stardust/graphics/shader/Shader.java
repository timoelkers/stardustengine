package de.oelkers.stardust.graphics.shader;

import de.oelkers.stardust.config.ConfigStore;
import de.oelkers.stardust.config.StardustErrorCallback.ErrorType;
import de.oelkers.stardust.config.StardustErrorCallback.Severity;
import de.oelkers.stardust.entities.ShamIdentifiedObject;
import de.oelkers.stardust.utils.Lazy;
import de.oelkers.stardust.utils.NonThrowingAutoCloseable;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL32;
import org.lwjgl.opengl.GL43;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

public class Shader extends ShamIdentifiedObject implements NonThrowingAutoCloseable, Serializable {

    public enum ShaderType {

        VERTEX_SHADER(GL20.GL_VERTEX_SHADER),
        FRAGMENT_SHADER(GL20.GL_FRAGMENT_SHADER),
        GEOMETRY_SHADER(GL32.GL_GEOMETRY_SHADER),
        COMPUTE_SHADER(GL43.GL_COMPUTE_SHADER);

        private final int glCode;

        ShaderType(int glCode) {
            this.glCode = glCode;
        }

        public int getGlCode() {
            return glCode;
        }
    }

    private final ShaderType shaderType;
    private final boolean isStatic;
    private String shaderSource;

    public static Shader newStaticShader(String shaderSource, ShaderType shaderType) {
        return new Shader(shaderSource, shaderType, true, false);
    }

    public static Shader newStaticLazyShader(String shaderSource, ShaderType shaderType) {
        return new Shader(shaderSource, shaderType, true, true);
    }

    public static Shader newDynamicShader(String shaderSource, ShaderType shaderType) {
        return new Shader(shaderSource, shaderType, false, false);
    }

    public static Shader newDynamicLazyShader(String shaderSource, ShaderType shaderType) {
        return new Shader(shaderSource, shaderType, false, true);
    }

    protected Shader(String shaderSource, ShaderType shaderType, boolean isStatic, boolean isLazy) {
        super(isLazy ? new Lazy<>(() -> createShader(shaderType, shaderSource)) : new Lazy<>(createShader(shaderType, shaderSource)));
        this.shaderType = shaderType;
        this.isStatic = isStatic;
        this.shaderSource = shaderSource;
    }

    public String getSource() {
        return shaderSource;
    }

    public boolean updateSource(String newSource) {
        if (isStatic) {
            return false;
        }
        if (Objects.equals(shaderSource, newSource)) {
            return false;
        }
        shaderSource = newSource;
        compileShader();
        return true;
    }

    public ShaderType getType() {
        return shaderType;
    }

    public boolean isStatic() {
        return isStatic;
    }

    @Override
    public void close() {
        GL20.glDeleteShader(getId());
    }

    private void compileShader() {
        compileShader(getId(), shaderSource);
    }

    @Serial
    protected Object writeReplace() {
        return new SerializationProxy(shaderType, isStatic, shaderSource);
    }

    private static final class SerializationProxy implements Serializable {

        @Serial
        private static final long serialVersionUID = 1L;

        private final ShaderType shaderType;
        private final boolean isStatic;
        private final String shaderSource;

        private SerializationProxy(ShaderType shaderType, boolean isStatic, String shaderSource) {
            this.shaderType = shaderType;
            this.isStatic = isStatic;
            this.shaderSource = shaderSource;
        }

        @Serial
        private Object readResolve() {
            return new Shader(shaderSource, shaderType, isStatic, false);
        }
    }

    private static int createShader(ShaderType shaderType, CharSequence shaderSource) {
        int shaderId = GL20.glCreateShader(shaderType.getGlCode());
        compileShader(shaderId, shaderSource);
        return shaderId;
    }

    private static void compileShader(int id, CharSequence shaderSource) {
        GL20.glShaderSource(id, shaderSource);
        GL20.glCompileShader(id);
        if (GL20.glGetShaderi(id, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
            String shaderLog = GL20.glGetShaderInfoLog(id, GL20.glGetShaderi(id, GL20.GL_INFO_LOG_LENGTH));
            ConfigStore.get().getErrorCallback().onError(0, shaderLog, ErrorType.ERROR, Severity.HIGH);
            throw new IllegalStateException("Shader with id " + id + " could not be compiled!");
        }
    }
}
