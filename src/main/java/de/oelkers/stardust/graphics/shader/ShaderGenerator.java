package de.oelkers.stardust.graphics.shader;

import de.oelkers.stardust.config.ConfigStore;
import de.oelkers.stardust.graphics.assets.models.BaseModel.PrimitiveMode;
import de.oelkers.stardust.graphics.rendering.components.RenderTechnique;
import de.oelkers.stardust.graphics.rendering.components.RenderTechnique.FilePathOrContent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static de.oelkers.stardust.graphics.shader.Shader.ShaderType.*;
import static de.oelkers.stardust.graphics.shader.ShaderProgram.newLazyShaderProgram;
import static de.oelkers.stardust.graphics.shader.ShaderProgram.newShaderProgram;
import static java.lang.System.lineSeparator;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public final class ShaderGenerator {

    private static final Logger LOGGER = LogManager.getLogger(ShaderGenerator.class);

    private static final String VERSION_DIRECTIVE = "#version";
    private static final String INCLUDE_DIRECTIVE = "#include";
    private static final String DEFINE_DIRECTIVE = "#define ";
    private static final String MAIN_FUNCTION = "void main(";
    private static final Pattern TECHNIQUE_FUNCTION = Pattern.compile("applyNextTechnique");
    private static final Pattern NEXT_TEXTURE_UNIT = Pattern.compile("NEXT_TEXTURE_UNIT");

    private static final PatternAndGroupName UNIFORM_VARIABLE = new PatternAndGroupName(".*\\s*uniform\\s+.+\\s+(?<variable>.+);", "variable");
    private static final PatternAndGroupName DECLARED_FUNCTION = new PatternAndGroupName("\\S+\\s+(?<function>.+)\\(.*\\).*", "function");
    private static final FindAndReplaceDuplicates FIND_AND_REPLACE_DUPLICATES = new FindAndReplaceDuplicates(UNIFORM_VARIABLE, DECLARED_FUNCTION);

    private static final Map<RenderTechniqueKey, ShaderProgram> SHADER_PROGRAM_CACHE = new HashMap<>();

    private ShaderGenerator() {}

    public static synchronized ShaderProgram generate(List<RenderTechnique> renderTechniques, String vertexShaderPath, String fragmentShaderPath, PrimitiveMode primitiveMode) {
        return generate(renderTechniques, vertexShaderPath, null, fragmentShaderPath, primitiveMode, false);
    }

    public static synchronized ShaderProgram generate(List<RenderTechnique> renderTechniques, String vertexShaderPath, String geometryShaderPath, String fragmentShaderPath) {
        return generate(renderTechniques, vertexShaderPath, geometryShaderPath, fragmentShaderPath, null, false);
    }

    public static synchronized ShaderProgram generateLazily(List<RenderTechnique> renderTechniques, String vertexShaderPath, String fragmentShaderPath, PrimitiveMode primitiveMode) {
        return generate(renderTechniques, vertexShaderPath, null, fragmentShaderPath, primitiveMode, true);
    }

    public static synchronized ShaderProgram generateLazily(List<RenderTechnique> renderTechniques, String vertexShaderPath, String geometryShaderPath, String fragmentShaderPath) {
        return generate(renderTechniques, vertexShaderPath, geometryShaderPath, fragmentShaderPath, null, true);
    }

    private static ShaderProgram generate(List<RenderTechnique> renderTechniques, String vertexShaderPath, String geometryShaderPath, String fragmentShaderPath, PrimitiveMode primitiveMode, boolean isLazy) {
        RenderTechniqueKey key = new RenderTechniqueKey(vertexShaderPath, geometryShaderPath, fragmentShaderPath, renderTechniques, primitiveMode);
        return SHADER_PROGRAM_CACHE.computeIfAbsent(key, k -> {
            StringBuilder sourceBuffer = new StringBuilder();
            loadSource(fragmentShaderPath, line -> processLineForShaderWithTechniques(line, sourceBuffer, renderTechniques));
            Shader[] shaders = new Shader[geometryShaderPath != null || ConfigStore.get().isUsingIndividualViewports() ? 3 : 2];
            shaders[0] = isLazy ? Shader.newStaticLazyShader(sourceBuffer.toString(), FRAGMENT_SHADER) : Shader.newStaticShader(sourceBuffer.toString(), FRAGMENT_SHADER);
            shaders[1] = isLazy ? FileBasedShader.newStaticLazyShader(vertexShaderPath, VERTEX_SHADER) : FileBasedShader.newStaticShader(vertexShaderPath, VERTEX_SHADER);
            if (geometryShaderPath != null) {
                shaders[2] = isLazy ? FileBasedShader.newStaticLazyShader(geometryShaderPath, GEOMETRY_SHADER) : FileBasedShader.newStaticShader(geometryShaderPath, GEOMETRY_SHADER);
            } else if (ConfigStore.get().isUsingIndividualViewports()) {
                if (isLazy) {
                    shaders[2] = PassThroughGeometryShaderGenerator.generateLazily(shaders[1], "shader/chooseViewport.geom", primitiveMode);
                } else {
                    shaders[2] = PassThroughGeometryShaderGenerator.generate(shaders[1], "shader/chooseViewport.geom", primitiveMode);
                }
            }
            if (renderTechniques.isEmpty()) {
                if (geometryShaderPath == null) {
                    LOGGER.info("Generated shaders for " + vertexShaderPath + " and " + fragmentShaderPath);
                } else {
                    LOGGER.info("Generated shaders for " + vertexShaderPath + ", " + geometryShaderPath + " and " + fragmentShaderPath);
                }
            } else {
                String renderTechniquesString = renderTechniques.stream().map(RenderTechnique::getFilePathOrContent).map(Objects::toString).collect(joining(", "));
                if (geometryShaderPath == null) {
                    LOGGER.info("Generated shaders for " + vertexShaderPath + ", " + fragmentShaderPath + " and " + renderTechniquesString);
                } else {
                    LOGGER.info("Generated shaders for " + vertexShaderPath + ", " + geometryShaderPath + ", " + fragmentShaderPath + " and " + renderTechniquesString);
                }
            }
            return isLazy ? newLazyShaderProgram(shaders) : newShaderProgram(shaders);
        });
    }

    public static String getShaderSource(String shaderPath, Map<String, Object> flags) {
        StringBuilder sourceBuffer = new StringBuilder();
        loadSource(shaderPath, line -> processLine(line, sourceBuffer, flags));
        return sourceBuffer.toString();
    }

    private static void loadSource(String shaderPath, Consumer<String> lineConsumer) {
        InputStream shaderInputStream = ClassLoader.getSystemResourceAsStream(shaderPath);
        if (shaderInputStream == null) {
            throw new IllegalStateException("The file '" + shaderPath + "' does not exist!");
        }
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(shaderInputStream))) {
            reader.lines().forEachOrdered(lineConsumer);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private static void processLine(String line, StringBuilder sourceBuffer, Map<String, Object> flags) {
        if (line.startsWith(VERSION_DIRECTIVE)) {
            String flagString = flags.entrySet().stream().map(flag -> DEFINE_DIRECTIVE + flag.getKey() + ' ' + flag.getValue() + lineSeparator()).collect(joining());
            sourceBuffer.append(line).append(lineSeparator()).append(flagString);
        } else if (line.startsWith(INCLUDE_DIRECTIVE)) {
            String includePath = line.substring(INCLUDE_DIRECTIVE.length() + 2, line.trim().length() - 1);
            loadSource(includePath, l -> processLine(l, sourceBuffer, flags));
        } else {
            sourceBuffer.append(line).append(lineSeparator());
        }
    }

    private static void processLineForShaderWithTechniques(String line, StringBuilder sourceBuffer, List<RenderTechnique> renderTechniques) {
        if (line.startsWith(MAIN_FUNCTION)) {
            processRenderTechniques(renderTechniques, sourceBuffer);
            sourceBuffer.append(lineSeparator()).append(line).append(lineSeparator());
        } else if (line.startsWith(INCLUDE_DIRECTIVE)) {
            String includePath = line.substring(INCLUDE_DIRECTIVE.length() + 2, line.trim().length() - 1);
            loadSource(includePath, l -> processLineForShaderWithTechniques(l, sourceBuffer, renderTechniques));
        } else {
            Matcher techniqueFunctionMatcher = TECHNIQUE_FUNCTION.matcher(line);
            if (techniqueFunctionMatcher.find()) {
                int endOfFunction = techniqueFunctionMatcher.end();
                for (int i = 0; i < renderTechniques.size(); i++) {
                    sourceBuffer.append(line, 0, endOfFunction).append('_').append(i).append(line, endOfFunction, line.length()).append(lineSeparator());
                }
            } else {
                sourceBuffer.append(line).append(lineSeparator());
            }
        }
    }

    private static void processRenderTechniques(List<RenderTechnique> renderTechniques, StringBuilder sourceBuffer) {
        AtomicInteger textureUnitCounter = new AtomicInteger();
        for (int i = 0; i < renderTechniques.size(); i++) {
            FIND_AND_REPLACE_DUPLICATES.setTechnique(renderTechniques.get(i), i);
            Consumer<String> lineConsumer = line -> processLineForTechnique(line, sourceBuffer, textureUnitCounter);
            FilePathOrContent filePathOrContent = renderTechniques.get(i).getFilePathOrContent();
            if (filePathOrContent.isFilePath()) {
                loadSource(filePathOrContent.getFilePathOrContent(), lineConsumer);
            } else {
                filePathOrContent.getFilePathOrContent().lines().forEachOrdered(lineConsumer);
            }
        }
    }

    private static void processLineForTechnique(String line, StringBuilder sourceBuffer, AtomicInteger textureUnitCounter) {
        if (line.startsWith(INCLUDE_DIRECTIVE)) {
            String includePath = line.substring(INCLUDE_DIRECTIVE.length() + 2, line.trim().length() - 1);
            loadSource(includePath, l -> processLineForTechnique(l, sourceBuffer, textureUnitCounter));
        } else {
            line = NEXT_TEXTURE_UNIT.matcher(line).replaceAll(result -> String.valueOf(textureUnitCounter.incrementAndGet()));
            line = FIND_AND_REPLACE_DUPLICATES.process(line);
            sourceBuffer.append(line).append(lineSeparator());
        }
    }

    private static final class FindAndReplaceDuplicates {

        private final Set<FilePathOrContent> alreadyProcessed = new HashSet<>();
        private final Set<String> foundNames = new HashSet<>();
        private final PatternAndGroupName[] patternAndGroupNames;

        private int techniqueIndex;
        private boolean notYetProcessed;

        private FindAndReplaceDuplicates(PatternAndGroupName... patternAndGroupNames) {
            this.patternAndGroupNames = patternAndGroupNames;
        }

        private void setTechnique(RenderTechnique renderTechnique, int techniqueIndex) {
            notYetProcessed = alreadyProcessed.add(renderTechnique.getFilePathOrContent());
            this.techniqueIndex = techniqueIndex;
        }

        private String process(String line) {
            if (notYetProcessed) {
                for (PatternAndGroupName patternAndGroupName : patternAndGroupNames) {
                    Matcher matcher = patternAndGroupName.pattern.matcher(line);
                    if (matcher.matches()) {
                        foundNames.add(matcher.group(patternAndGroupName.groupName));
                        break;
                    }
                }
            }
            for (String foundName : foundNames) {
                line = line.replaceAll(foundName, foundName + '_' + techniqueIndex);
            }
            return line;
        }
    }

    private static final class PatternAndGroupName {

        private final Pattern pattern;
        private final String groupName;

        private PatternAndGroupName(String pattern, String groupName) {
            this.pattern = Pattern.compile(pattern);
            this.groupName = groupName;
        }
    }

    private static final class RenderTechniqueKey {

        private final String vertexShaderPath;
        private final String geometryShaderPath;
        private final String fragmentShaderPath;
        private final List<FilePathOrContent> renderTechniques;
        private final PrimitiveMode primitiveMode;

        private RenderTechniqueKey(String vertexShaderPath, String geometryShaderPath, String fragmentShaderPath, Collection<RenderTechnique> renderTechniques, PrimitiveMode primitiveMode) {
            this.vertexShaderPath = vertexShaderPath;
            this.geometryShaderPath = geometryShaderPath;
            this.fragmentShaderPath = fragmentShaderPath;
            this.renderTechniques = renderTechniques.stream().map(RenderTechnique::getFilePathOrContent).collect(toList());
            this.primitiveMode = primitiveMode;
        }

        @Override
        public int hashCode() {
            return Objects.hash(vertexShaderPath, geometryShaderPath, fragmentShaderPath, renderTechniques, primitiveMode);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            RenderTechniqueKey other = (RenderTechniqueKey) obj;
            boolean geometryShaderEqual = Objects.equals(geometryShaderPath, other.geometryShaderPath);
            boolean primitiveModesEqual = !ConfigStore.get().isUsingIndividualViewports() || primitiveMode == other.primitiveMode;
            return vertexShaderPath.equals(other.vertexShaderPath) && geometryShaderEqual && primitiveModesEqual &&
                    fragmentShaderPath.equals(other.fragmentShaderPath) && renderTechniques.equals(other.renderTechniques);
        }
    }
}
