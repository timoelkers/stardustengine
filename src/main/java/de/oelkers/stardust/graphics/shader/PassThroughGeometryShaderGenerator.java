package de.oelkers.stardust.graphics.shader;

import de.oelkers.stardust.graphics.assets.models.BaseModel.PrimitiveMode;
import de.oelkers.stardust.graphics.shader.Shader.ShaderType;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static de.oelkers.stardust.graphics.shader.Shader.newStaticLazyShader;
import static de.oelkers.stardust.graphics.shader.Shader.newStaticShader;
import static java.lang.System.lineSeparator;
import static java.util.stream.Collectors.joining;

public final class PassThroughGeometryShaderGenerator {

    private static final String VERSION_DIRECTIVE = "#version";
    private static final String MAIN_FUNCTION = "void main(";
    private static final Pattern OUT_INTERFACE_BLOCK = Pattern.compile(".*out\\s(?<name>.*?)[{](?<content>.*?)[}].*", Pattern.DOTALL);
    private static final Pattern ANYTHING_FOLLOWED_BY_WHITESPACE = Pattern.compile(".*\\s");

    private PassThroughGeometryShaderGenerator() {}

    public static Shader generateLazily(Shader vertexShader, String geometryShaderPath, PrimitiveMode primitiveMode) {
        return generate(vertexShader, geometryShaderPath, primitiveMode, true);
    }

    public static Shader generate(Shader vertexShader, String geometryShaderPath, PrimitiveMode primitiveMode) {
        return generate(vertexShader, geometryShaderPath, primitiveMode, false);
    }

    private static Shader generate(Shader vertexShader, String geometryShaderPath, PrimitiveMode primitiveMode, boolean isLazy) {
        Matcher matcher = OUT_INTERFACE_BLOCK.matcher(vertexShader.getSource());
        if (matcher.matches()) {
            InputStream shaderInputStream = ClassLoader.getSystemResourceAsStream(geometryShaderPath);
            String interfaceName = matcher.group("name");
            String interfaceContent = matcher.group("content");
            PrimitiveModeData modeData = PrimitiveModeData.from(primitiveMode);
            StringBuilder sourceBuilder = new StringBuilder();
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(shaderInputStream))) {
                String line;
                boolean startOfMain = false;
                while ((line = reader.readLine()) != null) {
                    if (line.startsWith(VERSION_DIRECTIVE)) {
                        sourceBuilder.append(line).append(lineSeparator()).append(lineSeparator());
                        sourceBuilder.append(modeData.getInAndOutputLayout()).append(lineSeparator()).append(lineSeparator());
                        inAndOutInterfaces(sourceBuilder, interfaceName, interfaceContent, modeData.getVertexCount());
                    } else if (line.startsWith(MAIN_FUNCTION)) {
                        sourceBuilder.append(line).append(lineSeparator());
                        startOfMain = true;
                    } else if (startOfMain && line.startsWith("}")) {
                        modeData.copyPrimitives(sourceBuilder, interfaceContentToVariables(interfaceContent));
                        sourceBuilder.append("\tEndPrimitive();").append(lineSeparator());
                        sourceBuilder.append(line).append(lineSeparator());
                        startOfMain = false;
                    } else {
                        sourceBuilder.append(line).append(lineSeparator());
                    }
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
            return isLazy ? newStaticLazyShader(sourceBuilder.toString(), ShaderType.GEOMETRY_SHADER) : newStaticShader(sourceBuilder.toString(), ShaderType.GEOMETRY_SHADER);
        }
        throw new IllegalStateException("Unable to determine output of " + vertexShader);
    }

    private static String[] interfaceContentToVariables(String interfaceContent) {
        String[] split = interfaceContent.strip().split(lineSeparator());
        return Arrays.stream(split).map(s -> ANYTHING_FOLLOWED_BY_WHITESPACE.matcher(s).replaceFirst(""))
                .map(s -> s.substring(0, s.length() - 1)).toArray(String[]::new);
    }

    private static void inAndOutInterfaces(StringBuilder sourceBuilder, String interfaceName, String interfaceContent, int vertexCount) {
        sourceBuilder.append("in ").append(interfaceName).append('{').append(interfaceContent).append("} gs_in[").append(vertexCount).append("];")
                .append(lineSeparator()).append(lineSeparator())
                .append("out ").append(interfaceName).append('{').append(interfaceContent).append("} gs_out;")
                .append(lineSeparator());
    }

    private enum PrimitiveModeData {

        POINTS("layout (points) in;" + lineSeparator() + "layout (points, max_vertices = 1) out;", 1),
        LINES("layout (lines) in;" + lineSeparator() + "layout (line_strip, max_vertices = 2) out;", 2),
        TRIANGLES("layout (triangles) in;" + lineSeparator() + "layout (triangle_strip, max_vertices = 3) out;", 3),
        LINES_ADJACENCY("layout (lines_adjacency) in;" + lineSeparator() + "layout (line_strip, max_vertices = 2) out;", 4, 1, 3, 1),
        TRIANGLES_ADJACENCY("layout (triangles_adjacency) in;" + lineSeparator() + "layout (triangle_strip, max_vertices = 3) out;", 6, 0, 6, 2);

        private final String inAndOutputLayout;
        private final int vertexCount;
        private final int start, end, advance;

        PrimitiveModeData(String inAndOutputLayout, int vertexCount, int start, int end, int advance) {
            this.inAndOutputLayout = inAndOutputLayout;
            this.vertexCount = vertexCount;
            this.start = start;
            this.end = end;
            this.advance = advance;
        }

        PrimitiveModeData(String inAndOutputLayout, int vertexCount) {
            this(inAndOutputLayout, vertexCount, 0, vertexCount, 1);
        }

        private String getInAndOutputLayout() {
            return inAndOutputLayout;
        }

        private int getVertexCount() {
            return vertexCount;
        }

        @SuppressWarnings("Convert2streamapi")
        private void copyPrimitives(StringBuilder sourceBuilder, String[] interfaceContent) {
            for (int i = start; i < end; i += advance) {
                sourceBuilder.append("\tgl_Position = gl_in[").append(i).append("].gl_Position;").append(lineSeparator());
                sourceBuilder.append(copyInterfaceContent(interfaceContent, i));
                sourceBuilder.append("\tEmitVertex();").append(lineSeparator());
            }
        }

        private static PrimitiveModeData from(PrimitiveMode primitiveMode) {
            return switch (primitiveMode) {
                case POINTS -> POINTS;
                case LINES, LINE_STRIP, LINE_LOOP -> LINES;
                case TRIANGLES, TRIANGLE_STRIP, TRIANGLE_FAN -> TRIANGLES;
                case LINES_ADJACENCY, LINE_STRIP_ADJACENCY -> LINES_ADJACENCY;
                case TRIANGLES_ADJACENCY, TRIANGLE_STRIP_ADJACENCY -> TRIANGLES_ADJACENCY;
            };
        }

        private static String copyInterfaceContent(String[] interfaceContent, int vertexIndex) {
            return Arrays.stream(interfaceContent).map(variableName -> "\tgs_out." + variableName + " = gs_in[" + vertexIndex + "]." + variableName + ';' + lineSeparator()).collect(joining());
        }
    }
}
