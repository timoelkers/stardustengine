package de.oelkers.stardust.graphics.rendering.components;

import de.oelkers.stardust.graphics.assets.textures.BaseTexture;
import de.oelkers.stardust.graphics.assets.textures.CubeMap;
import de.oelkers.stardust.graphics.assets.textures.Texture1D;
import de.oelkers.stardust.graphics.shader.UniformAccessor;

import java.io.Serial;
import java.io.Serializable;

import static de.oelkers.stardust.graphics.rendering.components.RenderTechnique.FilePathOrContent.fromFilePath;

public class TextureBlendTechnique<T extends BaseTexture & Serializable> implements RenderTechnique {

    @Serial
    private static final long serialVersionUID = 1L;
    private static final FilePathOrContent FILE_PATH_2D = fromFilePath("techniques/blendTextures2D.tech");
    private static final FilePathOrContent FILE_PATH_CUBE_MAP = fromFilePath("techniques/blendTextureCubeMap.tech");

    private final T blendTexture;
    private float blendFactor;
    private boolean isIncreasing;

    public TextureBlendTechnique(T blendTexture) {
        this.blendTexture = blendTexture;
        if (blendTexture instanceof Texture1D) {
            throw new IllegalArgumentException("Texture blending is not supported on 1D textures!");
        }
    }

    public T getBlendTexture() {
        return blendTexture;
    }

    public float getBlendFactor() {
        return blendFactor;
    }

    public void setBlendFactor(float blendFactor) {
        this.blendFactor = Math.max(Math.min(blendFactor, 1.0f), 0.0f);
    }

    public void increment(float increment) {
        blendFactor = Math.min(blendFactor + increment, 1.0f);
    }

    public void decrement(float decrement) {
        blendFactor = Math.max(blendFactor - decrement, 0.0f);
    }

    public void oscillate(float change) {
        if (isIncreasing) {
            increment(change);
            if (blendFactor == 1.0f) {
                isIncreasing = false;
            }
        } else {
            decrement(change);
            if (blendFactor == 0.0f) {
                isIncreasing = true;
            }
        }
    }

    @Override
    public int apply(UniformAccessor uniformAccessor, int techniqueNumber, int textureUnitIndex) {
        uniformAccessor.setUniform("blendFactor_" + techniqueNumber, blendFactor);
        blendTexture.bind(textureUnitIndex++);
        return textureUnitIndex;
    }

    @Override
    public FilePathOrContent getFilePathOrContent() {
        return blendTexture instanceof CubeMap ? FILE_PATH_CUBE_MAP : FILE_PATH_2D;
    }
}
