package de.oelkers.stardust.graphics.rendering;

import de.oelkers.stardust.entities.Entity;
import de.oelkers.stardust.graphics.rendering.components.RenderComponent;

import java.util.ArrayList;
import java.util.Collection;

public abstract class ToggleRendering {

    private final Collection<RenderComponent> renderComponents = new ArrayList<>();
    private final Collection<ToggleRendering> children = new ArrayList<>();
    private final RenderSystem renderSystem;
    private boolean isRendering = true;

    protected ToggleRendering(RenderSystem renderSystem) {
        this.renderSystem = renderSystem;
    }

    public void setRendering(boolean rendering) {
        if (isRendering != rendering) {
            for (RenderComponent component : renderComponents) {
                component.setEnabled(rendering);
            }
            for (ToggleRendering child : children) {
                child.setRendering(rendering);
            }
            isRendering = rendering;
        }
    }

    public void disableRendering() {
        setRendering(false);
    }

    public void enableRendering() {
        setRendering(true);
    }

    public void toggleRendering() {
        setRendering(!isRendering);
    }

    public void clearEntities() {
        for (Entity entity : getEntities()) {
            renderSystem.removeEntity(entity);
        }
        getEntities().clear();
        renderComponents.clear();
    }

    protected Entity addEntity(Entity entity) {
        getEntities().add(entity);
        RenderComponent renderComponent = entity.getComponentOrNull(RenderComponent.class);
        renderComponent.setEnabled(isRendering);
        renderComponents.add(renderComponent);
        renderSystem.addEntity(entity);
        return entity;
    }

    protected <T extends ToggleRendering> T addChild(T child) {
        children.add(child);
        return child;
    }

    protected abstract Collection<Entity> getEntities();
}
