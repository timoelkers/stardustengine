package de.oelkers.stardust.graphics.rendering;

import de.oelkers.stardust.entities.Entity;
import de.oelkers.stardust.graphics.rendering.components.RenderComponent;
import de.oelkers.stardust.utils.SerializableComparator;
import de.oelkers.stardust.utils.collections.ConcurrentLinkedHashSet;

import java.io.Serial;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.function.Supplier;

public final class RenderingOrder implements Supplier<Set<Entity>> {

    /**
     * Defines a rendering order based on the given comparator. The comparator should
     * never return 0 unless the entity is not supposed to be rendered.
     *
     * @param comparator the comparator defining the rendering order
     */
    public static RenderingOrder of(SerializableComparator<Entity> comparator) {
        return new RenderingOrder(() -> new ConcurrentSkipListSet<>(comparator));
    }

    /**
     * Defines a rendering order based on the comparator defined by {@link RenderComponent#compareTo(RenderComponent)},
     * which normally tries to provide the most efficient rendering order.
     * This requires all entities to have a {@link RenderComponent}, an exception is thrown otherwise.
     * This does not exclude any entities and uses the natural entity order (order in which they were
     * created) if the components are equal.
     */
    public static RenderingOrder defaultOrder() {
        return of(new DefaultEntityComparator());
    }

    /**
     * Defines a rendering order that sticks with the order the entity was inserted
     * in whatever system that renders the entity.
     */
    public static RenderingOrder insertionOrder() {
        return new RenderingOrder(ConcurrentLinkedHashSet::new);
    }

    /**
     * Defines a rendering order based on the natural entity order (order
     * in which they were created).
     */
    public static RenderingOrder creationOrder() {
        return of(new NaturalComparator());
    }

    /**
     * Defines a undefined rendering order, which is not very efficient
     * and thus not recommended. If you do not care about the order, stick
     * with {@link #defaultOrder()}.
     */
    public static RenderingOrder undefined() {
        return new RenderingOrder(ConcurrentHashMap::newKeySet);
    }

    private final Supplier<Set<Entity>> entitySet;

    private RenderingOrder(Supplier<Set<Entity>> entitySet) {
        this.entitySet = entitySet;
    }

    @Override
    public Set<Entity> get() {
        return entitySet.get();
    }

    private static final class DefaultEntityComparator implements SerializableComparator<Entity> {

        @Serial
        private static final long serialVersionUID = 1L;

        @Override
        public int compare(Entity o1, Entity o2) {
            RenderComponent renderComponent1 = o1.getComponent(RenderComponent.class).orElseThrow();
            RenderComponent renderComponent2 = o2.getComponent(RenderComponent.class).orElseThrow();
            int comparedComponents = renderComponent1.compareTo(renderComponent2);
            if (comparedComponents == 0) {
                return o1.compareTo(o2);
            }
            return comparedComponents;
        }
    }

    private static final class NaturalComparator implements SerializableComparator<Entity> {

        @Serial
        private static final long serialVersionUID = 1L;

        @Override
        public int compare(Entity o1, Entity o2) {
            return o1.compareTo(o2);
        }
    }
}
