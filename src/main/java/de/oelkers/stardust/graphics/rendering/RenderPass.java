package de.oelkers.stardust.graphics.rendering;

import de.oelkers.stardust.camera.Camera;
import de.oelkers.stardust.entities.Entity;
import de.oelkers.stardust.graphics.rendering.components.RenderComponent;
import de.oelkers.stardust.math.transformations.Transformable;
import de.oelkers.stardust.utils.NonThrowingAutoCloseable;

public interface RenderPass extends NonThrowingAutoCloseable {

    /**
     * This method should be used to set uniform variables or OpenGL states which need to be set only
     * once per frame. If you are rendering using the {@link RenderSystem},
     * this will be called before the rendering process starts.
     */
    void onRenderStart(Camera camera);

    /**
     * This method should be used to set uniform variables or OpenGL states which need to be set for each
     * individual {@link RenderComponent}. If you are rendering using a {@link RenderSystem},
     * this will be called for each render component.
     *
     * @param entity the currently processed entity
     */
    void render(Entity entity, Transformable transformable, Camera camera);

    /**
     * This method should be used to clean up between frames and undo OpenGL state changes.
     * If you are rendering using a {@link RenderSystem}, this will be called after the rendering process ends.
     */
    void onRenderStop();

    boolean accepts(Entity entity);
}
