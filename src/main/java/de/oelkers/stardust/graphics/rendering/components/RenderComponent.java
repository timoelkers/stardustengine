package de.oelkers.stardust.graphics.rendering.components;

import de.oelkers.stardust.OpenGLContext;
import de.oelkers.stardust.camera.Camera;
import de.oelkers.stardust.config.ConfigStore;
import de.oelkers.stardust.entities.Component;
import de.oelkers.stardust.graphics.assets.models.BaseModel;
import de.oelkers.stardust.graphics.assets.textures.BaseTexture;
import de.oelkers.stardust.graphics.rendering.BlendMode;
import de.oelkers.stardust.graphics.shader.ShaderProgram;
import de.oelkers.stardust.graphics.shader.UniformAccessor;
import de.oelkers.stardust.math.coordinates.ClipRegion;
import de.oelkers.stardust.math.transformations.Transformable;
import org.jetbrains.annotations.NotNull;

import java.io.Serial;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public abstract class RenderComponent implements Component, Comparable<RenderComponent> {

    @Serial
    private static final long serialVersionUID = 1L;

    private final List<RenderTechnique> renderTechniques;
    private final BaseModel model;
    private final BaseTexture texture;
    private final ShaderProgram shaderProgram;
    private final BlendMode blendMode;
    private final int renderOrder;
    private final boolean fixedViewport;
    private final ClipRegion clipRegion;
    private boolean isEnabled = true;

    protected RenderComponent(AbstractBuilder<?, ?> builder) {
        renderTechniques = builder.renderTechniques;
        model = builder.model;
        texture = builder.texture;
        blendMode = builder.blendMode;
        shaderProgram = builder.shaderProgram;
        renderOrder = builder.renderOrder;
        fixedViewport = builder.fixedViewport;
        clipRegion = builder.clipRegion;
    }

    public ShaderProgram getShaderProgram() {
        return shaderProgram;
    }

    public BlendMode getBlendMode() {
        return blendMode;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public int getRenderOrder() {
        return renderOrder;
    }

    public Iterable<RenderTechnique> getRenderTechniques() {
        return renderTechniques;
    }

    public boolean hasFixedViewport() {
        return fixedViewport;
    }

    public ClipRegion getClipRegion() {
        return clipRegion;
    }

    @Override
    public int compareTo(@NotNull RenderComponent o) {
        int comparedRenderOrder = Integer.compare(renderOrder, o.renderOrder);
        if (comparedRenderOrder == 0) {
            int comparedShaders = shaderProgram.compareTo(o.shaderProgram);
            if (comparedShaders == 0) {
                int comparedModel = model.compareTo(o.model);
                if (comparedModel == 0) {
                    int comparedTexture = texture.compareTo(o.texture);
                    if (comparedTexture == 0) {
                        int comparedClipRegion = compareNull(clipRegion, o.clipRegion);
                        if (comparedClipRegion == 0) {
                            return blendMode.compareTo(o.blendMode);
                        }
                        return comparedClipRegion;
                    }
                    return comparedTexture;
                }
                return comparedModel;
            }
            return comparedShaders;
        }
        return comparedRenderOrder;
    }

    public final void render(Camera camera, Transformable transformable) {
        shaderProgram.bind();
        blendMode.bind();
        model.bind();
        texture.bind(0);
        applyTechniques();
        if (ConfigStore.get().isUsingIndividualViewports()) {
            shaderProgram.getUniformAccessor().setUniform("useFixedViewport", fixedViewport);
        }
        if (clipRegion == null) {
            OpenGLContext.get().setScissorTest(false);
        } else {
            OpenGLContext.get().setScissorTest(true);
            clipRegion.apply(camera.getWindow());
        }
        onPreRender(shaderProgram.getUniformAccessor(), camera, transformable);
        model.render();
        onPostRender(shaderProgram.getUniformAccessor(), camera, transformable);
    }

    protected abstract void onPreRender(UniformAccessor uniformAccessor, Camera camera, Transformable transformable);
    protected abstract void onPostRender(UniformAccessor uniformAccessor, Camera camera, Transformable transformable);

    private void applyTechniques() {
        int currentTextureUnitIndex = 1;
        for (int i = 0; i < renderTechniques.size(); i++) {
            RenderTechnique renderTechnique = renderTechniques.get(i);
            currentTextureUnitIndex = renderTechnique.apply(shaderProgram.getUniformAccessor(), i, currentTextureUnitIndex);
        }
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof RenderComponent && compareTo((RenderComponent) obj) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(renderTechniques, model, texture, shaderProgram, clipRegion, blendMode);
    }

    private static int compareNull(Object object1, Object object2) {
        if ((object1 == null) == (object2 == null)) {
            return 0;
        }
        return object1 == null ? -1 : 1;
    }

    protected abstract static class AbstractBuilder<C extends RenderComponent, B extends AbstractBuilder<C, B>> {

        private final BaseModel model;
        private final BaseTexture texture;
        private final ShaderProgram shaderProgram;
        private final List<RenderTechnique> renderTechniques = new ArrayList<>();
        private BlendMode blendMode = BlendMode.NONE;
        private int renderOrder = 0;
        private boolean fixedViewport;
        private ClipRegion clipRegion;

        protected AbstractBuilder(BaseModel model, BaseTexture texture, ShaderProgram shaderProgram) {
            this.model = model;
            this.texture = texture;
            this.shaderProgram = shaderProgram;
        }

        @SuppressWarnings("unchecked")
        public B withRenderTechniques(Collection<RenderTechnique> renderTechniques) {
            this.renderTechniques.addAll(renderTechniques);
            return (B) this;
        }

        @SuppressWarnings("unchecked")
        public B withRenderOrder(int renderOrder) {
            this.renderOrder = renderOrder;
            return (B) this;
        }

        @SuppressWarnings("unchecked")
        public B withBlendMode(BlendMode blendMode) {
            this.blendMode = blendMode;
            return (B) this;
        }

        @SuppressWarnings("unchecked")
        public B withFixedViewport(boolean fixedViewport) {
            this.fixedViewport = fixedViewport;
            return (B) this;
        }

        @SuppressWarnings("unchecked")
        public B withClipRegion(ClipRegion clipRegion) {
            this.clipRegion = clipRegion;
            return (B) this;
        }

        public abstract C build();
    }
}