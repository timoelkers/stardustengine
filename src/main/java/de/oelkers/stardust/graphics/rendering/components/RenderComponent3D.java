package de.oelkers.stardust.graphics.rendering.components;

import de.oelkers.stardust.camera.Camera;
import de.oelkers.stardust.graphics.assets.models.Model3D;
import de.oelkers.stardust.graphics.assets.textures.Texture2D;
import de.oelkers.stardust.graphics.shader.ShaderProgram;
import de.oelkers.stardust.graphics.shader.UniformAccessor;
import de.oelkers.stardust.math.matrices.Matrix4f;
import de.oelkers.stardust.math.transformations.Transformable;

import java.io.Serial;

public final class RenderComponent3D extends RenderComponent {

    @Serial
    private static final long serialVersionUID = 1L;

    private final Model3D model;
    private final Texture2D texture;

    private RenderComponent3D(Builder builder) {
        super(builder);
        model = builder.model;
        texture = builder.texture;
    }

    public Model3D getModel() {
        return model;
    }

    public Texture2D getTexture() {
        return texture;
    }

    @Override
    protected void onPreRender(UniformAccessor uniformAccessor, Camera camera, Transformable transformable) {
        Matrix4f.POOL.consume(modelViewProjectionMatrix -> {
            camera.getProjectionViewMatrix().mul(transformable.getModelMatrix(), modelViewProjectionMatrix);
            uniformAccessor.setUniform("modelViewProjectionMatrix", modelViewProjectionMatrix);
        });
        Matrix4f.POOL.consume(normalMatrix -> {
            transformable.getModelMatrix().invert(normalMatrix).transpose();
            uniformAccessor.setUniform("normalMatrix", normalMatrix);
        });
        uniformAccessor.setUniform("modelMatrix", transformable.getModelMatrix());
    }

    @Override
    protected void onPostRender(UniformAccessor uniformAccessor, Camera camera, Transformable transformable) {}

    public static class Builder extends AbstractBuilder<RenderComponent3D, Builder> {

        private final Model3D model;
        private final Texture2D texture;

        public Builder(Model3D model, Texture2D texture, ShaderProgram shaderProgram) {
            super(model, texture, shaderProgram);
            this.model = model;
            this.texture = texture;
        }

        @Override
        public RenderComponent3D build() {
            return new RenderComponent3D(this);
        }
    }
}
