package de.oelkers.stardust.graphics.rendering.components;

import de.oelkers.stardust.graphics.assets.textures.Texture1D;
import de.oelkers.stardust.graphics.shader.UniformAccessor;

import java.awt.*;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serial;
import java.util.HashMap;
import java.util.Map;

import static de.oelkers.stardust.graphics.rendering.components.RenderTechnique.FilePathOrContent.fromFilePath;

public class ColorReplacementTechnique implements RenderTechnique {

    @Serial
    private static final long serialVersionUID = 1L;
    private static final FilePathOrContent FILE_PATH = fromFilePath("techniques/colorReplacement.tech");

    private final transient Map<Color, Color> replacements = new HashMap<>();
    private final transient boolean textureChangeable;
    private float tolerance;
    private Texture1D srcTexture;
    private Texture1D dstTexture;

    public ColorReplacementTechnique() {
        textureChangeable = true;
    }

    public ColorReplacementTechnique(Color srcColor, Color dstColor) {
        replacements.put(srcColor, dstColor);
        textureChangeable = true;
    }

    public ColorReplacementTechnique(Texture1D srcTexture, Texture1D dstTexture) {
        this.srcTexture = srcTexture;
        this.dstTexture = dstTexture;
        textureChangeable = false;
    }

    public void setTolerance(float tolerance) {
        this.tolerance = Math.max(Math.min(tolerance, 1.0f), 0.0f);
    }

    public void addReplacement(Color srcColor, Color dstColor) {
        if (textureChangeable) {
            replacements.put(srcColor, dstColor);
        }
    }

    public int getNumberOfReplacements() {
        return srcTexture.getWidth();
    }

    public Texture1D getSrcTexture() {
        if (srcTexture == null || textureChangeable && srcTexture.getWidth() != replacements.size()) {
            srcTexture = new Texture1D(replacements.keySet().toArray(new Color[0]));
        }
        return srcTexture;
    }

    public Texture1D getDstTexture() {
        if (dstTexture == null || textureChangeable && dstTexture.getWidth() != replacements.size()) {
            dstTexture = new Texture1D(replacements.values().toArray(new Color[0]));
        }
        return dstTexture;
    }

    @Override
    public int apply(UniformAccessor uniformAccessor, int techniqueNumber, int textureUnitIndex) {
        getSrcTexture().bind(textureUnitIndex++);
        getDstTexture().bind(textureUnitIndex++);
        uniformAccessor.setUniform("tolerance_" + techniqueNumber, tolerance);
        uniformAccessor.setUniform("numberOfReplacements_" + techniqueNumber, srcTexture.getWidth());
        return textureUnitIndex;
    }

    @Override
    public FilePathOrContent getFilePathOrContent() {
        return FILE_PATH;
    }

    @Serial
    private void writeObject(ObjectOutputStream stream) throws IOException {
        getSrcTexture(); // initialize srcTexture if needed
        getDstTexture(); // initialize dstTexture if needed
        stream.defaultWriteObject();
    }
}
