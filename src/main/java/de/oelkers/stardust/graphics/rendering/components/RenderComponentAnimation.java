package de.oelkers.stardust.graphics.rendering.components;

import de.oelkers.stardust.camera.Camera;
import de.oelkers.stardust.graphics.animations.AnimatedModel3D;
import de.oelkers.stardust.graphics.animations.AnimationComponent;
import de.oelkers.stardust.graphics.assets.textures.Texture2D;
import de.oelkers.stardust.graphics.shader.ShaderProgram;
import de.oelkers.stardust.graphics.shader.UniformAccessor;
import de.oelkers.stardust.math.matrices.Matrix4f;
import de.oelkers.stardust.math.transformations.Transformable;

import java.io.Serial;

public final class RenderComponentAnimation extends RenderComponent {

    @Serial
    private static final long serialVersionUID = 1L;

    private final AnimationComponent animation;
    private final AnimatedModel3D model;
    private final Texture2D texture;

    private RenderComponentAnimation(Builder builder) {
        super(builder);
        animation = builder.animation;
        model = builder.model;
        texture = builder.texture;
    }

    public AnimatedModel3D getModel() {
        return model;
    }

    public Texture2D getTexture() {
        return texture;
    }

    @Override
    protected void onPreRender(UniformAccessor uniformAccessor, Camera camera, Transformable transformable) {
        Matrix4f.POOL.consume(modelViewProjectionMatrix -> {
            camera.getProjectionViewMatrix().mul(transformable.getModelMatrix(), modelViewProjectionMatrix);
            uniformAccessor.setUniform("modelViewProjectionMatrix", modelViewProjectionMatrix);
            uniformAccessor.setUniform("jointTransforms", animation.getCurrentJointTransforms());
        });
        Matrix4f.POOL.consume(normalMatrix -> {
            transformable.getModelMatrix().invert(normalMatrix).transpose();
            uniformAccessor.setUniform("normalMatrix", normalMatrix);
        });
        uniformAccessor.setUniform("modelMatrix", transformable.getModelMatrix());
    }

    @Override
    protected void onPostRender(UniformAccessor uniformAccessor, Camera camera, Transformable transformable) {}

    public static class Builder extends AbstractBuilder<RenderComponentAnimation, Builder> {

        private final AnimationComponent animation;
        private final AnimatedModel3D model;
        private final Texture2D texture;

        public Builder(AnimationComponent animation, AnimatedModel3D model, Texture2D texture, ShaderProgram shaderProgram) {
            super(model, texture, shaderProgram);
            this.animation = animation;
            this.model = model;
            this.texture = texture;
        }

        @Override
        public RenderComponentAnimation build() {
            return new RenderComponentAnimation(this);
        }
    }
}
