package de.oelkers.stardust.graphics.rendering.components;

import de.oelkers.stardust.graphics.shader.UniformAccessor;

import java.io.Serializable;
import java.util.Objects;

public interface RenderTechnique extends Serializable {

    int apply(UniformAccessor uniformAccessor, int techniqueNumber, int textureUnitIndex);

    /**
     * Returns either the file path to the render technique or the actual technique code. Please note that
     * caching is performed on the given value, so changing the content of the file while keeping the
     * file path the same has no effect. Dynamically changing the file path or the content itself is fine on the other hand.
     */
    FilePathOrContent getFilePathOrContent();

    final class FilePathOrContent {

        public static FilePathOrContent fromFilePath(String filePath) {
            return new FilePathOrContent(filePath, true);
        }

        public static FilePathOrContent fromContent(String content) {
            return new FilePathOrContent(content, false);
        }

        private final String filePathOrContent;
        private final boolean isFilePath;

        private FilePathOrContent(String filePathOrContent, boolean isFilePath) {
            this.filePathOrContent = filePathOrContent;
            this.isFilePath = isFilePath;
        }

        public String getFilePathOrContent() {
            return filePathOrContent;
        }

        public boolean isFilePath() {
            return isFilePath;
        }

        @Override
        public int hashCode() {
            return Objects.hash(filePathOrContent, isFilePath);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            FilePathOrContent other = (FilePathOrContent) obj;
            return filePathOrContent.equals(other.filePathOrContent) && isFilePath == other.isFilePath;
        }

        @Override
        public String toString() {
            return filePathOrContent;
        }
    }
}
