package de.oelkers.stardust.graphics.rendering.components;

import de.oelkers.stardust.entities.Entity;
import de.oelkers.stardust.graphics.animations.AnimatedModel3D;
import de.oelkers.stardust.graphics.animations.AnimationComponent;
import de.oelkers.stardust.graphics.assets.models.BaseModel.PrimitiveMode;
import de.oelkers.stardust.graphics.assets.models.Model2D;
import de.oelkers.stardust.graphics.assets.models.Model3D;
import de.oelkers.stardust.graphics.assets.textures.BaseTexture;
import de.oelkers.stardust.graphics.assets.textures.CubeMap;
import de.oelkers.stardust.graphics.assets.textures.Texture1D;
import de.oelkers.stardust.graphics.assets.textures.Texture2D;
import de.oelkers.stardust.graphics.lights.LightingTechnique;
import de.oelkers.stardust.graphics.rendering.components.RenderComponent2D.Builder;
import de.oelkers.stardust.graphics.shader.ShaderGenerator;
import de.oelkers.stardust.graphics.shader.ShaderProgram;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static de.oelkers.stardust.graphics.rendering.components.RenderComponentSkybox.Builder.SKYBOX_MODEL;

public final class RenderTechniques {

    private RenderTechniques() {}

    public static RenderComponentBuilder2D withTexture(Model2D model, Texture2D texture) {
        return new RenderComponentBuilder2D(model, texture, "shader/guiShader.vert", "shader/guiTextureShader.frag");
    }

    public static RenderComponentBuilder3D withTexture(Model3D model, Texture2D texture) {
        return new RenderComponentBuilder3D(model, texture, "shader/entityShader.vert", "shader/entityTextureShader.frag");
    }

    public static RenderComponentBuilderAnimation withAnimation(AnimationComponent animation, AnimatedModel3D model, Texture2D texture) {
        return new RenderComponentBuilderAnimation(animation, model, texture, "shader/animatedEntityShader.vert", "shader/entityTextureShader.frag");
    }

    public static RenderComponentBuilderSkybox asSkybox(CubeMap texture) {
        return new RenderComponentBuilderSkybox(texture, "shader/skyboxShader.vert", "shader/skyboxShader.frag");
    }

    public static RenderComponentBuilderLine asLine(Model2D model, Color color, float lineWidth) {
        if (model.getPrimitiveMode() != PrimitiveMode.LINES) {
            throw new IllegalArgumentException(model + " has to have a primitive mode of " + PrimitiveMode.LINES);
        }
        return new RenderComponentBuilderLine(model, new Texture2D(color), lineWidth, "shader/guiShader.vert", "shader/lineShader.geom", "shader/guiTextureShader.frag");
    }

    private abstract static class BaseBuilder<C extends RenderComponent, B extends RenderComponent.AbstractBuilder<C, B>> {

        private final List<RenderTechnique> techniques = new ArrayList<>();
        private final String vertexShaderPath, geometryShader, fragmentShaderPath;
        private final PrimitiveMode primitiveMode;

        private BaseBuilder(String vertexShaderPath, String geometryShader, String fragmentShaderPath) {
            this.vertexShaderPath = vertexShaderPath;
            this.geometryShader = geometryShader;
            this.fragmentShaderPath = fragmentShaderPath;
            primitiveMode = null;
        }

        private BaseBuilder(String vertexShaderPath, String fragmentShaderPath, PrimitiveMode primitiveMode) {
            this.vertexShaderPath = vertexShaderPath;
            this.fragmentShaderPath = fragmentShaderPath;
            geometryShader = null;
            this.primitiveMode = primitiveMode;
        }

        protected <L extends RenderTechnique> L add(L renderTechnique) {
            techniques.add(renderTechnique);
            return renderTechnique;
        }

        public B getBuilder() {
            ShaderProgram shaderProgram;
            if (geometryShader == null) {
                shaderProgram = ShaderGenerator.generateLazily(techniques, vertexShaderPath, fragmentShaderPath, primitiveMode);
            } else {
                shaderProgram = ShaderGenerator.generateLazily(techniques, vertexShaderPath, geometryShader, fragmentShaderPath);
            }
            return getBuilder(shaderProgram).withRenderTechniques(techniques);
        }

        public C build(Entity entity) {
            C renderComponent = getBuilder().build();
            entity.addComponent(renderComponent);
            return renderComponent;
        }

        protected abstract B getBuilder(ShaderProgram shaderProgram);
    }

    private abstract static class BaseTextureBuilder<C extends RenderComponent, T extends BaseTexture & Serializable, B extends RenderComponent.AbstractBuilder<C, B>> extends BaseBuilder<C, B>  {

        private BaseTextureBuilder(String vertexShaderPath, String fragmentShaderPath, PrimitiveMode primitiveMode) {
            super(vertexShaderPath, fragmentShaderPath, primitiveMode);
        }

        public ColorReplacementTechnique addColorReplacement() {
            return add(new ColorReplacementTechnique());
        }

        public ColorReplacementTechnique addColorReplacement(Color srcColor, Color dstColor) {
            return add(new ColorReplacementTechnique(srcColor, dstColor));
        }

        public ColorReplacementTechnique addColorReplacement(Texture1D srcTexture, Texture1D dstTexture) {
            return add(new ColorReplacementTechnique(srcTexture, dstTexture));
        }

        public TextureBlendTechnique<T> addTextureBlending(T blendTexture) {
            return add(new TextureBlendTechnique<>(blendTexture));
        }
    }

    public static final class RenderComponentBuilder3D extends BaseTextureBuilder<RenderComponent3D, Texture2D, RenderComponent3D.Builder> {

        private final Model3D model;
        private final Texture2D texture;

        public LightingTechnique addLightingTechnique() {
            return addLightingTechnique(new LightingTechnique());
        }

        public LightingTechnique addLightingTechnique(LightingTechnique lightingTechnique) {
            return add(lightingTechnique);
        }

        private RenderComponentBuilder3D(Model3D model, Texture2D texture, String vertexShaderPath, String fragmentShaderPath) {
            super(vertexShaderPath, fragmentShaderPath, model.getPrimitiveMode());
            this.model = model;
            this.texture = texture;
        }

        @Override
        public RenderComponent3D.Builder getBuilder(ShaderProgram shaderProgram) {
            return new RenderComponent3D.Builder(model, texture, shaderProgram);
        }
    }

    public static final class RenderComponentBuilder2D extends BaseTextureBuilder<RenderComponent2D, Texture2D, Builder> {

        private final Model2D model;
        private final Texture2D texture;

        private RenderComponentBuilder2D(Model2D model, Texture2D texture, String vertexShaderPath,  String fragmentShaderPath) {
            super(vertexShaderPath, fragmentShaderPath, model.getPrimitiveMode());
            this.model = model;
            this.texture = texture;
        }

        @Override
        protected Builder getBuilder(ShaderProgram shaderProgram) {
            return new RenderComponent2D.Builder(model, texture, shaderProgram);
        }
    }

    public static final class RenderComponentBuilderSkybox extends BaseTextureBuilder<RenderComponentSkybox, CubeMap, RenderComponentSkybox.Builder> {

        private final CubeMap cubeMap;

        private RenderComponentBuilderSkybox(CubeMap cubeMap, String vertexShaderPath, String fragmentShaderPath) {
            super(vertexShaderPath, fragmentShaderPath, SKYBOX_MODEL.getPrimitiveMode());
            this.cubeMap = cubeMap;
        }

        @Override
        protected RenderComponentSkybox.Builder getBuilder(ShaderProgram shaderProgram) {
            return new RenderComponentSkybox.Builder(cubeMap, shaderProgram);
        }
    }

    public static final class RenderComponentBuilderAnimation extends BaseTextureBuilder<RenderComponentAnimation, Texture2D, RenderComponentAnimation.Builder> {

        private final AnimationComponent animation;
        private final AnimatedModel3D model;
        private final Texture2D texture;

        private RenderComponentBuilderAnimation(AnimationComponent animation, AnimatedModel3D model, Texture2D texture, String vertexShaderPath, String fragmentShaderPath) {
            super(vertexShaderPath, fragmentShaderPath, model.getPrimitiveMode());
            this.animation = animation;
            this.model = model;
            this.texture = texture;
        }

        public LightingTechnique addLightingTechnique() {
            return addLightingTechnique(new LightingTechnique());
        }

        public LightingTechnique addLightingTechnique(LightingTechnique lightingTechnique) {
            return add(lightingTechnique);
        }

        @Override
        protected RenderComponentAnimation.Builder getBuilder(ShaderProgram shaderProgram) {
            return new RenderComponentAnimation.Builder(animation, model, texture, shaderProgram);
        }
    }

    public static final class RenderComponentBuilderLine extends BaseBuilder<RenderComponentLine, RenderComponentLine.Builder> {

        private final Model2D model;
        private final Texture2D texture;
        private final float lineWidth;

        private RenderComponentBuilderLine(Model2D model, Texture2D texture, float lineWidth, String vertexShaderPath, String geometryShaderPath, String fragmentShaderPath) {
            super(vertexShaderPath, geometryShaderPath, fragmentShaderPath);
            this.model = model;
            this.texture = texture;
            this.lineWidth = lineWidth;
        }

        @Override
        protected RenderComponentLine.Builder getBuilder(ShaderProgram shaderProgram) {
            return new RenderComponentLine.Builder(model, texture, lineWidth, shaderProgram);
        }
    }
}
