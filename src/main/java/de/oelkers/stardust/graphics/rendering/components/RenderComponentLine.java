package de.oelkers.stardust.graphics.rendering.components;

import de.oelkers.stardust.OpenGLContext;
import de.oelkers.stardust.camera.Camera;
import de.oelkers.stardust.graphics.assets.models.Model2D;
import de.oelkers.stardust.graphics.assets.textures.Texture2D;
import de.oelkers.stardust.graphics.shader.ShaderProgram;
import de.oelkers.stardust.graphics.shader.UniformAccessor;
import de.oelkers.stardust.math.transformations.Transformable;

import java.io.Serial;

public final class RenderComponentLine extends RenderComponent {

    @Serial
    private static final long serialVersionUID = 1L;

    private final Model2D model;
    private final Texture2D texture;
    private final float lineWidth;

    protected RenderComponentLine(Builder builder) {
        super(builder);
        model = builder.model;
        texture = builder.texture;
        lineWidth = builder.lineWidth;
    }

    public Model2D getModel() {
        return model;
    }

    public Texture2D getTexture() {
        return texture;
    }

    @Override
    protected void onPreRender(UniformAccessor uniformAccessor, Camera camera, Transformable transformable) {
        OpenGLContext.get().setDepthTest(false);
        uniformAccessor.setUniform("modelMatrix", transformable.getModelMatrix());
        uniformAccessor.setUniform("lineWidth", lineWidth);
    }

    @Override
    protected void onPostRender(UniformAccessor uniformAccessor, Camera camera, Transformable transformable) {
        OpenGLContext.get().setDepthTest(true);
    }

    public static final class Builder extends AbstractBuilder<RenderComponentLine, Builder> {

        private static final int DEFAULT_RENDER_ORDER = Integer.MAX_VALUE - 1000;

        private final Model2D model;
        private final Texture2D texture;
        private final float lineWidth;

        public Builder(Model2D model, Texture2D texture, float lineWidth, ShaderProgram shaderProgram) {
            super(model, texture, shaderProgram);
            this.model = model;
            this.texture = texture;
            this.lineWidth = lineWidth;
            withRenderOrder(DEFAULT_RENDER_ORDER);
        }

        @Override
        public RenderComponentLine build() {
            return new RenderComponentLine(this);
        }
    }
}
