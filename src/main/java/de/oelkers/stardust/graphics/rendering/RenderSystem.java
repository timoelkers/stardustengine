package de.oelkers.stardust.graphics.rendering;

import de.oelkers.stardust.camera.Camera;
import de.oelkers.stardust.entities.BaseEntitySystem;
import de.oelkers.stardust.entities.Entity;
import de.oelkers.stardust.entities.RootEntitySystem;
import de.oelkers.stardust.graphics.rendering.components.RenderComponent;
import de.oelkers.stardust.math.shapes.CollisionComponent;
import de.oelkers.stardust.math.transformations.Transformable;
import net.jcip.annotations.ThreadSafe;

import java.util.Optional;
import java.util.Set;

@ThreadSafe
public class RenderSystem extends BaseEntitySystem<Set<Entity>> {

    private volatile Camera camera;

    /**
     * Constructs the render system with the {@link RenderingOrder#defaultOrder()}
     * and the given camera. The rendering order can not be changed at runtime, the camera is passed
     * to the render pass and can be changed. This also registers itself as a subsystem of the {@link RootEntitySystem}.
     *
     * @param camera the camera used to render
     */
    public RenderSystem(Camera camera) {
        this(camera, RenderingOrder.defaultOrder());
    }

    /**
     * Constructs the render system with a given rendering order and camera.
     * The rendering order can not be changed at runtime, the camera is passed
     * to the render pass and can be changed. This also registers itself as a subsystem of the {@link RootEntitySystem}.
     *
     * @param camera the camera used to render
     * @param renderingOrder the order if which entities are rendered
     */
    public RenderSystem(Camera camera, RenderingOrder renderingOrder) {
        super(renderingOrder);
        this.camera = camera;
        RootEntitySystem.get().registerSubsystem(this);
    }

    public Camera getCamera() {
        return camera;
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
    }

    public void render() {
        render(BaseEntitySystem.GLOBAL_GROUP);
    }

    public synchronized void render(String entityGroup) {
        forEach(this::renderEntity, entityGroup);
    }

    @Override
    public boolean acceptsEntity(Entity entity) {
        return entity.hasComponents(RenderComponent.class, Transformable.class);
    }

    @Override
    public boolean addEntity(Entity entity) {
        entity.lockComponents(RenderComponent.class, Transformable.class);
        return super.addEntity(entity);
    }

    @Override
    public void removeEntity(Entity entity) {
        entity.unlockComponents(RenderComponent.class, Transformable.class);
        super.removeEntity(entity);
    }

    private void renderEntity(Entity entity) {
        RenderComponent renderComponent = entity.getComponentOrNull(RenderComponent.class);
        if (renderComponent.isEnabled()) {
            Optional<CollisionComponent> collisionComponent = entity.getComponent(CollisionComponent.class);
            Transformable transformable = entity.getComponentOrNull(Transformable.class);
            if (collisionComponent.isPresent()) {
                if (collisionComponent.get().isVisible(camera.getViewFrustum())) {
                    renderComponent.render(camera, transformable);
                }
            } else {
                renderComponent.render(camera, transformable);
            }
        }
    }
}
