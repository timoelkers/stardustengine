package de.oelkers.stardust.graphics.rendering;

import de.oelkers.stardust.OpenGLContext;
import org.lwjgl.opengl.GL11;

public enum BlendMode {

    NONE(GL11.GL_ONE, GL11.GL_ZERO),
    ADDITIVE(GL11.GL_ONE, GL11.GL_ONE),
    ALPHA(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

    private final int sFactor, dFactor;

    BlendMode(int sFactor, int dFactor) {
        this.sFactor = sFactor;
        this.dFactor = dFactor;
    }

    public int getSFactor() {
        return sFactor;
    }

    public int getDFactor() {
        return dFactor;
    }

    public void bind() {
        OpenGLContext.get().bindBlendMode(this);
    }
}

