package de.oelkers.stardust.graphics.rendering.components;

import de.oelkers.stardust.OpenGLContext;
import de.oelkers.stardust.camera.Camera;
import de.oelkers.stardust.graphics.assets.models.BaseModel.PrimitiveMode;
import de.oelkers.stardust.graphics.assets.models.Model3D;
import de.oelkers.stardust.graphics.assets.textures.CubeMap;
import de.oelkers.stardust.graphics.shader.ShaderProgram;
import de.oelkers.stardust.graphics.shader.UniformAccessor;
import de.oelkers.stardust.math.matrices.Matrix4f;
import de.oelkers.stardust.math.transformations.Transformable;
import de.oelkers.stardust.math.vectors.Vector3f;

import java.io.Serial;

public final class RenderComponentSkybox extends RenderComponent {

    @Serial
    private static final long serialVersionUID = 1L;

    private final CubeMap texture;

    private RenderComponentSkybox(Builder builder) {
        super(builder);
        texture = builder.texture;
    }

    public CubeMap getTexture() {
        return texture;
    }

    @Override
    protected void onPreRender(UniformAccessor uniformAccessor, Camera camera, Transformable transformable) {
        Matrix4f.POOL.consume(skyboxMatrix -> {
            skyboxMatrix.set(camera.getViewMatrix()).setTranslation(Vector3f.ZERO);
            camera.getProjectionMatrix().mul(skyboxMatrix, skyboxMatrix);
            skyboxMatrix.mul(transformable.getModelMatrix());
            uniformAccessor.setUniform("modelViewProjectionMatrix", skyboxMatrix);
        });
        OpenGLContext.get().setDepthWrite(false);
    }

    @Override
    protected void onPostRender(UniformAccessor uniformAccessor, Camera camera, Transformable transformable) {
        OpenGLContext.get().setDepthWrite(true);
    }

    public static class Builder extends AbstractBuilder<RenderComponentSkybox, Builder> {

        public static final Model3D SKYBOX_MODEL = getSkyboxModel();

        private static Model3D getSkyboxModel() {
            Vector3f[] vertices = {
                    new Vector3f(1.0f, -1.0f, -1.0f), new Vector3f(1.0f, -1.0f, 1.0f), new Vector3f(-1.0f, -1.0f, 1.0f), new Vector3f(-1.0f, -1.0f, -1.0f),
                    new Vector3f(1.0f, 1.0f, -1.0f), new Vector3f(-1.0f, 1.0f, -1.0f), new Vector3f(-1.0f, 1.0f, 1.0f), new Vector3f(1.0f, 1.0f, 1.0f),
                    new Vector3f(1.0f, -1.0f, -1.0f), new Vector3f(1.0f, 1.0f, -1.0f), new Vector3f(1.0f, 1.0f, 1.0f), new Vector3f(1.0f, -1.0f, 1.0f),
                    new Vector3f(1.0f, -1.0f, 1.0f), new Vector3f(1.0f, 1.0f, 1.0f), new Vector3f(-1.0f, 1.0f, 1.0f), new Vector3f(-1.0f, -1.0f, 1.0f),
                    new Vector3f(-1.0f, -1.0f, 1.0f), new Vector3f(-1.0f, 1.0f, 1.0f), new Vector3f(-1.0f, 1.0f, -1.0f), new Vector3f(-1.0f, -1.0f, -1.0f),
                    new Vector3f(1.0f, 1.0f, -1.0f), new Vector3f(1.0f, -1.0f, -1.0f), new Vector3f(-1.0f, -1.0f, -1.0f), new Vector3f(-1.0f, 1.0f, -1.0f)
            };
            int[] indices = {0, 2, 1, 0, 3, 2, 4, 6, 5, 4, 7, 6, 8, 10, 9, 8, 11, 10, 12, 14, 13, 12, 15, 14, 16, 18, 17, 16, 19, 18, 20, 22, 21, 20, 23, 22};

            Model3D model = new Model3D();
            model.addVertices(vertices, PrimitiveMode.TRIANGLES, true, false);
            model.addIndices(indices, true, false);
            return model;
        }

        private final CubeMap texture;

        public Builder(CubeMap texture, ShaderProgram shaderProgram) {
            super(SKYBOX_MODEL, texture, shaderProgram);
            this.texture = texture;
        }

        @Override
        public RenderComponentSkybox build() {
            return new RenderComponentSkybox(this);
        }
    }
}
