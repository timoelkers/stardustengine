package de.oelkers.stardust.graphics.animations;

import de.oelkers.stardust.math.matrices.ReadOnlyMatrix4f;
import de.oelkers.stardust.math.quaternions.ReadOnlyQuaternion;
import de.oelkers.stardust.math.vectors.ReadOnlyVector3f;

import java.io.Serial;
import java.io.Serializable;

public class KeyframeData implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    public float timeStamp;
    public JointTransformData[] jointTransforms;

    public static class JointTransformData implements Serializable {

        @Serial
        private static final long serialVersionUID = 1L;

        public String jointName;
        public ReadOnlyMatrix4f jointTransform;
        public ReadOnlyVector3f jointTranslation;
        public ReadOnlyQuaternion jointRotation;
    }
}
