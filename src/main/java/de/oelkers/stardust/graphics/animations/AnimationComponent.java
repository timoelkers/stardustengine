package de.oelkers.stardust.graphics.animations;

import de.oelkers.stardust.entities.Component;
import de.oelkers.stardust.graphics.animations.KeyframeData.JointTransformData;
import de.oelkers.stardust.graphics.assets.loading.AssetContainer;
import de.oelkers.stardust.math.matrices.Matrix4f;
import de.oelkers.stardust.math.matrices.ReadOnlyMatrix4f;
import de.oelkers.stardust.math.quaternions.Quaternion;
import de.oelkers.stardust.math.vectors.Vector3f;
import de.oelkers.stardust.utils.container.Container;
import de.oelkers.stardust.utils.container.FixedSizeContainer;

import java.io.Serial;
import java.util.HashMap;
import java.util.Map;

import static de.oelkers.stardust.utils.ArrayUtils.setAll;

public final class AnimationComponent implements Component, AssetContainer<AnimationComponent.AnimationData> {

    @Serial
    private static final long serialVersionUID = 1L;

    private KeyframeData[] keyframeData;
    private JointData rootJoint;
    private float animationTime;
    private boolean dataInjected;

    private transient float progression;
    private transient KeyframeData previousFrameData;
    private transient KeyframeData currentFrameData;
    private transient Map<String, Integer> jointNameToIndex;
    private transient Matrix4f[] currentJointTransforms;

    @Override
    public void inject(Container<AnimationData> container) {
        AnimationData data = container.getOnly();
        keyframeData = data.keyframeData;
        rootJoint = data.rootJoint;
        readResolve();
        dataInjected = true;
    }

    @Serial
    private Object readResolve() {
        jointNameToIndex = new HashMap<>();
        fillJointNameToIndexMap(rootJoint);
        currentJointTransforms = setAll(Matrix4f::new, new Matrix4f[jointNameToIndex.size()]);
        return this;
    }

    private void fillJointNameToIndexMap(JointData currentJoint) {
        jointNameToIndex.put(currentJoint.getJointName(), currentJoint.getJointIndex());
        for (JointData child : currentJoint.getChildren()) {
            fillJointNameToIndexMap(child);
        }
    }

    @Override
    public Container<AnimationData> getContainer() {
        return new FixedSizeContainer<>(1);
    }

    public void update(double frameTimeSeconds) {
        if (dataInjected) {
            increaseAnimationTime(frameTimeSeconds);
            calculateAnimationPose();
            applyAnimationPose(rootJoint, Matrix4f.IDENTITY);
        }
    }

    public ReadOnlyMatrix4f[] getCurrentJointTransforms() {
        return currentJointTransforms;
    }

    private void increaseAnimationTime(double frameTimeDeltaSeconds) {
        animationTime += frameTimeDeltaSeconds;
        float totalLength = keyframeData[keyframeData.length - 1].timeStamp;
        if (animationTime > totalLength) {
            animationTime %= totalLength;
        }
    }

    private void calculateAnimationPose() {
        setCurrentFrameData();
        setProgression();
        interpolate();
    }

    private void setCurrentFrameData() {
        previousFrameData = keyframeData[0];
        currentFrameData = keyframeData[0];
        for (int i = 1; i < keyframeData.length; i++) {
            currentFrameData = keyframeData[i];
            if (currentFrameData.timeStamp > animationTime) {
                break;
            }
            previousFrameData = keyframeData[i];
        }
    }

    private void setProgression() {
        float totalTime = currentFrameData.timeStamp - previousFrameData.timeStamp;
        float currentTime = animationTime - previousFrameData.timeStamp;
        progression = currentTime / totalTime;
    }

    // @FutureWork: we need to give the user the ability to chose the interpolation method or respect the interpolation method given
    // by the given file format (e.g INTERPOLATION semantic in collada files), linear interpolation should not be forced
    private void interpolate() {
        for (JointTransformData previousJointTransform : previousFrameData.jointTransforms) {
            // @Robustness: although the current implementation keeps the joint order consistent between keyframes, we do not state anywhere
            // that this is required and the cost of searching for the corresponding joint is minimal
            JointTransformData currentJointTransform = getByJointName(currentFrameData.jointTransforms, previousJointTransform.jointName);
            Matrix4f finalJointTransform = currentJointTransforms[jointNameToIndex.get(previousJointTransform.jointName)];
            Quaternion.POOL.consume(rotation -> {
                previousJointTransform.jointRotation.lerp(currentJointTransform.jointRotation, progression, rotation);
                Matrix4f.POOL.consume(matrix -> {
                    rotation.getRotationMatrix(matrix);
                    finalJointTransform.setRotation(matrix);
                });
            });
            Vector3f.POOL.consume(translation -> {
                previousJointTransform.jointTranslation.lerp(currentJointTransform.jointTranslation, progression, translation);
                finalJointTransform.setTranslation(translation);
            });
        }
    }

    private void applyAnimationPose(JointData currentJoint, ReadOnlyMatrix4f parentTransform) {
        Matrix4f currentTransform = currentJointTransforms[currentJoint.getJointIndex()];
        parentTransform.mul(currentTransform, currentTransform);
        for (JointData child : currentJoint.getChildren()) {
            applyAnimationPose(child, currentTransform);
        }
        currentTransform.mul(currentJoint.getInverseBindTransform());
    }

    private static JointTransformData getByJointName(JointTransformData[] jointData, String jointName) {
        for (JointTransformData jointTransformData : jointData) {
            if (jointTransformData.jointName.equals(jointName)) {
                return jointTransformData;
            }
        }
        throw new IllegalStateException("The loaded joint transform data does not contain the joint: " + jointName + '!');
    }

    public static class AnimationData {

        public KeyframeData[] keyframeData;
        public JointData rootJoint;
    }
}
