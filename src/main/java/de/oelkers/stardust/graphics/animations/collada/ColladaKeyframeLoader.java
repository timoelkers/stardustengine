package de.oelkers.stardust.graphics.animations.collada;

import de.oelkers.stardust.graphics.animations.KeyframeData;
import de.oelkers.stardust.graphics.animations.KeyframeData.JointTransformData;
import de.oelkers.stardust.math.matrices.Matrix4f;
import de.oelkers.stardust.math.quaternions.Quaternion;
import de.oelkers.stardust.math.vectors.Vector3f;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathException;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathNodes;

import static de.oelkers.stardust.graphics.animations.collada.ColladaAnimationLoader.CORRECTION_MATRIX;
import static java.lang.Float.parseFloat;

public final class ColladaKeyframeLoader {

    private ColladaKeyframeLoader() {}

    public static KeyframeData[] loadAnimationData(Document document, XPath xPath) throws XPathException {
        XPathNodes joints = xPath.evaluateExpression("/COLLADA/library_animations/animation", document, XPathNodes.class);
        KeyframeData[] keyframeData = extractKeyframeData(document, xPath, joints.size());
        String rootJointName = xPath.evaluate("/COLLADA/library_visual_scenes/visual_scene/node[@id='Armature']/node/@id", document);
        for (int i = 0; i < joints.size(); i++) {
            extractJointTransforms(i, keyframeData, joints.get(i), xPath, rootJointName);
        }
        return keyframeData;
    }

    private static KeyframeData[] extractKeyframeData(Document document, XPath xPath, int jointCount) throws XPathExpressionException {
        String[] timeData = xPath.evaluate("/COLLADA/library_animations/animation/source/float_array/text()", document).split(" ");
        KeyframeData[] keyframeData = new KeyframeData[timeData.length];
        for (int i = 0; i < timeData.length; i++) {
            keyframeData[i] = new KeyframeData();
            keyframeData[i].timeStamp = parseFloat(timeData[i]);
            keyframeData[i].jointTransforms = new JointTransformData[jointCount];
        }
        return keyframeData;
    }

    private static void extractJointTransforms(int jointIndex, KeyframeData[] keyframeData, Node animationNode, XPath xPath, String rootJointName) throws XPathExpressionException {
        String jointName = xPath.evaluate("./channel/@target", animationNode).split("/")[0];
        String jointDataID = xPath.evaluate("./sampler/input[@semantic='OUTPUT']/@source", animationNode).substring(1);
        String[] transformData = xPath.evaluate("./source[@id='" + jointDataID + "']/float_array/text()", animationNode).split(" ");
        for (int i = 0; i < keyframeData.length; i++) {
            Matrix4f transformation = toMatrix(transformData, i * 16);
            if (jointName.equals(rootJointName)) {
                CORRECTION_MATRIX.mul(transformation, transformation);
            }
            JointTransformData jointTransformData = new JointTransformData();
            jointTransformData.jointName = jointName;
            jointTransformData.jointTransform = transformation;
            Vector3f jointTranslation = new Vector3f();
            transformation.getTranslation(jointTranslation);
            jointTransformData.jointTranslation = jointTranslation;
            jointTransformData.jointRotation = new Quaternion().setFromMatrix(transformation);
            keyframeData[i].jointTransforms[jointIndex] = jointTransformData;
        }
    }

    private static Matrix4f toMatrix(String[] matrixData, int currentStart) {
        Matrix4f matrix = new Matrix4f();
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                matrix.setValue(i, j, parseFloat(matrixData[i * 4 + j + currentStart]));
            }
        }
        return matrix.transpose();
    }
}
