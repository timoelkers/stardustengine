package de.oelkers.stardust.graphics.animations.collada;

import de.oelkers.stardust.graphics.assets.loading.AsynchronousAssetLoader;
import de.oelkers.stardust.graphics.assets.models.BaseModel.PrimitiveMode;
import de.oelkers.stardust.graphics.assets.models.Model3D.ModelData3D;
import de.oelkers.stardust.math.vectors.Vector2f;
import de.oelkers.stardust.math.vectors.Vector3f;
import org.w3c.dom.Document;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;

import static de.oelkers.stardust.graphics.animations.collada.ColladaAnimationLoader.*;
import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;

public class ColladaModelLoader implements AsynchronousAssetLoader<ModelData3D> {

    private static final UnrollVertexData NULL_UNROLLED_VERTEX_DATA = new NullUnrolledVertexData();

    @Override
    public ModelData3D[] loadAsync(String... filePaths) {
        if (filePaths.length != 1) {
            throw new IllegalStateException("You have to specify a single path to load collada models.");
        }
        try {
            return new ModelData3D[] { loadModelData(getDocument(filePaths[0]), getXPath()) };
        } catch (XPathExpressionException e) {
            throw new RuntimeException(e);
        }
    }

    public static ModelData3D loadModelData(Document document, XPath xPath) throws XPathExpressionException {
        return loadModelData(document, xPath, NULL_UNROLLED_VERTEX_DATA);
    }

    public static ModelData3D loadModelData(Document document, XPath xPath, UnrollVertexData unrollVertexData) throws XPathExpressionException {
        ModelData3D modelData = new ModelData3D();
        modelData.primitiveMode = PrimitiveMode.TRIANGLES;
        modelData.vertices = extractVertices(document, xPath);
        modelData.normals = extractNormals(document, xPath);
        modelData.uvs = extractUVs(document, xPath);
        unrollIndexedData(document, xPath, modelData, unrollVertexData);
        return modelData;
    }

    private static Vector3f[] extractVertices(Document document, XPath xPath) throws XPathExpressionException {
        String verticesId = xPath.evaluate("/COLLADA/library_geometries/geometry/mesh/vertices/input/@source", document).substring(1);
        String[] vertexData = xPath.evaluate("/COLLADA/library_geometries/geometry/mesh/source[@id='" + verticesId + "']/float_array/text()", document).split(" ");
        int vertexCount = parseInt(xPath.evaluate("/COLLADA/library_geometries/geometry/mesh/source[@id='" + verticesId + "']/float_array/@count", document)) / 3;
        Vector3f[] vertices = new Vector3f[vertexCount];
        for (int i = 0; i < vertexCount; i++) {
            float x = parseFloat(vertexData[i * 3]);
            float y = parseFloat(vertexData[i * 3 + 1]);
            float z = parseFloat(vertexData[i * 3 + 2]);
            vertices[i] = new Vector3f(x, y, z).transform(CORRECTION_MATRIX);
        }
        return vertices;
    }

    private static Vector3f[] extractNormals(Document document, XPath xPath) throws XPathExpressionException {
        String normalsId = xPath.evaluate("/COLLADA/library_geometries/geometry/mesh/polylist/input[@semantic='NORMAL']/@source", document).substring(1);
        String[] normalData = xPath.evaluate("/COLLADA/library_geometries/geometry/mesh/source[@id='" + normalsId + "']/float_array/text()", document).split(" ");
        int normalsCount = parseInt(xPath.evaluate("/COLLADA/library_geometries/geometry/mesh/source[@id='" + normalsId + "']/float_array/@count", document)) / 3;
        Vector3f[] normals = new Vector3f[normalsCount];
        for (int i = 0; i < normalsCount; i++) {
            float x = parseFloat(normalData[i * 3]);
            float y = parseFloat(normalData[i * 3 + 1]);
            float z = parseFloat(normalData[i * 3 + 2]);
            normals[i] = new Vector3f(x, y, z).transform(CORRECTION_MATRIX);
        }
        return normals;
    }

    private static Vector2f[] extractUVs(Document document, XPath xPath) throws XPathExpressionException {
        String uvId = xPath.evaluate("/COLLADA/library_geometries/geometry/mesh/polylist/input[@semantic='TEXCOORD']/@source", document).substring(1);
        String[] uvData = xPath.evaluate("/COLLADA/library_geometries/geometry/mesh/source[@id='" + uvId + "']/float_array/text()", document).split(" ");
        int uvsCount = parseInt(xPath.evaluate("/COLLADA/library_geometries/geometry/mesh/source[@id='" + uvId + "']/float_array/@count", document)) / 2;
        Vector2f[] uvs = new Vector2f[uvsCount];
        for (int i = 0; i < uvsCount; i++) {
            float u = parseFloat(uvData[i * 2]);
            float v = parseFloat(uvData[i * 2 + 1]);
            uvs[i] = new Vector2f(u, 1 - v);
        }
        return uvs;
    }

    // @Performance: It´s a real shame OpenGL seems to only allow one index buffer for all attributes, so we have to unroll all indexed
    // values here... there might be slight improvements in using indices for vertices where all attributes are the same, but that is not common
    private static void unrollIndexedData(Document document, XPath xPath, ModelData3D modelData, UnrollVertexData unrollVertexData) throws XPathExpressionException {
        String[] indexData = xPath.evaluate("/COLLADA/library_geometries/geometry/mesh/polylist/p/text()", document).split(" ");
        int typeCount = xPath.evaluateExpression("count(/COLLADA/library_geometries/geometry/mesh/polylist/input)", document, Integer.class);
        int valueCount = indexData.length / typeCount;
        Vector3f[] unrolledVertices = new Vector3f[valueCount];
        Vector3f[] unrolledNormals = new Vector3f[valueCount];
        Vector2f[] unrolledUvs = new Vector2f[valueCount];
        unrollVertexData.setUnrolledSize(valueCount);
        for (int i = 0; i < valueCount; i++) {
            int vertexIndex = parseInt(indexData[i * typeCount]);
            unrolledVertices[i] = modelData.vertices[vertexIndex];
            unrolledNormals[i] = modelData.normals[parseInt(indexData[i * typeCount + 1])];
            unrolledUvs[i] = modelData.uvs[parseInt(indexData[i * typeCount + 2])];
            unrollVertexData.apply(i, vertexIndex);
        }
        modelData.vertices = unrolledVertices;
        modelData.normals = unrolledNormals;
        modelData.uvs = unrolledUvs;
    }

    public interface UnrollVertexData {

        void setUnrolledSize(int size);
        void apply(int index, int vertexIndex);
    }

    private static class NullUnrolledVertexData implements UnrollVertexData {

        @Override
        public void setUnrolledSize(int size) {}

        @Override
        public void apply(int index, int vertexIndex) {}
    }
}
