package de.oelkers.stardust.graphics.animations.collada;

import de.oelkers.stardust.graphics.animations.JointData;
import de.oelkers.stardust.math.matrices.Matrix4f;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathException;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathNodes;

import static de.oelkers.stardust.graphics.animations.collada.ColladaAnimationLoader.CORRECTION_MATRIX;
import static java.lang.Float.parseFloat;

public final class ColladaSkeletonLoader {

    private ColladaSkeletonLoader() {}

    public static JointData loadSkeleton(Document document, XPath xPath) throws XPathException {
        String[] jointNames = extractJointNames(document, xPath);
        Node rootNode = xPath.evaluateExpression("/COLLADA/library_visual_scenes/visual_scene/node[@id='Armature']/node", document, Node.class);
        return extractJointDataRecursively(rootNode, xPath, jointNames, true).calculateChildTransforms();
    }

    private static String[] extractJointNames(Document document, XPath xPath) throws XPathExpressionException {
        String jointDataId = xPath.evaluate("/COLLADA/library_controllers/controller/skin/vertex_weights/input[@semantic='JOINT']/@source", document).substring(1);
        return xPath.evaluate("/COLLADA/library_controllers/controller/skin/source[@id='" + jointDataId + "']/Name_array/text()", document).split(" ");
    }

    private static JointData extractJointDataRecursively(Node currentNode, XPath xPath, String[] jointNames, boolean isRoot) throws XPathException {
        JointData currentNodeData = extractJointNames(currentNode, xPath, jointNames, isRoot);
        XPathNodes childNodes = xPath.evaluateExpression("./node", currentNode, XPathNodes.class);
        for (int i = 0; i < childNodes.size(); i++) {
            currentNodeData.addChild(extractJointDataRecursively(childNodes.get(i), xPath, jointNames, false));
        }
        return currentNodeData;
    }

    private static JointData extractJointNames(Node currentNode, XPath xPath, String[] jointNames, boolean isRoot) throws XPathExpressionException {
        String jointName = xPath.evaluate("./@id", currentNode);
        String[] matrixData = xPath.evaluate("./matrix/text()", currentNode).split(" ");
        Matrix4f jointTransform = toMatrix(matrixData);
        int jointIndex = getJointIndex(jointNames, jointName);
        if (isRoot) {
            CORRECTION_MATRIX.mul(jointTransform, jointTransform);
        }
        return new JointData(jointIndex, jointName, jointTransform);
    }

    private static int getJointIndex(String[] jointNames, String jointID) {
        for (int i = 0; i < jointNames.length; i++) {
            if (jointNames[i].equals(jointID)) {
                return i;
            }
        }
        throw new IllegalStateException("Joint with id " + jointID + " not found in list of joints!");
    }

    private static Matrix4f toMatrix(String[] matrixData) {
        Matrix4f matrix = new Matrix4f();
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                matrix.setValue(i, j, parseFloat(matrixData[i * 4 + j]));
            }
        }
        return matrix.transpose();
    }
}
