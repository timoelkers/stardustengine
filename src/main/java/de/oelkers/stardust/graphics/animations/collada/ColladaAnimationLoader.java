package de.oelkers.stardust.graphics.animations.collada;

import de.oelkers.stardust.graphics.animations.AnimationComponent.AnimationData;
import de.oelkers.stardust.graphics.assets.loading.AsynchronousAssetLoader;
import de.oelkers.stardust.math.internal.MathDelegate;
import de.oelkers.stardust.math.matrices.Matrix4f;
import de.oelkers.stardust.math.matrices.ReadOnlyMatrix4f;
import de.oelkers.stardust.math.vectors.Vector3f;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ColladaAnimationLoader implements AsynchronousAssetLoader<AnimationData> {

    private static final Map<String, Document> DOCUMENT_CACHE = new HashMap<>();
    private static final Map<String, Document> DOCUMENT_BY_THREAD_NAME = new ConcurrentHashMap<>();

    private static final DocumentBuilder DOCUMENT_BUILDER;
    private static final ThreadLocal<XPath> X_PATH = ThreadLocal.withInitial(() -> XPathFactory.newDefaultInstance().newXPath());
    public static final ReadOnlyMatrix4f CORRECTION_MATRIX = new Matrix4f().rotate(-MathDelegate.HALF_PI_FLOAT, Vector3f.RIGHT);

    static {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newDefaultInstance();
            factory.setExpandEntityReferences(false);
            factory.setIgnoringComments(true);
            DOCUMENT_BUILDER = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public AnimationData[] loadAsync(String... filePaths) {
        if (filePaths.length != 1) {
            throw new IllegalStateException("You have to specify a single path to load collada models.");
        }
        try {
            return new AnimationData[] { loadAnimationData(getDocument(filePaths[0]), getXPath()) };
        } catch (XPathException e) {
            throw new RuntimeException(e);
        }
    }

    public static AnimationData loadAnimationData(Document document, XPath xPath) throws XPathException {
        AnimationData animationData = new AnimationData();
        animationData.keyframeData = ColladaKeyframeLoader.loadAnimationData(document, xPath);
        animationData.rootJoint = ColladaSkeletonLoader.loadSkeleton(document, xPath);
        return animationData;
    }

    public static Document getDocument(String filePath) {
        return DOCUMENT_BY_THREAD_NAME.computeIfAbsent(Thread.currentThread().getName(), name -> {
            if (DOCUMENT_CACHE.containsKey(filePath)) {
                return (Document) DOCUMENT_CACHE.get(filePath).cloneNode(true);
            }
            try {
                Document document = parseDocument(filePath);
                DOCUMENT_CACHE.put(filePath, document);
                return document;
            } catch (SAXException | IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public static XPath getXPath() {
        return X_PATH.get();
    }

    // the document builder is not thread safe, so synchronisation is needed (alternatively a thread local
    // builder could be used, but I think the cost of construction is bigger than the cost of synchronisation)
    private static synchronized Document parseDocument(String filePath) throws IOException, SAXException {
        return DOCUMENT_BUILDER.parse(ClassLoader.getSystemResourceAsStream(filePath));
    }
}
