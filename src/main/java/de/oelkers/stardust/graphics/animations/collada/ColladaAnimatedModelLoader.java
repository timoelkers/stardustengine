package de.oelkers.stardust.graphics.animations.collada;

import de.oelkers.stardust.graphics.animations.AnimatedModel3D.AnimatedModelData3D;
import de.oelkers.stardust.graphics.assets.loading.AsynchronousAssetLoader;
import org.w3c.dom.Document;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static de.oelkers.stardust.graphics.animations.AnimatedModel3D.MAX_JOINTS_PER_VERTEX;
import static de.oelkers.stardust.graphics.animations.collada.ColladaAnimationLoader.getDocument;
import static de.oelkers.stardust.graphics.animations.collada.ColladaAnimationLoader.getXPath;
import static de.oelkers.stardust.graphics.animations.collada.ColladaModelLoader.UnrollVertexData;
import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;

public class ColladaAnimatedModelLoader implements AsynchronousAssetLoader<AnimatedModelData3D> {

    @Override
    public AnimatedModelData3D[] loadAsync(String... filePaths) {
        if (filePaths.length != 1) {
            throw new IllegalStateException("You have to specify a single path to load collada models.");
        }
        try {
            return new AnimatedModelData3D[] { loadModelData(getDocument(filePaths[0]), getXPath()) };
        } catch (XPathExpressionException e) {
            throw new RuntimeException(e);
        }
    }

    public static AnimatedModelData3D loadModelData(Document document, XPath xPath) throws XPathExpressionException {
        float[] weights = extractWeights(document, xPath);
        int[] jointCounts = extractJointCounts(document, xPath);
        return applyIndexData(document, xPath, jointCounts, weights);
    }

    private static float[] extractWeights(Document document, XPath xPath) throws XPathExpressionException {
        String weightDataId = xPath.evaluate("/COLLADA/library_controllers/controller/skin/vertex_weights/input[@semantic='WEIGHT']/@source", document).substring(1);
        String[] weightNodes = xPath.evaluate("/COLLADA/library_controllers/controller/skin/source[@id='" + weightDataId + "']/float_array/text()", document).split(" ");
        float[] convertedWeights = new float[weightNodes.length];
        for (int i = 0; i < weightNodes.length; i++) {
            convertedWeights[i] = parseFloat(weightNodes[i]);
        }
        return convertedWeights;
    }

    private static int[] extractJointCounts(Document document, XPath xPath) throws XPathExpressionException {
        String[] jointCounts = xPath.evaluate("/COLLADA/library_controllers/controller/skin/vertex_weights/vcount/text()", document).split(" ");
        return Arrays.stream(jointCounts).mapToInt(Integer::parseInt).toArray();
    }

    private static AnimatedModelData3D applyIndexData(Document document, XPath xPath, int[] counts, float[] weights) throws XPathExpressionException {
        String[] rawIndices = xPath.evaluate("/COLLADA/library_controllers/controller/skin/vertex_weights/v/text()", document).split(" ");
        JointData jointData = new JointData(MAX_JOINTS_PER_VERTEX * counts.length);
        fillModelData(jointData, rawIndices, weights, counts);
        AnimatedModelData3D modelData = new AnimatedModelData3D(ColladaModelLoader.loadModelData(document, xPath, jointData));
        modelData.weights = jointData.weights;
        modelData.jointIndices = jointData.jointIndices;
        return modelData;
    }

    private static void fillModelData(JointData jointData, String[] rawIndices, float[] weights, int[] counts) {
        int dataPointer = 0;
        for (int i = 0; i < counts.length; i++) {
            VertexSkinData vertexSkinData = new VertexSkinData(counts[i]);
            for (int j = 0; j < counts[i]; j++) {
                int jointId = parseInt(rawIndices[dataPointer++]);
                int weightId = parseInt(rawIndices[dataPointer++]);
                vertexSkinData.addJointEffect(jointId, weights[weightId]);
            }
            vertexSkinData.limitJointNumber();
            for (int j = 0; j < MAX_JOINTS_PER_VERTEX; j++) {
                jointData.originalWeights[i * MAX_JOINTS_PER_VERTEX + j] = vertexSkinData.weights.get(j);
                jointData.originalJointIndices[i * MAX_JOINTS_PER_VERTEX + j] = vertexSkinData.jointIds.get(j);
            }
        }
    }

    private static final class JointData implements UnrollVertexData {

        private final float[] originalWeights;
        private final int[] originalJointIndices;
        private float[] weights;
        private int[] jointIndices;

        private JointData(int originalSize) {
            originalWeights = new float[originalSize];
            originalJointIndices = new int[originalSize];
        }

        @Override
        public void setUnrolledSize(int size) {
            weights = new float[size * MAX_JOINTS_PER_VERTEX];
            jointIndices = new int[size * MAX_JOINTS_PER_VERTEX];
        }

        @Override
        public void apply(int index, int vertexIndex) {
            System.arraycopy(originalWeights, vertexIndex * MAX_JOINTS_PER_VERTEX, weights, index * MAX_JOINTS_PER_VERTEX, MAX_JOINTS_PER_VERTEX);
            System.arraycopy(originalJointIndices, vertexIndex * MAX_JOINTS_PER_VERTEX, jointIndices, index * MAX_JOINTS_PER_VERTEX, MAX_JOINTS_PER_VERTEX);
        }
    }

    private static final class VertexSkinData {

        private final List<Integer> jointIds;
        private final List<Float> weights;

        private VertexSkinData(int count) {
            jointIds = new ArrayList<>(count);
            weights = new ArrayList<>(count);
        }

        private void addJointEffect(int jointId, float weight) {
            for (int i = 0; i < weights.size(); i++) {
                if (weight > weights.get(i)) {
                    jointIds.add(i, jointId);
                    weights.add(i, weight);
                    return;
                }
            }
            jointIds.add(jointId);
            weights.add(weight);
        }

        private void limitJointNumber() {
            if (jointIds.size() > MAX_JOINTS_PER_VERTEX) {
                float[] topWeights = new float[MAX_JOINTS_PER_VERTEX];
                float total = saveTopWeights(topWeights);
                refillWeightList(topWeights, total);
                removeExcessJointIds();
            } else if (jointIds.size() < MAX_JOINTS_PER_VERTEX) {
                fillEmptyWeightsAndJointIds();
            }
        }

        private void fillEmptyWeightsAndJointIds() {
            while (jointIds.size() < MAX_JOINTS_PER_VERTEX) {
                jointIds.add(0);
                weights.add(0f);
            }
        }

        private float saveTopWeights(float[] topWeightsArray) {
            float total = 0;
            for(int i = 0; i < topWeightsArray.length; i++) {
                topWeightsArray[i] = weights.get(i);
                total += topWeightsArray[i];
            }
            return total;
        }

        private void refillWeightList(float[] topWeights, float total) {
            weights.clear();
            for (float topWeight : topWeights) {
                weights.add(Math.min(topWeight / total, 1));
            }
        }

        private void removeExcessJointIds() {
            while (jointIds.size() > MAX_JOINTS_PER_VERTEX) {
                jointIds.remove(jointIds.size() - 1);
            }
        }
    }
}
