package de.oelkers.stardust.graphics.animations;

import de.oelkers.stardust.math.matrices.Matrix4f;
import de.oelkers.stardust.math.matrices.ReadOnlyMatrix4f;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class JointData implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private final int jointIndex;
    private final String jointName;
    private final transient Matrix4f localTransform;
    private final Matrix4f inverseBindTransform = new Matrix4f();
    private final Collection<JointData> children = new ArrayList<>();

    public JointData(int jointIndex, String jointName, Matrix4f localTransform) {
        this.jointIndex = jointIndex;
        this.jointName = jointName;
        this.localTransform = localTransform;
    }

    public int getJointIndex() {
        return jointIndex;
    }

    public String getJointName() {
        return jointName;
    }

    public Iterable<JointData> getChildren() {
        return children;
    }

    public void addChild(JointData child) {
        children.add(child);
    }

    public JointData calculateChildTransforms() {
        calculateTransforms(Matrix4f.IDENTITY);
        return this;
    }

    private void calculateTransforms(ReadOnlyMatrix4f parentBindTransform) {
        Matrix4f.POOL.consume(bindTransform -> {
            parentBindTransform.mul(localTransform, bindTransform);
            bindTransform.invert(inverseBindTransform);
            for (JointData child : children) {
                child.calculateTransforms(bindTransform);
            }
        });
    }

    public ReadOnlyMatrix4f getInverseBindTransform() {
        return inverseBindTransform;
    }
}
