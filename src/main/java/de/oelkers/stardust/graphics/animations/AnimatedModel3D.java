package de.oelkers.stardust.graphics.animations;

import de.oelkers.stardust.graphics.assets.loading.AssetContainer;
import de.oelkers.stardust.graphics.assets.models.BufferObject;
import de.oelkers.stardust.graphics.assets.models.BufferObject.BufferTarget;
import de.oelkers.stardust.graphics.assets.models.Model3D;
import de.oelkers.stardust.utils.container.Container;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import java.io.Serial;
import java.nio.ByteBuffer;
import java.util.Arrays;

import static de.oelkers.stardust.utils.buffers.BufferUtils.store;

public class AnimatedModel3D extends Model3D {

    // has to stay in sync with shader/animatedEntityShader.vert
    public static final int MAX_JOINTS_PER_VERTEX = 4;
    private static final String JOINT_INDEX_IDENTIFIER = "joint_indices";
    private static final String WEIGHT_IDENTIFIER = "weights";

    private int[] jointIndices;
    private float[] weights;

    @Override
    public void inject(Container<ModelData3D> container) {
        if (container.getOnly() instanceof AnimatedModelData3D) {
            super.inject(container);
            AnimatedModelData3D modelData = (AnimatedModelData3D) container.getOnly();
            if (modelData.jointIndices != null && modelData.jointIndices.length != 0) {
                addJointIndices(modelData.jointIndices, true, false);
            }
            if (modelData.weights != null && modelData.weights.length != 0) {
                addWeights(modelData.weights, true, false);
            }
        }
    }

    public void addJointIndices(int[] jointIndices, boolean isStatic, boolean storeData) {
        ByteBuffer buffer = store(jointIndices);
        vertexBuffer.put(JOINT_INDEX_IDENTIFIER, new BufferObject(BufferTarget.VERTEX_BUFFER, buffer, JOINT_INDEX_IDENTIFIER, isStatic));
        bind();
        GL30.glVertexAttribIPointer(3, MAX_JOINTS_PER_VERTEX, GL11.GL_INT, 0, 0);
        GL20.glEnableVertexAttribArray(3);
        totalByteSize += buffer.remaining();
        attributeCount++;
        if (storeData) {
            this.jointIndices = Arrays.copyOf(jointIndices, jointIndices.length);
        }
    }

    public void addWeights(float[] weights, boolean isStatic, boolean storeData) {
        ByteBuffer buffer = store(weights);
        vertexBuffer.put(WEIGHT_IDENTIFIER, new BufferObject(BufferTarget.VERTEX_BUFFER, buffer, WEIGHT_IDENTIFIER, isStatic));
        bind();
        GL20.glVertexAttribPointer(4, MAX_JOINTS_PER_VERTEX, GL11.GL_FLOAT, false, 0, 0);
        GL20.glEnableVertexAttribArray(4);
        totalByteSize += buffer.remaining();
        attributeCount++;
        if (storeData) {
            this.weights = Arrays.copyOf(weights, weights.length);
        }
    }

    public boolean updateJointIndices() {
        BufferObject vbo = vertexBuffer.get(JOINT_INDEX_IDENTIFIER);
        if (!storesJointIndices() || vbo.isStatic()) {
            return false;
        }
        vbo.update(store(jointIndices));
        return true;
    }

    public boolean updateWeights() {
        BufferObject vbo = vertexBuffer.get(WEIGHT_IDENTIFIER);
        if (!storesWeights() || vbo.isStatic()) {
            return false;
        }
        vbo.update(store(weights));
        return true;
    }

    public boolean storesJointIndices() {
        return jointIndices != null;
    }

    public boolean storesWeights() {
        return weights != null;
    }

    public int[] getJointIndices() {
        if (storesJointIndices()) {
            return jointIndices;
        }
        BufferObject vbo = vertexBuffer.get(JOINT_INDEX_IDENTIFIER);
        if (vbo == null) {
            return new int[0];
        }
        int[] jointIndices = new int[(int) (vbo.getByteSize() / Integer.BYTES)];
        vbo.getData().asIntBuffer().get(jointIndices);
        return jointIndices;
    }

    public float[] getWeights() {
        if (storesWeights()) {
            return weights;
        }
        BufferObject vbo = vertexBuffer.get(WEIGHT_IDENTIFIER);
        if (vbo == null) {
            return new float[0];
        }
        float[] weights = new float[(int) (vbo.getByteSize() / Float.BYTES)];
        vbo.getData().asFloatBuffer().get(weights);
        return weights;
    }

    @Serial
    @Override
    protected Object writeReplace() {
        AnimatedModelData3D modelData = new AnimatedModelData3D();
        modelData.vertices = getVertices();
        modelData.primitiveMode = getPrimitiveMode();
        modelData.indices = getIndices();
        modelData.normals = getNormals();
        modelData.uvs = getUVs();
        modelData.jointIndices = getJointIndices();
        modelData.weights = getWeights();
        return modelData;
    }

    public static class AnimatedModelData3D extends ModelData3D {

        @Serial
        private static final long serialVersionUID = 1L;

        public int[] jointIndices;
        public float[] weights;

        public AnimatedModelData3D() {}

        public AnimatedModelData3D(ModelData3D modelData) {
            vertices = modelData.vertices;
            primitiveMode = modelData.primitiveMode;
            indices = modelData.indices;
            uvs = modelData.uvs;
            normals = modelData.normals;
        }

        @Serial
        @Override
        protected Object readResolve() {
            AssetContainer<ModelData3D> model = new AnimatedModel3D();
            Container<ModelData3D> container = model.getContainer();
            container.set(this);
            model.inject(container);
            return model;
        }
    }
}
