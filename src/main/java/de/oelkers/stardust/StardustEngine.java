package de.oelkers.stardust;

import de.oelkers.stardust.config.ConfigStore;
import de.oelkers.stardust.config.StardustErrorCallback;
import de.oelkers.stardust.config.StardustErrorCallback.ErrorType;
import de.oelkers.stardust.config.StardustErrorCallback.Severity;
import de.oelkers.stardust.utils.NonThrowingAutoCloseable;
import de.oelkers.stardust.utils.timer.DefaultTimeProvider;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.glfw.GLFW;

import static org.lwjgl.glfw.GLFWErrorCallback.getDescription;

public class StardustEngine implements NonThrowingAutoCloseable {

    private static final Logger LOGGER = LogManager.getLogger(StardustEngine.class);
    public static final Level ALLOCATION = Level.forName("ALLOCATION", Level.INFO.intLevel());

    private long lastTime;

    public StardustEngine() {
        GLFW.glfwSetErrorCallback((error, description) -> {
            StardustErrorCallback errorCallback = ConfigStore.get().getErrorCallback();
            errorCallback.onError(error, getDescription(description), ErrorType.ERROR, Severity.HIGH);
        });
        if (!GLFW.glfwInit()) {
            throw new IllegalStateException("GLFW failed to initialize!");
        }
        lastTime = DefaultTimeProvider.GLFW_TIMER.getCurrentTime();
        LOGGER.info("StardustEngine successfully initialized.");
    }

    /**
     * Returns the elapsed time in seconds between calls to this method. To measure an exact frame time, you need to
     * call this method every frame and in order to achieve frame rate independent movement, you need to multiply the
     * delta with the movement factor. This uses a time provider with at least microsecond frequency as lower frequencies
     * might not be sufficient for higher frame rates and would return 0.
     */
    public double getFrameTimeDelta() {
        long currentTime = DefaultTimeProvider.GLFW_TIMER.getCurrentTime();
        double delta = (currentTime - lastTime) / (double) DefaultTimeProvider.GLFW_TIMER.getFrequency();
        lastTime = currentTime;
        return delta;
    }

    @Override
    public void close() {
        GLFW.glfwTerminate();
        LOGGER.info("StardustEngine was terminated.");
    }
}
