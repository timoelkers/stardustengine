package de.oelkers.stardust.window.cursor;

import de.oelkers.stardust.utils.NonThrowingAutoCloseable;
import org.lwjgl.glfw.GLFW;

public interface Cursor extends NonThrowingAutoCloseable {

    long getHandle();

    @Override
    default void close() {
        GLFW.glfwDestroyCursor(getHandle());
    }
}
