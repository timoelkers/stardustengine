package de.oelkers.stardust.window.cursor;

import de.oelkers.stardust.graphics.assets.textures.BaseTexture;
import de.oelkers.stardust.graphics.assets.textures.TextureLoader;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWImage;

public class CustomCursor implements Cursor {

    private final long handle;

    public CustomCursor(String cursorImagePath, int xHot, int yHot) {
        BaseTexture.TextureData textureData = TextureLoader.loadTexture(cursorImagePath, BaseTexture.TextureFormat.RGBA, 0, true);
        GLFWImage image = GLFWImage.create().set(textureData.width, textureData.height, textureData.imageData);
        handle = GLFW.glfwCreateCursor(image, xHot, yHot);
    }

    @Override
    public long getHandle() {
        return handle;
    }
}
