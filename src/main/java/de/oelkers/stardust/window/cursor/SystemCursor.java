package de.oelkers.stardust.window.cursor;

import org.lwjgl.glfw.GLFW;

public enum SystemCursor implements Cursor {

    ARROW(GLFW.GLFW_ARROW_CURSOR),
    IBEAM(GLFW.GLFW_IBEAM_CURSOR),
    CROSSHAIR(GLFW.GLFW_CROSSHAIR_CURSOR),
    HAND(GLFW.GLFW_HAND_CURSOR),
    HRESIZE(GLFW.GLFW_HRESIZE_CURSOR),
    VRESIZE(GLFW.GLFW_VRESIZE_CURSOR);

    private final long cursorHandle;

    SystemCursor(int glfwCode) {
        cursorHandle = GLFW.glfwCreateStandardCursor(glfwCode);
    }

    @Override
    public long getHandle() {
        return cursorHandle;
    }
}
