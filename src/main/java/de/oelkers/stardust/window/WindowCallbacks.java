package de.oelkers.stardust.window;

import java.util.HashMap;
import java.util.Map;

public class WindowCallbacks {

    @FunctionalInterface
    public interface ResizeCallback {
        void onResize(int width, int height);
    }

    @FunctionalInterface
    public interface MoveCallback {
        void onMove(int xPos, int yPos);
    }

    @FunctionalInterface
    public interface RefreshCallback {
        void onRefresh();
    }

    @FunctionalInterface
    public interface CloseCallback {
        void onClose();
    }

    @FunctionalInterface
    public interface KeyCallback {
        void onKey(Key key, ButtonState state);
    }

    @FunctionalInterface
    public interface MouseButtonCallback {
        void onMouseButton(MouseButton button, ButtonState state);
    }

    @FunctionalInterface
    public interface MousePositionCallback {
        void onMousePosition(double xPos, double yPos);
    }

    @FunctionalInterface
    public interface ScrollCallback {
        void onScroll(double xOffset, double yOffset);
    }

    @FunctionalInterface
    public interface IconifyCallback {
        void onIconify(boolean iconified);
    }

    @FunctionalInterface
    public interface FocusCallback {
        void onFocus(boolean focused);
    }

    @FunctionalInterface
    public interface DropCallback {
        void onDrop(String[] dropPaths);
    }

    @FunctionalInterface
    public interface CharCallback {
        void onChar(int codePoint);
    }

    @FunctionalInterface
    public interface CursorEnterCallback {
        void onEnter();
    }

    @FunctionalInterface
    public interface CursorLeaveCallback {
        void onLeave();
    }

    private final Map<Class<?>, Object> onlyTarget = new HashMap<>();
    private final Map<Object, ResizeCallback> resizeCallbacks = new HashMap<>();
    private final Map<Object, MoveCallback> moveCallbacks = new HashMap<>();
    private final Map<Object, RefreshCallback> refreshCallbacks = new HashMap<>();
    private final Map<Object, CloseCallback> closeCallbacks = new HashMap<>();
    private final Map<Object, KeyCallback> keyCallbacks = new HashMap<>();
    private final Map<Object, MouseButtonCallback> mouseButtonCallbacks = new HashMap<>();
    private final Map<Object, ScrollCallback> scrollCallbacks = new HashMap<>();
    private final Map<Object, MousePositionCallback> mousePositionCallbacks = new HashMap<>();
    private final Map<Object, IconifyCallback> iconifyCallbacks = new HashMap<>();
    private final Map<Object, FocusCallback> focusCallbacks = new HashMap<>();
    private final Map<Object, DropCallback> dropCallbacks = new HashMap<>();
    private final Map<Object, CharCallback> charCallbacks = new HashMap<>();
    private final Map<Object, CursorEnterCallback> enterCallbacks = new HashMap<>();
    private final Map<Object, CursorLeaveCallback> leaveCallbacks = new HashMap<>();

    public void removeRestrictionFrom(Object target, Class<?> eventType) {
        if (onlyTarget.get(eventType) == target) {
            onlyTarget.remove(eventType);
        }
    }

    public void restrictToTarget(Object target, Class<?> eventType) {
        onlyTarget.put(eventType, target);
    }

    /**
     * Adds the specified callback which gets called when the client area of the window was resized. When called it provides
     * the new width and height of the default frame buffer. This does change the viewport to the new size but you still
     * need to update the projection matrix.
     *
     * @param callback the callback which accepts the new width and height
     */
    public void addResizeCallback(Object target, ResizeCallback callback) {
        resizeCallbacks.put(target, callback);
    }

    public void addMoveCallbacks(Object target, MoveCallback callback) {
        moveCallbacks.put(target, callback);
    }

    /**
     * Adds the specified callback which gets called when the client area of the window needs to be redrawn, for example
     * if the window gets resized or exposed after it had been covered by another window.
     *
     * @param callback the callback which gets called when the contents need to be refreshed
     */
    public void addRefreshCallback(Object target, RefreshCallback callback) {
        refreshCallbacks.put(target, callback);
    }

    /**
     * Add the specified callback which gets called when this window has been closed.
     *
     * @param callback the callback which gets called when this window has been closed
     */
    public void addCloseCallback(Object target, CloseCallback callback) {
        closeCallbacks.put(target, callback);
    }

    /**
     * Adds the specified callback which gets called when a button was pressed or released. When called it provides the
     * key and whether the key was pressed. After the window has lost focus synthetic key releases are sent if the key was pressed but not released.
     *
     * @param callback the callback which accepts the key and whether it was pressed
     */
    public void addKeyCallback(Object target, KeyCallback callback) {
        keyCallbacks.put(target, callback);
    }

    /**
     * Adds the specified callback which gets called when the state of any mouse button had changed. When called it provides
     * the mouse button and whether the button was pressed. After the window has lost focus synthetic mouse button releases
     * are sent if the button was pressed but not released.
     *
     * @param callback the callback which accepts the button and whether it was pressed
     */
    public void addMouseButtonCallback(Object target, MouseButtonCallback callback) {
        mouseButtonCallbacks.put(target, callback);
    }

    /**
     * Adds the specified callback which gets called when any scroll device was scrolled. When called it provides
     * the scroll offset on the x-axis and y-axis relative to the previous offset.
     *
     * @param callback the callback which accepts the scroll offset on the x-axis and y-axis
     */
    public void addScrollCallback(Object target, ScrollCallback callback) {
        scrollCallbacks.put(target, callback);
    }

    /**
     * Adds the specified callback which gets called when the mouse position has changed. When called it provides the
     * new xPos and yPos in pixels from the upper-left corner. If the mouse is captured the values provided have no
     * relation to the screen and should be used to calculate differences.
     *
     * @param callback the callback which accepts the xPos and yPos of the new position
     */
    public void addMousePositionCallback(Object target, MousePositionCallback callback) {
        mousePositionCallbacks.put(target, callback);
    }

    public void addIconifyCallback(Object target, IconifyCallback callback) {
        iconifyCallbacks.put(target, callback);
    }

    public void addFocusCallback(Object target, FocusCallback callback) {
        focusCallbacks.put(target, callback);
    }

    public void addDropCallback(Object target, DropCallback callback) {
        dropCallbacks.put(target, callback);
    }

    public void addCharCallback(Object target, CharCallback callback) {
        charCallbacks.put(target, callback);
    }

    public void addCursorEnterCallback(Object target, CursorEnterCallback callback) {
        enterCallbacks.put(target, callback);
    }

    public void addCursorLeaveCallback(Object target, CursorLeaveCallback callback) {
        leaveCallbacks.put(target, callback);
    }

    protected void runResizeCallbacks(int width, int height) {
        Object target = onlyTarget.get(ResizeCallback.class);
        if (target == null) {
            for (ResizeCallback callback : resizeCallbacks.values()) {
                callback.onResize(width, height);
            }
        } else {
            resizeCallbacks.get(target).onResize(width, height);
        }
    }

    protected void runMoveCallbacks(int xPos, int yPos) {
        Object target = onlyTarget.get(MoveCallback.class);
        if (target == null) {
            for (MoveCallback callback : moveCallbacks.values()) {
                callback.onMove(xPos, yPos);
            }
        } else {
            moveCallbacks.get(target).onMove(xPos, yPos);
        }
    }

    protected void runRefreshCallbacks() {
        Object target = onlyTarget.get(RefreshCallback.class);
        if (target == null) {
            for (RefreshCallback callback : refreshCallbacks.values()) {
                callback.onRefresh();
            }
        } else {
            refreshCallbacks.get(target).onRefresh();
        }
    }

    protected void runCloseCallbacks() {
        Object target = onlyTarget.get(CloseCallback.class);
        if (target == null) {
            for (CloseCallback callback : closeCallbacks.values()) {
                callback.onClose();
            }
        } else {
            closeCallbacks.get(target).onClose();
        }
    }

    protected void runKeyCallbacks(Key key, ButtonState state) {
        Object target = onlyTarget.get(KeyCallback.class);
        if (target == null) {
            for (KeyCallback callback : keyCallbacks.values()) {
                callback.onKey(key, state);
            }
        } else {
            keyCallbacks.get(target).onKey(key, state);
        }
    }

    protected void runMouseButtonCallbacks(MouseButton mouseButton, ButtonState state) {
        Object target = onlyTarget.get(MouseButtonCallback.class);
        if (target == null) {
            for (MouseButtonCallback callback : mouseButtonCallbacks.values()) {
                callback.onMouseButton(mouseButton, state);
            }
        } else {
            mouseButtonCallbacks.get(target).onMouseButton(mouseButton, state);
        }
    }

    protected void runScrollEventCallbacks(double xOffset, double yOffset) {
        Object target = onlyTarget.get(ScrollCallback.class);
        if (target == null) {
            for (ScrollCallback callback : scrollCallbacks.values()) {
                callback.onScroll(xOffset, yOffset);
            }
        } else {
            scrollCallbacks.get(target).onScroll(xOffset, yOffset);
        }
    }

    protected void runMousePositionCallbacks(double xPos, double yPos) {
        Object target = onlyTarget.get(MousePositionCallback.class);
        if (target == null) {
            for (MousePositionCallback callback : mousePositionCallbacks.values()) {
                callback.onMousePosition(xPos, yPos);
            }
        } else {
            mousePositionCallbacks.get(target).onMousePosition(xPos, yPos);
        }
    }

    protected void runIconifyCallbacks(boolean iconified) {
        Object target = onlyTarget.get(IconifyCallback.class);
        if (target == null) {
            for (IconifyCallback callback : iconifyCallbacks.values()) {
                callback.onIconify(iconified);
            }
        } else {
            iconifyCallbacks.get(target).onIconify(iconified);
        }
    }

    protected void runFocusCallbacks(boolean focused) {
        Object target = onlyTarget.get(FocusCallback.class);
        if (target == null) {
            for (FocusCallback callback : focusCallbacks.values()) {
                callback.onFocus(focused);
            }
        } else {
            focusCallbacks.get(target).onFocus(focused);
        }
    }

    protected void runDropCallbacks(String[] paths) {
        Object target = onlyTarget.get(DropCallback.class);
        if (target == null) {
            for (DropCallback callback : dropCallbacks.values()) {
                callback.onDrop(paths);
            }
        } else {
            dropCallbacks.get(target).onDrop(paths);
        }
    }

    protected void runCharCallbacks(int codePoint) {
        Object target = onlyTarget.get(CharCallback.class);
        if (target == null) {
            for (CharCallback callback : charCallbacks.values()) {
                callback.onChar(codePoint);
            }
        } else {
            charCallbacks.get(target).onChar(codePoint);
        }
    }

    protected void runCursorEnterCallbacks() {
        Object target = onlyTarget.get(CursorEnterCallback.class);
        if (target == null) {
            for (CursorEnterCallback callback : enterCallbacks.values()) {
                callback.onEnter();
            }
        } else {
            enterCallbacks.get(target).onEnter();
        }
    }

    protected void runCursorLeaveCallbacks() {
        Object target = onlyTarget.get(CursorLeaveCallback.class);
        if (target == null) {
            for (CursorLeaveCallback callback : leaveCallbacks.values()) {
                callback.onLeave();
            }
        } else {
            leaveCallbacks.get(target).onLeave();
        }
    }
}
