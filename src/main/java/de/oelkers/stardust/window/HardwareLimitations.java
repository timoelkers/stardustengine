package de.oelkers.stardust.window;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.glfw.GLFWVidMode.Buffer;
import org.lwjgl.opengl.*;
import org.lwjgl.system.MemoryUtil;

public final class HardwareLimitations {

    private static final HardwareLimitations INSTANCE = new HardwareLimitations();

    public static HardwareLimitations get() {
        return INSTANCE;
    }

    private final GLCapabilities capabilities;
    private final int maxSamples;
    private final int maxTextureUnits;
    private final float maxAnisotropy;
    private final int maxWidth;
    private final int maxHeight;

    private HardwareLimitations() {
        long windowHandle = getDummyWindowHandle();
        GLFW.glfwMakeContextCurrent(windowHandle);
        capabilities = GL.createCapabilities(true);
        maxSamples = GL11.glGetInteger(GL30.GL_MAX_SAMPLES);
        maxTextureUnits = GL11.glGetInteger(GL20.GL_MAX_TEXTURE_IMAGE_UNITS);
        maxAnisotropy = capabilities.GL_EXT_texture_filter_anisotropic ? GL11.glGetFloat(EXTTextureFilterAnisotropic.GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT) : 0;
        GLFWVidMode highestResolution = getHighestResolution();
        maxWidth = highestResolution.width();
        maxHeight = highestResolution.height();
        GLFW.glfwDestroyWindow(windowHandle);
    }

    public GLCapabilities getCapabilities() {
        return capabilities;
    }

    public int getMaxSamples() {
        return maxSamples;
    }

    public int getMaxTextureUnits() {
        return maxTextureUnits;
    }

    public float getMaxAnisotropy() {
        return maxAnisotropy;
    }

    public int getMaxWidth() {
        return maxWidth;
    }

    public int getMaxHeight() {
        return maxHeight;
    }

    private static long getDummyWindowHandle() {
        GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, GLFW.GLFW_FALSE);
        long windowHandle = GLFW.glfwCreateWindow(720, 480, "", MemoryUtil.NULL, MemoryUtil.NULL);
        if (windowHandle == MemoryUtil.NULL) {
            throw new IllegalStateException("Dummy context could not be created!");
        }
        return windowHandle;
    }

    private static GLFWVidMode getHighestResolution() {
        long monitorHandle = GLFW.glfwGetPrimaryMonitor();
        Buffer videoModes = GLFW.glfwGetVideoModes(monitorHandle);
        if (videoModes == null) {
            throw new IllegalStateException("Failed to query the video modes of your primary monitor!");
        }
        int bestResolution = Integer.MIN_VALUE;
        GLFWVidMode bestVideoMode = null;
        for (GLFWVidMode videoMode : videoModes) {
            int currentResolution = videoMode.width() * videoMode.height();
            if (currentResolution > bestResolution) {
                bestResolution = currentResolution;
                bestVideoMode = videoMode;
            }
        }
        return bestVideoMode;
    }
}
