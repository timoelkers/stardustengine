package de.oelkers.stardust.window;

import org.lwjgl.opengl.GL11;

public enum WindowHint {

    FASTEST(GL11.GL_FASTEST),
    NICEST(GL11.GL_NICEST),
    DON_NOT_CARE(GL11.GL_DONT_CARE);

    private final int glCode;

    WindowHint(int glCode) {
        this.glCode = glCode;
    }

    public int getGlCode() {
        return glCode;
    }
}
