package de.oelkers.stardust.window;

import org.lwjgl.glfw.GLFW;

import java.util.Arrays;

public enum MouseButton {

    BUTTON_UNKNOWN(GLFW.GLFW_KEY_UNKNOWN),
    BUTTON_LEFT(GLFW.GLFW_MOUSE_BUTTON_LEFT),
    BUTTON_RIGHT(GLFW.GLFW_MOUSE_BUTTON_RIGHT),
    BUTTON_MIDDLE(GLFW.GLFW_MOUSE_BUTTON_MIDDLE);

    private final int buttonCode;

    MouseButton(int buttonCode) {
        this.buttonCode = buttonCode;
    }

    public int getButtonCode() {
        return buttonCode;
    }

    public static MouseButton getButtonByCode(int code) {
        return Arrays.stream(values()).filter(button -> button.buttonCode == code).findAny().orElse(BUTTON_UNKNOWN);
    }
}
