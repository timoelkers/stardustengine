package de.oelkers.stardust.window;

import de.oelkers.stardust.OpenGLContext;
import de.oelkers.stardust.config.ConfigStore;
import de.oelkers.stardust.config.StardustErrorCallback;
import de.oelkers.stardust.config.StardustErrorCallback.ErrorType;
import de.oelkers.stardust.config.StardustErrorCallback.Severity;
import de.oelkers.stardust.graphics.assets.textures.BaseTexture.TextureData;
import de.oelkers.stardust.graphics.assets.textures.BaseTexture.TextureFormat;
import de.oelkers.stardust.graphics.assets.textures.TextureLoader;
import de.oelkers.stardust.math.coordinates.ScreenClipSpaceConverter;
import de.oelkers.stardust.math.internal.MathDelegate;
import de.oelkers.stardust.math.vectors.ReadOnlyVector4f;
import de.oelkers.stardust.utils.NonThrowingAutoCloseable;
import de.oelkers.stardust.window.cursor.Cursor;
import de.oelkers.stardust.window.cursor.SystemCursor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.PointerBuffer;
import org.lwjgl.glfw.Callbacks;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWImage;
import org.lwjgl.glfw.GLFWImage.Buffer;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.*;
import org.lwjgl.system.MemoryUtil;

import java.util.stream.IntStream;

import static de.oelkers.stardust.window.MouseButton.getButtonByCode;
import static de.oelkers.stardust.window.ViewportStrategy.DYNAMIC;
import static de.oelkers.stardust.window.ViewportStrategy.MULTIPLE;
import static org.lwjgl.system.MemoryUtil.memPointerBuffer;
import static org.lwjgl.system.MemoryUtil.memUTF8;

public final class StardustWindow extends WindowCallbacks implements NonThrowingAutoCloseable, ScreenClipSpaceConverter {

    private static final Logger LOGGER = LogManager.getLogger(StardustWindow.class);

    private long monitorHandle;
    private long windowHandle;
    private GLFWVidMode videoMode;
    private Cursor cursor = SystemCursor.ARROW;
    private CursorMode cursorMode;
    private WindowMode windowMode;
    private int width, height;
    private int minWidth = GLFW.GLFW_DONT_CARE, minHeight = GLFW.GLFW_DONT_CARE;
    private int maxWidth = GLFW.GLFW_DONT_CARE, maxHeight = GLFW.GLFW_DONT_CARE;
    private int xPos, yPos;
    private ViewportStrategy viewportStrategy;

    private StardustWindow(Builder builder) {
        setupInitialValues(builder);
        preContextInit(builder);
        contextInit(builder);
        postContextInit(builder);
        LOGGER.info("Window " + windowHandle + " has been created.");
    }

    public long getWindowHandle() {
        return windowHandle;
    }

    /**
     * Sets the position for the top left corner of the window. The coordinate system is a OpenGL-like system where (0|0) is the
     * center of the screen and the positive x-axis goes to the right, the positive y-axis goes to the top. This has no affect
     * for full screen windows and you should avoid calling this if the window is visible.
     *
     * @param xPos the x-position in pixels from the center of the screen
     * @param yPos the y-position in pixels from the center of the screen
     */
    public void setPosition(int xPos, int yPos) {
        if (windowMode != WindowMode.FULLSCREEN && windowMode != WindowMode.WINDOWED_FULLSCREEN) {
            if (this.xPos != xPos || this.yPos != yPos) {
                int screenX = this.xPos == xPos ? xPos : HardwareLimitations.get().getMaxWidth() / 2 + xPos;
                int screenY = this.yPos == yPos ? yPos : HardwareLimitations.get().getMaxHeight() / 2 - yPos;
                GLFW.glfwSetWindowPos(windowHandle, screenX, screenY);
            }
        }
    }

    public int getXPos() {
        return xPos;
    }

    public int getYPos() {
        return yPos;
    }

    public void setMinSize(int width, int height) {
        setSizeLimits(width, height, maxWidth, maxHeight);
    }

    public void setMaxSize(int width, int height) {
        setSizeLimits(minWidth, maxWidth, width, height);
    }

    public void setSizeLimits(int minWidth, int minHeight, int maxWidth, int maxHeight) {
        if (this.minWidth != minWidth || this.minHeight != minHeight || this.maxWidth != maxWidth || this.maxHeight != maxHeight) {
            this.minWidth = minWidth;
            this.minHeight = minHeight;
            this.maxWidth = maxWidth;
            this.maxHeight = maxHeight;
            GLFW.glfwSetWindowSizeLimits(windowHandle, minWidth, minHeight, maxWidth, maxHeight);
        }
    }

    /**
     * Sets the size of the window in pixels. Please note that for full screen windows, this changes the resolution
     * of the window to the closest resolution supported by the monitor. You have to query the new size or rely
     * on the resize callback if the resolution is important for you.
     *
     * @param width  the desired width in pixels
     * @param height the desired height in pixels
     */
    public void setSize(int width, int height) {
        if (this.width != width || this.height != height) {
            GLFW.glfwSetWindowSize(windowHandle, width, height);
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public float getAspectRatio() {
        return width / (float) height;
    }

    public int getMinWidth() {
        return minWidth;
    }

    public int getMinHeight() {
        return minHeight;
    }

    public int getMaxWidth() {
        return maxWidth;
    }

    public int getMaxHeight() {
        return maxHeight;
    }

    public void setCursor(Cursor cursor) {
        if (this.cursor != cursor) {
            this.cursor = cursor;
            GLFW.glfwSetCursor(windowHandle, cursor.getHandle());
        }
    }

    public Cursor getCursor() {
        return cursor;
    }

    public void setCursorMode(CursorMode cursorMode) {
        if (this.cursorMode != cursorMode) {
            this.cursorMode = cursorMode;
            GLFW.glfwSetInputMode(windowHandle, GLFW.GLFW_CURSOR, cursorMode.getGLFWCode());
        }
    }

    public CursorMode getCursorMode() {
        return cursorMode;
    }

    public WindowMode getWindowMode() {
        return windowMode;
    }

    public void makeContextCurrent() {
        LOGGER.trace("Setting current OpenGL context handle: " + windowHandle);
        OpenGLContext.get().bindContext(windowHandle);
    }

    /**
     * Processes the event in the system event queue and calls the registered callbacks for the window. Some systems
     * block the event processing when window resize, move or menu operations are detected. This means none of the callbacks
     * gets executed. You should call this function every frame.
     */
    public void pollWindowEvents() {
        GLFW.glfwPollEvents();
    }

    public void setClearColor(ReadOnlyVector4f color) {
        GL11.glClearColor(color.getX(), color.getY(), color.getZ(), color.getW());
    }

    public void clearWindowContents() {
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
    }

    public void updateWindowContents() {
        GLFW.glfwSwapBuffers(windowHandle);
    }

    /**
     * Returns whether the user requested the window to close. This can be usually done by clicking
     * the close button or through a keyboard shortcut. This can happen on full screen or undecorated windows as well.
     * If you ignore this request, the window will not close.
     */
    public boolean isCloseRequested() {
        return GLFW.glfwWindowShouldClose(windowHandle);
    }

    private void setupInitialValues(Builder builder) {
        windowMode = builder.windowMode;
        viewportStrategy = builder.viewportStrategy;
        width = builder.width;
        height = builder.height;
    }

    private void preContextInit(Builder builder) {
        GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, GLFW.GLFW_FALSE);
        GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MAJOR, 4);
        GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MINOR, 3);
        GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_PROFILE, GLFW.GLFW_OPENGL_CORE_PROFILE);
        GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_FORWARD_COMPAT, GLFW.GLFW_TRUE);

        GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_DEBUG_CONTEXT, builder.debug ? GLFW.GLFW_TRUE : GLFW.GLFW_FALSE);
        GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, builder.resizable ? GLFW.GLFW_TRUE : GLFW.GLFW_FALSE);
        GLFW.glfwWindowHint(GLFW.GLFW_FLOATING, builder.floating ? GLFW.GLFW_TRUE : GLFW.GLFW_FALSE);
        GLFW.glfwWindowHint(GLFW.GLFW_DECORATED, builder.decorated ? GLFW.GLFW_TRUE : GLFW.GLFW_FALSE);
        GLFW.glfwWindowHint(GLFW.GLFW_ALPHA_BITS, builder.alphaBits);
        GLFW.glfwWindowHint(GLFW.GLFW_DEPTH_BITS, builder.depthBits);
        GLFW.glfwWindowHint(GLFW.GLFW_STENCIL_BITS, builder.stencilBits);

        monitorHandle = GLFW.glfwGetPrimaryMonitor();
        videoMode = GLFW.glfwGetVideoMode(monitorHandle);
        if (videoMode == null) {
            throw new IllegalStateException("Failed to query the current video mode of your primary monitor!");
        }
        GLFW.glfwWindowHint(GLFW.GLFW_RED_BITS, videoMode.redBits());
        GLFW.glfwWindowHint(GLFW.GLFW_GREEN_BITS, videoMode.greenBits());
        GLFW.glfwWindowHint(GLFW.GLFW_BLUE_BITS, videoMode.blueBits());
        GLFW.glfwWindowHint(GLFW.GLFW_REFRESH_RATE, videoMode.refreshRate());

        int maxSamples = HardwareLimitations.get().getMaxSamples();
        int samples = builder.antiAliasing == AntiAliasing.MAX ? maxSamples : MathDelegate.min(builder.antiAliasing.getSamples(), maxSamples);
        GLFW.glfwWindowHint(GLFW.GLFW_SAMPLES, samples);
    }

    private void contextInit(Builder builder) {
        if (builder.windowMode == WindowMode.NORMAL) {
            windowHandle = GLFW.glfwCreateWindow(builder.width, builder.height, builder.title, MemoryUtil.NULL, builder.sharedContext);
        } else if (builder.windowMode == WindowMode.FULLSCREEN) {
            windowHandle = GLFW.glfwCreateWindow(builder.width, builder.height, builder.title, monitorHandle, builder.sharedContext);
        } else {
            windowHandle = GLFW.glfwCreateWindow(videoMode.width(), videoMode.width(), builder.title, monitorHandle, builder.sharedContext);
        }
        if (windowHandle == MemoryUtil.NULL) {
            throw new IllegalStateException("Window and OpenGL context could not be created!");
        }
        LOGGER.trace("Finished creating a window with OpenGL context and handle: " + windowHandle);
        makeContextCurrent();
    }

    private void postContextInit(Builder builder) {
        setupDebugCallback();
        setupWindowCallbacks();
        setupWindowIcons(builder);
        setupDefaultStates(builder);

        setCursorMode(builder.cursorMode);
        if (builder.xPos != GLFW.GLFW_DONT_CARE && builder.yPos != GLFW.GLFW_DONT_CARE) {
            setPosition(builder.xPos, builder.yPos);
        }
        if (builder.vSync) {
            boolean adaptiveSwap = GLFW.glfwExtensionSupported("WGL_EXT_swap_control_tear") || GLFW.glfwExtensionSupported("GLX_EXT_swap_control_tear");
            GLFW.glfwSwapInterval(adaptiveSwap ? -1 : 1);
        } else {
            GLFW.glfwSwapInterval(0);
        }
        if (windowMode == WindowMode.NORMAL) {
            GLFW.glfwShowWindow(windowHandle);
        }
    }

    private static void setupDebugCallback() {
        GL11.glEnable(GL43.GL_DEBUG_OUTPUT);
        GL11.glEnable(GL43.GL_DEBUG_OUTPUT_SYNCHRONOUS);
        GL43.glDebugMessageCallback((source, type, id, severity, length, message, userParam) -> {
            StardustErrorCallback errorCallback = ConfigStore.get().getErrorCallback();
            errorCallback.onError(id, GLDebugMessageCallback.getMessage(length, message), ErrorType.getByTypeCode(type), Severity.getBySeverityCode(severity));
        }, MemoryUtil.NULL);
        LOGGER.trace("Finished setting up OpenGL error callback.");
    }

    private void setupWindowCallbacks() {
        GLFW.glfwSetFramebufferSizeCallback(windowHandle, (window, width, height) -> runResizeCallbacks(width, height));
        GLFW.glfwSetWindowRefreshCallback(windowHandle, window -> runRefreshCallbacks());
        GLFW.glfwSetWindowPosCallback(windowHandle, (window, xPos, yPos) -> runMoveCallbacks(xPos, yPos));
        GLFW.glfwSetKeyCallback(windowHandle, (window, key, scancode, action, mods) -> runKeyCallbacks(Key.getKeyByCode(key), ButtonState.getStateByCode(action)));
        GLFW.glfwSetCharCallback(windowHandle, (window, codePoint) -> runCharCallbacks(codePoint));
        GLFW.glfwSetMouseButtonCallback(windowHandle, (window, button, action, mods) -> runMouseButtonCallbacks(getButtonByCode(button), ButtonState.getStateByCode(action)));
        GLFW.glfwSetScrollCallback(windowHandle, (window, xOffset, yOffset) -> runScrollEventCallbacks(xOffset, yOffset));
        GLFW.glfwSetCursorPosCallback(windowHandle, (window, xPos, yPos) -> runMousePositionCallbacks(xPos, yPos));
        GLFW.glfwSetWindowIconifyCallback(windowHandle, (window, iconified) -> runIconifyCallbacks(iconified));
        GLFW.glfwSetWindowFocusCallback(windowHandle, (window, focused) -> runFocusCallbacks(focused));
        GLFW.glfwSetDropCallback(windowHandle, (window, count, names) -> {
            PointerBuffer nameBuffer = memPointerBuffer(names, count);
            runDropCallbacks(IntStream.range(0, count).mapToObj(i -> memUTF8(nameBuffer.get())).toArray(String[]::new));
        });
        GLFW.glfwSetCursorEnterCallback(windowHandle, (window, entered) -> {
            if (entered) {
                runCursorEnterCallbacks();
            } else {
                runCursorLeaveCallbacks();
            }
        });
    }

    private void setupWindowIcons(Builder builder) {
        if (builder.iconPaths != null) {
            Buffer imageBuffer = GLFWImage.create(builder.iconPaths.length);
            for (int i = 0; i < builder.iconPaths.length; i++) {
                TextureData textureData = TextureLoader.loadTexture(builder.iconPaths[i], TextureFormat.RGBA, 0, true);
                GLFWImage image = GLFWImage.create().set(textureData.width, textureData.height, textureData.imageData);
                imageBuffer.put(image);
            }
            GLFW.glfwSetWindowIcon(windowHandle, imageBuffer.flip());
        }
    }

    private static void setupDefaultStates(Builder builder) {
        if (GL11.glGetInteger(GL13.GL_SAMPLES) > 0) {
            GL11.glEnable(GL13.GL_MULTISAMPLE);
        }
        GLCapabilities capabilities = HardwareLimitations.get().getCapabilities();
        if (capabilities.GL_ARB_seamless_cube_map) {
            GL11.glEnable(ARBSeamlessCubeMap.GL_TEXTURE_CUBE_MAP_SEAMLESS);
        }
        GL11.glHint(GL13.GL_TEXTURE_COMPRESSION_HINT, builder.textureCompressionHint.getGlCode());
        OpenGLContext glContext = OpenGLContext.get();
        glContext.setBlending(true);
        glContext.setCulling(true);
        glContext.setDepthTest(true);
    }

    @Override
    protected void runResizeCallbacks(int width, int height) {
        this.width = width;
        this.height = height;
        if (viewportStrategy == DYNAMIC) {
            GL11.glViewport(0, 0, width, height);
        } else if (viewportStrategy == MULTIPLE) {
            GL41.glViewportIndexedf(0, 0, 0, width, height);
        }
        super.runResizeCallbacks(width, height);
    }

    @Override
    protected void runMoveCallbacks(int xPos, int yPos) {
        this.xPos = xPos - HardwareLimitations.get().getMaxWidth() / 2;
        this.yPos = HardwareLimitations.get().getMaxHeight() / 2 - yPos;
        super.runMoveCallbacks(this.xPos, this.yPos);
    }

    @Override
    public void close() {
        Callbacks.glfwFreeCallbacks(windowHandle);
        GLFW.glfwDestroyWindow(windowHandle);
        OpenGLContext.get().unbindContext(windowHandle);
        runCloseCallbacks();
        LOGGER.info("Window " + windowHandle + " has been closed.");
    }

    @Override
    public float toScreenSpaceX(float xPos) {
        return (xPos + 1) / 2 * getWidth();
    }

    @Override
    public float toScreenSpaceY(float yPos) {
        return (yPos - 1) / -2 * getHeight();
    }

    @Override
    public float toClipSpaceX(float xPos) {
        return 2 * xPos / getWidth() - 1;
    }

    @Override
    public float toClipSpaceY(float yPos) {
        return -2 * yPos / getHeight() + 1;
    }

    public static class Builder {

        private int xPos = GLFW.GLFW_DONT_CARE;
        private int yPos = GLFW.GLFW_DONT_CARE;
        private int alphaBits = 8;
        private int depthBits = 24;
        private int stencilBits = 8;
        private boolean resizable = true;
        private boolean decorated = true;
        private CharSequence title = "";
        private final int width, height;
        private boolean debug;
        private boolean floating;
        private boolean vSync;
        private String[] iconPaths;
        private AntiAliasing antiAliasing = AntiAliasing.NONE;
        private CursorMode cursorMode = CursorMode.NORMAL;
        private WindowMode windowMode = WindowMode.NORMAL;
        private WindowHint textureCompressionHint = WindowHint.DON_NOT_CARE;
        private ViewportStrategy viewportStrategy = DYNAMIC;
        private long sharedContext = MemoryUtil.NULL;

        /**
         * Initializes the builder and sets the size of the window in pixels. Both values have to be greater than zero.
         * Please note that for full screen windows, the size is actually the desired resolution of the window.
         * The actual resolution is chosen by the operating system, which chooses a resolution supported by the monitor which is as close as possible to the desired one.
         * The size is ignored for "windowed full screen" windows as this is a paraphrase for choosing the current active video mode. This speeds
         * up the creation and allows for faster application switching as the video mode does not have to be changed in between.
         *
         * @param width  the desired width in pixels
         * @param height the desired height in pixels
         * @throws IllegalArgumentException if with or height are smaller than one
         */
        public Builder(int width, int height) throws IllegalArgumentException {
            if (width <= 0 || height <= 0) {
                throw new IllegalArgumentException("Width and height must both be greater than zero!");
            }
            this.width = width;
            this.height = height;
        }

        /**
         * Sets the title of the window. This is ignored for full screen windows and and empty window
         * title is shown on default.
         *
         * @param title the title of the window
         * @return the builder to allow for method chaining
         */
        public Builder withWindowTitle(CharSequence title) {
            this.title = title;
            return this;
        }

        /**
         * Sets the starting position for the top left corner of the window. The coordinate system is a OpenGL-like system where (0|0) is the
         * center of the screen and the positive x-axis goes to the right, the positive y-axis goes to the top. The starting position
         * is ignored for full screen windows and is chosen by the operating systems on default.
         *
         * @param xPos the x-position in pixels from the center of the screen
         * @param yPos the y-position in pixels from the center of the screen
         * @return the builder to allow for method chaining
         */
        public Builder withStartingPosition(int xPos, int yPos) {
            this.xPos = xPos;
            this.yPos = yPos;
            return this;
        }

        /**
         * Specifies whether the window should use vertical synchronization. This is a technique to prevent screen tearing by setting the update rate of the GPU to
         * the refresh rate of the monitor. This is achieved by stalling the rendering thread (CPU) until the monitor is ready.<p>
         * Please note that if the rendering is slower that the refresh rate of the monitor, the synchronization is still applied and will basically halve your frame rate.
         * To prevent this, some drivers allow adaptive vertical synchronization which does not synchronize if that case happens. If this extension is
         * available and vSync is enabled, it is used.<p>
         * This also has the great side effect of saving power of CPU and GPU as they don't have to compute unnecessary data as fast as possible. No vertical synchronization
         * is applied on default. Please note that graphics card driver settings may override what you have chosen, so don't rely on it.
         *
         * <blockquote>
         * - GPU takes 4ms to render, refresh rate is at 60Hz -> thread will wait 12.6ms -> 60FPS<br>
         * - GPU takes 18ms to render, refresh rate is at 60Hz -> monitor skips a frame ("jumping frames") and thread will wait 15.3ms -> 30FPS
         * </blockquote>
         *
         * @param vSync whether the window should use vertical synchronization (vSync)
         * @return the builder to allow for method chaining
         */
        public Builder withVSync(boolean vSync) {
            this.vSync = vSync;
            return this;
        }

        /**
         * Specifies whether the window is resizable by the user. Please note that turning it off does not prevent you from
         * setting the size programmatically. This hint is ignored for full screen windows and is meaningless
         * for undecorated windows as the user can not resize it anyway. The window is resizable on default.
         *
         * @param resizable whether the window can be resized by the user
         * @return the builder to allow for method chaining
         */
        public Builder withResizableWindow(boolean resizable) {
            this.resizable = resizable;
            return this;
        }

        /**
         * Specifies whether the window should be always on top of others. This is often referred to as a
         * "floating" window. Please note that other windows will still be able to receive focus and this flag
         * gets overridden by other applications choosing this mode. This hint is ignored for full screen windows
         * and is false on default.
         *
         * @param floating whether the window should be always be on top of others
         * @return the builder to allow for method chaining
         */
        public Builder withFloatingWindow(boolean floating) {
            this.floating = floating;
            return this;
        }

        /**
         * Specifies whether the window is decorated. The window decoration depends on the operating system but generally
         * contains a border, a toolbar with the title and widgets to iconify, maximize and close the window. This hint is ignored
         * for full screen windows and is true on default.
         *
         * @param decorated whether the window should be decorated
         * @return the builder to allow for method chaining
         */
        public Builder withWindowDecoration(boolean decorated) {
            this.decorated = decorated;
            return this;
        }

        /**
         * Specifies whether to create a debug OpenGL context which may report different errors and performance issues. The quality and
         * quantity of those messages depend on the implementation of OpenGL. If the debug context is enabled, the implementation is forced
         * to produce OpenGL and GLSL errors at least, if it's not enabled, the implementation does not have to provide any messages at all.
         * This hint is false on default.
         *
         * @param debug whether a debug context should be created
         * @return the builder to allow for method chaining
         */
        public Builder withDebugContext(boolean debug) {
            this.debug = debug;
            return this;
        }

        /**
         * Specifies the icon paths for this window. You should provide icons in multiple sizes and the ones closest to the sizes desired by the system are selected.
         * The desired image sizes depend on the platform and system settings but generally include 16x16, 32x32 and 48x48. The system default application icon is chosen
         * on default.
         *
         * @param iconPaths an array of paths to the icons
         * @return the builder to allow for method chaining
         */
        public Builder withWindowIcons(String... iconPaths) {
            this.iconPaths = iconPaths;
            return this;
        }

        /**
         * Specifies the window mode you want to use for your window. Please note that the behavior
         * of some of the hints might change depending on the mode you choose. The normal windowed window is chosen on default.
         *
         * @param windowMode the window mode you want to use
         * @return the builder to allow for method chaining
         */
        public Builder withWindowMode(WindowMode windowMode) {
            this.windowMode = windowMode;
            return this;
        }

        /**
         * Specifies the cursor mode you want to use for your window. This generally means how the cursor behaves
         * when your window is visible or focused. You should not implement any of the modes yourself as the windowing system provides you with virtual mouse events
         * even if the cursor is not visible. The normal cursor behavior is chosen on default.
         *
         * @param cursorMode the cursor mode you want to use
         * @return the builder to allow for method chaining
         */
        public Builder withCursorMode(CursorMode cursorMode) {
            this.cursorMode = cursorMode;
            return this;
        }

        /**
         * Specifies the anti aliasing value used by the window. Anti aliasing is generally an expensive technique
         * but can lead to better graphics quality especially on low resolution windows. If you choose a higher value
         * that the maximum value supported by your system, than the maximum is chosen.<p>
         * No anti aliasing is applied on default. Please note that graphics card driver settings may override what you have chosen, so don't rely on it.
         *
         * @param antiAliasing the anti aliasing value you want to use
         * @return the builder to allow for method chaining
         */
        public Builder withAntiAliasing(AntiAliasing antiAliasing) {
            this.antiAliasing = antiAliasing;
            return this;
        }

        /**
         * Specifies the bit depth of the alpha buffer for the default frame buffer. The default value is 8 bits.
         *
         * @param alphaBits the bit depth of the alpha buffer
         * @return the builder to allow for method chaining
         */
        public Builder withAlphaBits(int alphaBits) {
            this.alphaBits = alphaBits;
            return this;
        }

        /**
         * Specifies the bit depth of the depth buffer for the default frame buffer. The default value is 24 bits.
         *
         * @param depthBits the bit depth of the depth buffer
         * @return the builder to allow for method chaining
         */
        public Builder withDepthBits(int depthBits) {
            this.depthBits = depthBits;
            return this;
        }

        /**
         * Specifies the bit depth of the stencil buffer for the default frame buffer. The default value is 8 bits.
         *
         * @param stencilBits the bit depth of the stencil buffer
         * @return the builder to allow for method chaining
         */
        public Builder withStencilBits(int stencilBits) {
            this.stencilBits = stencilBits;
            return this;
        }

        public Builder withTextureCompressionHint(WindowHint textureCompressionHint) {
            this.textureCompressionHint = textureCompressionHint;
            return this;
        }

        public Builder withViewportStrategy(ViewportStrategy viewportStrategy) {
            this.viewportStrategy = viewportStrategy;
            return this;
        }

        public Builder withSharedContext(StardustWindow sharedContext) {
            this.sharedContext = sharedContext.getWindowHandle();
            return this;
        }

        public StardustWindow build() {
            if (viewportStrategy == MULTIPLE) {
                ConfigStore.get().useIndividualViewports(true);
            }
            return new StardustWindow(this);
        }
    }
}
