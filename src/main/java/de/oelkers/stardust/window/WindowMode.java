package de.oelkers.stardust.window;

public enum WindowMode {

    NORMAL,
    WINDOWED_FULLSCREEN,
    FULLSCREEN
}
