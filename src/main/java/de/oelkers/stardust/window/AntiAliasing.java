package de.oelkers.stardust.window;

public enum AntiAliasing {

    NONE(0),
    X2(2),
    X4(4),
    X8(8),
    X16(16),
    X32(32),
    MAX(-1);

    private final int samples;

    AntiAliasing(int samples) {
        this.samples = samples;
    }

    public int getSamples() {
        return samples;
    }
}
