package de.oelkers.stardust.window;

import org.lwjgl.opengl.GL11;

public enum RenderMode {

    FILLED(GL11.GL_FILL),
    EDGES(GL11.GL_LINE),
    VERTICES(GL11.GL_POINT);

    private final int glCode;

    RenderMode(int glCode) {
        this.glCode = glCode;
    }

    public int getGlCode() {
        return glCode;
    }
}
