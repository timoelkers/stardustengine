package de.oelkers.stardust.window;

public enum ViewportStrategy {

    FIXED, DYNAMIC, MULTIPLE
}
