package de.oelkers.stardust.window;

import org.lwjgl.glfw.GLFW;

import java.util.Arrays;

public enum ButtonState {

    PRESSED(GLFW.GLFW_PRESS),
    RELEASED(GLFW.GLFW_RELEASE),
    REPEATED(GLFW.GLFW_REPEAT);

    private final int glfwCode;

    ButtonState(int glfwCode) {
        this.glfwCode = glfwCode;
    }

    public static ButtonState getStateByCode(int code) {
        return Arrays.stream(values()).filter(buttonState -> buttonState.glfwCode == code).findAny().orElseThrow();
    }
}
