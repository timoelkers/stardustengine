package de.oelkers.stardust.window;

import org.lwjgl.glfw.GLFW;

public enum CursorMode {

    NORMAL(GLFW.GLFW_CURSOR_NORMAL),
    HIDDEN(GLFW.GLFW_CURSOR_HIDDEN),
    CAPTURED(GLFW.GLFW_CURSOR_DISABLED);

    private final int glfwCode;

    CursorMode(int glfwCode) {
        this.glfwCode = glfwCode;
    }

    public int getGLFWCode() {
        return glfwCode;
    }
}
