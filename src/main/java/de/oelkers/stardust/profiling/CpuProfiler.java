package de.oelkers.stardust.profiling;

import com.sun.management.OperatingSystemMXBean;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CpuProfiler {

    public boolean isSupported() {
        ThreadMXBean threadBean = ManagementFactory.getThreadMXBean();
        return threadBean.isCurrentThreadCpuTimeSupported() && threadBean.isThreadCpuTimeEnabled();
    }

    public CpuProfilingResult get() {
        if (isSupported()) {
            ThreadGroup threadGroup = Thread.currentThread().getThreadGroup();
            Thread[] threads = new Thread[threadGroup.activeCount()];
            threadGroup.enumerate(threads);
            return new CpuProfilingResult(threads);
        }
        return CpuProfilingResult.EMPTY;
    }

    public static final class CpuProfilingResult {

        public static final CpuProfilingResult EMPTY = new CpuProfilingResult();

        private final Map<String, Long> threadUserTimes;
        private final Map<String, Long> threadCpuTimes;

        private long processCpuTime;
        private double processCpuLoad;

        private CpuProfilingResult() {
            threadUserTimes = Collections.emptyMap();
            threadCpuTimes = Collections.emptyMap();
        }

        private CpuProfilingResult(Thread[] threads) {
            threadUserTimes = new HashMap<>(threads.length);
            threadCpuTimes = new HashMap<>(threads.length);
            ThreadMXBean threadBean = ManagementFactory.getThreadMXBean();
            for (Thread thread : threads) {
                threadUserTimes.put(thread.getName(), threadBean.getThreadUserTime(thread.getId()));
                threadCpuTimes.put(thread.getName(), threadBean.getThreadCpuTime(thread.getId()));
            }
            OperatingSystemMXBean platformBean = ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class);
            processCpuTime = platformBean.getProcessCpuTime();
            processCpuLoad = platformBean.getProcessCpuLoad();
        }

        public Set<String> getThreadNames() {
            return threadCpuTimes.keySet();
        }

        public long getThreadUserTime(String threadName) {
            return threadUserTimes.get(threadName);
        }

        public long getThreadCpuTime(String threadName) {
            return threadCpuTimes.get(threadName);
        }

        public long getProcessCpuTime() {
            return processCpuTime;
        }

        public double getProcessCpuLoad() {
            return processCpuLoad;
        }
    }
}
