package de.oelkers.stardust.profiling;

import de.oelkers.stardust.entities.Entity;
import de.oelkers.stardust.graphics.assets.fonts.GuiText;
import de.oelkers.stardust.graphics.assets.models.ModelProvider;
import de.oelkers.stardust.graphics.assets.textures.Texture2D;
import de.oelkers.stardust.graphics.rendering.RenderSystem;
import de.oelkers.stardust.graphics.rendering.ToggleRendering;
import de.oelkers.stardust.graphs.LineGraph;
import de.oelkers.stardust.math.coordinates.ScreenClipSpaceConverter;
import de.oelkers.stardust.math.coordinates.ScreenCoordinates;
import de.oelkers.stardust.math.transformations.Transform2D;
import de.oelkers.stardust.math.transformations.TransformSpace;
import de.oelkers.stardust.math.transformations.Transformable2D;
import de.oelkers.stardust.profiling.CpuProfiler.CpuProfilingResult;
import de.oelkers.stardust.profiling.GpuProfiler.GpuProfilingResult;
import de.oelkers.stardust.utils.GraphicsStack;
import de.oelkers.stardust.utils.NonThrowingAutoCloseable;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;

import static de.oelkers.stardust.graphics.assets.fonts.AngelCodeFontLoader.DEFAULT_FONT;
import static de.oelkers.stardust.graphics.rendering.components.RenderTechniques.withTexture;
import static java.lang.System.lineSeparator;

public final class ProfilerOverlay extends ToggleRendering implements NonThrowingAutoCloseable {

    private final Collection<Entity> entities = new ArrayList<>();
    private final Thread pollingThread;
    private final LineGraph gpuUsageGraph;
    private final LineGraph cpuUsageGraph;
    private final GuiText statistics;

    public ProfilerOverlay(GraphicsStack graphicsStack) {
        this(graphicsStack.getRenderSystem(), graphicsStack.getModelProvider(), graphicsStack.getWindow());
    }

    public ProfilerOverlay(RenderSystem renderSystem, ModelProvider modelProvider, ScreenClipSpaceConverter converter) {
        super(renderSystem);
        statistics = addChild(new GuiText.Builder(renderSystem, modelProvider, converter)
                .withColor(Color.WHITE)
                .withScale(0.45f)
                .withFont(DEFAULT_FONT)
                .withPreLoadedCharacters(32, 126)
                .build());
        setupBaseGeometry(modelProvider);
        gpuUsageGraph = addChild(new LineGraph.Builder(renderSystem, modelProvider)
                .withScale(0.5f, 0.5f)
                .withSections(10, 101)
                .withColor(Color.GREEN)
                .build());
        addChild(new GuiText.Builder(renderSystem, modelProvider, converter)
                .withColor(Color.GREEN)
                .withOrigin(ScreenCoordinates.relative(0.8f, 0.95f))
                .withScale(0.45f)
                .withFont(DEFAULT_FONT)
                .withPreLoadedCharacters(32, 126)
                .withText("GPU Usage(%)")
                .build());
        cpuUsageGraph = addChild(new LineGraph.Builder(renderSystem, modelProvider)
                .withScale(0.5f, 0.5f)
                .withSections(10, 101)
                .withColor(Color.BLUE)
                .build());
        addChild(new GuiText.Builder(renderSystem, modelProvider, converter)
                .withColor(Color.BLUE)
                .withOrigin(ScreenCoordinates.relative(0.8f, 0.9f))
                .withScale(0.45f)
                .withFont(DEFAULT_FONT)
                .withPreLoadedCharacters(32, 126)
                .withText("CPU Usage(%)")
                .build());
        pollingThread = new Thread(this::pollProfiler, "GpuProfilerPollingThread");
        pollingThread.start();
    }

    @Override
    public void close() {
        pollingThread.interrupt();
    }

    @Override
    protected Collection<Entity> getEntities() {
        return entities;
    }

    private void pollProfiler() {
        long previousCpuTime = -1;
        GpuProfiler gpuProfiler = new GpuProfiler().start();
        CpuProfiler cpuProfiler = new CpuProfiler();
        while (!Thread.currentThread().isInterrupted()) {
            GpuProfilingResult gpuResult = gpuProfiler.get();
            StringBuilder statisticsBuilder = new StringBuilder();
            if (GpuProfilingResult.EMPTY != gpuResult) {
                gpuUsageGraph.addEntry(gpuResult.getGpuUsagePercent());
                statisticsBuilder.append("GPU Usage(%): ").append(gpuResult.getGpuUsagePercent()).append(lineSeparator());
                statisticsBuilder.append("Vertex Shader Invocations: ").append(gpuResult.getVertexShaderInvocations()).append(lineSeparator());
                statisticsBuilder.append("Fragment Shader Invocations: ").append(gpuResult.getFragmentShaderInvocations()).append(lineSeparator());
                statisticsBuilder.append("Geometry Shader Invocations: ").append(gpuResult.getGeometryShaderInvocations()).append(lineSeparator());
                statisticsBuilder.append("Vertices Fetched: ").append(gpuResult.getVerticesFetched()).append(lineSeparator());
                statisticsBuilder.append("Primitives Fetched: ").append(gpuResult.getPrimitivesFetched()).append(lineSeparator()).append(lineSeparator());
            }
            CpuProfilingResult cpuResult = cpuProfiler.get();
            if (CpuProfilingResult.EMPTY != cpuResult) {
                cpuUsageGraph.addEntry((int) (cpuResult.getProcessCpuLoad() * 100));
                statisticsBuilder.append("CPU Usage(%): ").append((int) (cpuResult.getProcessCpuLoad() * 100)).append(lineSeparator());
                long currentCpuTime = cpuResult.getProcessCpuTime();
                if (previousCpuTime != -1) {
                    statisticsBuilder.append("Total CPU Time (ms): ").append((currentCpuTime - previousCpuTime) / 1000000).append(lineSeparator());
                }
                previousCpuTime = currentCpuTime;
            }
            if (GpuProfilingResult.EMPTY != gpuResult || CpuProfilingResult.EMPTY != cpuResult) {
                statistics.setText(statisticsBuilder);
            }
        }
        gpuProfiler.close();
    }

    private void setupBaseGeometry(ModelProvider modelProvider) {
        Entity backgroundPane = new Entity("BackgroundPane");
        withTexture(modelProvider.getRectangle(), new Texture2D(Color.BLACK)).build(backgroundPane);
        Transformable2D paneTransform = new Transform2D();
        paneTransform.translate(0.5f, 0, TransformSpace.WORLD);
        paneTransform.scale(0.5f, 1f);
        backgroundPane.addComponent(paneTransform);
        addEntity(backgroundPane);

        Entity textGraphSeparator = new Entity("TextGraphSeparator");
        withTexture(modelProvider.getLine(), new Texture2D(Color.WHITE)).build(textGraphSeparator);
        Transformable2D separatorTransform = new Transform2D();
        separatorTransform.translate(0.5f, 0, TransformSpace.WORLD);
        separatorTransform.scale(0.5f, 1f);
        textGraphSeparator.addComponent(separatorTransform);
        addEntity(textGraphSeparator);
    }
}
