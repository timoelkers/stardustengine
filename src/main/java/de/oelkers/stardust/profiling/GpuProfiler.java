package de.oelkers.stardust.profiling;

import de.oelkers.stardust.utils.NonThrowingAutoCloseable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.regex.Pattern;

public class GpuProfiler implements NonThrowingAutoCloseable {

    private static final Logger LOGGER = LogManager.getLogger(GpuProfiler.class);

    private final boolean isSupported = checkIfSystemIsSupported();
    private Process profilingProcess;
    private BufferedReader processReader;

    public boolean isSupported() {
        return isSupported;
    }

    public GpuProfiler start() {
        if (isSupported) {
            profilingProcess = startIntelGpuProfiling();
            if (profilingProcess != null) {
                processReader = new BufferedReader(new InputStreamReader(profilingProcess.getInputStream()));
            }
        }
        return this;
    }

    /**
     * Queries the profiling process for the gpu statistics since the last run. This blocks until new results
     * are available, which depends on the report rate of the users graphics driver (generally not more than 1 second).
     * This returns {@link GpuProfilingResult#EMPTY} if gpu profiling is not supported or the output could not be parsed.
     */
    public GpuProfilingResult get() {
        if (processReader != null) {
            try {
                String nextSample = processReader.readLine();
                return nextSample.startsWith("#") ? GpuProfilingResult.EMPTY : new GpuProfilingResult(nextSample);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return GpuProfilingResult.EMPTY;
    }

    @Override
    public void close() {
        if (profilingProcess != null) {
            profilingProcess.destroy();
            try {
                processReader.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static final class GpuProfilingResult {

        public static final GpuProfilingResult EMPTY = new GpuProfilingResult();
        private static final Pattern WHITESPACE = Pattern.compile("\\s+");

        private final double timePassedSeconds;
        private final int gpuUsagePercent;
        private final BigInteger verticesFetched;
        private final BigInteger primitivesFetched;
        private final BigInteger vertexShaderInvocations;
        private final BigInteger geometryShaderInvocations;
        private final BigInteger fragmentShaderInvocations;

        private GpuProfilingResult() {
            timePassedSeconds = 0;
            gpuUsagePercent = 0;
            verticesFetched = BigInteger.ZERO;
            primitivesFetched = BigInteger.ZERO;
            vertexShaderInvocations = BigInteger.ZERO;
            geometryShaderInvocations = BigInteger.ZERO;
            fragmentShaderInvocations = BigInteger.ZERO;
        }

        private GpuProfilingResult(CharSequence line) {
            String[] tokens = WHITESPACE.split(line);
            timePassedSeconds = Double.parseDouble(tokens[0]);
            gpuUsagePercent = Integer.parseInt(tokens[1]);
            verticesFetched = new BigInteger(tokens[9]);
            primitivesFetched = new BigInteger(tokens[10]);
            vertexShaderInvocations = new BigInteger(tokens[11]);
            geometryShaderInvocations = new BigInteger(tokens[12]);
            fragmentShaderInvocations = new BigInteger(tokens[16]);
        }

        public double getTimePassedSeconds() {
            return timePassedSeconds;
        }

        public int getGpuUsagePercent() {
            return gpuUsagePercent;
        }

        public BigInteger getVerticesFetched() {
            return verticesFetched;
        }

        public BigInteger getPrimitivesFetched() {
            return primitivesFetched;
        }

        public BigInteger getVertexShaderInvocations() {
            return vertexShaderInvocations;
        }

        public BigInteger getGeometryShaderInvocations() {
            return geometryShaderInvocations;
        }

        public BigInteger getFragmentShaderInvocations() {
            return fragmentShaderInvocations;
        }
    }

    private static boolean checkIfSystemIsSupported() {
        try {
            if (System.getProperty("os.name").contains("Linux")) {
                if (!hasIntelGpuTools()) {
                    LOGGER.warn("Did not find the \"intel-gpu-tools\" package on you system, unable to support GPU profiling!");
                    return false;
                }
                return true;
            }
            LOGGER.warn("GPU profiling is currently not supported on " + System.getProperty("os.name"));
            return false;
        } catch (IOException | InterruptedException e) {
            LOGGER.warn("Error during setup of the GPU profiling, unable to support it!", e);
            return false;
        }
    }

    private static boolean hasIntelGpuTools() throws InterruptedException, IOException {
        Process process = new ProcessBuilder("/bin/sh", "-c", "dpkg -l | grep \"intel-gpu-tools\"").start();
        process.waitFor();
        CharSequence result = new String(process.getInputStream().readAllBytes());
        return !result.isEmpty();
    }

    private static Process startIntelGpuProfiling() {
        try {
            return new ProcessBuilder("sudo", "intel_gpu_top", "-o", "-").start();
        } catch (IOException e) {
            LOGGER.warn("Error during the start of the profiling process!", e);
        }
        return null;
    }
}