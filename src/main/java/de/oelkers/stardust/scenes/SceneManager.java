package de.oelkers.stardust.scenes;

import de.oelkers.stardust.config.ConfigStore;
import de.oelkers.stardust.config.LoadingStrategy;
import de.oelkers.stardust.entities.IdentifiedObject;
import de.oelkers.stardust.graphics.rendering.RenderSystem;
import de.oelkers.stardust.utils.NonThrowingAutoCloseable;

import java.util.HashSet;
import java.util.Set;

public class SceneManager implements NonThrowingAutoCloseable {

    private final Set<Scene> scenes = new HashSet<>();
    private Scene currentScene;

    public void addScene(Scene scene) {
        if (!scene.isClosed() && scenes.add(scene)) {
            if (ConfigStore.get().getLoadingStrategy() == LoadingStrategy.EAGER) {
                loadSceneIfNeeded(scene);
            }
        }
    }

    public void setCurrentScene(Scene scene) {
        if (!scene.equals(currentScene) && !scene.isClosed()) {
            if (currentScene != null) {
                currentScene.onSceneLeave();
            }
            loadSceneIfNeeded(scene);
            currentScene = scene;
            scene.onSceneEnter();
        }
    }

    public void update(double frameTimeSeconds) {
        if (currentScene != null) {
            currentScene.update(frameTimeSeconds);
        }
    }

    public void render(RenderSystem renderSystem) {
        if (currentScene != null) {
            currentScene.render(renderSystem);
        }
    }

    public void close(Scene scene) {
        close(scene, true);
    }

    @Override
    public void close() {
        for (Scene scene : scenes) {
            close(scene, false);
        }
        scenes.clear();
    }

    private void close(Scene scene, boolean remove) {
        if (scene.isLoaded() && !scene.isClosed()) {
            if (scene.equals(currentScene)) {
                scene.onSceneLeave();
                currentScene = null;
            }
            scene.close();
            scene.isClosed = true;
            if (remove) {
                scenes.remove(scene);
            }
        }
    }

    private static void loadSceneIfNeeded(Scene scene) {
        if (!scene.isLoaded()) {
            scene.onSceneLoad();
            scene.isLoaded = true;
        }
    }

    public static abstract class Scene extends IdentifiedObject implements NonThrowingAutoCloseable {

        private boolean isLoaded, isClosed;

        public final boolean isLoaded() {
            return isLoaded;
        }

        public final boolean isClosed() {
            return isClosed;
        }

        protected abstract void onSceneLoad();
        protected abstract void onSceneEnter();
        protected abstract void onSceneLeave();
        protected abstract void update(double frameTimeSeconds);
        protected abstract void render(RenderSystem renderSystem);
    }
}
