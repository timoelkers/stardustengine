package de.oelkers.stardust.entities;

import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static java.util.Collections.emptyIterator;

public abstract class BaseEntitySystem<T extends Collection<Entity>> implements EntitySystem {

    public static final String GLOBAL_GROUP = "GLOBAL_GROUP";

    private final Map<String, T> entities = new HashMap<>();
    private final Collection<EntitySystem> subSystems = new ArrayList<>();
    private final Supplier<T> storageSupplier;

    protected BaseEntitySystem(Supplier<T> storageSupplier) {
        this.storageSupplier = storageSupplier;
    }

    public T getEntities(String entityGroup) {
        return entities.getOrDefault(entityGroup, storageSupplier.get());
    }

    @Override
    public boolean addEntity(Entity entity) {
        return addEntity(entity, GLOBAL_GROUP);
    }

    @Override
    public boolean addEntity(Entity entity, String entityGroup) {
        if (acceptsEntity(entity)) {
            boolean added = entities.computeIfAbsent(entityGroup, t -> storageSupplier.get()).add(entity);
            for (EntitySystem subSystem : subSystems) {
                subSystem.addEntity(entity, entityGroup);
            }
            return added;
        }
        return false;
    }

    @Override
    public Optional<Entity> getEntity(String name) {
        return getEntity(name, GLOBAL_GROUP);
    }

    @Override
    public Optional<Entity> getEntity(String name, String entityGroup) {
        Collection<Entity> entitySet = entities.get(entityGroup);
        for (Entity entity : entitySet) {
            if (entity.getName().equals(name)) {
                return Optional.of(entity);
            }
        }
        return Optional.empty();
    }

    @Override
    public void removeEntity(Entity entity) {
        for (Collection<Entity> entitySet : entities.values()) {
            entitySet.remove(entity);
        }
        for (EntitySystem subSystem : subSystems) {
            subSystem.removeEntity(entity);
        }
    }

    @NotNull
    @Override
    public Iterator<Entity> iterator() {
        Collection<Entity> entitySet = entities.get(GLOBAL_GROUP);
        return entitySet == null ? emptyIterator() : entitySet.iterator();
    }

    @Override
    public void forEach(Consumer<? super Entity> action) {
        forEach(action, GLOBAL_GROUP);
    }

    @Override
    public void forEach(Consumer<? super Entity> consumer, String entityGroup) {
        forEach(consumer, entityGroup, e -> true);
    }

    @Override
    public void forEach(Consumer<? super Entity> consumer, Predicate<Entity> filter) {
        forEach(consumer, GLOBAL_GROUP, filter);
    }

    @Override
    public void forEach(Consumer<? super Entity> consumer, String entityGroup, Predicate<Entity> filter) {
        Collection<Entity> entitySet = entities.get(entityGroup);
        if (entitySet != null) {
            for (Entity entity : entitySet) {
                if (filter.test(entity)) {
                    consumer.accept(entity);
                }
            }
        }
    }

    @Override
    public int getSize() {
        return getSize(GLOBAL_GROUP);
    }

    @Override
    public int getSize(String entityGroup) {
        Collection<Entity> entitySet = entities.get(entityGroup);
        return Optional.ofNullable(entitySet).map(Collection::size).orElse(0);
    }

    @Override
    public int getTotalSize() {
        return entities.values().stream().mapToInt(Collection::size).sum();
    }

    @Override
    public void registerSubsystem(EntitySystem entitySystem) {
        subSystems.add(entitySystem);
    }

    @Override
    public void removeSubsystem(EntitySystem entitySystem) {
        subSystems.remove(entitySystem);
    }
}
