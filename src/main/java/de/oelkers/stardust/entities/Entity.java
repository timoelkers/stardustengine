package de.oelkers.stardust.entities;

import org.jetbrains.annotations.Nullable;

import java.io.Serial;
import java.io.Serializable;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Entity extends IdentifiedObject implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private final Collection<Component> componentsAdded = new HashSet<>();
    private final Map<Class<?>, String> lockedComponents = new HashMap<>();
    private final Map<Class<?>, List<Component>> components = new HashMap<>();
    private final String name;

    public Entity(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Entity addComponent(Component component) {
        if (componentsAdded.add(component)) {
            consumeComponentTypes(component.getClass(), type -> {
                components.computeIfAbsent(type, t -> new ArrayList<>()).add(component);
            });
        }
        return this;
    }

    public Entity removeComponent(Component component) {
        getLockOwner(component.getClass()).ifPresent(owner -> {
            throw new IllegalStateException(component + " is currently locked by " + owner + ", you can not remove it!");
        });
        if (componentsAdded.remove(component)) {
            consumeComponentTypes(component.getClass(), type -> {
                List<Component> subList = components.get(type);
                if (subList.size() == 1) {
                    components.remove(type);
                } else {
                    subList.remove(component);
                }
            });
        }
        return this;
    }

    public boolean hasComponent(Class<? extends Component> componentType) {
        return components.containsKey(componentType);
    }

    @SafeVarargs
    public final boolean hasComponents(Class<? extends Component>... componentTypes) {
        return Arrays.stream(componentTypes).allMatch(this::hasComponent);
    }

    public <T extends Component> Optional<T> getComponent(Class<T> componentType) {
        return Optional.ofNullable(getComponentOrNull(componentType));
    }

    @Nullable
    @SuppressWarnings("unchecked")
    public <T extends Component> T getComponentOrNull(Class<T> componentType) {
        List<Component> components = this.components.get(componentType);
        if (components == null) {
            return null;
        }
        return (T) components.get(0);
    }

    public int getComponentCount() {
        return componentsAdded.size();
    }

    @SafeVarargs
    public final Entity lockComponents(Class<? extends Component>... componentTypes) {
        for (Class<? extends Component> type : componentTypes) {
            lockComponent(type);
        }
        return this;
    }

    /**
     * Locks the given component to prevent it from being removed. This is useful if a system absolutely needs a component to be present
     * while processing the entity. Removing an entity from such a system should always result in the component being unlocked.
     * The lock can only be unlocked by the calling class. Locking an already locked component leads an exception being thrown.
     *
     * @param componentType the type of the component to lock
     */
    public Entity lockComponent(Class<? extends Component> componentType) {
        getLockOwner(componentType).ifPresent(owner -> {
            throw new IllegalStateException(componentType.getSimpleName() + " is already locked by " + owner + ", you can not lock it!");
        });
        String callingClassName = Thread.currentThread().getStackTrace()[2].getClassName();
        lockedComponents.put(componentType, callingClassName);
        return this;
    }

    @SafeVarargs
    public final Entity unlockComponents(Class<? extends Component>... componentTypes) {
        for (Class<? extends Component> type : componentTypes) {
            unlockComponent(type);
        }
        return this;
    }

    public Entity unlockComponent(Class<? extends Component> componentType) {
        String callingClassName = Thread.currentThread().getStackTrace()[2].getClassName();
        getLockOwner(componentType).ifPresent(owner -> {
            if (!callingClassName.equals(owner)) {
                throw new IllegalStateException(componentType.getSimpleName() + " was locked by " + owner + ", you can not unlock it!");
            }
        });
        lockedComponents.remove(componentType);
        return this;
    }

    public boolean isComponentLocked(Class<? extends Component> componentType) {
        return getLockOwner(componentType).isPresent();
    }

    public Optional<String> getLockOwner(Class<? extends Component> componentType) {
        return Optional.ofNullable(getFirstComponentType(componentType, lockedComponents::containsKey)).map(lockedComponents::get);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '[' + name + ']';
    }

    private static void consumeComponentTypes(Class<?> componentType, Consumer<Class<?>> action) {
        action.accept(componentType);
        for (Class<?> interfaceType : componentType.getInterfaces()) {
            if (interfaceType != Component.class) {
                consumeComponentTypes(interfaceType, action);
            }
        }
        componentType = componentType.getSuperclass();
        if (componentType != null && componentType != Object.class) {
            consumeComponentTypes(componentType, action);
        }
    }

    private static Class<?> getFirstComponentType(Class<?> componentType, Predicate<Class<?>> predicate) {
        if (predicate.test(componentType)) {
            return componentType;
        }
        for (Class<?> interfaceType : componentType.getInterfaces()) {
            if (interfaceType != Component.class) {
                return getFirstComponentType(interfaceType, predicate);
            }
        }
        componentType = componentType.getSuperclass();
        if (componentType != null && componentType != Object.class) {
            return getFirstComponentType(componentType, predicate);
        }
        return null;
    }
}
