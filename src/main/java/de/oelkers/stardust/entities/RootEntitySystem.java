package de.oelkers.stardust.entities;

import java.util.HashSet;
import java.util.Set;

public final class RootEntitySystem extends BaseEntitySystem<Set<Entity>> {

    private static final EntitySystem INSTANCE = new RootEntitySystem();

    public static EntitySystem get() {
        return INSTANCE;
    }

    private RootEntitySystem() {
        super(HashSet::new);
    }

    @Override
    public boolean acceptsEntity(Entity entity) {
        return true;
    }
}
