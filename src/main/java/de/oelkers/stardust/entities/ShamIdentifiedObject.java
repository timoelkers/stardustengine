package de.oelkers.stardust.entities;

import de.oelkers.stardust.utils.Lazy;

public abstract class ShamIdentifiedObject extends IdentifiedObject {

    private final Lazy<Integer> id;

    protected ShamIdentifiedObject(int id) {
        this.id = new Lazy<>(id);
    }

    protected ShamIdentifiedObject(Lazy<Integer> id) {
        this.id = id;
    }

    @Override
    public final int getId() {
        return id.get();
    }
}
