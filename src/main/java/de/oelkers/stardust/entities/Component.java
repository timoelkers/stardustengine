package de.oelkers.stardust.entities;

import java.io.Serializable;

public interface Component extends Serializable {}
