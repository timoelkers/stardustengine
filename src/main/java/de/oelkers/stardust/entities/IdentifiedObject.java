package de.oelkers.stardust.entities;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.atomic.AtomicInteger;

public abstract class IdentifiedObject implements Comparable<IdentifiedObject> {

    private static final AtomicInteger ID_COUNTER = new AtomicInteger(0);

    private final int id;

    protected IdentifiedObject() {
        id = ID_COUNTER.getAndIncrement();
    }

    /**
     * Returns the id of the object. Although each object has its own unique id, implementations
     * may choose to expose a different id instead. This id is never used when comparing objects or identifying
     * uniqueness (e.g. in {@link #hashCode()}, {@link #equals(Object)} and {@link #compareTo(IdentifiedObject)}).
     */
    public int getId() {
        return id;
    }

    @Override
    public final int compareTo(@NotNull IdentifiedObject o) {
        return Integer.compare(id, o.id);
    }

    @Override
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        IdentifiedObject other = (IdentifiedObject) obj;
        return id == other.id;
    }

    @Override
    public final int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '[' + getId() + ']';
    }
}
