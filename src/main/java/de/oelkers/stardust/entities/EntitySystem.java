package de.oelkers.stardust.entities;

import org.jetbrains.annotations.NotNull;

import java.util.Iterator;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;

public interface EntitySystem extends Iterable<Entity> {

    /**
     * Adds the entity to this system under the global entity group. The entity is rejected
     * if the entity is not accepted by the system or if it is already present in the global group.
     * The entity is also passed to subsystems, which also accept the entity. Returns true if the
     * entity was not rejected by this system.
     *
     * @param entity the entity you want to add
     */
    boolean addEntity(Entity entity);

    /**
     * Adds the entity to this system under the specified entity group. The entity is rejected
     * if the entity is not accepted by the system or if it is already present in the specified entity group.
     * The entity is also passed to subsystems, which also accept the entity. Returns true if the
     * entity was not rejected by this system.
     *
     * @param entity the entity you want to add
     * @param entityGroup the entity group the entity is under
     */
    boolean addEntity(Entity entity, String entityGroup);

    /**
     * Returns the first entity with the given name in this system in the global entity group.
     *
     * @param name the name of the entity
     */
    Optional<Entity> getEntity(String name);

    /**
     * Returns the first entity with the given name in this system in the given entity group.
     *
     * @param name the name of the entity
     * @param entityGroup the entity group you want to search in
     */
    Optional<Entity> getEntity(String name, String entityGroup);

    /**
     * Removes the entity from this system. This will remove it from every entity group it is in.
     * The entity is also removed from every subsystem.
     *
     * @param entity the entity you want to remove
     */
    void removeEntity(Entity entity);

    /**
     * Returns an iterator over all entities in the global entity group.
     * The iterator has no specified order and does not contain entities from any subsystem.
     */
    @NotNull
    @Override
    Iterator<Entity> iterator();

    /**
     * Iterates over all entities in the global entity group and performs the given
     * action on each of them. The order of iteration is unspecified and entities
     * in subsystems are not taken into account.
     *
     * @param action the action you want to perform on each entity
     */
    @Override
    void forEach(Consumer<? super Entity> action);

    /**
     * Iterates over all entities in the specified entity group and
     * performs the given action on each of them. The order of iteration
     * is unspecified and entities in subsystems are not taken into account.
     *
     * @param consumer the action you want to perform on each entity
     * @param entityGroup the entity group you want to iterate over
     */
    void forEach(Consumer<? super Entity> consumer, String entityGroup);

    /**
     * Iterates over all entities in the global entity group and
     * performs the given action on each entity which passes the given filter.
     * The order of iteration is unspecified and entities in subsystems are not taken into account.
     *
     * @param consumer the action you want to perform on entity
     * @param filter the filter which should be applied to each entity
     */
    void forEach(Consumer<? super Entity> consumer, Predicate<Entity> filter);

    /**
     * Iterates over all entities in the specified entity group and
     * performs the given action on each entity which passes the given filter.
     * The order of iteration is unspecified and entities in subsystems are not taken into account.
     *
     * @param consumer the action you want to perform on entity
     * @param entityGroup the entity group you want to iterate over
     * @param filter the filter which should be applied to each entity
     */
    void forEach(Consumer<? super Entity> consumer, String entityGroup, Predicate<Entity> filter);

    /**
     * Returns the number of entities in this system under the global entity group.
     * Entities in subsystems are not taken into account.
     */
    int getSize();

    /**
     * Returns the number of entities in this system under the specified entity group.
     * Entities in subsystems are not taken into account.
     *
     * @param entityGroup the entity group you want to check the size of
     */
    int getSize(String entityGroup);

    /**
     * Returns the total number of entities in this system across all entity groups.
     * Entities in subsystems are not taken into account.
     */
    int getTotalSize();

    /**
     * Returns whether the given entity is accepted and can be processed by this system.
     *
     * @param entity the entity you want to check
     */
    boolean acceptsEntity(Entity entity);

    void registerSubsystem(EntitySystem entitySystem);
    void removeSubsystem(EntitySystem entitySystem);
}
