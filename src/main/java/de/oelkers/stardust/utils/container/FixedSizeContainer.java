package de.oelkers.stardust.utils.container;

public class FixedSizeContainer<E> implements Container<E> {

    private final int size;
    private E[] elements;
    private E element;

    public FixedSizeContainer(int size) {
        this.size = size;
    }

    @Override
    public void set(E element) throws RestrictionNotMetException {
        checkSizeOnFilling(1);
        this.element = element;
    }

    @Override
    public void set(E[] elements) throws RestrictionNotMetException {
        checkSizeOnFilling(elements.length);
        if (elements.length == 1) {
            element = elements[0];
        } else {
            this.elements = elements;
        }
    }

    @Override
    public E[] getMultiple() throws ExpectationNotMetException {
        checkSizeOnReading(false);
        return elements;
    }

    @Override
    public E getOnly() throws ExpectationNotMetException {
        checkSizeOnReading(true);
        return element;
    }

    @Override
    public int getSize() {
        return size;
    }

    private void checkSizeOnFilling(int actual) {
        if (actual != size) {
            throw new RestrictionNotMetException("This container requires " + size + " elements, you provided " + actual);
        }
    }

    private void checkSizeOnReading(boolean singleElement) {
        if (singleElement && size != 1) {
            throw new ExpectationNotMetException("This container contains multiple elements, you requested a single one!");
        }
        if (!singleElement && size == 1) {
            throw new ExpectationNotMetException("This container contains a single element, you requested multiple ones!");
        }
    }
}
