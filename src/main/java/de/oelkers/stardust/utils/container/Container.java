package de.oelkers.stardust.utils.container;

import java.io.Serial;

/**
 * A container represents, somewhat contrary to a {@link java.util.Collection}, a data structure
 * that is created by the receiver of some sort of data to be populated by a producer. It allows
 * the receiver to apply certain restrictions when the data is filled. It also helps when dealing
 * with implementations, that take either one or many (possibly generic) elements without having to
 * deal with generic arrays when only one element is expected.
 *
 * @param <E> the type of data in this container
 */
public interface Container<E> {

    void set(E element) throws RestrictionNotMetException;
    void set(E[] elements) throws RestrictionNotMetException;

    /**
     * Returns the elements in this container or null if the container has not been populated yet. This
     * throws an {@link ExpectationNotMetException} if this container only holds a single element. You can check
     * {@link #getSize()} before calling this if your implementation allows one or multiple elements.
     */
    E[] getMultiple() throws ExpectationNotMetException;

    /**
     * Returns the element in this container or null if the container has not been populated yet. This
     * throws {@link ExpectationNotMetException} exception if this container holds multiple elements. You can check
     * {@link #getSize()} before calling this if you implementation allows one or multiple elements.
     */
    E getOnly() throws ExpectationNotMetException;
    int getSize();

    class RestrictionNotMetException extends RuntimeException {

        @Serial
        private static final long serialVersionUID = 1L;

        public RestrictionNotMetException(String message) {
            super(message);
        }
    }

    class ExpectationNotMetException extends RuntimeException {

        @Serial
        private static final long serialVersionUID = 1L;

        public ExpectationNotMetException(String message) {
            super(message);
        }
    }
}