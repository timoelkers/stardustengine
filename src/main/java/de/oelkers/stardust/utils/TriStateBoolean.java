package de.oelkers.stardust.utils;

public enum TriStateBoolean {

    TRUE, FALSE, UNDEFINED;

    public static TriStateBoolean of(boolean value) {
        return value ? TRUE : FALSE;
    }
}
