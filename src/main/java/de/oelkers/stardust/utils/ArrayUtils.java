package de.oelkers.stardust.utils;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.function.Supplier;

public final class ArrayUtils {

    private ArrayUtils() {}

    public static <T> T[] append(T[] array, T element) {
        array = Arrays.copyOf(array, array.length + 1);
        array[array.length - 1] = element;
        return array;
    }

    public static <T> T[] prepend(T[] array, T element) {
        array = Arrays.copyOf(array, array.length + 1);
        System.arraycopy(array, 0, array, 1, array.length - 1);
        array[0] = element;
        return array;
    }

    /**
     * Flattens the given 2 dimensional array by adding each row to a 1 dimensional array.
     * The order of the elements stays the same. Contrary to {@link #flatten(Object[][])}, each
     * row must have the same given length to be able to determine the flattened arrays length.
     *
     * @param array the array you want to flatten
     * @param individualSize the size of each individual row
     * @param <T> the type of the elements in the array
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] flatten(T[][] array, int individualSize) {
        T[] flattened = (T[]) Array.newInstance(array[0][0].getClass(), individualSize * array.length);
        for (int i = 0; i < array.length; i++) {
            System.arraycopy(array[i], 0, flattened, i * individualSize, individualSize);
        }
        return flattened;
    }

    /**
     * Flattens the given 2 dimensional array by adding each row to a 1 dimensional array.
     * The order of the elements stays the same. Contrary to {@link #flatten(Object[][], int)}, there
     * is no restriction on the length of each individual row, but an extra iteration is used
     * to determine the flattened arrays length.
     *
     * @param array the array you want to flatten
     * @param <T> the type of the elements in the array
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] flatten(T[][] array) {
        int totalLength = Arrays.stream(array).mapToInt(Array::getLength).sum();
        T[] flattened = (T[]) Array.newInstance(array[0][0].getClass(), totalLength);
        int arrayIndex = 0;
        for (T[] row : array) {
            System.arraycopy(row, 0, flattened, arrayIndex, row.length);
            arrayIndex += row.length;
        }
        return flattened;
    }

    public static <T> T[] trim(T[] array) {
        for (int i = array.length - 1; i >= 0; i--) {
            if (array[i] != null) {
                return Arrays.copyOf(array, i);
            }
        }
        return Arrays.copyOf(array, 0);
    }

    public static <T> T[] setAll(Supplier<T> supplier, T[] array) {
        Arrays.setAll(array, i -> supplier.get());
        return array;
    }

    public static <T> T[] swap(T[] array, int index1, int index2) {
        T tmp = array[index1];
        array[index1] = array[index2];
        array[index2] = tmp;
        return array;
    }
}
