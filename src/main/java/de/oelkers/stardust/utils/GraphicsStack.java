package de.oelkers.stardust.utils;

import de.oelkers.stardust.graphics.assets.loading.FileAssetLoader;
import de.oelkers.stardust.graphics.assets.models.ModelProvider;
import de.oelkers.stardust.graphics.rendering.RenderSystem;
import de.oelkers.stardust.scenes.SceneManager;
import de.oelkers.stardust.window.StardustWindow;

public class GraphicsStack {

    private StardustWindow window;
    private RenderSystem renderSystem;
    private ModelProvider modelProvider;
    private FileAssetLoader assetLoader;
    private SceneManager sceneManager;

    public StardustWindow getWindow() {
        return window;
    }

    public StardustWindow setWindow(StardustWindow window) {
        return this.window = window;
    }

    public RenderSystem getRenderSystem() {
        return renderSystem;
    }

    public RenderSystem setRenderSystem(RenderSystem renderSystem) {
        return this.renderSystem = renderSystem;
    }

    public ModelProvider getModelProvider() {
        return modelProvider;
    }

    public ModelProvider setModelProvider(ModelProvider modelProvider) {
        return this.modelProvider = modelProvider;
    }

    public FileAssetLoader getAssetLoader() {
        return assetLoader;
    }

    public FileAssetLoader setAssetLoader(FileAssetLoader assetLoader) {
        return this.assetLoader = assetLoader;
    }

    public SceneManager getSceneManager() {
        return sceneManager;
    }

    public SceneManager setSceneManager(SceneManager sceneManager) {
        return this.sceneManager = sceneManager;
    }
}
