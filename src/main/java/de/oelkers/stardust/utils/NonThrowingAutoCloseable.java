package de.oelkers.stardust.utils;

public interface NonThrowingAutoCloseable extends AutoCloseable {

    @Override
    void close();
}