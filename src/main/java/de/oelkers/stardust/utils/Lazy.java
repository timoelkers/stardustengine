package de.oelkers.stardust.utils;

import net.jcip.annotations.ThreadSafe;

import java.util.NoSuchElementException;
import java.util.function.Consumer;
import java.util.function.Supplier;

@ThreadSafe
public class Lazy<T> implements Supplier<T> {

    private volatile boolean isLoaded;
    private volatile Supplier<T> current;

    public Lazy(Supplier<T> delegate) {
        current = () -> swap(delegate);
    }

    public Lazy(T value) {
        current = () -> value;
        isLoaded = true;
    }

    public boolean isLoaded() {
        return isLoaded;
    }

    @Override
    public T get() {
        return current.get();
    }

    public T orElse(T other) {
        return isLoaded ? current.get() : other;
    }

    public T orElseGet(Supplier<T> supplier) {
        return (isLoaded ? current : supplier).get();
    }

    public T orElseThrow() {
        if (isLoaded) {
            return get();
        }
        throw new NoSuchElementException("Lazy loaded value not loaded yet!");
    }

    public <X extends Throwable> T orElseThrow(Supplier<X> exceptionSupplier) throws X {
        if (isLoaded) {
            return get();
        }
        throw exceptionSupplier.get();
    }

    public void ifLoaded(Consumer<T> action) {
        if (isLoaded) {
            action.accept(get());
        }
    }

    private synchronized T swap(Supplier<T> delegate) {
        if (isLoaded) {
            return current.get();
        }
        T obj = delegate.get();
        current = () -> obj;
        isLoaded = true;
        return obj;
    }
}
