package de.oelkers.stardust.utils.buffers;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.function.Supplier;

public final class BufferUtils {

    private BufferUtils() {}

    /**
     * Returns either the backed array if the buffer is a heap buffer or allocates a new array
     * and puts the buffers content into it if it is a direct (non-heap) buffer.
     */
    public static byte[] toArray(ByteBuffer data) {
        if (data.hasArray()) {
            return data.array();
        }
        byte[] result = new byte[data.remaining()];
        data.get(result);
        return result;
    }

    public static <T extends BufferElement<T>> T[] load(ByteBuffer data, T[] array, Supplier<T> factory) {
        for (int i = 0; i < array.length; i++) {
            T bufferElement = factory.get();
            bufferElement.load(data);
            array[i] = bufferElement;
        }
        return array;
    }

    /**
     * Stores all given elements in a direct {@link ByteBuffer} with native byte order
     * and returns it ready for reading. This does not require all elements to have the
     * same size, but an additional loop over all elements to determine the final size
     * is needed. You should prefer {@link #storeUnsafe(BufferElement[])} if you know
     * for sure that all elements have the same size.
     *
     * @param elements the elements you want to store
     */
    public static ByteBuffer store(BufferElement<?>[] elements) {
        int totalSize = Arrays.stream(elements).mapToInt(element -> (int) element.getByteSize().getBytes()).sum();
        ByteBuffer buffer = ByteBuffer.allocateDirect(totalSize).order(ByteOrder.nativeOrder());
        for (BufferElement<?> element : elements) {
            element.store(buffer);
        }
        return buffer.flip();
    }

    /**
     * Stores all given elements in a direct {@link ByteBuffer} with native byte order
     * and returns it ready for reading. This requires that all elements have the same byte size.
     * A buffer over- or underflow may happen if that is not the case.
     * You should prefer {@link #store(BufferElement[])} if you do not know if all sizes are equal.
     *
     * @param elements the elements you want to store
     */
    public static ByteBuffer storeUnsafe(BufferElement<?>[] elements) {
        int size = (int) elements[0].getByteSize().getBytes() * elements.length;
        ByteBuffer buffer = ByteBuffer.allocateDirect(size).order(ByteOrder.nativeOrder());
        for (BufferElement<?> element : elements) {
            element.store(buffer);
        }
        return buffer.flip();
    }

    /**
     * Stores all given elements in a direct {@link ByteBuffer} with native byte order
     * and returns it ready for reading.
     *
     * @param elements the elements you want to store
     */
    public static ByteBuffer store(byte[] elements) {
        ByteBuffer buffer = ByteBuffer.allocateDirect(elements.length * Byte.BYTES).order(ByteOrder.nativeOrder());
        for (byte element : elements) {
            buffer.put(element);
        }
        return buffer.flip();
    }

    /**
     * Stores all given elements in a direct {@link ByteBuffer} with native byte order
     * and returns it ready for reading.
     *
     * @param elements the elements you want to store
     */
    public static ByteBuffer store(short[] elements) {
        ByteBuffer buffer = ByteBuffer.allocateDirect(elements.length * Short.BYTES).order(ByteOrder.nativeOrder());
        for (short element : elements) {
            buffer.putShort(element);
        }
        return buffer.flip();
    }

    /**
     * Stores all given elements in a direct {@link ByteBuffer} with native byte order
     * and returns it ready for reading.
     *
     * @param elements the elements you want to store
     */
    public static ByteBuffer store(char[] elements) {
        ByteBuffer buffer = ByteBuffer.allocateDirect(elements.length * Character.BYTES).order(ByteOrder.nativeOrder());
        for (char element : elements) {
            buffer.putChar(element);
        }
        return buffer.flip();
    }

    /**
     * Stores all given elements in a direct {@link ByteBuffer} with native byte order
     * and returns it ready for reading.
     *
     * @param elements the elements you want to store
     */
    public static ByteBuffer store(int[] elements) {
        ByteBuffer buffer = ByteBuffer.allocateDirect(elements.length * Integer.BYTES).order(ByteOrder.nativeOrder());
        for (int element : elements) {
            buffer.putInt(element);
        }
        return buffer.flip();
    }

    /**
     * Stores all given elements in a direct {@link ByteBuffer} with native byte order
     * and returns it ready for reading.
     *
     * @param elements the elements you want to store
     */
    public static ByteBuffer store(float[] elements) {
        ByteBuffer buffer = ByteBuffer.allocateDirect(elements.length * Float.BYTES).order(ByteOrder.nativeOrder());
        for (float element : elements) {
            buffer.putFloat(element);
        }
        return buffer.flip();
    }

    /**
     * Stores all given elements in a direct {@link ByteBuffer} with native byte order
     * and returns it ready for reading.
     *
     * @param elements the elements you want to store
     */
    public static ByteBuffer store(double[] elements) {
        ByteBuffer buffer = ByteBuffer.allocateDirect(elements.length * Double.BYTES).order(ByteOrder.nativeOrder());
        for (double element : elements) {
            buffer.putDouble(element);
        }
        return buffer.flip();
    }
}
