package de.oelkers.stardust.utils.buffers;

import de.oelkers.stardust.utils.ByteSize;

import java.nio.ByteBuffer;

public interface BufferElement<T extends BufferElement<T>> {

    ByteSize getByteSize();
    void store(ByteBuffer buffer, int index);
    T load(ByteBuffer buffer);

    default void store(ByteBuffer buffer) {
        int currentPosition = buffer.position();
        store(buffer, currentPosition);
        buffer.position(currentPosition + (int) getByteSize().getBytes());
    }
}
