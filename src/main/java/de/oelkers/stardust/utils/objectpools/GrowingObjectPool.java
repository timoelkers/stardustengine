package de.oelkers.stardust.utils.objectpools;

import de.oelkers.stardust.utils.collections.UniqueDeque;
import net.jcip.annotations.ThreadSafe;

import java.util.function.Supplier;

@ThreadSafe
public class GrowingObjectPool<T> implements ObjectPool<T> {

    private final UniqueDeque<T> availableObjects = new UniqueDeque<>();
    private final Supplier<? extends T> objectCreation;
    private volatile int objectsCreated = 0;

    public GrowingObjectPool(Supplier<? extends T> objectCreation) {
        this.objectCreation = objectCreation;
    }

    @Override
    public T get() {
        synchronized (availableObjects) {
            if (availableObjects.isEmpty()) {
                objectsCreated++;
                return objectCreation.get();
            }
            return availableObjects.poll();
        }
    }

    @Override
    public void free(T object) {
        synchronized (availableObjects) {
            availableObjects.add(object);
        }
    }

    @Override
    public int getFreeSize() {
        return availableObjects.size();
    }

    @Override
    public int getPoolSize() {
        return objectsCreated;
    }
}
