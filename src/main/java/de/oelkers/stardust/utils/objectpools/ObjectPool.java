package de.oelkers.stardust.utils.objectpools;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public interface ObjectPool<T> extends Supplier<T> {

    void free(T object);
    int getFreeSize();
    int getPoolSize();

    default void consume(Consumer<? super T> consumer) {
        T object = get();
        try {
            consumer.accept(object);
        } finally {
            free(object);
        }
    }

    default <R> R apply(Function<T, R> function) {
        T object = get();
        try {
            return function.apply(object);
        } finally {
            free(object);
        }
    }
}
