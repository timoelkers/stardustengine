package de.oelkers.stardust.utils.collections;

import de.oelkers.stardust.utils.iterators.ImmutableIterator;
import net.jcip.annotations.NotThreadSafe;
import org.jetbrains.annotations.NotNull;

import java.io.Serial;
import java.io.Serializable;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.stream.Stream;

@NotThreadSafe
public class UniqueDeque<T> implements Deque<T>, Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private final ArrayDeque<T> elements = new ArrayDeque<>();
    private final HashSet<T> uniqueness = new HashSet<>();

    @Override
    public void addFirst(T e) {
        if (uniqueness.add(e)) {
            elements.addFirst(e);
        }
    }

    @Override
    public void addLast(T e) {
        if (uniqueness.add(e)) {
            elements.addLast(e);
        }
    }

    @Override
    public boolean offerFirst(T e) {
        return uniqueness.add(e) && elements.offerFirst(e);
    }

    @Override
    public boolean offerLast(T e) {
        return uniqueness.add(e) && elements.offerLast(e);
    }

    @Override
    public T removeFirst() {
        T element = elements.removeFirst();
        uniqueness.remove(element);
        return element;
    }

    @Override
    public T removeLast() {
        T element = elements.removeLast();
        uniqueness.remove(element);
        return element;
    }

    @Override
    public T pollFirst() {
        T element = elements.pollFirst();
        uniqueness.remove(element);
        return element;
    }

    @Override
    public T pollLast() {
        T element = elements.pollLast();
        uniqueness.remove(element);
        return element;
    }

    @Override
    public T getFirst() {
        return elements.getFirst();
    }

    @Override
    public T getLast() {
        return elements.getLast();
    }

    @Override
    public T peekFirst() {
        return elements.peekFirst();
    }

    @Override
    public T peekLast() {
        return elements.peekLast();
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        return remove(o);
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        return remove(o);
    }

    @Override
    public boolean add(T e) {
        return uniqueness.add(e) && elements.add(e);
    }

    @Override
    public boolean offer(T e) {
        return uniqueness.add(e) && elements.offer(e);
    }

    @Override
    public T remove() {
        T element = elements.remove();
        uniqueness.remove(element);
        return element;
    }

    @Override
    public T poll() {
        T element = elements.poll();
        uniqueness.remove(element);
        return element;
    }

    @Override
    public T element() {
        return elements.element();
    }

    @Override
    public T peek() {
        return elements.peek();
    }

    @Override
    public boolean addAll(@NotNull Collection<? extends T> c) {
        return c.stream().map(this::add).reduce(false, (a, b) -> a || b);
    }

    @Override
    public void push(T e) {
        if (uniqueness.add(e)) {
            elements.push(e);
        }
    }

    @Override
    public T pop() {
        T element = elements.pop();
        uniqueness.remove(element);
        return element;
    }

    @Override
    public boolean remove(Object o) {
        elements.remove(o);
        return uniqueness.remove(o);
    }

    @Override
    public boolean contains(Object o) {
        return uniqueness.contains(o);
    }

    @Override
    public int size() {
        return elements.size();
    }

    @NotNull
    @Override
    public Iterator<T> iterator() {
        return new ImmutableIterator<>(elements.iterator());
    }

    @NotNull
    @Override
    public Iterator<T> descendingIterator() {
        return new ImmutableIterator<>(elements.descendingIterator());
    }

    @Override
    public boolean isEmpty() {
        return elements.isEmpty();
    }

    @NotNull
    @Override
    public Object[] toArray() {
        return elements.toArray();
    }

    @SuppressWarnings("SuspiciousToArrayCall")
    @NotNull
    @Override
    public <E> E[] toArray(@NotNull E[] a) {
        return elements.toArray(a);
    }

    @Override
    public <T1> T1[] toArray(IntFunction<T1[]> generator) {
        return elements.toArray(generator);
    }

    @Override
    public boolean containsAll(@NotNull Collection<?> c) {
        return uniqueness.containsAll(c);
    }

    @Override
    public boolean removeAll(@NotNull Collection<?> c) {
        elements.removeAll(c);
        return uniqueness.removeAll(c);
    }

    @Override
    public boolean removeIf(Predicate<? super T> filter) {
        elements.removeIf(filter);
        return uniqueness.removeIf(filter);
    }

    @Override
    public boolean retainAll(@NotNull Collection<?> c) {
        elements.retainAll(c);
        return uniqueness.retainAll(c);
    }

    @Override
    public void clear() {
        elements.clear();
        uniqueness.clear();
    }

    @Override
    public Spliterator<T> spliterator() {
        return elements.spliterator();
    }

    @Override
    public Stream<T> stream() {
        return elements.stream();
    }

    @Override
    public Stream<T> parallelStream() {
        return elements.parallelStream();
    }

    @Override
    public void forEach(Consumer<? super T> action) {
        elements.forEach(action);
    }

    @Override
    public int hashCode() {
        return elements.hashCode();
    }

    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    @Override
    public boolean equals(Object obj) {
        return elements.equals(obj);
    }

    @Override
    public String toString() {
        return elements.toString();
    }
}
