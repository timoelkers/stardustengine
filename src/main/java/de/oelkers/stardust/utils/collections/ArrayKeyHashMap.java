package de.oelkers.stardust.utils.collections;

import net.jcip.annotations.NotThreadSafe;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serial;
import java.io.Serializable;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

import static java.util.stream.Collectors.toSet;

@NotThreadSafe
public class ArrayKeyHashMap<K, V> implements Map<K[], V>, Serializable {

    @Serial
    private static final long serialVersionUID = -8154056909019881680L;

    private final HashMap<ArrayKey<K>, V> map = new HashMap<>();

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean containsKey(Object key) {
        return map.containsKey(new ArrayKey<>((K[]) key));
    }

    @Override
    public boolean containsValue(Object value) {
        return map.containsValue(value);
    }

    @SuppressWarnings("unchecked")
    @Override
    public V get(Object key) {
        return map.get(new ArrayKey<>((K[]) key));
    }

    @Nullable
    @Override
    public V put(K[] key, V value) {
        return map.put(new ArrayKey<>(key), value);
    }

    @SuppressWarnings("unchecked")
    @Override
    public V remove(Object key) {
        return map.remove(new ArrayKey<>((K[]) key));
    }

    @Override
    public void putAll(@NotNull Map<? extends K[], ? extends V> m) {
        for (Entry<? extends K[], ? extends V> entry : m.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public void clear() {
        map.clear();
    }

    @NotNull
    @Override
    public Set<K[]> keySet() {
        return map.keySet().stream().map(k -> k.key).collect(toSet());
    }

    @NotNull
    @Override
    public Collection<V> values() {
        return map.values();
    }

    @NotNull
    @Override
    public Set<Entry<K[], V>> entrySet() {
        return map.entrySet().stream().map(ArrayKeyEntry::new).collect(toSet());
    }

    @SuppressWarnings("unchecked")
    @Override
    public V getOrDefault(Object key, V defaultValue) {
        return map.getOrDefault(new ArrayKey<>((K[]) key), defaultValue);
    }

    @Override
    public void forEach(BiConsumer<? super K[], ? super V> action) {
        map.forEach((k, v) -> action.accept(k.key, v));
    }

    @Override
    public void replaceAll(BiFunction<? super K[], ? super V, ? extends V> function) {
        map.replaceAll((k, v) -> function.apply(k.key, v));
    }

    @Nullable
    @Override
    public V putIfAbsent(K[] key, V value) {
        return map.putIfAbsent(new ArrayKey<>(key), value);
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean remove(Object key, Object value) {
        return map.remove(new ArrayKey<>((K[]) key), value);
    }

    @Override
    public boolean replace(K[] key, V oldValue, V newValue) {
        return map.replace(new ArrayKey<>(key), oldValue, newValue);
    }

    @Nullable
    @Override
    public V replace(K[] key, V value) {
        return map.replace(new ArrayKey<>(key), value);
    }

    @Override
    public V computeIfAbsent(K[] key, Function<? super K[], ? extends V> mappingFunction) {
        return map.computeIfAbsent(new ArrayKey<>(key), k -> mappingFunction.apply(k.key));
    }

    @Override
    public V computeIfPresent(K[] key, BiFunction<? super K[], ? super V, ? extends V> remappingFunction) {
        return map.computeIfPresent(new ArrayKey<>(key), (k, v) -> remappingFunction.apply(k.key, v));
    }

    @Override
    public V compute(K[] key, BiFunction<? super K[], ? super V, ? extends V> remappingFunction) {
        return map.compute(new ArrayKey<>(key), (k, v) -> remappingFunction.apply(k.key, v));
    }

    @Override
    public V merge(K[] key, V value, BiFunction<? super V, ? super V, ? extends V> remappingFunction) {
        return map.merge(new ArrayKey<>(key), value, remappingFunction);
    }

    @Override
    public int hashCode() {
        return map.hashCode();
    }

    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    @Override
    public boolean equals(Object obj) {
        return map.equals(obj);
    }

    @Override
    public String toString() {
        return map.toString();
    }

    private static final class ArrayKey<K> {

        private final K[] key;

        private ArrayKey(K[] key) {
            this.key = key;
        }

        @Override
        public int hashCode() {
            return Arrays.hashCode(key);
        }

        @SuppressWarnings("unchecked")
        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ArrayKey<K> other = (ArrayKey<K>) obj;
            return Arrays.equals(key, other.key);
        }

        @Override
        public String toString() {
            return Arrays.toString(key);
        }
    }

    private static final class ArrayKeyEntry<K, V> implements Entry<K[], V> {

        private final Entry<ArrayKey<K>, V> entry;

        private ArrayKeyEntry(Entry<ArrayKey<K>, V> entry) {
            this.entry = entry;
        }

        @Override
        public K[] getKey() {
            return entry.getKey().key;
        }

        @Override
        public V getValue() {
            return entry.getValue();
        }

        @Override
        public V setValue(V value) {
            return entry.setValue(value);
        }

        @Override
        public int hashCode() {
            return entry.hashCode();
        }

        @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
        @Override
        public boolean equals(Object obj) {
            return entry.equals(obj);
        }

        @Override
        public String toString() {
            return entry.toString();
        }
    }
}
