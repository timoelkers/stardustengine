package de.oelkers.stardust.utils.collections;

import org.jetbrains.annotations.NotNull;

import java.io.Serial;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.Spliterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentHashMap.KeySetView;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class ConcurrentLinkedHashSet<E> implements Set<E>, Serializable {

    @Serial
    private static final long serialVersionUID = -5712310204674700262L;

    private final KeySetView<E, Boolean> elements = ConcurrentHashMap.newKeySet();
    private final ConcurrentLinkedQueue<E> insertionOrder = new ConcurrentLinkedQueue<>();

    @Override
    public int size() {
        return elements.size();
    }

    @Override
    public boolean isEmpty() {
        return elements.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return elements.contains(o);
    }

    @NotNull
    @Override
    public Iterator<E> iterator() {
        return insertionOrder.iterator();
    }

    @Override
    public void forEach(Consumer<? super E> action) {
        insertionOrder.forEach(action);
    }

    @NotNull
    @Override
    public Object[] toArray() {
        return insertionOrder.toArray();
    }

    @SuppressWarnings("SuspiciousToArrayCall")
    @NotNull
    @Override
    public <T> T[] toArray(@NotNull T[] a) {
        return insertionOrder.toArray(a);
    }

    @Override
    public <T> T[] toArray(IntFunction<T[]> generator) {
        return insertionOrder.toArray(generator);
    }

    @Override
    public boolean add(E e) {
        return elements.add(e) && insertionOrder.add(e);
    }

    @Override
    public boolean remove(Object o) {
        return elements.remove(o) && insertionOrder.remove(o);
    }

    @Override
    public boolean containsAll(@NotNull Collection<?> c) {
        return elements.containsAll(c);
    }

    @Override
    public boolean addAll(@NotNull Collection<? extends E> c) {
        return c.stream().map(this::add).reduce(false, (a, b) -> a || b);
    }

    @Override
    public boolean retainAll(@NotNull Collection<?> c) {
        insertionOrder.retainAll(c);
        return elements.retainAll(c);
    }

    @Override
    public boolean removeAll(@NotNull Collection<?> c) {
        insertionOrder.removeAll(c);
        return elements.removeAll(c);
    }

    @Override
    public boolean removeIf(Predicate<? super E> filter) {
        insertionOrder.removeIf(filter);
        return elements.removeIf(filter);
    }

    @Override
    public void clear() {
        insertionOrder.clear();
        elements.clear();
    }

    @Override
    public Spliterator<E> spliterator() {
        return insertionOrder.spliterator();
    }

    @Override
    public Stream<E> stream() {
        return insertionOrder.stream();
    }

    @Override
    public Stream<E> parallelStream() {
        return insertionOrder.parallelStream();
    }

    @Override
    public int hashCode() {
        return elements.hashCode();
    }

    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    @Override
    public boolean equals(Object obj) {
        return elements.equals(obj);
    }

    @Override
    public String toString() {
        return elements.toString();
    }
}
