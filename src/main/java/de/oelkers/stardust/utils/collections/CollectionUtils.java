package de.oelkers.stardust.utils.collections;

import java.util.EnumSet;

public final class CollectionUtils {

    private CollectionUtils() {}

    @SafeVarargs
    public static <T extends Enum<T>> EnumSet<T> newEnumSetOf(Class<T> type, T... values) {
        return values.length == 0 ? EnumSet.noneOf(type) : EnumSet.of(values[0], values);
    }
}
