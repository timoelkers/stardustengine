package de.oelkers.stardust.utils.streams;

import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class EndianDataInputStream extends FilterInputStream implements DataInput {

    private final ByteBuffer buffer = ByteBuffer.allocate(8);
    private final DataInputStream dataInputStream;
    private ByteOrder byteOrder = ByteOrder.BIG_ENDIAN;

    public EndianDataInputStream(InputStream in) {
        super(in);
        dataInputStream = new DataInputStream(in);
    }

    public EndianDataInputStream setByteOrder(ByteOrder byteOrder) {
        this.byteOrder = byteOrder;
        return this;
    }

    @Override
    public void readFully(@NotNull byte[] b) throws IOException {
        dataInputStream.readFully(b);
    }

    @Override
    public void readFully(@NotNull byte[] b, int off, int len) throws IOException {
        dataInputStream.readFully(b, off, len);
    }

    @Override
    public int skipBytes(int n) throws IOException {
        return dataInputStream.skipBytes(n);
    }

    @Override
    public boolean readBoolean() throws IOException {
        return dataInputStream.readBoolean();
    }

    @Override
    public byte readByte() throws IOException {
        return dataInputStream.readByte();
    }

    @Override
    public int readUnsignedByte() throws IOException {
        return dataInputStream.readUnsignedByte();
    }

    @Override
    public short readShort() throws IOException {
        buffer.clear().order(ByteOrder.BIG_ENDIAN);
        buffer.putShort(dataInputStream.readShort());
        return buffer.flip().order(byteOrder).getShort();
    }

    @Override
    public int readUnsignedShort() throws IOException {
        return readShort();
    }

    @Override
    public char readChar() throws IOException {
        return dataInputStream.readChar();
    }

    @Override
    public int readInt() throws IOException {
        buffer.clear().order(ByteOrder.BIG_ENDIAN);
        buffer.putInt(dataInputStream.readInt());
        return buffer.flip().order(byteOrder).getInt();
    }

    @Override
    public long readLong() throws IOException {
        buffer.clear().order(ByteOrder.BIG_ENDIAN);
        buffer.putLong(dataInputStream.readLong());
        return buffer.flip().order(byteOrder).getLong();
    }

    @Override
    public float readFloat() throws IOException {
        return Float.intBitsToFloat(readInt());
    }

    @Override
    public double readDouble() throws IOException {
        return Double.longBitsToDouble(readLong());
    }

    @Override
    @Deprecated
    public String readLine() throws IOException {
        return dataInputStream.readLine();
    }

    @NotNull
    @Override
    public String readUTF() throws IOException {
        return dataInputStream.readUTF();
    }
}
