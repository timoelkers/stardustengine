package de.oelkers.stardust.utils.timer;

import java.util.concurrent.TimeUnit;

public interface TimeProvider {

    long getCurrentTime();
    long getFrequency();
    TimeUnit getTimeUnit();
}
