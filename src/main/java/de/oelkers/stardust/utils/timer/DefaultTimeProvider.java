package de.oelkers.stardust.utils.timer;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.glfw.GLFW;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

public enum DefaultTimeProvider implements TimeProvider {

    SYSTEM_NANO_TIMER(System::nanoTime, () -> 1000000000L, TimeUnit.NANOSECONDS),
    SYSTEM_MILLI_TIMER(System::currentTimeMillis, () -> 1000L, TimeUnit.MILLISECONDS),
    GLFW_TIMER(GLFW::glfwGetTimerValue, GLFW::glfwGetTimerFrequency);

    private final LongSupplier timeSupplier;
    private final LongSupplier frequencySeconds;
    private TimeUnit timeUnit;

    DefaultTimeProvider(LongSupplier timeSupplier, LongSupplier frequencySeconds, TimeUnit timeUnit) {
        this.timeSupplier = timeSupplier;
        this.frequencySeconds = frequencySeconds;
        this.timeUnit = timeUnit;
    }

    DefaultTimeProvider(LongSupplier timeSupplier, LongSupplier frequencySeconds) {
        this.timeSupplier = timeSupplier;
        this.frequencySeconds = frequencySeconds;
    }

    @Override
    public long getCurrentTime() {
        return timeSupplier.get();
    }

    @Override
    public TimeUnit getTimeUnit() {
        return Optional.ofNullable(timeUnit).orElseGet(() -> initTimeUnit(getFrequency()));
    }

    @Override
    public long getFrequency() {
        return frequencySeconds.get();
    }

    private TimeUnit initTimeUnit(long frequencySeconds) {
        if (frequencySeconds == 1L) {
            timeUnit = TimeUnit.SECONDS;
        } else if (frequencySeconds == 1000L) {
            timeUnit = TimeUnit.MILLISECONDS;
        } else if (frequencySeconds == 1000000L) {
            timeUnit = TimeUnit.MICROSECONDS;
        } else if (frequencySeconds == 1000000000L) {
            timeUnit = TimeUnit.NANOSECONDS;
        } else {
            // non static initialisation as there is no way to initialize the logger before the enum values
            Logger logger = LogManager.getLogger(DefaultTimeProvider.class);
            logger.log(Level.WARN, "There is no TimeUnit matching the timer frequency of " + frequencySeconds + " seconds.");
        }
        return timeUnit;
    }

    @FunctionalInterface
    private interface LongSupplier {

        long get();
    }
}
