package de.oelkers.stardust.utils.timer;

import java.util.concurrent.TimeUnit;

public interface StopWatch {

    StopWatch start();
    long getElapsedTime();
    long getElapsedTime(TimeUnit timeUnit);
    StopWatch stop();
}
