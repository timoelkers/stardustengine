package de.oelkers.stardust.utils.timer;

import de.oelkers.stardust.config.ConfigStore;

import java.util.concurrent.TimeUnit;

public class DefaultStopWatch implements StopWatch {

    private final TimeProvider timeProvider;
    private boolean started, stopped;
    private long startTime, stopTime;

    public DefaultStopWatch() {
        this(ConfigStore.get().getTimeProvider());
    }

    public DefaultStopWatch(TimeProvider timeProvider) {
        this.timeProvider = timeProvider;
    }

    @Override
    public DefaultStopWatch start() {
        startTime = timeProvider.getCurrentTime();
        started = true;
        stopped = false;
        return this;
    }

    @Override
    public long getElapsedTime() {
        return getElapsedTime(timeProvider.getTimeUnit());
    }

    @Override
    public long getElapsedTime(TimeUnit timeUnit) {
        if (!started) {
            return 0;
        }
        long elapsedTime = (stopped ? stopTime : timeProvider.getCurrentTime()) - startTime;
        return timeUnit.convert(elapsedTime, timeProvider.getTimeUnit());
    }

    @Override
    public DefaultStopWatch stop() {
        stopTime = timeProvider.getCurrentTime();
        stopped = true;
        return this;
    }

    public TimeProvider getTimeProvider() {
        return timeProvider;
    }

    public boolean isStarted() {
        return started;
    }

    public boolean isStopped() {
        return stopped;
    }

    public boolean isRunning() {
        return started && !stopped;
    }
}
