package de.oelkers.stardust.utils.files;

import net.jcip.annotations.ThreadSafe;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@ThreadSafe
public class FileWatcher implements AutoCloseable {

    public static final String THREAD_NAME = "FileWatcherThread";
    private static final Logger LOGGER = LogManager.getLogger(FileWatcher.class);

    private final Map<Path, Runnable> fileChangeCallbacks = new HashMap<>();
    private final WatchService watchService;
    private Thread pollingThread;

    public FileWatcher() {
        try {
            watchService = FileSystems.getDefault().newWatchService();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Watches the give file for changes and executes the given callback
     * if changes are detected. The callback does not get executed on the calling
     * thread. Deletion of the file does not count as a change.
     *
     * @param filePath the path to the file you want to watch
     * @param onFileChanged the callback that gets executed on file changes
     */
    public void watch(String filePath, Runnable onFileChanged) {
        watch(Path.of(filePath), onFileChanged);
    }

    /**
     * Watches the give file for changes and executes the given callback
     * if changes are detected. The callback does not get executed on the calling
     * thread. Deletion of the file does not count as a change.
     *
     * @param path the path to the file you want to watch
     * @param onFileChanged the callback that gets executed on file changes
     */
    public void watch(Path path, Runnable onFileChanged) {
        if (Files.isDirectory(path)) {
            throw new IllegalArgumentException("You are not allowed to specify directories: " + path + '!');
        }
        try {
            startThreadIfNotStarted();
            fileChangeCallbacks.put(path, onFileChanged);
            LOGGER.info("Started watching " + path + " for file changes.");
            path.getParent().register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public synchronized void close() {
        try {
            if (pollingThread != null) {
                LOGGER.info("Stopping file watcher thread " + pollingThread + '.');
                pollingThread.interrupt();
            }
            watchService.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private synchronized void startThreadIfNotStarted() {
        if (pollingThread == null) {
            pollingThread = new Thread(this::poll, THREAD_NAME);
            LOGGER.info("Starting file watcher thread " + pollingThread + '.');
            pollingThread.setDaemon(true);
            pollingThread.start();
        }
    }

    private void poll() {
        try {
            WatchKey key;
            while ((key = watchService.take()) != null) {
                Path directory = (Path) key.watchable();
                for (WatchEvent<?> event : key.pollEvents()) {
                    if (event.kind() == StandardWatchEventKinds.ENTRY_MODIFY) {
                        Path filePath = directory.resolve((Path) event.context());
                        Optional.ofNullable(fileChangeCallbacks.get(filePath)).ifPresent(Runnable::run);
                    }
                }
                key.reset();
            }
        } catch (InterruptedException ignored) {}
    }
}
