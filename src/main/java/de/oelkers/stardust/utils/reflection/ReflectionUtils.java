package de.oelkers.stardust.utils.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;

import static java.util.stream.Collectors.joining;

public final class ReflectionUtils {

    private ReflectionUtils() {}

    public static <T> Optional<T> find(Class<?> type, SearchStep<T> step) {
        Optional<T> result = step.find(type);
        if (result.isPresent()) {
            return result;
        }
        Class<?> superclass = type.getSuperclass();
        if (superclass != null) {
            return find(superclass, step);
        }
        return Optional.empty();
    }

    public static <T> Constructor<?> getDeclaredConstructor(Class<T> startType, Class<?>... parameterTypes) throws NoSuchMethodException {
        return find(startType, type -> {
            return Arrays.stream(type.getDeclaredConstructors()).filter(c -> Arrays.equals(c.getParameterTypes(), parameterTypes)).findAny();
        }).orElseThrow(() -> {
            if (parameterTypes.length == 0) {
                return new NoSuchMethodException("No argument constructor does not exist in " + startType + '!');
            }
            if (parameterTypes.length == 1) {
                return new NoSuchMethodException("Constructor with parameter type " + parameterTypes[0].getName() + " does not exist in " + startType + '!');
            }
            String parametersAsString = Arrays.stream(parameterTypes).map(Class::getName).collect(joining(", "));
            return new NoSuchMethodException("Constructor with parameter types " + parametersAsString + " does not exist in " + startType + '!');
        });
    }

    public static <T> Field getDeclaredField(Class<T> startType, String name) throws NoSuchFieldException {
        return find(startType, type -> {
            return Arrays.stream(type.getDeclaredFields()).filter(f -> f.getName().equals(name)).findAny();
        }).orElseThrow(() -> new NoSuchFieldException("Field with name " + name + " does not exist in " + startType + '!'));
    }

    public static <T> Method getDeclaredMethod(Class<T> startType, String name, Class<?>... parameterTypes) throws NoSuchMethodException {
        return find(startType, type -> {
            return Arrays.stream(type.getDeclaredMethods()).filter(m -> m.getName().equals(name) && Arrays.equals(m.getParameterTypes(), parameterTypes)).findAny();
        }).orElseThrow(() -> {
            if (parameterTypes.length == 0) {
                return new NoSuchMethodException("Method with name " + name + " does not exist in " + startType + '!');
            }
            if (parameterTypes.length == 1) {
                return new NoSuchMethodException("Method with name " + name + " and parameter type " + parameterTypes[0].getName() + " does not exist in " + startType + '!');
            }
            String parametersAsString = Arrays.stream(parameterTypes).map(Class::getName).collect(joining(", "));
            return new NoSuchMethodException("Method with name " + name + " and parameter types " + parametersAsString + " does not exist in " + startType + '!');
        });
    }

    @FunctionalInterface
    public interface SearchStep<T> {

        Optional<T> find(Class<?> type);
    }
}
