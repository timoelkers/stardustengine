package de.oelkers.stardust.utils.threading;

import de.oelkers.stardust.utils.timer.StopWatch;

import java.time.Duration;

public interface Runner {

    Runner addRunnable(Runnable runnable, int amount);
    Runner addRunnables(Runnable... runnables);
    Runner waitForCompletion();
    Runner waitForCompletion(Duration timeout);
    Runner waitForCompletion(Duration timeout, StopWatch stopWatch);
    Runner run();
    Runner clear();
}
