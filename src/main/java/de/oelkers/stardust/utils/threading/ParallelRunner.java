package de.oelkers.stardust.utils.threading;

import de.oelkers.stardust.utils.timer.DefaultStopWatch;
import de.oelkers.stardust.utils.timer.StopWatch;
import net.jcip.annotations.NotThreadSafe;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@NotThreadSafe
public class ParallelRunner implements Runner {

    private final ExecutorService executor = Executors.newCachedThreadPool();
    private final Collection<Runnable> runnables = new ArrayList<>();
    private final AtomicInteger tasksCompleted = new AtomicInteger();
    private CountDownLatch parallelLatch = new CountDownLatch(1);

    @Override
    public ParallelRunner addRunnable(Runnable runnable, int amount) {
        for (int i = 0; i < amount; i++) {
            runnables.add(() -> {
                try {
                    parallelLatch.await();
                    runnable.run();
                    tasksCompleted.decrementAndGet();
                } catch (InterruptedException ignored) {}
            });
        }
        return this;
    }

    @Override
    public ParallelRunner addRunnables(Runnable... runnables) {
        for (Runnable runnable : runnables) {
            addRunnable(runnable, 1);
        }
        return this;
    }

    @Override
    public ParallelRunner clear() {
        runnables.clear();
        parallelLatch = new CountDownLatch(1);
        return this;
    }

    @Override
    public ParallelRunner waitForCompletion() {
        while (tasksCompleted.get() != 0 && !Thread.currentThread().isInterrupted());
        return this;
    }

    @Override
    public ParallelRunner waitForCompletion(Duration timeout) {
        return waitForCompletion(timeout, new DefaultStopWatch());
    }

    @Override
    public ParallelRunner waitForCompletion(Duration timeout, StopWatch stopWatch) {
        stopWatch.start();
        while (tasksCompleted.get() != 0 && stopWatch.getElapsedTime(TimeUnit.MILLISECONDS) < timeout.toMillis() && !Thread.currentThread().isInterrupted());
        stopWatch.stop();
        return this;
    }

    @Override
    public ParallelRunner run() {
        tasksCompleted.set(runnables.size());
        for (Runnable runnable : runnables) {
            executor.execute(runnable);
        }
        parallelLatch.countDown();
        return this;
    }
}
