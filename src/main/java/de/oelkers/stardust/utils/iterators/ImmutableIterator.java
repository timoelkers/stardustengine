package de.oelkers.stardust.utils.iterators;

import java.util.Iterator;
import java.util.function.Consumer;

public class ImmutableIterator<T> implements Iterator<T> {

    private final Iterator<T> iterator;

    public ImmutableIterator(Iterator<T> iterator) {
        this.iterator = iterator;
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public T next() {
        return iterator.next();
    }

    @Override
    public void forEachRemaining(Consumer<? super T> action) {
        iterator.forEachRemaining(action);
    }
}
