package de.oelkers.stardust.utils;

import org.jetbrains.annotations.NotNull;

import java.io.Serial;
import java.io.Serializable;

public final class ByteSize implements Comparable<ByteSize>, Serializable {

    @Serial
    private static final long serialVersionUID = 7417122923500171340L;
    private static final long KIB_SCALE = 1024;
    private static final long KB_SCALE = 1000;
    private static final long MIB_SCALE = KIB_SCALE * KIB_SCALE;
    private static final long MB_SCALE = KB_SCALE * KB_SCALE;
    private static final long GIB_SCALE = MIB_SCALE * KIB_SCALE;
    private static final long GB_SCALE = MB_SCALE * KB_SCALE;
    private static final long TIB_SCALE = GIB_SCALE * KIB_SCALE;
    private static final long TB_SCALE = GB_SCALE * KB_SCALE;
    private static final long PIB_SCALE = TIB_SCALE * KIB_SCALE;
    private static final long PB_SCALE = TB_SCALE * KB_SCALE;

    private final long bytes;

    private ByteSize(long bytes) {
        this.bytes = bytes;
    }

    public long getBytes() {
        return bytes;
    }

    @Override
    public int compareTo(@NotNull ByteSize o) {
        return Long.compare(bytes, o.bytes);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ByteSize byteSize = (ByteSize) obj;
        return bytes == byteSize.bytes;
    }

    @Override
    public int hashCode() {
        return Long.hashCode(bytes);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '[' + bytes + ']';
    }

    public static ByteSize ofBytes(long bytes) {
        return new ByteSize(bytes);
    }

    public static ByteSize ofKibiBytes(long bytes) {
        return new ByteSize(bytes * KIB_SCALE);
    }

    public static ByteSize ofKiloBytes(long bytes) {
        return new ByteSize(bytes * KB_SCALE);
    }

    public static ByteSize ofMibiBytes(long bytes) {
        return new ByteSize(bytes * MIB_SCALE);
    }

    public static ByteSize ofMegaBytes(long bytes) {
        return new ByteSize(bytes * MB_SCALE);
    }

    public static ByteSize ofGibiBytes(long bytes) {
        return new ByteSize(bytes * GIB_SCALE);
    }

    public static ByteSize ofGigaBytes(long bytes) {
        return new ByteSize(bytes * GB_SCALE);
    }

    public static ByteSize ofTebiByte(long bytes) {
        return new ByteSize(bytes * TIB_SCALE);
    }

    public static ByteSize ofTeraByte(long bytes) {
        return new ByteSize(bytes * TB_SCALE);
    }

    public static ByteSize ofPebiByte(long bytes) {
        return new ByteSize(bytes * PIB_SCALE);
    }

    public static ByteSize ofPetaByte(long bytes) {
        return new ByteSize(bytes * PB_SCALE);
    }
}
