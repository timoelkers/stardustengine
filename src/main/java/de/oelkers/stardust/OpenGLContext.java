package de.oelkers.stardust;

import de.oelkers.stardust.graphics.assets.models.BufferObject.BufferTarget;
import de.oelkers.stardust.graphics.assets.textures.BaseTexture.TextureType;
import de.oelkers.stardust.graphics.rendering.BlendMode;
import de.oelkers.stardust.utils.TriStateBoolean;
import de.oelkers.stardust.window.HardwareLimitations;
import de.oelkers.stardust.window.RenderMode;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public final class OpenGLContext {

    private static final OpenGLContext INSTANCE = new OpenGLContext();

    public static OpenGLContext get() {
        return INSTANCE;
    }

    private final Map<Long, CurrentState> states = new HashMap<>(1);
    private CurrentState currentState;
    private long currentContextHandle;

    private OpenGLContext() {}

    public long bindContext(long contextHandle) {
        long previous = currentContextHandle;
        if (previous != contextHandle) {
            currentContextHandle = contextHandle;
            currentState = states.computeIfAbsent(contextHandle, k -> new CurrentState());
            GLFW.glfwMakeContextCurrent(contextHandle);
        }
        return previous;
    }

    public void unbindContext(long contextHandle) {
        if (currentContextHandle == contextHandle) {
            currentContextHandle = 0;
        }
        states.remove(contextHandle);
    }

    public void resetContext(long contextHandle) {
        states.get(contextHandle).reset();
    }

    public int bindShaderProgram(int programHandle) {
        int previous = currentState.shaderProgram;
        if (previous != programHandle) {
            currentState.shaderProgram = programHandle;
            GL20.glUseProgram(programHandle);
        }
        return previous;
    }

    public void unbindShaderProgram(int programHandle) {
        if (currentState.shaderProgram == programHandle) {
            currentState.shaderProgram = 0;
        }
    }

    public int bindTexture(int textureHandle, TextureType textureType, int textureUnit) {
        int textureUnitLimit = HardwareLimitations.get().getMaxTextureUnits();
        if (textureUnit > textureUnitLimit - 1) {
            throw new IllegalStateException("Texture unit limit of " + textureUnit + " was exceeded. Unable to bind texture.");
        }
        int previous = currentState.textures[textureUnit];
        if (previous != textureHandle) {
            currentState.textures[textureUnit] = textureHandle;
            GL13.glActiveTexture(GL13.GL_TEXTURE0 + textureUnit);
            GL11.glBindTexture(textureType.getGLCode(), textureHandle);
        }
        return previous;
    }

    public void unbindTexture(int textureHandle) {
        for (int i = 0; i < currentState.textures.length; i++) {
            if (currentState.textures[i] == textureHandle) {
                currentState.textures[i] = 0;
            }
        }
    }

    public int bindBuffer(int bufferHandle, BufferTarget bufferTarget) {
        int previous = currentState.buffer;
        if (previous != bufferHandle) {
            currentState.buffer = bufferHandle;
            GL15.glBindBuffer(bufferTarget.getGlCode(), bufferHandle);
        }
        return previous;
    }

    public void unbindBuffer(int bufferHandle) {
        if (currentState.buffer == bufferHandle) {
            currentState.buffer = 0;
        }
    }

    public int bindVertexArray(int vertexArrayHandle) {
        int previous = currentState.vertexArray;
        if (previous != vertexArrayHandle) {
            currentState.vertexArray = vertexArrayHandle;
            GL30.glBindVertexArray(vertexArrayHandle);
        }
        return previous;
    }

    public void unbindVertexArray(int vertexArrayHandle) {
        if (currentState.vertexArray == vertexArrayHandle) {
            currentState.vertexArray = 0;
        }
    }

    public BlendMode bindBlendMode(BlendMode blendMode) {
        BlendMode previous = currentState.blendMode;
        if (previous != blendMode) {
            currentState.blendMode = blendMode;
            GL11.glBlendFunc(blendMode.getSFactor(), blendMode.getDFactor());
        }
        return previous;
    }

    public RenderMode bindRenderMode(RenderMode renderMode) {
        RenderMode previous = currentState.renderMode;
        if (previous != renderMode) {
            currentState.renderMode = renderMode;
            GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, renderMode.getGlCode());
        }
        return previous;
    }

    public TriStateBoolean setCulling(boolean enable) {
        TriStateBoolean value = TriStateBoolean.of(enable);
        TriStateBoolean previous = currentState.culling;
        if (previous != value) {
            currentState.culling = value;
            if (enable) {
                GL11.glEnable(GL11.GL_CULL_FACE);
                GL11.glCullFace(GL11.GL_BACK);
            } else {
                GL11.glDisable(GL11.GL_CULL_FACE);
            }
        }
        return previous;
    }

    public TriStateBoolean setBlending(boolean enable) {
        TriStateBoolean value = TriStateBoolean.of(enable);
        TriStateBoolean previous = currentState.blending;
        if (previous != value) {
            currentState.blending = value;
            if (enable) {
                GL11.glEnable(GL11.GL_BLEND);
            } else {
                GL11.glDisable(GL11.GL_BLEND);
            }
        }
        return previous;
    }

    public TriStateBoolean setDepthTest(boolean enable) {
        TriStateBoolean value = TriStateBoolean.of(enable);
        TriStateBoolean previous = currentState.depthTest;
        if (previous != value) {
            currentState.depthTest = value;
            if (enable) {
                GL11.glEnable(GL11.GL_DEPTH_TEST);
            } else {
                GL11.glDisable(GL11.GL_DEPTH_TEST);
            }
        }
        return previous;
    }

    public TriStateBoolean setDepthWrite(boolean enable) {
        TriStateBoolean value = TriStateBoolean.of(enable);
        TriStateBoolean previous = currentState.depthWrite;
        if (previous != value) {
            currentState.depthWrite = value;
            GL11.glDepthMask(enable);
        }
        return previous;
    }

    public TriStateBoolean setScissorTest(boolean enable) {
        TriStateBoolean value = TriStateBoolean.of(enable);
        TriStateBoolean previous = currentState.scissorTest;
        if (previous != value) {
            currentState.scissorTest = value;
            if (enable) {
                GL11.glEnable(GL11.GL_SCISSOR_TEST);
            } else {
                GL11.glDisable(GL11.GL_SCISSOR_TEST);
            }
        }
        return previous;
    }

    private static class CurrentState {

        private final int[] textures = new int[HardwareLimitations.get().getMaxTextureUnits()];
        private int vertexArray, buffer, shaderProgram;
        private TriStateBoolean culling = TriStateBoolean.UNDEFINED;
        private TriStateBoolean blending = TriStateBoolean.UNDEFINED;
        private TriStateBoolean depthTest = TriStateBoolean.UNDEFINED;
        private TriStateBoolean depthWrite = TriStateBoolean.UNDEFINED;
        private TriStateBoolean scissorTest = TriStateBoolean.UNDEFINED;
        private BlendMode blendMode = null;
        private RenderMode renderMode = null;

        private void reset() {
            // direct handles can never be zero, so setting them to zero ensures cache miss
            Arrays.fill(textures, 0);
            vertexArray = buffer = shaderProgram = 0;
            culling = blending = depthTest = depthWrite = TriStateBoolean.UNDEFINED;
            blendMode = null;
            renderMode = null;
        }
    }
}
